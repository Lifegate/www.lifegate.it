<?php
/*
Plugin Name: Lifegate NoFollow
Description: Aggiunge nofollow a sponsorizzati
Version: 1.0.0
Author: Marco Buttarini
Author URI: https://bititup.it/
*/

/**
 * @param $content
 * @return string|string[]
 */
function lifegate_url_parse( $content )
{
	// skip id articolo specifico
	if((get_the_ID() == 282193) || (get_the_ID() == 390679)){
		return $content;
	}

	if(!lifegate_post_is_sponsored(get_the_ID())){
		return $content;
	}

	$matches = lifegate_is_link_available($content);

	if ($matches === null) {
		return $content;
	}

	// loop through each links
	for ($i=0; $i < count($matches); $i++)
	{
		$tag  = $matches[$i][0];
		$url  = $matches[$i][0];

		if(lifegate_is_internal_link($url)) {
			continue;
		}

		$tag = lifegate_add_rel_nofollow($url, $tag);

		$content = str_replace($url, $tag, $content);

	} // end for loop

	$content = str_replace(']]>', ']]&gt;', $content);
	return $content;
}

add_filter( 'the_content', 'lifegate_url_parse');

/**
 * @param string $content
 * @return null
 */
function lifegate_is_link_available($content='')
{
	if ($content=='') {
		return null;
	}

	$regexp = "<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>";

	if(preg_match_all("/$regexp/siU", $content, $matches, PREG_SET_ORDER)) {
		return $matches;
	}

	return null;
}

/**
 * @param $url
 * @return bool
 */
function lifegate_is_internal_link($url)
{
	// bypass #more type internal link
	$result = preg_match('/href(\s)*=(\s)*"[#|\/]*[a-zA-Z0-9-_\/]+"/', $url);

	if ($result) {
		return true;
	}

	$pos = strpos($url, "lifegate.");
	if ($pos !== false) {
		return true;
	}

	return false;
}

/**
 * @param $url
 * @param $tag
 * @return mixed|string|string[]
 */
function lifegate_add_rel_nofollow($url, $tag)
{
	$no_follow = '';
	// $pattern = '/rel\s*=\s*"\s*[n|d]ofollow\s*"/';
	// $pattern = '/rel\s*=\s*\"[a-zA-Z0-9_\s]*[n|d]ofollow[a-zA-Z0-9_\s]*\"/';
	$pattern = '/rel\s*=\s*\"[a-zA-Z0-9_\s]*\"/';

	$result = preg_match($pattern, $url, $match);

	if ($result === 0) {
		$no_follow .= ' rel="nofollow"';
	} else {
		if (strpos($match[0], 'nofollow') === false &&
			strpos($match[0], 'dofollow') === false) {
			$temp = $match[0];
			$temp = substr_replace($temp, ' nofollow"', -1);
			$tag = str_replace($match[0], $temp, $tag);
		}
	}

	if ($no_follow) {
		$tag = lifegate_update_close_tag($tag, $no_follow);
	}

	return $tag;
}

/**
 * @param $tag
 * @param $no_follow
 * @return string|string[]
 */
function lifegate_update_close_tag($tag, $no_follow)
{
	return substr_replace($tag, $no_follow.'>', -1);
}
