# html.wpplugin.tbm-common

Wordpress plugin to handle Triboo Media video

#Features

##ACF Fields
The plugin add the common ACF fields to post post type, to speciale taxonomy and to sponsor taxonomy. Take a look into `functions/tbm-acf-fields.php` file.

## Sitemap
A video sitemap is added for every site. Check, for example, the url http://www.motori.it/tbmsitemapvideo.xml

#Filters

## tbm_process_video
Enable or disable the automatic processing of new videos.
###Example (for disable)
```php
add_filter('tbm_process_video', '__return_false');
```

#Function
- `html_video_generate_video_player( $post = null, $class = '', $display = 0 )`: Genera JS e HTML per il player desktop

- `html_video_generate_amp_video_player( $post = null, $vast, $atts = array(), $display = 0 )`: Genera HTML per il player AMP

- `tbm_video_get_file_url($post)` Get the url of the video file

- `tbm_video_get_poster($post,$size)` Get the url of the poster (featurd image)
