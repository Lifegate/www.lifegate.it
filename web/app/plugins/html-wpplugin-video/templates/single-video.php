<?php
get_header();
?>
	<div class="wrap">
		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">
				<?php while ( have_posts() ) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<header class="entry-header">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
							<?php edit_post_link( "Edit", '<span class="edit-link">', '</span>' ); ?>
						</header><!-- .entry-header -->
						<div class="entry-content">
							<?php
							the_content();

							// If plugin is enabled
							html_video_generate_video_player( $post->ID, "", true );
							?>

						</div><!-- .entry-content -->
					</article><!-- #post-## -->
				<?php endwhile; // End of the loop. ?>
			</main><!-- #main -->
		</div><!-- #primary -->
		<aside id="secondary" class="widget-area" role="complementary">
			<ul>
				<?php
				$terms = get_terms( array(
					'taxonomy'   => 'category',
					'hide_empty' => false,
				) );

				foreach ( $terms as $term ) {
					echo '<li><a href="' . get_term_link( $term ) . '?post_type=' . HTML_VIDEO_POST_TYPE . '">' . $term->name . '</a></li>';
				}
				?>
			</ul>
		</aside><!-- #secondary -->
	</div><!-- .wrap -->
<?php
get_footer();


