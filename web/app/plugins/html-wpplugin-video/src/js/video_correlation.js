/* globals console: false */
jQuery(document).ready(function ($) {

	var posttype = '',
		service = '',
		lookup = 'search',
		gh = document.querySelector('button.button-save-correlator') ? Ladda.create(document.querySelector('button.button-save-correlator')) : null;


	/**
	 * Gestione dei pulsanti degli shortcode
	 */
	$('#save-correlator').click(function (e) {
		e.preventDefault();
		if ($(this).hasClass("button-ok")) {
			return;
		}

		if (typeof tbmVideoId !== 'undefined' && typeof tbmPostId !== 'undefined') {
			gh.start();
			var el = $(this);

			$.ajax({
				url: ajaxurl,
				datatype: "json",
				cache: false,
				data: {
					action: "tbm_video_save_correlator",
					video_id: tbmVideoId,
					post_id: tbmPostId,
					mynonce: videoNonce
				}
			}).done(function (d) {
				el.addClass("button-ok");
			}).fail(function () {
				el.addClass("button-ko");
				$('.message').html('<p><strong>Errore!</strong> Abbiamo avuto un problema. Se si dovesse ripetere avvisa la NASA</p>');
			}).always(function () {
				gh.stop();
			});
		} else {
			$('.message').html('<p><strong>Attenzione!</strong> Non possediamo il video ID</p>');
		}
	});

});
