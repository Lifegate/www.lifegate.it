<?php
/**
 * Created by PhpStorm.
 * User: Francesco
 * Date: 16/12/2018
 * Time: 21:02
 */

/**
 * Rewrite for video sitemap
 */
add_action( 'init', function () {
	add_rewrite_tag( '%tbm_sitemap%', '([^&]+)' );
	add_rewrite_rule( 'tbmsitemapvideo.xml', 'index.php?tbm_sitemap=video', 'top' );
}, 10 );


/**
 * Rewrite for video feed
 */
add_action( 'init', function () {
	add_rewrite_tag( '%tbm_feed%', '([^&]+)' );
	add_rewrite_rule( 'videorss.xml', 'index.php?tbm_feed=video', 'top' );
}, 10 );
