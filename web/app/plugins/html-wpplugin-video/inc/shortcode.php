<?php
/**
 * Funzione per la generazione dello shortcode video
 * example: [htmlvideo id="123"]
 *
 * @param $atts
 *
 * @return string|void
 */
function htmlvideo_shortcode( $atts ) {
	if ( ! $atts['id'] ) {
		return;
	}
	html_video_generate_video_player( $atts['id'], "", true );
}

add_shortcode( 'htmlvideo', 'htmlvideo_shortcode' );
