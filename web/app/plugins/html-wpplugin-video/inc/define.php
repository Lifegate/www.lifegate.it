<?php
if ( ! defined( "HTML_VIDEO_URL_NOTIFY_MEDIACENTER" ) ) {
	DEFINE( "HTML_VIDEO_URL_NOTIFY_MEDIACENTER", "https://video.triboomedia.it/api/setCorrelationItem" );
}

if ( ! defined( "HTML_VIDEO_URL_CHECK_MEDIACENTER" ) ) {
	DEFINE( "HTML_VIDEO_URL_CHECK_MEDIACENTER", "https://video.triboomedia.it/api/getCorrelationItem" );
}

if ( ! defined( "HTML_VIDEO_FTP_PATH_NEW" ) ) {
	// path per l'upload dei video via ftp
	DEFINE( "HTML_VIDEO_FTP_PATH_NEW", "/var/www/babelee/" );
}

if ( ! defined( "HTML_VIDEO_FTP_PATH_TRASH" ) ) {
	DEFINE( "HTML_VIDEO_FTP_PATH_TRASH", "/var/www/babelee/trash/" );
}

if ( ! defined( "HTML_VIDEO_FORMAT_LIST" ) ) {
	DEFINE( "HTML_VIDEO_FORMAT_LIST", "mp4, mpeg, mkv, avi, mov, flv" );

}
if ( ! defined( "HTML_VIDEO_POST_TYPE" ) ) {
	DEFINE( "HTML_VIDEO_POST_TYPE", "video" );
}

if ( ! defined( "HTML_VIDEO_POST_TYPE_NAME" ) ) {
	DEFINE( "HTML_VIDEO_POST_TYPE_NAME", "Video" );
}

if ( ! defined( "HTML_VIDEO_POST_TYPE_SLUG" ) ) {
	DEFINE( "HTML_VIDEO_POST_TYPE_SLUG", "video" );
}

if ( ! defined( "HTML_VIDEO_POST_TYPE_DESCRIPTION" ) ) {
	DEFINE( "HTML_VIDEO_POST_TYPE_DESCRIPTION", "I Video" );
}

if ( ! defined( "HTML_PLAYLIST_VIDEO_POST_TYPE" ) ) {
	DEFINE( "HTML_PLAYLIST_VIDEO_POST_TYPE", "playlist-video" );
}

if ( ! defined( "HTML_PLAYLIST_VIDEO_POST_TYPE_NAME" ) ) {
	DEFINE( "HTML_PLAYLIST_VIDEO_POST_TYPE_NAME", "Playlist Video" );
}

if ( ! defined( "HTML_PLAYLIST_VIDEO_POST_TYPE_SLUG" ) ) {
	DEFINE( "HTML_PLAYLIST_VIDEO_POST_TYPE_SLUG", "playlist-video" );
}

if ( ! defined( "HTML_PLAYLIST_VIDEO_POST_TYPE_DESCRIPTION" ) ) {
	DEFINE( "HTML_PLAYLIST_VIDEO_POST_TYPE_DESCRIPTION", "Le Playlist dei video" );
}
