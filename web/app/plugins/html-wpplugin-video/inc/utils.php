<?php
/**
 * Utils functions
 */

/**
 * Get the array of main video metadata
 *
 * @param string|array $video ACF Array of data or attachment ID
 *
 * @return array|bool Array if success. False if not video metadata
 */
function html_get_video_url( $video = '' ) {
	if ( empty( $video ) || ( empty( $video['ID'] ) && empty( $video['url'] ) ) ) {
		return false;
	}


}

/**
 * Get the video Outplay Category based on site domain
 *
 * @return string
 */
function html_get_theoutplay_video_category() {
	$site = tbm_get_domain();
	switch ( $site ) {
		case 'motori':
			return "Automotive";
			break;
		case "agrodolce":
			return "Food and drinks";
			break;
		case "pmi":
			return "Law, gov’t and politics";
			break;
		case "webnews":
			return "Technology and computing";
			break;
		case "greenstyle":
			return "Health and fitness";
			break;
		default:
			return '';
	}
}

/**
 * Get the array of main video metadata
 *
 * @param string|array $video ACF Array of data or attachment ID
 *
 * @return array|bool Array if success. False if not video metadata
 */
function html_get_main_video_array( $video = '' ) {

	if ( empty( $video ) || ( empty( $video['ID'] ) && empty( $video['url'] ) ) ) {
		return false;
	}

	// Declare array
	$first = array();

	/**
	 * If we have $video array
	 */

	$prop_video = wp_get_attachment_metadata( $video["ID"] );
	if ( isset( $prop_video ) ) {
		$first = array(
				"videoSize"   => isset( $prop_video["filesize"] ) ?: '',
				"videoWidth"  => isset( $prop_video["width"] ) ?: '',
				"videoHeight" => isset( $prop_video["height"] ) ?: '',
				"videoLenght" => isset( $prop_video["length"] ) ?: ''
		);
	}

	// Set videoId
	$first["videoId"] = ! empty( $video['ID'] ) ? $video['ID'] : 'noid_' . random_int( 1, 100 );

	// Check if the video array has the url
	if ( ! empty( $video["url"] ) ) {
		$first["media_url"] = $video["url"];

		return $first;
	}

	// Otherwise, get the video url
	$video_url = wp_get_attachment_url( $video['ID'] );
	if ( filter_var( $video_url, FILTER_VALIDATE_URL ) ) {
		$first["media_url"] = $video_url;

		return $first;
	}

	return false;
}

/**
 * Get the array of videos in playlist
 *
 * @param $playlist The playlist ACF field
 * @param int $id The post id
 *
 * @return array|bool
 */
function html_get_playlist_video_array( $playlist, $id = array() ) {

	$out             = [];
	$playlist_videos = get_field( "playlist_video", $playlist->ID );

	if ( $playlist_videos ) {
		foreach ( $playlist_videos as $playlist_video ) {
			$video = tbm_get_videoid_field( $playlist_video->ID );

			// elemento già esistente, salto
			if ( in_array( $video["ID"], $id ) ) {
				continue;
			}
			$terms  = wp_get_post_terms( $playlist_video->ID, "post_tag", array( "fields" => "all" ) );
			$pterms = "";
			$c      = 0;
			foreach ( $terms as $term ) {
				if ( $c > 0 ) {
					$pterms .= ", ";
				}
				$c ++;
				$pterms .= "'" . $term->slug . "'";
			}

			$url_video_elaborato_alta_playlist  = get_field( "url_video_elaborato_alta", $playlist_video->ID );
			$url_video_elaborato_bassa_playlist = get_field( "url_video_elaborato_bassa", $playlist_video->ID );
			$url_thumb                          = get_the_post_thumbnail_url( $playlist_video->ID, 'full' );
			$prop_video_playlist                = wp_get_attachment_metadata( $video["ID"] );
			if ( ! isset( $prop_video_playlist ) || ! is_array( $prop_video_playlist ) ) {
				continue;
			}
			$item = array(
					"videoId"              => $video["ID"],
					"title"                => $playlist_video->post_title,
					"description"          => $playlist_video->post_excerpt,
					"media_url"            => $video["url"],
					"hq_encoded_video_url" => $url_video_elaborato_alta_playlist,
					"lq_encoded_video_url" => $url_video_elaborato_bassa_playlist,
					"thumbUrl"             => $url_thumb ?: '',
					"videoSize"            => isset( $prop_video_playlist["filesize"] ) ?: '',
					"videoWidth"           => isset( $prop_video_playlist["width"] ) ?: '',
					"videoHeight"          => isset( $prop_video_playlist["height"] ) ?: '',
					"videoLenght"          => isset( $prop_video_playlist["length"] ) ?: '',
					"PageKeywords"         => $pterms,
					"VideoKeywords"        => $pterms,
			);

			$out[] = $item;
		}

		return $out;
	}

	return false;
}

/**
 * genero js e html per il play del video
 *
 * @param int|WP_Post $post Required. Post ID or post object of the single video
 * @param string $class La classe da aggiungere al DIV segnaposto
 * @param bool $display True to echo the content, false to return. Default false
 */
function html_video_generate_video_player( $post = 0, $class = '', $display = 0 ) {
	$post = get_post( $post );

	/**
	 * Check if is a public preview (enabled by the "public preview" plugin)
	 */
	$force_preview = isset( $_GET['_ppp'] );

	/**
	 * Check if post video exists
	 */
	if ( ! $post || $post->post_type !== HTML_VIDEO_POST_TYPE ) {
		return null;
	}

	/**
	 * Check if post video is published
	 */
	if ( $post->post_status !== 'publish' && ! $force_preview ) {
		return '<em><small>Siamo spiacenti: video non disponibile o non pubblicato.</small></em>';
	}

	$post_id = $post->ID;
	$video   = tbm_get_videoid_field( $post_id );

	/**
	 * Check External MP4
	 */
	if ( ! $video ) {
		$url = get_field( 'video_external_mp4', $post_id );

		if ( $url ) {
			$code = '<video controls controlsList="nodownload" src="' . $url . '" poster="' . tbm_get_the_post_thumbnail_url( get_the_ID(), array(
							660,
							400
					) ) . '" style="max-width: 100%;"></video>';

			if ( $display ) {
				echo $code;

				return true;
			}

			return $code;


		}
	}

	/**
	 * Check Oembed
	 */
	if ( ! $video ) {
		$url = get_field( 'video_oembed_url', $post_id );

		if ( $url && wp_oembed_get( $url ) ) {

			// Return the oembed
			$code = wp_oembed_get( $url );

			if ( $display ) {
				echo $code;

				return true;
			}

			return $code;
		}
	}

	/**
	 * Check video without ID
	 */
	if ( ! $video ) {
		$video = tbm_get_videourl_field( $post_id );
	}

	/**
	 * If the post has not video, move to the draft
	 */
	if ( ! $video ) {
		tbm_move_video_to_draft( $post_id );

		return '';
	}

	/**
	 * If Triboo video is not enabled, returns the standard HTML5 video tag
	 */
	if ( ! get_field( 'tbm_enable_triboovideo_plugin', 'option' ) ) {

		return '<video controls controlsList="nodownload" src="' . $video['url'] . '" poster="' . tbm_get_the_post_thumbnail_url( get_the_ID(), array(
						660,
						400
				) ) . '" style="max-width: 100%;"></video>';
	}

	/**
	 * Elaborate the TribooADV options
	 */
	$main                      = array();
	$playlist_array            = array();
	$videos                    = array();
	$post_id                   = $post->ID;
	$duration                  = (int) get_field( "tbm_video_durata", $post_id );
	$url_video_elaborato_alta  = get_field( "url_video_elaborato_alta", $post_id );
	$url_video_elaborato_bassa = get_field( "url_video_elaborato_bassa", $post_id );
	$preroll_disabled          = get_field( "preroll_disabled" ) ? 'false' : 'true';
	$sticky_disabled           = get_field( "sticky_disabled" ) ? 'false' : 'true';

	if ( is_singular( 'video' ) ) {
		$sticky_disabled = 'false';
	}

	// Get the terms
	$terms  = wp_get_post_terms( $post_id, "post_tag", array( "fields" => "all" ) );
	$pterms = "";
	$c      = 0;
	foreach ( $terms as $term ) {
		if ( $c > 0 ) {
			$pterms .= ", ";
		}
		$c ++;
		$pterms .= "'" . $term->slug . "'";
	}

	$main_defaults = array(
			"videoId"              => 0,
			"title"                => get_the_title( $post ),
			"description"          => get_the_excerpt( $post ),
			"media_url"            => '',
			"hq_encoded_video_url" => $url_video_elaborato_alta,
			"lq_encoded_video_url" => $url_video_elaborato_bassa,
			"thumbUrl"             => get_the_post_thumbnail_url( $post->ID, 'full' ),
			"videoSize"            => '',
			"videoWidth"           => '',
			"videoHeight"          => '',
			"videoLenght"          => $duration,
			"PageKeywords"         => $pterms,
			"VideoKeywords"        => $pterms,
	);

	/**
	 * Get the main video info array
	 */
	if ( false !== ( $main_id = html_get_main_video_array( $video ) ) ) {
		$main[] = wp_parse_args( $main_id, $main_defaults );
	}

	/**
	 * Get the playlist video info array
	 */
	$playlist = html_video_get_related_playlist( $post_id );
	if ( $playlist ) {
		$playlist_array = html_get_playlist_video_array( $playlist );
	}

	$videos = array_merge( $main, $playlist_array );

	$producer       = get_field( 'video_producer', 'option' );
	$distributor    = get_field( 'video_distributor', 'option' );
	$video_cnvid_id = get_field( 'video_cnvid_id', 'option' );
	$spotx_mobile   = get_field( 'video_spotx_mobile_id', 'option' );
	$spotx_desktop  = get_field( 'video_spotx_desktop_id', 'option' );
	$spotx          = json_encode( array( 'mobile' => (int) $spotx_mobile, 'desktop' => (int) $spotx_desktop ) );

	// Se non abbiamo tutte le info necessarie, non ritorno il TribooAdv
	if ( ! $producer || ! $distributor || ! $video_cnvid_id ) {
		return false;
	}

	ob_start();
	?>
	<div id="tbv-<?php echo $post_id; ?>" class="<?php echo $class; ?>"></div>
	<script>
		window.tribooVideo = window.tribooVideo || [];
		window.tribooVideo.push({
			site: "<?php echo $video_cnvid_id; ?>",
			divId: "tbv-<?php echo $post_id; ?>",
			advReq: <?php echo $preroll_disabled; ?>,
			stick: <?php echo $sticky_disabled; ?>,
			spotX: <?php echo $spotx; ?>,
			VideoContent: [
					<?php
					$c = 0;
					foreach ($videos as $item){
					if ( $c > 0 ) {
						echo ",";
					}
					$c ++;
					?>{
					Id: <?php echo $item["videoId"]; ?>,
					Title: "<?php echo addslashes( trim( $item["title"] ) ); ?>",
					Description: "<?php echo addslashes( trim( $item["description"] ) ); ?>",
					VideoLenght: "<?php echo $item["videoLenght"]; ?>",
					Media: "<?php echo $item["media_url"]; ?>",
					Video: "<?php echo $item["lq_encoded_video_url"]; ?>",
					VideoHLS: "<?php echo $item["hq_encoded_video_url"]; ?>",
					ThumbUrl: "<?php echo $item["thumbUrl"]; ?>",
					Producer: "<?php echo $producer; ?>",
					Distributor: "<?php echo $distributor; ?>",
					PageKeywords: [<?php echo $item["PageKeywords"]; ?>],
					VideoKeywords: [<?php echo $item["PageKeywords"]; ?>]
				}<?php } // chiudo ciclo sull'array ?>

			]
		});
	</script>
	<?php
	$code = ob_get_contents();
	ob_end_clean();

	if ( $display ) {
		echo $code;

		return true;
	}

	return $code;
}

/**
 * Return or print the code of the AMP video player
 *
 * @param int|WP_Post $post Post ID or post object of the video post
 * @param string $vast The Vast URL
 * @param array $atts The $atts array from shortcode
 * @param int $display True to display, false (default) to return the code
 *
 * @return bool|string
 */
function html_video_generate_amp_video_player( $post = null, $vast, $atts = array(), $display = 0 ) {
	$post = get_post( $post );
	$atts = (array) $atts;


	if ( ! $post || $post->post_type !== HTML_VIDEO_POST_TYPE ) {
		return '';
	}

	if ( $post->post_status !== 'publish' ) {
		return '';
	}

	/**
	 * Try to get the vast
	 */
	if ( empty( $vast ) ) {
		$vast = tbm_get_the_banner( 'AMP_VAST', ' ', ' ', false, true );
	}

	$post_id = $post->ID;
	$video   = tbm_get_videoid_field( $post_id );

	/**
	 * The post has not video
	 */
	if ( ! $video ) {
		return '';
	}

	$url_video_elaborato_alta  = get_field( "url_video_elaborato_alta", $post_id );
	$url_video_elaborato_bassa = get_field( "url_video_elaborato_bassa", $post_id );
	$preroll_enabled           = get_field( "preroll_disabled" ) ? 'false' : 'true';

	if ( $preroll_enabled === 'false' ) {
		$vast = '';
	}

	$main_defaults = array(
			"videoId"              => 0,
			"title"                => isset( $atts['title'] ) ? $atts['title'] : $post->post_title,
			"description"          => $post->post_excerpt,
			"media_url"            => '',
			"hq_encoded_video_url" => $url_video_elaborato_alta,
			"lq_encoded_video_url" => $url_video_elaborato_bassa,
			"thumbUrl"             => get_the_post_thumbnail_url( $post->ID, 'full' ),
			"videoSize"            => '',
			"videoWidth"           => '',
			"videoHeight"          => '',
	);

	/**
	 * Fill the $main array wih video data and defaults args
	 */
	$video_data = html_get_main_video_array( $video );
	if ( false !== $video_data ) {
		$video = wp_parse_args( $video_data, $main_defaults );
	}

	/**
	 * The video hasn't url
	 */
	if ( ! isset( $video["media_url"] ) ) {
		return '';
	}

	ob_start();
	if ( $vast ) : ?>
		<div class="partial-amp-innerrelated-video">
			<amp-ima-video id="video-inside-<?php echo $video["videoId"]; ?>"
						   title="<?php echo $video["title"]; ?>"
						   width="640"
						   height="360"
						   layout="responsive"
						   data-src="<?php echo $video["media_url"]; ?>"
						   data-poster="<?php echo $video["thumbUrl"]; ?>"
						   data-tag="<?php echo $vast ?>"
						   autoplay>
				<script type="application/json">
					{
						"locale": "it",
						"numRedirects": 12
					}
				</script>
			</amp-ima-video>
		</div>
	<?php else : ?>
		<div class="partial-amp-innerrelated-video">
			<amp-video controls id="video-aside-<?php echo $atts['id'] ?>"
					   title="<?php echo $atts['title'] ?>"
					   width="640"
					   height="360"
					   layout="responsive"
					<?php if ( empty( $video["hq_encoded_video_url"] ) ): ?>
						src="<?php echo $video["media_url"]; ?>"
					<?php endif; ?>
					   poster="<?php echo $video["thumbUrl"]; ?>">
				<div fallback>
					<p>Spiacenti, ma il tuo browser non supporta i video in HTML5</p>
				</div>
				<?php if ( ! empty( $video["hq_encoded_video_url"] ) ): ?>
					<source type="application/vnd.apple.mpegurl" src="<?php echo $video["hq_encoded_video_url"]; ?>">
					<source type="video/mp4" src="<?php echo $video["media_url"]; ?>">
				<?php endif; ?>
			</amp-video>
		</div>
	<?php endif;

	$code = ob_get_contents();
	ob_end_clean();

	if ( $display ) {
		echo $code;

		return true;
	}

	return $code;
}

/**
 * 1) Controlla se c’è una playlist prioritaria (se c’è, la imposta, altrimenti va avanti).
 * 2) Controlla se c’è una playlist associata esplicitamente al video 345 (se c’è, la imposta, altrimenti va avanti).
 * 3) Controlla se il video 345 ha un term cui è associata una playlist (se c’è, la imposta, altrimenti va avanti).
 * 4) Chiede alla funzione di correlazione (che si chiama tbm_get_custom_related_posts) di creare una playlist.
 *
 * @param $post_id
 *
 */
function html_video_get_related_playlist( $post_id ) {
// Controlla se c’è una playlist prioritaria
	$playlist = html_video_get_priority_playlist();
	if ( $playlist ) {
		return $playlist;
	}

// Controlla se c’è una playlist associata esplicitamente al video
	$video_playlists = get_field( "playlist_video", $post_id );
	if ( $video_playlists ) {
		$playlist = $video_playlists[0];
	}
	if ( $playlist ) {
		return $playlist;
	}

// Controlla se il video  ha un term cui è associata una playlist
	$taxonomy = "category";
	$terms    = wp_get_post_terms( $post_id, $taxonomy, array( "fields" => "all" ) );
	if ( $terms ) {
		foreach ( $terms as $term ) {
// controllo se esiste una playlist associata a questo term

			$playlists = get_posts( array(
					'post_type'   => HTML_PLAYLIST_VIDEO_POST_TYPE,
					'numberposts' => 1,
					'tax_query'   => array(
							array(
									'taxonomy'         => $taxonomy,
									'field'            => 'id',
									'terms'            => $term->term_id,
									'include_children' => false
							)
					)
			) );

			if ( $playlists && is_array( $playlists ) && isset( $playlists[0] ) ) {
				return $playlists[0];
			}
		}
	}

// Chiede alla funzione di correlazione
	if ( function_exists( "tbm_get_custom_related_posts" ) ) {
// TODO Verificare efficienza della funzione `tbm_get_custom_related_posts`
		return false;
		$playlist = tbm_get_custom_related_posts();
		if ( $playlist ) {
			return $playlist;
		}
	}

	return false;
}

/**
 * recupero la playlist di default
 * @return bool
 */
function html_video_get_priority_playlist() {
	if ( get_field( "enable_default_playlist", "options" ) ) {
		$default_playlists = get_field( "enable_default_playlist", "options" );
		$default_playlist  = $default_playlists[0];
		if ( $default_playlist ) {
			return $default_playlist;
		}
	}

	return false;
}

/**
 * Create sitemap loop
 */
function tbm_video_sitemap_loop() {

// WP_Query arguments
	$args = array(
			'post_type'      => array( 'video' ),
			'nopaging'       => true,
			'posts_per_page' => '-1',
	);

// The Query
	$query = new WP_Query( $args );

// The Loop
	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			$id              = get_the_ID();
			$thumbnail       = get_the_post_thumbnail_url( $id );
			$video_array     = tbm_get_videoid_field( $id );
			$permalink       = get_the_permalink();
			$title           = get_the_title_rss();
			$description     = get_the_excerpt();
			$description_rss = apply_filters( 'the_excerpt_rss', $description );
			$date            = get_the_time( 'c' );
			if ( ! $video_array ) {
				continue;
			}
			$video = html_get_main_video_array( $video_array );


			$output[] = "
<url>
    <loc>{$permalink}</loc>
    <video:video>
        <video:thumbnail_loc>{$thumbnail}</video:thumbnail_loc>
        <video:title>{$title}</video:title>
        <video:description>{$description_rss}</video:description>
        <video:content_loc>{$video['media_url']}</video:content_loc>
        <video:publication_date>{$date}</video:publication_date>
        <video:live>no</video:live>
    </video:video>
</url>";

		}
	}

// Restore original Post Data
	wp_reset_postdata();

	return implode( '', $output );

}

/**
 * Create feed loop
 */
function tbm_video_feed_loop() {

	$rsstag  = array();
	$keywords = '';

// WP_Query arguments
	$args = array(
			'post_type'      => array( 'video' ),
			'posts_per_page' => '200',
	);

	// The Query
	$query = new WP_Query( $args );

	// The Loop
	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			$id          = get_the_ID();
			$thumbnail   = get_the_post_thumbnail_url( $id );
			$video_array = tbm_get_videoid_field( $id );
			$title       = get_the_title_rss();
			$description = get_the_excerpt();
			$tags        = get_the_tags();
			if ( $tags ) {
				foreach ( $tags as $tag ) {
					$rsstag[] = $tag->name;
				}
				$keywords = implode( $rsstag, ',' );
			}
			$description_rss = apply_filters( 'the_excerpt_rss', $description );
			$date            = get_the_time( 'c' );
			if ( ! $video_array ) {
				continue;
			}
			$video    = html_get_main_video_array( $video_array );
			$category = html_get_theoutplay_video_category();

			$output[] = "
<item>
    <title>{$title}</title>
    <pubDate>{$date}</pubDate>
            <description>{$description_rss}</description>
            <media:content url=\"{$video['media_url']}\"></media:content>
            <media:thumbnail url=\"{$thumbnail}\"></media:thumbnail>
            <media:category>{$category}</media:category>
            <media:keywords>{$keywords}</media:keywords>
            <guid>{$id}</guid>
</item>";

		}
	}

// Restore original Post Data
	wp_reset_postdata();

	return implode( '', $output );

}

/**
 * Send a XML response back to an Ajax request.
 *
 * @param mixed $response Variable (usually an array or object) to then print and die.
 * @param int $status_code The HTTP status code to output.
 */
function tbm_send_xml( $response, $status_code = null ) {
	$seconds_to_cache = 43200;
	$ts               = gmdate( "D, d M Y H:i:s", time() + $seconds_to_cache ) . " GMT";
	@header( 'Content-Type: text/xml; charset=' . get_option( 'blog_charset' ) );
	@header( 'Expires: ' . $ts . '' );
	@header( 'Pragma: cache' );
	@header( 'Cache-Control: max-age=' . $seconds_to_cache . '' );
	if ( null !== $status_code ) {
		status_header( $status_code );
	}
	echo $response;

	if ( wp_doing_ajax() ) {
		wp_die( '', '', array(
				'response' => null,
		) );
	} else {
		die;
	}
}

/**
 * Get the url of the video file (internal or external)
 *
 * @param int|WP_Post $post Optional. Post ID or post object. Default is the global `$post`.
 *
 * @return mixed|string The url of the file or empty
 */
function tbm_video_get_file_url( $post = null ) {

	$video_url = '';
	$post      = get_post( $post );

	if ( ! $post || $post->post_type !== HTML_VIDEO_POST_TYPE ) {
		return '';
	}

	if ( $post->post_status !== 'publish' ) {
		return '';
	}

	/**
	 * Check internal video
	 */
	$video_internal = tbm_get_videoid_field( $post->ID );
	if ( $video_internal ) {
		$video_url = $video_internal['url'];
	}

	/**
	 * Check external video
	 */
	$video_external = get_field( 'video_external_mp4', $post->ID );
	if ( $video_external ) {
		$video_url = $video_external;
	}

	return $video_url;
}

/**
 * Get the url of the image attached to the video post
 *
 * @param int|WP_Post $post Optional. Post ID or post object. Default is the global `$post`.
 * @param array $size The size of the image to retrieve. Array of width and height.Default to 660x400
 *
 * @return mixed|string The url of the file or empty
 */
function tbm_video_get_poster( $post = null, $size = array( 660, 400, ) ) {

	$poster = '';
	$post   = get_post( $post );

	if ( ! $post || $post->post_type !== HTML_VIDEO_POST_TYPE ) {
		return '';
	}

	if ( $post->post_status !== 'publish' ) {
		return '';
	}

	if ( ! is_array( $size ) ) {
		$size = array( 660, 400, );
	}

	/**
	 * Get the poster
	 */
	$video_poster = tbm_get_the_post_thumbnail_url( $post->ID, $size );

	if ( $video_poster ) {
		$poster = $video_poster;
	}

	return $poster;

}
