<?php
/**
 * Filtri e actions wp
 */

/**
 * Redirect video sitemap (ie )
 */
add_action( 'template_redirect', function () {

	global $wp_query;

	$tbm_sitemap = $wp_query->get( 'tbm_sitemap' );

	if ( ! $tbm_sitemap ) {
		return;
	}

	if ( $tbm_sitemap === 'video' ) {
		$output = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">';
		$output .= tbm_video_sitemap_loop();
		$output .= '</urlset>';
	}

	tbm_send_xml( $output );

} );

/**
 * Redirect video feed (ie )
 */
add_action( 'template_redirect', function () {

	global $wp_query;

	$tbm_feed = $wp_query->get( 'tbm_feed' );

	if ( ! $tbm_feed ) {
		return;
	}

	if ( $tbm_feed === 'video' ) {
		$output = '<rss version="2.0" xmlns:media="http://search.yahoo.com/mrss/"><title>' . get_bloginfo( 'name' ) . ' video feed</title>
<link>' . get_bloginfo( 'url' ) . '</link><description>Video feed</description>';
		$output .= tbm_video_feed_loop();
		$output .= '</rss>';
	}

	tbm_send_xml( $output );

} );

/**
 * Check if the m3u8 contains valid video stream. Returns false if the file is invalid or not well formed, true if is valid
 *
 * @param string $file The m3u8 file
 *
 * @return bool
 */
function tbm_check_m3u8( $file = '' ) {

	if ( ! $file ) {
		return true;
	}

	$response = wp_remote_get( $file, array( 'timeout' => 2, 'httpversion' => '1.1' ) );

	//$response =file_get_contents($file);

	if ( ! is_array( $response ) || 200 != wp_remote_retrieve_response_code( $response ) ) {
		return true;
	}

	$body = $response['body'];
	if ( $body == "false" ) {
		return true;
	}

	if ( preg_match( '/media-[1-3]\/stream.m3u8/', $body ) ) {
		return false;
	}

	return true;
}

/**
 * Change post status to draft given a post id
 *
 * @param int|WP_Post $post_id Required. Post ID or post object
 *
 * @return bool|void|null
 */
function tbm_move_video_to_draft( $post_id = 0 ) {
	return false;
}
