<?php
/**
 * ACF fields & functions
 */

/**
 * Opzioni generali
 */
if ( function_exists( 'acf_add_options_page' ) ) {
	$args = array(
		'page_title' => 'TBM Config',
		'menu_slug'  => 'tbm_config',
		'icon_url'   => 'dashicons-list-view',
		'capability' => 'activate_plugins',
	);
	acf_add_options_page( $args );

	acf_add_options_sub_page( array(
		'page_title'  => 'Configurazione TBM Video',
		'menu_title'  => '[Video] Configurazione',
		'menu_slug'   => 'video_options',
		'parent_slug' => 'tbm_config',
		'capability'  => 'activate_plugins',
	) );

}

// ACF fields
if ( function_exists( 'acf_add_local_field_group' ) ):

	/**
	 * Video configs: switch e Correlator
	 */
	acf_add_local_field_group( array(
		'key'                   => 'group_5a7b03a5d4bfd',
		'title'                 => 'Configurazione video',
		'fields'                => array(

			array(
				'key'               => 'field_5a70634560i82',
				'label'             => 'Attiva il TribooVideo',
				'name'              => 'tbm_enable_triboovideo_plugin',
				'type'              => 'true_false',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '100',
					'class' => '',
					'id'    => '',
				),
				'message'           => 'Se attiva, ablita il player di Triboo; altrimenti stampa il video in un semplice tag &lt;video&gt;',
				'default_value'     => 0,
				'ui'                => 1,
				'ui_on_text'        => '',
				'ui_off_text'       => '',
			),
			array(
				'key'               => 'field_5a7b03af66381',
				'label'             => 'Producer',
				'name'              => 'video_producer',
				'type'              => 'text',
				'instructions'      => 'Il producer dei video. Serve contattare andrea.paleni@triboo.it per farselo indicare',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '20',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
			),
			array(
				'key'               => 'field_5a7b03be66382',
				'label'             => 'Distributor',
				'name'              => 'video_distributor',
				'type'              => 'text',
				'instructions'      => 'Il distributor dei video. Serve contattare andrea.paleni@triboo.it per farselo indicare',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '20',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
			),
			array(
				'key'               => 'field_5a7b03be60682',
				'label'             => 'CNVID Id',
				'name'              => 'video_cnvid_id',
				'type'              => 'text',
				'instructions'      => 'Il valore da esporre per la proprietà "site" del tribooVideo',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '20',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
			),
			array(
				'key'               => 'field_5a7b03be34hdg',
				'label'             => 'Spotx Mobile Id',
				'name'              => 'video_spotx_mobile_id',
				'type'              => 'text',
				'instructions'      => 'ID Spotx Mobile (valore da chiedere a Trafficker)',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '20',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
			),
			array(
				'key'               => 'field_5a7b03be608367',
				'label'             => 'Spotx Desktop Id',
				'name'              => 'video_spotx_desktop_id',
				'type'              => 'text',
				'instructions'      => 'ID Spotx Desktop (valore da chiedere a Trafficker)',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '20',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
			),
			array(
				'key'               => 'field_5a7b03be02846d',
				'label'             => 'Email per notifiche',
				'name'              => 'video_email_notified',
				'type'              => 'text',
				'instructions'      => 'Email da notificare in caso di problemi. Dividere più indirizzi email con una virgola',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'options_page',
					'operator' => '==',
					'value'    => 'video_options',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => 1,
		'description'           => '',
	) );



	/**
	 * Single video
	 */
	acf_add_local_field_group( array(
		'key'                   => 'group_5a6af7ef45226',
		'title'                 => 'Video',
		'fields'                => array(

			array(
				'key'               => 'field_5a6b07cd600ea',
				'label'             => 'Upload',
				'name'              => '',
				'type'              => 'tab',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'placement'         => 'top',
				'endpoint'          => 0,
			),
			array(
				'key'               => 'field_5a6af7f552eec',
				'label'             => 'File Video',
				'name'              => 'tbm_videoid',
				'type'              => 'file',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'return_format'     => 'array',
				'library'           => 'all',
				'min_size'          => '',
				'max_size'          => '',
				'mime_types'        => HTML_VIDEO_FORMAT_LIST,
			),

			array(
				'key'               => 'field_5c6d8f94xxpi9',
				'label'             => 'Durata del video',
				'name'              => 'tbm_video_durata',
				'type'              => 'number',
				'instructions'      => 'Durata, in secondi, del video',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'min'               => '',
				'max'               => '',
				'step'              => '1',
			),
			array(
				'key'               => 'field_5a6b07ab601e9',
				'label'             => 'Youtube/Vimeo o altro Oembed',
				'name'              => '',
				'type'              => 'tab',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'placement'         => 'top',
				'endpoint'          => 0,
			),
			array(
				'key'          => 'field_5a7b03ahy6354',
				'label'        => 'URL del video',
				'name'         => 'video_oembed_url',
				'type'         => 'url',
				'instructions' => 'URL del video. Deve essere compatibile <a href="https://wordpress.org/support/article/embeds/#okay-so-what-sites-can-i-embed-from" target="_blank" rel="noreferrer noopener">Oembed</a>.
										<strong>Attenzione:</strong> il video non potrà avere <u>pubblicità</u>.',
			),
			array(
				'key'               => 'field_5a6b07ab602e9',
				'label'             => 'URL MP4 Esterna',
				'name'              => '',
				'type'              => 'tab',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'placement'         => 'top',
				'endpoint'          => 0,
			),
			array(
				'key'          => 'field_5a7b03asdf345',
				'label'        => 'URL del file MP4',
				'name'         => 'video_external_mp4',
				'type'         => 'url',
				'instructions' => 'URL del video. Deve essere un video compatibile con il tag <code>&lt;video&gt;</code>.<strong>Attenzione:</strong> il video non potrà avere <u>pubblicità</u>.',
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'post_type',
					'operator' => '==',
					'value'    => 'video',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => 1,
		'description'           => '',
	) );

	/**
	 * Single video playlist
	 */
	acf_add_local_field_group( array(
		'key'                   => 'group_5a6b04f064e05',
		'title'                 => 'Playlist',
		'fields'                => array(
			array(
				'key'               => 'field_5a6b04f499193',
				'label'             => 'Playlist Video',
				'name'              => 'playlist_video',
				'type'              => 'relationship',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'post_type'         => array(
					0 => 'video',
				),
				'taxonomy'          => array(),
				'filters'           => array(
					0 => 'search'
				),
				'elements'          => '',
				'min'               => 1,
				'max'               => '',
				'return_format'     => 'object',
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'post_type',
					'operator' => '==',
					'value'    => 'playlist-video',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => 1,
		'description'           => '',
	) );

	/**
	 * Video Playlist
	 */
	acf_add_local_field_group( array(
		'key'                   => 'group_5a6f2f84447d1',
		'title'                 => 'Video Playlist',
		'fields'                => array(
			array(
				'key'               => 'field_5a6f2f9269ac9',
				'label'             => 'Playlist video',
				'name'              => 'playlist_video',
				'type'              => 'relationship',
				'instructions'      => 'Seleziona la playlist da associare al video',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'post_type'         => array(
					0 => 'playlist-video',
				),
				'taxonomy'          => array(),
				'filters'           => array(
					0 => 'search'
				),
				'elements'          => '',
				'min'               => '',
				'max'               => 1,
				'return_format'     => 'object',
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'post_type',
					'operator' => '==',
					'value'    => 'video',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => 1,
		'description'           => '',
	) );

endif;

/**
 * Personalizzo il layout del file video esponendone le info
 *
 * @param $arg
 */
add_action( 'acf/render_field/type=file', function ( $arg ) {

	if ( $arg["key"] == "field_5a6af7f552eec" ) {

		/**
		 * Add metadata to post edit screen
		 */
		if ( isset( $arg["value"] ) && ( $arg["value"] != "" ) ) {
			$prop_video = wp_get_attachment_metadata( $arg["value"] );
			if ( $prop_video ) {
				echo '<div class="acf-input">';
				foreach ( $prop_video as $key => $value ) {

					if ( $value != "" ) {
						if ( is_array( $value ) ) {
							foreach ( $value as $item_key => $item ) {
								if ( is_string( $item ) ) {
									echo "<span style='border: solid 1px #ccc; margin:4px 4px 0px 0px ; padding: 4px 6px; display: inline-block;'><b>" . $item_key . "</b> : " . $item . "</span> ";
								}
							}
						} else {
							echo "<span style='	border: solid 1px #ccc; margin:4px 4px 0px 0px ; padding: 4px 6px; display: inline-block;'><b>" . $key . "</b> : " . $value . "</span> ";
						}
					}
				}
				echo "</div><br style='clear: both' />";
			}
		}


		/**
		 * Manage correlator
		 */
		if ( isset( $arg["value"] ) && ( $arg["value"] != "" ) && isset( $_GET['post'] ) && ( $_GET['post'] !== '' ) ) {

			/**
			 * Add link to correlator results
			 */
			$response = HTML_VIDEO_URL_CHECK_MEDIACENTER . "/" . get_field( "video_producer", "option" ) . "/" . $arg["value"];
			echo '<div class="acf-input">';
			echo 'Se hai abilitato il Correlator di TribooMedia, puoi <a href="' . $response . '" target="_blank" rel="noreferrer noopener">controllare la sua risposta</a>';
			echo "</div><br style='clear: both' />";


			if ( ! get_post_meta( $_GET['post'], "triboo_processed_video", true ) ) {

				/**
				 * Add Javascript to save correlator data
				 */
				$nonce = wp_create_nonce( 'tbm_video_set_correlator_data' );
				$text  = '<style>.button-ok{background: #4CAF50 !important;color: #fff !important;cursor: not-allowed;pointer-events: none;}</style>';
				$text  .= '<style>.button-ko{background: #af1717 !important;color: #fff !important;cursor: not-allowed;pointer-events: none;}</style>';
				$text  .= '<button class="button button-info button-large button-save-correlator" id="save-correlator" data-spinner-color="black" data-videoid="' . $arg["value"] . '">Salva i dati del Correlator</button>';
				$text  .= '<div class="message"></div>';
				$text  .= "<script>var ajaxurl = '" . admin_url( 'admin-ajax.php' ) . "', tbmVideoId = '" . $arg["value"] . "', tbmPostId = '" . $_GET['post'] . "', videoNonce = '" . $nonce . "';</script>";

				echo $text;

				/**
				 * Enqueue styles and scripts
				 */
				wp_enqueue_style( 'ladda', 'https://cdnjs.cloudflare.com/ajax/libs/Ladda/1.0.6/ladda.min.css' );
				wp_enqueue_script( 'tbm-search-spin', 'https://cdnjs.cloudflare.com/ajax/libs/Ladda/1.0.6/spin.min.js', array( 'jquery' ), '', true );
				wp_enqueue_script( 'tbm-search-ladda', 'https://cdnjs.cloudflare.com/ajax/libs/Ladda/1.0.6/ladda.min.js', array( 'tbm-search-spin' ), '', true );
				wp_enqueue_script( 'tbm-search-ladda-jquery', 'https://cdnjs.cloudflare.com/ajax/libs/Ladda/1.0.6/ladda.jquery.min.js', array( 'tbm-search-ladda' ), '', true );
				wp_enqueue_script( 'tbm-video_correlation', HTML_VIDEO_PLUGIN_URL . 'dist/js/video_correlation.min.js', array( 'jquery' ), HTML_VIDEO_PLUGIN_VERSION, true );
			}
		}

	}
}, 20 );

/**
 * imposta come readonly i campi di elaborazione video
 */
add_action( 'acf/input/admin_footer', function () {

	?>
	<script type="text/javascript">
		(function ($) {
			$('.readonly input').prop('readonly', true);
		})(jQuery);
	</script>
	<?php

} );


