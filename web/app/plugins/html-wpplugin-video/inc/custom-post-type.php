<?php
/**
 * Custom Post Type
 */

function html_video_custom_post_type() {

	/**
	 * Video
	 */
	register_post_type( HTML_VIDEO_POST_TYPE,
		[
			'labels'      => [
				'name' => ucfirst( HTML_VIDEO_POST_TYPE_NAME )
			],
			'public'      => true,
			'has_archive' => true,
			'rewrite'     => [
				'slug'  => HTML_VIDEO_POST_TYPE_SLUG,
				'feeds' => true
			],
			'menu_icon'   => 'dashicons-video-alt',
			'supports'    => [ 'title', 'author', 'editor', 'excerpt', 'thumbnail' ],
			'description' => HTML_VIDEO_POST_TYPE_DESCRIPTION
		]
	);

	// category & tags
	register_taxonomy_for_object_type( 'category', HTML_VIDEO_POST_TYPE );
	register_taxonomy_for_object_type( 'post_tag', HTML_VIDEO_POST_TYPE );


	/**
	 * Playlist video
	 */
	register_post_type( HTML_PLAYLIST_VIDEO_POST_TYPE,
		[
			'labels'      => [
				'name' => ucfirst( HTML_PLAYLIST_VIDEO_POST_TYPE_NAME )
			],
			'public'      => true,
			'has_archive' => true,
			'rewrite'     => [ 'slug' => HTML_PLAYLIST_VIDEO_POST_TYPE_SLUG ],
			'menu_icon'   => 'dashicons-playlist-video',
			'supports'    => [ 'title', 'author' ],
			'description' => HTML_PLAYLIST_VIDEO_POST_TYPE_DESCRIPTION
		]
	);

	// category
	register_taxonomy_for_object_type( 'category', HTML_PLAYLIST_VIDEO_POST_TYPE );

}

add_action( 'init', 'html_video_custom_post_type' );
