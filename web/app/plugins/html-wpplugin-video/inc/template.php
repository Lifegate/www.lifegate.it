<?php
/**
 * Choose plugin template if single not exist
 */

/**
 * Checks to see if appropriate templates are present in active template directory.
 * Otherwises uses templates present in plugin's template directory.
 */
add_filter( 'template_include', 'html_video_set_template' );
function html_video_set_template( $template ) {
	if ( is_singular( HTML_VIDEO_POST_TYPE ) ) {
		if ( function_exists( 'App\locate_template' ) ) {
			$located = App\locate_template( 'single-' . HTML_VIDEO_POST_TYPE . '.php' );
		} else {
			$located = locate_template( 'single-' . HTML_VIDEO_POST_TYPE . '.php' );
		}
		if ( empty( $located ) ) {
			//WordPress couldn't find an 'question' template. Use plug-in instead:
			$template = HTML_VIDEO_PLUGIN_DIR . '/templates/single-' . HTML_VIDEO_POST_TYPE . '.php';
		}
	}
	if ( is_post_type_archive( HTML_VIDEO_POST_TYPE ) ) {
		if ( function_exists( 'App\locate_template' ) ) {
			$located = App\locate_template( 'archive-' . HTML_VIDEO_POST_TYPE . '.php' );
		} else {
			$located = locate_template( 'archive-' . HTML_VIDEO_POST_TYPE . '.php' );
		}
		if ( empty( $located ) ) {
			//WordPress couldn't find an 'question' template. Use plug-in instead:
			$template = HTML_VIDEO_PLUGIN_DIR . '/templates/archive-' . HTML_VIDEO_POST_TYPE . '.php';
		}
	}

	return $template;
}
