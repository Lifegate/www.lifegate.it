<?php

/**
 * Returns the array of information related to the video
 *
 * @param $post_id The post ID
 *
 * @return array|bool
 */
function tbm_get_videoid_field( $post_id ) {

	// Defaults (mimes the AFC response array)
	$defaults = array(
		'ID'          => 0,
		'id'          => 0,
		'title'       => '',
		'filename'    => '',
		'filesize'    => 0,
		'url'         => '',
		'link'        => '',
		'alt'         => '',
		'author'      => '',
		'description' => '',
		'caption'     => '',
		'name'        => '',
		'status'      => '',
		'uploaded_to' => 0,
		'date'        => '',
		'modified'    => '',
		'menu_order'  => 0,
		'mime_type'   => '',
		'type'        => '',
		'subtype'     => '',
		'icon'        => '',
		'width'       => 0,
		'height'      => 0,
	);

	$field = get_field( "tbm_videoid", $post_id );

	if ( empty( $field ) ) {
		return false;
	}

	if ( is_array( $field ) && isset( $field['ID'] ) ) {

		return $field;
	}

	if ( 'attachment' === get_post_type( $field ) ) {
		$id     = (int) $field;
		$fields = array( 'ID' => $id, 'url' => wp_get_attachment_url( $id ) );
		$videos = wp_parse_args( $fields, $defaults );

		return $videos;
	}

	return false;

}

/**
 * Returns the array of information related to the video when
 *
 * @param $post_id The post ID
 *
 * @return array|bool
 */
function tbm_get_videourl_field( $post_id ) {

	$url_video_elaborato_alta  = get_post_meta( $post_id, "url_video_elaborato_alta", true );
	$url_video_elaborato_bassa = get_post_meta( $post_id, "url_video_elaborato_bassa", true );

	if ( empty( $url_video_elaborato_alta ) && empty( $url_video_elaborato_bassa ) ) {
		return false;
	}

	if ( $url_video_elaborato_alta && filter_var( $url_video_elaborato_alta, FILTER_VALIDATE_URL ) !== false ) {
		return array( 'url' => $url_video_elaborato_alta );
	}

	if ( $url_video_elaborato_bassa && filter_var( $url_video_elaborato_bassa, FILTER_VALIDATE_URL ) !== false ) {
		return array( 'url' => $url_video_elaborato_bassa );
	}

	return false;


}
