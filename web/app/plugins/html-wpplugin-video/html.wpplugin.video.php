<?php
/*
Plugin Name: Html.it Video
Description: Plugin to handle Video
Version: 2.9.1
*/

$version = 'version:2.9.1';

define( "HTML_VIDEO_PLUGIN_DIR", plugin_dir_path( __FILE__ ) );
define( "HTML_VIDEO_PLUGIN_URL", plugin_dir_url( __FILE__ ) );
define( "HTML_VIDEO_PLUGIN_VERSION", str_replace( 'version:', '', $version ) );

// configuration params
require_once( HTML_VIDEO_PLUGIN_DIR . 'inc/define.php' );

// custom post type
require_once( HTML_VIDEO_PLUGIN_DIR . 'inc/functions.php' );

// check activation dependencies
require_once( HTML_VIDEO_PLUGIN_DIR . 'inc/activate.php' );

// custom post type
require_once( HTML_VIDEO_PLUGIN_DIR . 'inc/custom-post-type.php' );

// acf fields
require_once( HTML_VIDEO_PLUGIN_DIR . 'inc/acf.php' );

// templating rules
require_once( HTML_VIDEO_PLUGIN_DIR . 'inc/template.php' );

// actions & filters
require_once( HTML_VIDEO_PLUGIN_DIR . 'inc/actions-filters.php' );

// utils
require_once( HTML_VIDEO_PLUGIN_DIR . 'inc/utils.php' );

// shortcode
require_once( HTML_VIDEO_PLUGIN_DIR . 'inc/shortcode.php' );

// sitemap
require_once( HTML_VIDEO_PLUGIN_DIR . 'inc/rewrite.php' );
