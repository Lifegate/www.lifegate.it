<?php
// configuration
if ( ! defined( "HTML_CARD_POST_TYPE" ) ) {
	DEFINE( "HTML_CARD_POST_TYPE", "card" );
}
if ( ! defined( "HTML_CARD_POST_TYPE_NAME" ) ) {
	DEFINE( "HTML_CARD_POST_TYPE_NAME", "Cards" );
}
if ( ! defined( "HTML_CARD_POST_TYPE_SLUG" ) ) {
	DEFINE( "HTML_CARD_POST_TYPE_SLUG", "card" );
}
if ( ! defined( "HTML_CARD_POST_TYPE_DESCRIPTION" ) ) {
	DEFINE( "HTML_CARD_POST_TYPE_DESCRIPTION", "" );
}

