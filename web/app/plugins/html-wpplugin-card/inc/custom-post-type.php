<?php
/**
 * Custom Post Type
 */


add_action( 'init', function () {
	define( 'CARD_PERMALINK', 1 );

	register_post_type( HTML_CARD_POST_TYPE,
		[
			'labels'      => [
				'name' => ucfirst( HTML_CARD_POST_TYPE_NAME )
			],
			'public'      => true,
			'has_archive' => true,
			'rewrite'     => [
				'slug'       => HTML_CARD_POST_TYPE_SLUG,
				'ep_mask'    => CARD_PERMALINK,
				'feeds'      => true,
				'with_front' => false
			],
			'menu_icon'   => 'dashicons-exerpt-view',
			'supports'    => [ 'title', 'author', 'thumbnail', 'excerpt' ],
			'description' => HTML_CARD_POST_TYPE_DESCRIPTION
		]
	);

	// category & tags
	register_taxonomy_for_object_type( 'category', HTML_CARD_POST_TYPE );
	register_taxonomy_for_object_type( 'post_tag', HTML_CARD_POST_TYPE );
} );