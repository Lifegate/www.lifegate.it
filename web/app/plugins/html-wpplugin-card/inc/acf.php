<?php
/**
 * ACF fields & functions
 */

if ( function_exists( 'acf_add_local_field_group' ) ):

	acf_add_local_field_group( array(
		'key'                   => 'group_5a393368841dd',
		'title'                 => 'Cards',
		'fields'                => array(
			array(
				'key'               => 'field_5a39341cbdba8',
				'label'             => 'Paragrafi',
				'name'              => 'html_card_paragrafo_repeater',
				'type'              => 'repeater',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'collapsed'         => '',
				'min'               => 1,
				'max'               => 0,
				'layout'            => 'table',
				'button_label'      => 'Aggiungi paragrafo',
				'sub_fields'        => array(
					array(
						'key'               => 'field_5a393466bdba9',
						'label'             => 'Paragrafo',
						'name'              => 'html_card_paragrafo',
						'type'              => 'wysiwyg',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'default_value'     => '',
						'tabs'              => 'all',
						'toolbar'           => 'full',
						'media_upload'      => 1,
						'delay'             => 0,
					),
				),
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'post_type',
					'operator' => '==',
					'value'    => 'card',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => 1,
		'description'           => '',
	) );


endif;




