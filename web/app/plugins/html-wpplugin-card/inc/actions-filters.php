<?php
/**
 * inclusione di js e css di owl carousel
 */


add_action( 'wp_enqueue_scripts', function () {
	global $post;
	//$card_pagination_options = get_field("opzioni_paginazione", "options");
	if ( is_singular( HTML_CARD_POST_TYPE ) || ( is_singular() && has_shortcode( $post->post_content, 'htmlcards' ) ) ) {
		wp_enqueue_style( 'swiper-min', plugin_dir_url( __FILE__ ) . '../css/swiper.min.css' );
		wp_enqueue_script( 'swiper-js', plugin_dir_url( __FILE__ ) . '../js/swiper.js', array( 'jquery' ), "4.1.0" );
	}
} );

/**
 * Add pagination endpoint
 *
 */
add_action( 'init', function () {
	add_rewrite_endpoint( 'doc', CARD_PERMALINK );
} );;