<?php

/**
 * Get the number of paragraphs of schede
 *
 * @param int|WP_Post $post Optional. Post ID or WP_Post object. Default is global $post.
 *
 * @return int|void
 */
function tbm_card_count_paragraphs( $post = 0 ) {

	$post = get_post( $post );
	if ( empty( $post ) ) {
		return;
	}

	$paragraphs = get_field( "html_card_paragrafo_repeater", $post->ID );

	if ( $paragraphs ) {
		return count( $paragraphs );
	}
}