<?php
/*
Plugin Name: Html.it Card
Description: Plugin to handle cards
Version: 1.1.0
*/

DEFINE( "HTML_CARD__PLUGIN_DIR", plugin_dir_path( __FILE__ ) );

// configuration params
require_once( HTML_CARD__PLUGIN_DIR . 'inc/define.php' );

// check activation dependencies
require_once( HTML_CARD__PLUGIN_DIR . 'inc/activate.php' );

// custom post type
require_once( HTML_CARD__PLUGIN_DIR . 'inc/custom-post-type.php' );

// acf fields
require_once( HTML_CARD__PLUGIN_DIR . 'inc/acf.php' );

// configuration params
require_once( HTML_CARD__PLUGIN_DIR . 'inc/functions.php' );

// templating rules
require_once( HTML_CARD__PLUGIN_DIR . 'inc/template.php' );

// actions & filters
require_once( HTML_CARD__PLUGIN_DIR . 'inc/actions-filters.php' );

// shortcode
require_once( HTML_CARD__PLUGIN_DIR . 'inc/shortcode.php' );