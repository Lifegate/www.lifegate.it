<?php
get_header();

global $post;

// option acf generali
$card_pagination_options = get_field( "opzioni_paginazione", "options" );

// opzioni acf singolo post
$card_pagination_post = get_field( "post_pagination", $post );
if ( ( $card_pagination_post != "" ) && ( $card_pagination_post != "general_option" ) ) {
	$card_pagination_options = $card_pagination_post;
}

?>
    <div class="wrap">
        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">
				<?php while ( have_posts() ) : the_post(); ?>
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <header class="entry-header">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
							<?php edit_post_link( "Edit", '<span class="edit-link">', '</span>' ); ?>
                        </header><!-- .entry-header -->
                        <div class="entry-content">
							<?php
							$paragraphs = get_field( "html_card_paragrafo_repeater" );
							// paginazione in pagina senza reload
							if ( $card_pagination_options == "same_page" ) { ?>


							<?php } else {
								// paginazione con reload di pagina
								$totpages = count( $paragraphs );
								$page     = ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1;
								if ( is_preview() ) {
									$next = get_preview_post_link( $post, array( 'page' => $page + 1 ) );
									$prev = get_preview_post_link( $post, array( 'page' => $page - 1 ) );
								} else {
									$next = get_permalink( $post ) . '/' . ( $page + 1 );
									$prev = get_permalink( $post ) . '/' . ( $page - 1 );
								}
								?>
								<?php if ( $page > 1 ) : ?>
                                    <a href="<?php echo $prev; ?>">
                                        <i class="left-arrow text-btn"></i>
                                    </a>
								<?php endif; ?>
                                <div>
									<?php echo $paragraphs[ $page - 1 ]["html_card_paragrafo"]; ?>
                                </div>
								<?php if ( ( $totpages - $page ) > 0 ) : ?>
                                    <div>
                                        <a href="<?php echo $next; ?>"
                                           class="cta cta--enter">Continua</a>
                                    </div>
								<?php endif; ?>

							<?php } ?>

                        </div><!-- .entry-content -->
                    </article><!-- #post-## -->
				<?php endwhile; // End of the loop. ?>
            </main><!-- #main -->
        </div><!-- #primary -->
        <aside id="secondary" class="widget-area" role="complementary">
            <ul>
				<?php
				$terms = get_terms( array(
					'taxonomy'   => 'category',
					'hide_empty' => false,
				) );

				foreach ( $terms as $term ) {
					echo '<li><a href="' . get_term_link( $term ) . '?post_type=' . HTML_CARD_POST_TYPE . '">' . $term->name . '</a></li>';
				}
				?>
            </ul>
        </aside><!-- #secondary -->
    </div><!-- .wrap -->
<?php
get_footer();
// swiper
if ( $card_pagination_options == "same_page" ) {
	?>
    <style>
        .swiper-pagination-bullet {
            width: 20px;
            height: 20px;
            text-align: center;
            line-height: 20px;
            font-size: 12px;
            color: #000;
            opacity: 1;
            background: rgba(0, 0, 0, 0.2);
        }

        .swiper-pagination-bullet-active {
            color: #fff;
            background: #007aff;
        }

    </style>
    <script>
		var mySwiper = new Swiper( '.swiper-container', {
			autoHeight: true,
			loop: true,
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
				renderBullet: function ( index, className ) {
					return '<span class="' + className + '">' + (
					       index + 1
					) + '</span>';
				},
			},
		} )
    </script>
	<?php
}


