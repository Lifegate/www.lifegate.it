<?php
/*
Plugin Name: Core Web Vitals Tracker by WPSpeedFix
Plugin URI: https://www.wpspeedfix.com/core-web-vitals-tracker
description: Core Web Vitals Tracker by WPSpeedFix
Version: 1.0.0
Author: WPSpeedFix
Author URI: https://www.wpspeedfix.com/
License: GPL2
*/

function cwvt_admin_menu() {
    add_menu_page( __( 'Core Web Vitals', 'cwvt' ), __( 'Core Web Vitals', 'cwvt' ), 'manage_options', 'core-web-vitals', 'cwt_tracker_admin_page', 'dashicons-performance');
}
add_action( 'admin_menu', 'cwvt_admin_menu' );

function cwt_tracker_admin_page() {
    $analytic = get_option( 'cwvt_analytic', 'analytic' );
?>
    <div class="wrap">
        <h1><?php esc_html_e( 'Core Web Vitals Tracker', 'cwvt' ); ?></h1>
        <form method="post" action="<?php echo esc_html( admin_url( 'admin-post.php' ) ); ?>">
            <input type="hidden" name="action" value="cwvt_settings" />
            <table class="form-table">
                <tbody>
                    <tr>
                        <th>
                            <label><?php _e( 'Choose your analytics script type', 'cwvt' ); ?></label>
                        </th>
                        <td>
                            <select name="analytic">
                                <?php
                                    if ( 'analytic' == $analytic ) {
                                ?>
                                        <option value="analytic" selected="selected">analytics.js</option>
                                        <option value="gtag">gtag.js</option>
                                <?php
                                    } else {
                                ?>
                                        <option value="analytic">analytics.js</option>
                                        <option value="gtag" selected="selected">gtag.js</option>
                                <?php
                                    }
                                ?>
                                
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php
                wp_nonce_field( 'cwvt_settings_save' );
                submit_button();
            ?>
        </form>
        <p><?php _e( 'Our core web vitals tracker pushes core web vitals speed data into Google Analytics. Right now we only support Universal Analytics (GA4 support coming).', 'cwvt' ); ?></p>
        <p>
            <?php _e( 'You’ll need to choose the script type from the dropdown. If you don’t choose the correct script type or don’t have Google Analytics installed no data will be tracked.', 'cwvt' ); ?><br />
            <?php echo sprintf( __( 'There\'s a video explanation at %s that explains which option to choose and how to check whether it\'s working', 'cwvt'), '<a href="https://www.wpspeedfix.com/core-web-vitals-tracker" target="_blank">https://www.wpspeedfix.com/core-web-vitals-tracker</a>' ); ?>
        </p>
        <p><?php _e( 'After choosing the analytics type hit the save button and then clear your WordPress cache and Cloudflare cache (if using Cloudflare)', 'cwvt' ); ?></p>
        <p><?php _e( 'To test whether the script is working, open a page on your website in Google Chrome, right click on the page and choose Inspect and then click the Console tab in the sidebar section that opens.', 'cwvt' );  ?></p>
        <p><?php _e( 'If the script is working you should see speed data appear after interacting with the page.', 'cwvt' ); ?></p>
    </div>
<?php
}

function cwvt_settings_save() {
    $nonce = sanitize_text_field( $_POST['_wpnonce'] );
    if ( !wp_verify_nonce( $nonce, 'cwvt_settings_save' ) ) {
        wp_exit( 'Sorry, your nonce did not verify' );
    } else {
        $analytic = sanitize_text_field( $_POST['analytic'] );
        update_option( 'cwvt_analytic', $analytic );
        wp_redirect( admin_url( 'admin.php?page=core-web-vitals&cwvt-settings-success' ) );
    }
}
add_action( 'admin_post_cwvt_settings', 'cwvt_settings_save' );

function cwvt_admin_notices() {
    if ( isset( $_GET['cwvt-settings-success'] ) ) {
?>
        <div class="notice notice-success is-dismissible">
            <p><?php _e( 'Settings have been saved', 'cwvt' ); ?></p>
        </div>
<?php   
    }
}
add_action( 'admin_notices', 'cwvt_admin_notices' );

function cwvt_insert_snippet() {
    $analytic = get_option( 'cwvt_analytic', 'analytic' );
    if ( 'analytic' == $analytic ) {
?>
        <!-- WPSpeedFix.com core web vitals tracker script START -->
        <script type="module">
            import { getCLS, getFID, getLCP, getTTFB, getFCP } from 'https://unpkg.com/web-vitals?module';
            function sendToGoogleAnalytics({ name, delta, id }) {
                ga('send', 'event', {
                    eventCategory: 'Web Vitals',
                    eventAction: name,
                    eventLabel: id,
                    eventValue: Math.round('CLS' === name ? delta * 1000 : delta),
                    nonInteraction: true,
                    transport: 'beacon',
                });
            }
            getCLS( sendToGoogleAnalytics );
            getFID( sendToGoogleAnalytics );
            getLCP( sendToGoogleAnalytics );
            getTTFB( sendToGoogleAnalytics );
            getFCP( sendToGoogleAnalytics );
            getCLS( console.log );
            getFID( console.log );
            getLCP( console.log );
            getTTFB( console.log );
            getFCP( console.log );
        </script>
        <!-- WPSpeedFix.com core web vitals tracker script END -->
<?php
    } else {
?>
        <!-- WPSpeedFix.com core web vitals tracker script START -->
        <script type="module">
            import { getCLS, getFID, getLCP, getTTFB, getFCP } from 'https://unpkg.com/web-vitals?module';
            function sendToGoogleAnalytics( {name, delta, id} ) {
                gtag('event', name, {
                    event_category: 'Web Vitals',
                    event_label: id,
                    value: Math.round( 'CLS' === name ? delta * 1000 : delta),
                    non_interaction: true,
                });
            }
            getCLS( sendToGoogleAnalytics );
            getFID( sendToGoogleAnalytics );
            getLCP( sendToGoogleAnalytics );
            getTTFB( sendToGoogleAnalytics );
            getFCP( sendToGoogleAnalytics );
            getCLS( console.log );
            getFID( console.log );
            getLCP( console.log );
            getTTFB( console.log );
            getFCP( console.log );
        </script>
        <!-- WPSpeedFix.com core web vitals tracker script END -->
<?php
    }
}
add_action( 'wp_footer', 'cwvt_insert_snippet' );