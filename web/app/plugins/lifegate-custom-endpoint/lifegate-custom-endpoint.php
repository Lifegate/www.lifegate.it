<?php
/*
Plugin Name: Lifegate Custom API Endpoint
Description: Adds a custom API endpoint to retrieve datas.
Version: 1.0
*/

// Register custom API endpoint
add_action('rest_api_init', 'custom_user_api_endpoint');

function custom_user_api_endpoint() {
	register_rest_route('lifegate/v1', '/post_user/(?P<id>\d+)', array(
		'methods' => 'GET',
		'callback' => 'get_post_user_details',
		'args' => array(
			'id' => array(
				'validate_callback' => function($param, $request, $key) {
					return is_numeric($param);
				}
			),
		),
	));
}

// Callback function to retrieve user details
function get_post_user_details($data) {
	$post_id = $data['id'];
	$post = get_post($post_id);
	$count = 0;
	$name = array();
	if (!$post) {
		return new WP_Error('post_not_found', 'Post not found', array('status' => 404));
	}


	$author_free = get_field( 'tbm_autore_news_free' , $post_id);

	$authors  = get_field( 'autore_news' , $post_id);

	if ( ! $authors  && ! $author_free) {
		$user_info[] = array("id" => "", "name" => __( 'Redazione LifeGate', 'lifegate' ), "image" => null );
	}

	/**
	 * Old custom fields are strings. Cast to array
	 */
	if ( $authors && !is_array( $authors ) ) {
		$authors = (array) $authors;
	}

	foreach ( $authors as $author ) {
		$post_author = get_post( $author );
		if ( empty( $post_author ) ) {
			$user_info[] = array("id" => "", "name" => __( 'Redazione LifeGate', 'lifegate' ), "image" => null );
			continue;
		}
		$user_info[] = array("id" => $post_author->ID, "name" => get_the_title( $author ), "image" => tbm_get_the_post_thumbnail_url( $post_author->ID, array( 320, 172 ) ) );
	}


	if ( $author_free ) {
		$user_info[] = array("id" => "", "name" => $author_free, "image" =>  null);

	}


	return $user_info;
}
