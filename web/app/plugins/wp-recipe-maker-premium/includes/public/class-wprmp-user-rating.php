<?php
/**
 * Handle the user ratings.
 *
 * @link       http://bootstrapped.ventures
 * @since      1.6.0
 *
 * @package    WP_Recipe_Maker_Premium
 * @subpackage WP_Recipe_Maker_Premium/includes/public
 */

/**
 * Handle the user ratings.
 *
 * @since      1.6.0
 * @package    WP_Recipe_Maker_Premium
 * @subpackage WP_Recipe_Maker_Premium/includes/public
 * @author     Brecht Vandersmissen <brecht@bootstrapped.ventures>
 */
class WPRMP_User_Rating {

	/**
	 * Current user UID.
	 *
	 * @since	8.4.0
	 * @access	private
	 * @var		mixed	$uid	UID for the current user.
	 */
	private static $uid = false;

	private static $modal_uid = false;

	/**
	 * Register actions and filters.
	 *
	 * @since    1.6.0
	 */
	public static function init() {
		add_action( 'wp_ajax_wprm_user_rate_recipe', array( __CLASS__, 'ajax_user_rate_recipe' ) );
		add_action( 'wp_ajax_nopriv_wprm_user_rate_recipe', array( __CLASS__, 'ajax_user_rate_recipe' ) );
	}

	/**
	 * Get user ratings modal UID.
	 *
	 * @since	9.2.0
	 */
	public static function get_modal_uid() {
		// Only create modal once and reuse for all user ratings.
		if ( false === self::$modal_uid ) {
			ob_start();
			require( WPRMP_DIR . 'templates/public/user-ratings-popup.php' );
			$modal_content = ob_get_contents();
			ob_end_clean();

			self::$modal_uid = WPRM_Popup::add(
				array(
					'type' => 'user-rating',
					'reuse' => true,
					'title' => WPRM_Settings::get( 'user_ratings_modal_title' ),
					'content' => $modal_content,
				)
			);
		}

		return self::$modal_uid;
	}

	/**
	 * Get user ratings for a specific recipe.
	 *
	 * @since	2.2.0
	 * @param	int $recipe_id ID of the recipe.
	 */
	public static function get_ratings_for( $recipe_id ) {
		$recipe_id = intval( $recipe_id );

		$ratings = array();

		if ( $recipe_id ) {
			$user_ratings = WPRM_Rating_Database::get_ratings(array(
				'where' => 'recipe_id = ' . $recipe_id,
			));

			$ratings = $user_ratings['ratings'];
		}

		return $ratings;
	}

	/**
	 * Add or update rating for a specific recipe.
	 *
	 * @since	2.2.0
	 * @param	int $recipe_id ID of the recipe.
	 * @param	int $user_rating Rating to add for this recipe.
	 */
	public static function add_or_update_rating_for( $recipe_id, $user_rating ) {
		$recipe_id = intval( $recipe_id );

		if ( $recipe_id ) {
			$rating = array(
				'recipe_id' => $recipe_id,
				'user_id' => get_current_user_id(),
				'ip' => self::get_user_ip(),
				'rating' => $user_rating,
			);

			WPRM_Rating_Database::add_or_update_rating( $rating );

			// Maybe clear cache.
			if ( WPRM_Settings::get( 'user_ratings_clear_cache' ) ) {
				// Get optional parent post ID.
				$parent_post_id = false;

				$recipe = WPRM_Recipe_Manager::get_recipe( $recipe_id );
				if ( $recipe ) {
					$parent_post_id = $recipe->parent_post_id();
				}

				// Try to clear caching plugins.
				if ( $parent_post_id ) {
					WPRM_Cache::clear( $recipe_id, false );
					WPRM_Cache::clear( $parent_post_id );
				} else {
					WPRM_Cache::clear( $recipe_id );
				}
			}
		}
	}

	/**
	 * Get the rating the current user has given to a specific recipe.
	 *
	 * @since    1.6.0
	 * @param	 int $recipe_id The Recipe to get the rating for.
	 */
	public static function get_user_rating_for( $recipe_id ) {
		if ( isset ( $_COOKIE[ 'WPRM_User_Voted_' . $recipe_id ] ) ) {
			return intval( $_COOKIE[ 'WPRM_User_Voted_' . $recipe_id ] );
		}

		$rating = 0;

		$ip = self::get_user_ip();
		$user = get_current_user_id();

		$user_ratings = self::get_ratings_for( $recipe_id );

		foreach ( $user_ratings as $user_rating ) {
			if ( ! $user && 'unknown' !== $ip && $ip === $user_rating->ip ) {
				$rating = $user_rating->rating;
			} elseif ( $user && $user === $user_rating->user_id ) {
				$rating = $user_rating->rating;
			}
		}

		return $rating;
	}

	/**
	 * Set the user rating for a recipe through AJAX.
	 * Duplicates functionality in the api_user_rating_for_recipe function.
	 *
	 * @since    1.6.0
	 */
	public static function ajax_user_rate_recipe() {
		if ( is_user_logged_in() && ! check_ajax_referer( 'wprm', 'security', false ) ) {
			// Logged in users need to be verified.
			wp_send_json_error();
		}

		$recipe_id = isset( $_POST['recipe_id'] ) ? intval( $_POST['recipe_id'] ) : 0; // Input var okay.
		$encoded = isset( $_POST['data'] ) ? $_POST['data'] : false; // Input var okay.

		if ( $recipe_id && $encoded ) {
			$encoded = rawurldecode( $encoded );
			$encoded = str_replace( '\"', '"', $encoded );
			$data = json_decode( $encoded );

			if ( $data ) {
				$data = (array) $data;
				$rated_recipe = self::rate_recipe( $recipe_id, $data );

				if ( $rated_recipe ) {
					// Get new recipe object.
					$recipe = WPRM_Recipe_Manager::get_recipe( $recipe_id );
		
					if ( $recipe ) {
						wp_send_json_success( $recipe->rating() );
					}
				}
			}
		}
		
		wp_send_json_error();
	}

	/**
	 * Set the user rating for a recipe.
	 *
	 * @since    9.2.0
	 */
	public static function rate_recipe( $recipe_id, $data ) {
		$recipe_id = intval( $recipe_id );
		$post_id = isset( $data['post_id'] ) ? intval( $data['post_id'] ) : 0;
		$rating = isset( $data['rating'] ) ? intval( $data['rating'] ) : 0;

		if ( $recipe_id && $rating && self::is_user_allowed_to_vote() ) {
			$comment = isset( $data['comment'] ) ? trim( $data['comment'] ) : '';
			if ( $comment ) {
				$user_id = get_current_user_id();
				$name = '';
				$email = '';

				if ( 0 === $user_id ) {
					$name = isset( $data['name'] ) ? trim( $data['name'] ) : '';
					$email = isset( $data['email'] ) ? trim( $data['email'] ) : '';
				}

				// Only allow comment if name and email are set.
				if ( $user_id || ( $name && $email ) ) {
					$recipe = WPRM_Recipe_Manager::get_recipe( $recipe_id );
					
					// Comment needs to be for the parent post of the recipe.
					if ( $recipe && $recipe->parent_post_id() ) {
						$comment_id = wp_new_comment( array(
							'comment_post_ID' => $recipe->parent_post_id(),
							'comment_author' => $name,
							'comment_author_email' => $email,
							'comment_author_IP' => self::get_user_ip(),
							'comment_content' => $comment,
							'user_id' => $user_id,
						), true );

						// Add rating to this new comment.
						if ( $comment_id && ! is_wp_error( $comment_id ) ) {
							WPRM_Comment_Rating::add_or_update_rating_for( $comment_id, $rating );

							return true;
						}
					}
				}
			}

			// No (successful) comment, so we'll make it a user rating.
			// Only allow user ratings if a comment was not required.
			if ( ! self::is_comment_required( $rating ) ) {
				self::add_or_update_rating_for( $recipe_id, $rating );
		
				// Track in analytics.
				WPRM_Analytics::register_action( $recipe_id, $post_id, 'user-rating', array(
					'rating' => $rating,
				) );

				// Set or update cookie for easy access.
				setcookie( 'WPRM_User_Voted_' . $recipe_id, $rating, time() + 60 * 60 * 24 * 30, '/' );
				$_COOKIE[ 'WPRM_User_Voted_' . $recipe_id ] = $rating; // Set in current request as well.

				// Invalidate recipe to prevent showing old ratings.
				WPRM_Recipe_Manager::invalidate_recipe( $recipe_id );

				return true;
			}
		}

		return false;
	}

	/**
	 * Check if a comment is required when giving a specific rating.
	 *
	 * @since    9.2.0
	 */
	public static function is_comment_required( $rating ) {
		// We can only be sure a comment was possible when using modal mode.
		if ( 'modal' === WPRM_Settings::get( 'user_ratings_mode' ) ) {
			$comment_required_for = WPRM_Settings::get( 'user_ratings_force_comment' );

			if ( 'never' !== $comment_required_for ) {
				$check_stars = array(
					'1_star' => 1,
					'2_star' => 2,
					'3_star' => 3,
					'4_star' => 4,
					'always' => 5,
				);
	
				if ( array_key_exists( $comment_required_for, $check_stars ) ) {
					if ( $rating <= $check_stars[ $comment_required_for ] ) {
						return true;
					}
				}
			}
		}

		return false;
	}

	/**
	 * Check if the current user is allowed to vote.
	 *
	 * @since    1.6.0
	 */
	public static function is_user_allowed_to_vote() {
		if ( 0 === get_current_user_id() && 'unknown' === self::get_user_ip() ) {
			return false;
		}

		return true;
	}

	/**
	 * Get the IP address of the current user.
	 * Source: http://stackoverflow.com/questions/6717926/function-to-get-user-ip-address
	 *
	 * @since    1.6.0
	 */
	public static function get_user_ip() {
		if ( 'uid' === WPRM_Settings::get( 'user_ratings_spam_prevention' ) ) {
			// Check if this user already has a UID.
			$uid = false;

			if ( self::$uid ) {
				$uid = self::$uid;
			}

			// Check if set in cookies.
			if ( ! $uid && isset ( $_COOKIE[ 'WPRM_UID' ] ) ) {
				$uid = intval( $_COOKIE[ 'WPRM_UID' ] );
			}

			// No UID set, generate new one.
			if ( ! $uid ) {
				$max_uid = intval( get_option( 'wprm_uid', 0 ) );
				$uid = $max_uid + 1;
				
				setcookie( 'WPRM_UID', $uid, time() + 60 * 60 * 24 * 30, '/' );
				update_option( 'wprm_uid', $uid, false );
				self::$uid = $uid;
			}

			return 'uid-' . $uid;
		} else {
			foreach ( array( 'REMOTE_ADDR', 'HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED' ) as $key ) {
				if ( array_key_exists( $key, $_SERVER ) === true ) {
					foreach ( array_map( 'trim', explode( ',', $_SERVER[ $key ] ) ) as $ip ) { // Input var ok.
						if ( filter_var( $ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE ) !== false ) {
							return $ip;
						}
					}
				}
			}
		}

		return 'unknown';
	}
}

WPRMP_User_Rating::init();