window.WPRecipeMaker = typeof window.WPRecipeMaker === "undefined" ? {} : window.WPRecipeMaker;

window.WPRecipeMaker.userRatingModal = {
	init() {
		// Check for opening user rating modals.
		document.addEventListener( 'wprm-modal-open', ( event ) => {
			if ( 'user-rating' === event.detail.type ) {
				window.WPRecipeMaker.userRatingModal.opened( event.detail.uid, event.detail.modal, event.detail.data );
			}
		});

		// Check for stars change.
		document.addEventListener( 'wprm-comment-rating-change', ( event ) => {
			if ( event.detail.hasOwnProperty( 'container' ) && event.detail.container.classList.contains( 'wprm-user-ratings-modal-stars' ) ) {
				window.WPRecipeMaker.userRatingModal.ratingChange( event.detail.rating );
			}
		});
	},
	modalUid: false,
	opened( uid, modal, data ) {
		// Store current modal UID.
		window.WPRecipeMaker.userRatingModal.modalUid = uid;

		// Start out with loader.
		window.WPRecipeMaker.userRatingModal.displayMessage( '<div class="wprm-loader"></div>' );

		// Set default rating passed along, but reset to 0 first.
		const starsContainer = modal.querySelector( '.wprm-user-ratings-modal-stars-container' );
		const inputs = starsContainer.querySelectorAll( 'input' );

		inputs[0].click();

		if ( data.hasOwnProperty( 'rating' ) ) {
			const rating = parseInt( data.rating );

			for ( let input of inputs ) {
				if ( rating === parseInt( input.value ) ) {
					input.click();
					break;
				}
			}
		}

		// Set recipe passed along.
		const recipeIdInput = modal.querySelector( 'input[name="wprm-user-rating-recipe-id"]' );
		recipeIdInput.value = data.hasOwnProperty( 'recipe' ) ? data.recipe : 0;

		// Load recipe first, which might be through API.
		window.WPRecipeMaker.manager.getRecipe( recipeIdInput.value ).then( ( recipe ) => {

			// Show form.
			window.WPRecipeMaker.userRatingModal.displayMessage( false );

			// Clear other fields.
			const comment = modal.querySelector( '.wprm-user-rating-modal-comment' );
			comment.value = '';
			window.WPRecipeMaker.userRatingModal.commentChange( comment ); // Trigger change.

			// Clear errors and waiting.
			window.WPRecipeMaker.userRatingModal.displayError( false );
			window.WPRecipeMaker.userRatingModal.displayWaiting( false );
			
			// Set the name of the recipe being rated.
			const recipeNameField = modal.querySelector( '.wprm-user-ratings-modal-recipe-name' );
			recipeNameField.innerHTML = recipe ? recipe.data.name : '';
		});
	},
	ratingChange( rating ) {
		// Trigger comment change to check if comment might be required now.
		const comment = document.querySelector( '.wprm-user-rating-modal-comment' );
		window.WPRecipeMaker.userRatingModal.commentChange( comment );
	},
	commentChange( el ) {
		let hasComment = el.value.length > 0;
		const isLoggedIn = 0 < parseInt( wprmp_public.user );
		const modal = el.closest( '.wprm-popup-modal-user-rating' );

		// Check if comment is required.
		const formData = new FormData( el.form );
		const formProps = Object.fromEntries( formData );
		const rating = parseInt( formProps['wprm-user-rating-stars'] );

		if ( window.WPRecipeMaker.userRating.isCommentRequired( rating ) ) {
			hasComment = true;

			// Make textarea required.
			el.required = true;
		} else {
			el.required = false;
		}
		
		// Apply correct class.
		if ( hasComment ) {
			modal.classList.add( 'wprm-user-rating-modal-has-comment' );
		} else {
			modal.classList.remove( 'wprm-user-rating-modal-has-comment' );
		}

		if ( isLoggedIn ) {
			modal.classList.add( 'wprm-user-rating-modal-logged-in' );
		}

		// Make inputs required only when visible.
		const inputs = modal.querySelectorAll( '.wprm-user-rating-modal-comment-meta input' );

		for ( let input of inputs ) {
			input.required = hasComment && ! isLoggedIn;
		}
	},
	submit( form ) {
		const formData = new FormData( form );
		const formProps = Object.fromEntries( formData );

		const recipeId = parseInt( formProps['wprm-user-rating-recipe-id'] );
		const rating = parseInt( formProps['wprm-user-rating-stars'] );
		const comment = formProps['wprm-user-rating-comment'].trim();
		const name = formProps['wprm-user-rating-name'].trim();
		const email = formProps['wprm-user-rating-email'].trim();

		// Check if rating is correct.
		if ( rating <= 0 || rating > 5 ) {
			window.WPRecipeMaker.userRatingModal.displayError( 'rating' );
			return false;
		}

		// Check if comment is correct, but only needed if user is not logged in.
		if ( comment && 0 === parseInt( wprmp_public.user ) ) {
			if ( ! name || ! email ) {
				window.WPRecipeMaker.userRatingModal.displayError( 'meta' );
				return false;
			}
		}

		// All good, hide any errors and submit.
		window.WPRecipeMaker.userRatingModal.displayError( false );
		window.WPRecipeMaker.userRatingModal.displayWaiting( true );

		// Submit rating.
		const data = {
			post_id: wprm_public.post_id,
			rating,
			comment,
			name,
			email,
		};

		window.WPRecipeMaker.manager.getRecipe( recipeId ).then( ( recipe ) => {
			const showMessage = ( success ) => {
				let message = '';
		
				if ( false === success ) {
					message = wprmp_public.settings.user_ratings_problem_message;
				} else {
					message = wprmp_public.settings.user_ratings_thank_you_message;
				}
	
				// Show message or close modal.
				if ( message ) {
					window.WPRecipeMaker.userRatingModal.displayMessage( message );
				} else {
					window.WPRecipeMaker.modal.close( window.WPRecipeMaker.userRatingModal.modalUid );
				}
			};

			if ( recipe ) {
				recipe.addRating( data ).then( ( success ) => {
					window.WPRecipeMaker.userRatingModal.displayWaiting( false );
					showMessage( success );
				} );
			} else {
				showMessage( false );
			}
		});
	},
	displayError( error ) {
		const container = document.querySelector( '#wprm-user-rating-modal-errors' );

		if ( container ) {
			container.querySelectorAll( 'div' ).forEach( ( el ) => {
				if ( error && el.id === 'wprm-user-rating-modal-error-' + error ) {
					el.style.display = 'block';
				} else {
					el.style.display = '';
				}
			});
		}
	},
	displayWaiting( waiting ) {
		const container = document.querySelector( '#wprm-user-rating-modal-waiting' );
		if ( container ) {
			container.style.display = waiting ? 'inline-block' : '';
		}

		const buttons = document.querySelectorAll( '.wprm-user-rating-modal-submit-rating, .wprm-user-rating-modal-submit-comment' );
		for ( let button of buttons ) {
			button.disabled = waiting;
		}
	},
	displayMessage( message ) {
		const form = document.querySelector( '#wprm-user-ratings-modal-stars-form' );
		const messageContainer = document.querySelector( '#wprm-user-ratings-modal-message' );

		if ( false === message ) {
			form.style.display = 'block';
			messageContainer.style.display = 'none';
		} else {
			form.style.display = 'none';
			messageContainer.innerHTML = message;
			messageContainer.style.display = 'block';
		}
	},
};

ready(() => {
	window.WPRecipeMaker.userRatingModal.init();
});

function ready( fn ) {
    if (document.readyState != 'loading'){
        fn();
    } else {
        document.addEventListener('DOMContentLoaded', fn);
    }
}