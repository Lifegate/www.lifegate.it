window.WPRecipeMaker = typeof window.WPRecipeMaker === "undefined" ? {} : window.WPRecipeMaker;

window.WPRecipeMaker.managerPremium = {
	loadRecipeObject: ( id, recipe ) => {
		// Add premium functionality to recipe object.
		return {
			...recipe,
            data: {
                ...recipe.data,
                currentServings: recipe.data.servings,
            },
            addRating: ( data ) => {
                // Maybe track analytics. Only for GA tracking, local tracking is through PHP.
                if ( window.WPRecipeMaker.hasOwnProperty( 'analytics' ) ) {
                    window.WPRecipeMaker.analytics.registerAction( id, wprm_public.post_id, 'user-rating', { rating: data.rating } );
                }

                // Save individual rating and get new totals after save.
                return window.WPRecipeMaker.userRating.addRatingForRecipe( data, id ).then( ( newRating ) => {
                    if ( false !== newRating ) {
                        window.WPRecipeMaker.manager.changeRecipeData( id, {
                            rating: newRating,
                        } );
        
                        window.WPRecipeMaker.manager.triggerChangeEvent( id, 'rating' );
                        return true;
                    }

                    return false;
                });
            },
			setServings: ( servings ) => {
                window.WPRecipeMaker.manager.changeRecipeData( id, {
                    currentServings: servings,
                } );

                window.WPRecipeMaker.manager.triggerChangeEvent( id, 'servings' );
			},
		};
	},
};