<?php
/*
Plugin Name: Set Posts to Draft
Description: Set posts to draft status from a list of URLs in a CSV file.
Version: 1.0
Author: Bititup srl
*/

// Hook per aggiungere una pagina di amministrazione al menu
add_action('admin_menu', 'spd_add_admin_page');

// Funzione per creare una nuova pagina nel menu di amministrazion
function spd_add_admin_page() {
	add_menu_page('Set Posts to Draft', 'Set Posts to Draft', 'manage_options', 'spd-admin-page', 'spd_create_admin_page', 'dashicons-admin-generic', 110);
}

// Funzione che visualizza il contenuto della pagina di amministrazione
function spd_create_admin_page() {
	?>
	<div class="wrap">
		<h1>Set Posts to Draft</h1>
		<form method="post" enctype="multipart/form-data">
			<input type="file" name="csv_file" id="csv_file" accept=".csv">
			<button type="submit" name="submit">Upload CSV</button>
		</form>
	</div>
	<?php
	spd_handle_post();
}

// Funzione per gestire il caricamento del file CSV e l'aggiornamento degli articoli
function spd_handle_post() {
	if (isset($_POST['submit'])) {
		if (is_uploaded_file($_FILES['csv_file']['tmp_name'])) {
			$csv_file = fopen($_FILES['csv_file']['tmp_name'], 'r');
			while (($url = fgetcsv($csv_file, 1000, ",")) !== FALSE) {
				spd_set_post_to_draft($url[0]);
			}
			fclose($csv_file);
			echo '<div class="updated"><p>All matched posts set to draft.</p></div>';
		}
	}
}

// Funzione per impostare un articolo in bozza dato un URL
function spd_set_post_to_draft($url) {
	$post_id = url_to_postid($url);
	if ($post_id) {
		// controllo se lo stato dell'articolo è già in bozza
		$post_status = get_post_status($post_id);
		if ($post_status == 'draft') {
			echo '<p>Post with URL ' . $url . ' is already in draft status.</p>';
			return;
		}

		echo '<p>Setting post with URL ' . $url . ' to draft...</p>';
		wp_update_post(array(
			'ID'            => $post_id,
			'post_status'   => 'draft'
		));
	}else{
		echo '<p>Post with URL ' . $url . ' not found.</p>';
	}
}
