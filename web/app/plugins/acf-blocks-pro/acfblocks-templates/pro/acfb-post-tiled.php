<?php 
echo parse_link(
    array(
        get_field('acfb_post_tiled_title_typo'),
        get_field('acfb_post_tiled_meta_typo'),
        get_field('acfb_post_tiled_excerpt_typo'),
        get_field('acfb_post_tiled_button_typo')
    )
);

$acfb_post_tiled_inner_padding = acfb_padding_name('acfb_post_tiled_inner_padding');
$acfb_post_tiled_padding = acfb_padding_name('acfb_post_tiled_padding');
$acfb_post_tiled_margin = acfb_margin_name('acfb_post_tiled_margin');
$acfb_post_tiled_title_typo = acfb_ffaimly_name('acfb_post_tiled_title_typo');
$acfb_post_tiled_meta_typo = acfb_ffaimly_name('acfb_post_tiled_meta_typo');
$acfb_post_tiled_excerpt_typo = acfb_ffaimly_name('acfb_post_tiled_excerpt_typo');
$acfb_post_tiled_button_typo = acfb_ffaimly_name('acfb_post_tiled_button_typo');


$uid = $block['id'];

$className = 'acfb_posts_tiled_block';
if( !empty($block['className']) ) {
   $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
   $className .= ' align' . $block['align'];
}

?>

<div class="<?php echo $uid; ?> <?php echo esc_attr($className); ?>">

<?php 
$post_tiled_overlay = hex2rgba(get_field('acfb_post_tiled_overlay_color'), get_field('acfb_post_tiled_overlay_opacity'));
$acfb_post_tiled_layout = get_field('acfb_post_tiled_layout');
$acfb_post_tiled_grid_number_of_posts = get_field('acfb_post_tiled_grid_number_of_posts');
$acfb_posts_tiled_columns = get_field('acfb_posts_tiled_columns');
$acfb_post_tiled_excerpt_length = get_field('acfb_post_tiled_excerpt_length');
$acfb_post_tiled_title_html_tag = get_field('acfb_post_tiled_title_html_tag');
$acfb_post_tiled_collage_grid = get_field('acfb_post_tiled_collage_grid');


?>
<style type="text/css">
.<?php echo $uid; ?> {
	<?php echo get_padding_field($acfb_post_tiled_padding); ?>
	<?php echo get_margin_field($acfb_post_tiled_margin); ?>
}

.<?php echo $uid; ?> .acfb_post_tiled_grid .acfb_post_tiled_item .acfb_post_tiled_item_inner, 
.<?php echo $uid; ?> .acfb_post_tiled_collage{
	min-height: <?php the_field('acfb_post_tiled_height'); ?>px;
	padding-top: <?php echo $acfb_post_tiled_inner_padding['padding_top']; ?>px;
    padding-bottom: <?php echo $acfb_post_tiled_inner_padding['padding_bottom']; ?>px;
    padding-left: <?php echo $acfb_post_tiled_inner_padding['padding_left']; ?>px;
    padding-right: <?php echo $acfb_post_tiled_inner_padding['padding_right']; ?>px;
}

.<?php echo $uid; ?> .acfb_post_tiled_item .acfb_posts_tiled_top_elements{
	display: flex;
	align-items: <?php the_field('acfb_post_tiled_top_elements_h_align'); ?>;
	justify-content: <?php the_field('acfb_post_tiled_top_elements_v_align'); ?>;
	flex-direction: column;
	<?php if(get_field('acfb_post_tiled_top_elements_h_align') == 'center'){ ?>
		text-align: center;
	<?php } ?>
}

.<?php echo $uid; ?> .acfb_post_tiled_item .acfb_posts_tiled_center_elements{
	display: flex;
	align-items: <?php the_field('acfb_post_tiled_center_elements_h_align'); ?>;
	justify-content: <?php the_field('acfb_post_tiled_center_elements_v_align'); ?>;
	flex-direction: column;
	<?php if(get_field('acfb_post_tiled_center_elements_h_align') == 'center'){ ?>
		text-align: center;
	<?php } ?>
}

.<?php echo $uid; ?> .acfb_post_tiled_item .acfb_posts_tiled_bottom_elements{
	display: flex;
	align-items: <?php the_field('acfb_post_tiled_bottom_elements_h_align'); ?>;
	justify-content: <?php the_field('acfb_post_tiled_bottom_elements_v_align'); ?>;
	flex-direction: column;
	<?php if(get_field('acfb_post_tiled_bottom_elements_h_align') == 'center'){ ?>
		text-align: center;
	<?php } ?>
}


<?php if( get_field('acfb_post_tiled_title_custom_typography') == '1' ): ?>
.<?php echo $uid; ?> .acfb_post_tiled_item .acfb_post_tiled_title a{
	<?php echo get_typo_field($acfb_post_tiled_title_typo); ?>
}
<?php endif; ?>


<?php if( get_field('acfb_post_tiled_title_custom_color') == '1' ): ?>
.<?php echo $uid; ?> .acfb_post_tiled_item .acfb_post_tiled_title a{
	color: <?php the_field('acfb_post_tiled_title_color'); ?> !important;
}

.<?php echo $uid; ?> .acfb_post_tiled_item .acfb_post_tiled_title a:hover{
	color: <?php the_field('acfb_post_tiled_title_hover_color'); ?> !important;
}
<?php endif; ?>



<?php if( get_field('acfb_post_tiled_meta_custom_typography') == '1' ): ?>
.<?php echo $uid; ?> .acfb_post_tiled_item .acfb_post_tiled_meta{
	<?php echo get_typo_field($acfb_post_tiled_meta_typo); ?>
}
<?php endif; ?>

<?php if( get_field('acfb_post_tiled_meta_custom_color') == '1' ): ?>
.<?php echo $uid; ?> .acfb_post_tiled_item .acfb_post_tiled_meta{
	color: <?php the_field('acfb_post_tiled_meta_color'); ?>;
}
<?php endif; ?>




<?php if( get_field('acfb_post_tiled_excerpt_custom_typography') == '1' ): ?>
.<?php echo $uid; ?> .acfb_post_tiled_item .acfb_post_tiled_excerpt{
	<?php echo get_typo_field($acfb_post_tiled_excerpt_typo); ?>
}
<?php endif; ?>

<?php if( get_field('acfb_post_tiled_excerpt_custom_color') == '1' ): ?>
.<?php echo $uid; ?> .acfb_post_tiled_item .acfb_post_tiled_excerpt{
	color: <?php the_field('acfb_post_tiled_excerpt_color'); ?>;
}
<?php endif; ?>



<?php if( get_field('acfb_post_tiled_button_custom_typography') == '1' ): ?>
.<?php echo $uid; ?> .acfb_post_tiled_item .acfb_post_tiled_button a{
	<?php echo get_typo_field($acfb_post_tiled_button_typo); ?>
}
<?php endif; ?>

<?php if( get_field('acfb_post_tiled_button_custom_color') == '1' ): ?>
.<?php echo $uid; ?> .acfb_post_tiled_item .acfb_post_tiled_button a{
	background-color: <?php the_field('acfb_post_tiled_button_background_color'); ?>;
	color: <?php the_field('acfb_post_tiled_button_text_color'); ?> !important;
	padding: 10px;
}

.<?php echo $uid; ?> .acfb_post_tiled_item .acfb_post_tiled_button a:hover{
	background-color: <?php the_field('acfb_post_tiled_button_background_hover_color'); ?>;
	color: <?php the_field('acfb_post_tiled_button_text_hover_color'); ?> !important;
}
<?php endif; ?>
</style>


<?php

$acfb_tiled_layout_col = '';
if($acfb_post_tiled_layout == 'grid'){
	$acfb_tiled_layout_col = "acfb_post_tiled_" . $acfb_posts_tiled_columns; 
}


$acfb_post_tiled_number_of_posts = '';
if($acfb_post_tiled_layout === 'grid'){
	$acfb_post_tiled_number_of_posts = $acfb_post_tiled_grid_number_of_posts;
} else if($acfb_post_tiled_layout === 'collage'){
	$acfb_post_tiled_number_of_posts = $acfb_post_tiled_collage_grid;
}


$acfb_collage_gl = '';
if($acfb_post_tiled_layout === 'collage' && $acfb_post_tiled_collage_grid === '3'){
	$acfb_collage_gl = 'acfb_post_collage_three_grid_style_' . get_field('acfb_post_tiled_collage_three_grid_layouts');
} elseif($acfb_post_tiled_layout === 'collage' && $acfb_post_tiled_collage_grid === '4'){
	$acfb_collage_gl = 'acfb_post_collage_four_grid_style_' . get_field('acfb_post_tiled_collage_four_grid_layouts');
} elseif($acfb_post_tiled_layout === 'collage' && $acfb_post_tiled_collage_grid === '5'){
	$acfb_collage_gl = 'acfb_post_collage_five_grid_style_' . get_field('acfb_post_tiled_collage_five_grid_layouts');
}


$acfb_post_tiled_offset = get_field('acfb_post_tiled_offset');


$tiled_post_args = array(
	'post_type' => get_field('acfb_post_type'),
	'post_status' => 'publish',
	'posts_per_page' => $acfb_post_tiled_number_of_posts, 
	'offset' => $acfb_post_tiled_offset,
);


// the query
$tiled_post_query = new WP_Query( $tiled_post_args ); ?>

<?php if ( $tiled_post_query->have_posts() ) : ?>

<?php $i = 1; ?>	

<div class="acfb_post_tiled_<?php echo $acfb_post_tiled_layout; ?> <?php echo $acfb_tiled_layout_col; ?> <?php echo $acfb_collage_gl; ?>">

	<?php while ( $tiled_post_query->have_posts() ) : $tiled_post_query->the_post(); ?>

		<div class="acfb_post_tiled_item <?php if($acfb_post_tiled_layout == 'collage'){ echo 'acfb_post_tiled_item_' . $i++; } ?>">

			<?php
			if($acfb_post_tiled_layout == 'grid'){ ?>

				<!-- Grid -->
				<div class="acfb_post_tiled_item_inner" style="background: linear-gradient(<?php echo $post_tiled_overlay; ?>, <?php echo $post_tiled_overlay; ?>), url(<?php the_post_thumbnail_url(); ?>);">

				<!-- Top Elements -->
				<?php if( have_rows('acfb_post_tiled_top_elements') ):?>
				<div class="acfb_posts_tiled_top_elements">
				<?php while ( have_rows('acfb_post_tiled_top_elements') ) : the_row(); ?>

				    <?php if( get_row_layout() == 'post_tiled_title' ): ?>
				    	<div class="acfb_post_tiled_title">	
				       		 <<?php echo $acfb_post_tiled_title_html_tag; ?>><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></<?php echo $acfb_post_tiled_title_html_tag; ?>>
				   		</div>
				    <?php endif; ?>

				    <?php if( get_row_layout() == 'post_tiled_meta_data' ): ?>
				    	<div class="acfb_post_tiled_meta">
				    		<span class="acfb_post_tiled_author"><?php the_author(); ?></span> -
							<span class="acfb_post_tiled_date"><?php the_time('F jS, Y') ?></span>
						</div>
				    <?php endif; ?>
				      
				    <?php if( get_row_layout() == 'post_tiled_content' ): ?>
				    	<div class="acfb_post_tiled_excerpt">
				        	<?php echo acfb_excerpt($acfb_post_tiled_excerpt_length); ?>
				    	</div>
				    <?php endif; ?>

					<?php if( get_row_layout() == 'post_tiled_read_more_button' ): ?>
						<div class="acfb_post_tiled_button">
							<a href="<?php the_permalink(); ?>" class="acfb_post_tiled_btn">Read More</a>
						</div>
				    <?php endif; ?>

				<?php endwhile; ?>
				</div>
				<?php endif; ?>


				<!-- Center Elements -->
				<?php if( have_rows('acfb_post_tiled_center_elements') ):?>
				<div class="acfb_posts_tiled_center_elements">
				<?php while ( have_rows('acfb_post_tiled_center_elements') ) : the_row(); ?>

				    <?php if( get_row_layout() == 'post_tiled_title' ): ?>
				    	<div class="acfb_post_tiled_title">	
				       		 <<?php echo $acfb_post_tiled_title_html_tag; ?>><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></<?php echo $acfb_post_tiled_title_html_tag; ?>>
				   		</div>
				    <?php endif; ?>

				    <?php if( get_row_layout() == 'post_tiled_meta_data' ): ?>
				    	<div class="acfb_post_tiled_meta">
				    		<span class="acfb_post_tiled_author"><?php the_author(); ?></span> -
							<span class="acfb_post_tiled_date"><?php the_time('F jS, Y') ?></span>
						</div>
				    <?php endif; ?>
				      
				    <?php if( get_row_layout() == 'post_tiled_content' ): ?>
				    	<div class="acfb_post_tiled_excerpt">
				        	<?php echo acfb_excerpt($acfb_post_tiled_excerpt_length); ?>
				    	</div>
				    <?php endif; ?>

					<?php if( get_row_layout() == 'post_tiled_read_more_button' ): ?>
						<div class="acfb_post_tiled_button">
							<a href="<?php the_permalink(); ?>" class="acfb_post_tiled_btn">Read More</a>
						</div>
				    <?php endif; ?>

				<?php endwhile; ?>
				</div>
				<?php endif; ?>


				<!-- Bottom Elements -->
				<?php if( have_rows('acfb_post_tiled_bottom_elements') ):?>
				<div class="acfb_posts_tiled_bottom_elements">
				<?php while ( have_rows('acfb_post_tiled_bottom_elements') ) : the_row(); ?>

				    <?php if( get_row_layout() == 'post_tiled_title' ): ?>
				    	<div class="acfb_post_tiled_title">	
				       		 <<?php echo $acfb_post_tiled_title_html_tag; ?>><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></<?php echo $acfb_post_tiled_title_html_tag; ?>>
				   		</div>
				    <?php endif; ?>

				    <?php if( get_row_layout() == 'post_tiled_meta_data' ): ?>
				    	<div class="acfb_post_tiled_meta">
				    		<span class="acfb_post_tiled_author"><?php the_author(); ?></span> -
							<span class="acfb_post_tiled_date"><?php the_time('F jS, Y') ?></span>
						</div>
				    <?php endif; ?>
				      
				    <?php if( get_row_layout() == 'post_tiled_content' ): ?>
				    	<div class="acfb_post_tiled_excerpt">
				        	<?php echo acfb_excerpt($acfb_post_tiled_excerpt_length); ?>
				    	</div>
				    <?php endif; ?>

					<?php if( get_row_layout() == 'post_tiled_read_more_button' ): ?>
						<div class="acfb_post_tiled_button">
							<a href="<?php the_permalink(); ?>" class="acfb_post_tiled_btn">Read More</a>
						</div>
				    <?php endif; ?>

				<?php endwhile; ?>
				</div>
				<?php endif; ?>

				</div>

			<?php } elseif ($acfb_post_tiled_layout == 'collage') { ?>

				<!-- Collage -->
				<div class="acfb_post_tiled_item_inner" style="background: linear-gradient(<?php echo $post_tiled_overlay; ?>, <?php echo $post_tiled_overlay; ?>), url(<?php the_post_thumbnail_url(); ?>);">

				<!-- Top Elements -->
				<?php if( have_rows('acfb_post_tiled_top_elements') ):?>
				<div class="acfb_posts_tiled_top_elements">
				<?php while ( have_rows('acfb_post_tiled_top_elements') ) : the_row(); ?>

				    <?php if( get_row_layout() == 'post_tiled_title' ): ?>
				    	<div class="acfb_post_tiled_title">	
				       		 <<?php echo $acfb_post_tiled_title_html_tag; ?>><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></<?php echo $acfb_post_tiled_title_html_tag; ?>>
				   		</div>
				    <?php endif; ?>

				    <?php if( get_row_layout() == 'post_tiled_meta_data' ): ?>
				    	<div class="acfb_post_tiled_meta">
				    		<span class="acfb_post_tiled_author"><?php the_author(); ?></span> -
							<span class="acfb_post_tiled_date"><?php the_time('F jS, Y') ?></span>
						</div>
				    <?php endif; ?>
				      
				    <?php if( get_row_layout() == 'post_tiled_content' ): ?>
				    	<div class="acfb_post_tiled_excerpt">
				        	<?php echo acfb_excerpt($acfb_post_tiled_excerpt_length); ?>
				    	</div>
				    <?php endif; ?>

					<?php if( get_row_layout() == 'post_tiled_read_more_button' ): ?>
						<div class="acfb_post_tiled_button">
							<a href="<?php the_permalink(); ?>" class="acfb_post_tiled_btn">Read More</a>
						</div>
				    <?php endif; ?>

				<?php endwhile; ?>
				</div>
				<?php endif; ?>



				<!-- Center Elements -->
				<?php if( have_rows('acfb_post_tiled_center_elements') ):?>
				<div class="acfb_posts_tiled_center_elements">
				<?php while ( have_rows('acfb_post_tiled_center_elements') ) : the_row(); ?>

				    <?php if( get_row_layout() == 'post_tiled_title' ): ?>
				    	<div class="acfb_post_tiled_title">	
				       		 <<?php echo $acfb_post_tiled_title_html_tag; ?>><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></<?php echo $acfb_post_tiled_title_html_tag; ?>>
				   		</div>
				    <?php endif; ?>

				    <?php if( get_row_layout() == 'post_tiled_meta_data' ): ?>
				    	<div class="acfb_post_tiled_meta">
				    		<span class="acfb_post_tiled_author"><?php the_author(); ?></span> -
							<span class="acfb_post_tiled_date"><?php the_time('F jS, Y') ?></span>
						</div>
				    <?php endif; ?>
				      
				    <?php if( get_row_layout() == 'post_tiled_content' ): ?>
				    	<div class="acfb_post_tiled_excerpt">
				        	<?php echo acfb_excerpt($acfb_post_tiled_excerpt_length); ?>
				    	</div>
				    <?php endif; ?>

					<?php if( get_row_layout() == 'post_tiled_read_more_button' ): ?>
						<div class="acfb_post_tiled_button">
							<a href="<?php the_permalink(); ?>" class="acfb_post_tiled_btn">Read More</a>
						</div>
				    <?php endif; ?>

				<?php endwhile; ?>
				</div>
				<?php endif; ?>


				<!-- Bottom Elements -->
				<?php if( have_rows('acfb_post_tiled_bottom_elements') ):?>
				<div class="acfb_posts_tiled_bottom_elements">
				<?php while ( have_rows('acfb_post_tiled_bottom_elements') ) : the_row(); ?>

				    <?php if( get_row_layout() == 'post_tiled_title' ): ?>
				    	<div class="acfb_post_tiled_title">	
				       		 <<?php echo $acfb_post_tiled_title_html_tag; ?>><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></<?php echo $acfb_post_tiled_title_html_tag; ?>>
				   		</div>
				    <?php endif; ?>

				    <?php if( get_row_layout() == 'post_tiled_meta_data' ): ?>
				    	<div class="acfb_post_tiled_meta">
				    		<span class="acfb_post_tiled_author"><?php the_author(); ?></span> -
							<span class="acfb_post_tiled_date"><?php the_time('F jS, Y') ?></span>
						</div>
				    <?php endif; ?>
				      
				    <?php if( get_row_layout() == 'post_tiled_content' ): ?>
				    	<div class="acfb_post_tiled_excerpt">
				        	<?php echo acfb_excerpt($acfb_post_tiled_excerpt_length); ?>
				    	</div>
				    <?php endif; ?>

					<?php if( get_row_layout() == 'post_tiled_read_more_button' ): ?>
						<div class="acfb_post_tiled_button">
							<a href="<?php the_permalink(); ?>" class="acfb_post_tiled_btn">Read More</a>
						</div>
				    <?php endif; ?>

				<?php endwhile; ?>
				</div>
				<?php endif; ?>

				</div>

			<?php } ?>

		</div>
	<?php endwhile; ?>

</div>

<?php wp_reset_postdata(); ?>

<?php else : ?>
	<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>

</div><!-- Uid -->