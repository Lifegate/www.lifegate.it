<?php 
echo parse_link(
    array(
        get_field('post_ticker_label_typo'),
        get_field('post_ticker_content_typo')
    )
);

$post_ticker_padding = acfb_padding_name('post_ticker_padding');
$post_ticker_margin = acfb_margin_name('post_ticker_margin');
$post_ticker_label_typo = acfb_ffaimly_name('post_ticker_label_typo');
$post_ticker_content_typo = acfb_ffaimly_name('post_ticker_content_typo');


$uid = $block['id'];

$className = 'acfb_post_ticker_block';
if( !empty($block['className']) ) {
   $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
   $className .= ' align' . $block['align'];
}

?>
<div class="<?php echo $uid; ?> <?php echo esc_attr($className); ?>">
<style type="text/css">
.<?php echo $uid; ?> {
	<?php echo get_padding_field($post_ticker_padding); ?>
	<?php echo get_margin_field($post_ticker_margin); ?>
}

.<?php echo $uid; ?> .acfb_post_ticker{
	background-color: <?php the_field('post_ticker_content_background_color'); ?>;
}

.<?php echo $uid; ?> .acfb_post_ticker .acfb_post_ticker_wrap .acfb_ptt{
	color: <?php the_field('post_ticker_content_color'); ?> !important;
	<?php echo get_typo_field($post_ticker_content_typo); ?>
}

.<?php echo $uid; ?> .acfb_post_ticker .acfb_post_ticker_heading{
	background-color: <?php the_field('post_ticker_label_background_color'); ?>;
	color: <?php the_field('post_ticker_label_color'); ?>;
	<?php echo get_typo_field($post_ticker_label_typo); ?>
}
.<?php echo $uid; ?> .acfb_post_ticker .acfb_post_ticker_heading:after{
	border-left-color: <?php the_field('post_ticker_label_background_color'); ?>;
}

.<?php echo $uid; ?> .acfb_post_ticker .swiper-container .acfb-ticker-button-next path, .<?php echo $uid; ?> .acfb_post_ticker .swiper-container .acfb-ticker-button-prev path{
  fill: <?php the_field('post_ticker_arrow_color'); ?>;
}
</style>

<div class="acfb_post_ticker">
	<div class="acfb_post_ticker_heading">
		<!-- <span class="acfb_post_ticker_heading_icon"><span class="dashicons dashicons-controls-forward"></span></span> -->
		<span class="acfb_post_ticker_heading_text"><?php the_field('post_ticker_label'); ?></span>
	</div>

	<div class="acfb_post_ticker_wrap">
		<div class="swiper-container" data-tickerdelay="<?php the_field("acfb_post_ticker_slide_delay"); ?>">
		    <div class="swiper-wrapper">

		    	<?php
				$post_ticker_args = array(
					'post_type' => get_field('acfb_post_type'),
					'post_status' => 'publish',
					'posts_per_page' => get_field('post_ticker_nop'),
				);

				// the query
				$the_query = new WP_Query( $post_ticker_args ); ?>

				<?php if ( $the_query->have_posts() ) : ?>

					<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						<div class="swiper-slide">
							<a href="<?php the_permalink(); ?>" class="acfb_ptt"><?php the_title(); ?></a>
						</div>
					<?php endwhile; ?>

				<?php wp_reset_postdata(); ?>

				<?php else : ?>
					<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
				<?php endif; ?>

		    </div>
		    
<!-- 		    <div class="acfb_post_ticker_navigation">
				<div class="acfb-ticker-button-prev">
					<svg enable-background="new 0 0 477.175 477.175" version="1.1" viewBox="0 0 477.18 477.18" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
				<path d="m145.19 238.58 215.5-215.5c5.3-5.3 5.3-13.8 0-19.1s-13.8-5.3-19.1 0l-225.1 225.1c-5.3 5.3-5.3 13.8 0 19.1l225.1 225c2.6 2.6 6.1 4 9.5 4s6.9-1.3 9.5-4c5.3-5.3 5.3-13.8 0-19.1l-215.4-215.5z"/>
				</svg>
				</div>
				<div class="acfb-ticker-button-next">
					<svg enable-background="new 0 0 477.175 477.175" version="1.1" viewBox="0 0 477.18 477.18" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
					<path d="m360.73 229.08-225.1-225.1c-5.3-5.3-13.8-5.3-19.1 0s-5.3 13.8 0 19.1l215.5 215.5-215.5 215.5c-5.3 5.3-5.3 13.8 0 19.1 2.6 2.6 6.1 4 9.5 4s6.9-1.3 9.5-4l225.1-225.1c5.3-5.2 5.3-13.8 0.1-19z"/>
				</svg>
				</div>
			</div> -->

		</div>
	</div>


</div>

</div><!-- Uid -->