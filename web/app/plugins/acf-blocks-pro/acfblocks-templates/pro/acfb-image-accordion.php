<?php 
echo parse_link(
    array(
        get_field('acfb_image_accordion_title_typo'),
        get_field('acfb_image_accordion_content_typo'),
        get_field('acfb_image_accordion_button_typo')
    )
);

$acfb_image_accordion_inner_padding = acfb_padding_name('acfb_image_accordion_inner_padding');
$acfb_image_accordion_padding = acfb_padding_name('acfb_image_accordion_padding');
$acfb_image_accordion_margin = acfb_margin_name('acfb_image_accordion_margin');
$acfb_image_accordion_title_typo = acfb_ffaimly_name('acfb_image_accordion_title_typo');
$acfb_image_accordion_content_typo = acfb_ffaimly_name('acfb_image_accordion_content_typo');
$acfb_image_accordion_button_typo = acfb_ffaimly_name('acfb_image_accordion_button_typo');


$uid = $block['id'];

$className = 'acfb_image_accordion_block';
if( !empty($block['className']) ) {
   $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
   $className .= ' align' . $block['align'];
}

?>
<div class="<?php echo $uid; ?> <?php echo esc_attr($className); ?>">
<style type="text/css">
.<?php echo $uid; ?> {
   <?php echo get_padding_field($acfb_image_accordion_padding); ?>
   <?php echo get_margin_field($acfb_image_accordion_margin); ?>
} 

.<?php echo $uid; ?> .acfb_image_accordion_active .acfb_image_accordion_overlay{
  background-color: <?php echo hex2rgba(get_field('acfb_image_accordion_overlay_color'), get_field('acfb_image_accordion_overlay_active_opacity')); ?>!important;
}

.<?php echo $uid; ?> .acfb_image_accordion .acfb_image_accordion_overlay{
  background-color: <?php echo hex2rgba(get_field('acfb_image_accordion_overlay_color'), get_field('acfb_image_accordion_overlay_inactive_opacity')); ?>;
    padding-top: <?php echo $acfb_image_accordion_inner_padding['padding_top']; ?>px;
    padding-bottom: <?php echo $acfb_image_accordion_inner_padding['padding_bottom']; ?>px;
    padding-left: <?php echo $acfb_image_accordion_inner_padding['padding_left']; ?>px;
    padding-right: <?php echo $acfb_image_accordion_inner_padding['padding_right']; ?>px;

}

.<?php echo $uid; ?> .acfb_image_accordion{
  flex-direction: <?php the_field('acfb_image_accordion_styles'); ?>;
  height: <?php the_field('acfb_image_accordion_height'); ?>px;
}

.<?php echo $uid; ?> .acfb_image_accordion .acfb_image_accordion_item .acfb_image_accordion_title{
 <?php echo get_typo_field($acfb_image_accordion_title_typo); ?>
  color: <?php the_field('acfb_image_accordion_title_color'); ?>;
}

.<?php echo $uid; ?> .acfb_image_accordion .acfb_image_accordion_item .acfb_image_accordion_description{
  <?php echo get_typo_field($acfb_image_accordion_content_typo); ?>
  color: <?php the_field('acfb_image_accordion_content_color'); ?>;
}

.<?php echo $uid; ?> .acfb_image_accordion .acfb_image_accordion_item .acfb_image_accordion_button{
  background-color: <?php the_field('acfb_image_accordion_button_background_color'); ?>;
  <?php echo get_typo_field($acfb_image_accordion_button_typo); ?>
  color: <?php the_field('acfb_image_accordion_button_text_color'); ?> !important;
}

.<?php echo $uid; ?> .acfb_image_accordion .acfb_image_accordion_item .acfb_image_accordion_button:hover{
  background-color: <?php the_field('acfb_image_accordion_button_background_hover_color'); ?>;
  color: <?php the_field('acfb_image_accordion_button_text_hover_color'); ?> !important;
}

<?php if(get_field('acfb_image_accordion_styles') == 'row'): ?>
.<?php echo $uid; ?> .acfb_image_accordion .acfb_image_accordion_item{margin-right: <?php the_field('acfb_image_accordion_gutter'); ?>px;}
.<?php echo $uid; ?> .acfb_image_accordion .acfb_image_accordion_item:last-child{margin-right: 0px;}
<?php else: ?>
.<?php echo $uid; ?> .acfb_image_accordion .acfb_image_accordion_item{margin-bottom: <?php the_field('acfb_image_accordion_gutter'); ?>px;}
.<?php echo $uid; ?> .acfb_image_accordion .acfb_image_accordion_item:last-child{margin-bottom: 0px;}
<?php endif; ?>  
</style>

<?php if( have_rows('acfb_image_accordion') ): ?>

  <div class="acfb_image_accordion">

  <?php while( have_rows('acfb_image_accordion') ): the_row(); ?>

  <style type="text/css">
  .<?php echo $uid; ?> .acfb_image_accordion .acfb_ia_order_<?php echo get_row_index(); ?> .acfb_image_accordion_overlay{
      align-items: <?php the_sub_field('acfb_image_accordion_cva') ?>;
      justify-content: <?php the_sub_field('acfb_image_accordion_cha') ?>;
      <?php if(get_sub_field('acfb_image_accordion_cha') === 'center'): ?>
        text-align: center;
      <?php elseif(get_sub_field('acfb_image_accordion_cha') === 'flex-end'): ?>
        text-align: right;
      <?php endif; ?>
  }

  .<?php echo $uid; ?> .acfb_image_accordion .acfb_ia_order_<?php echo get_row_index(); ?> .acfb_image_accordion_overlay .acfb_image_accordion_wrap{
      width: <?php the_sub_field('acfb_image_accordion_cw') ?>%;
  }
  </style>

  <?php
    // vars
    $acfb_image = '';
    if(!get_sub_field('acfb_image_accordion_image')){
      $acfb_image = plugins_url() . '/acf-blocks/img/placeholder-image.jpg';
    } else{
      $acfb_image = get_sub_field('acfb_image_accordion_image');
    }

    $acfb_image_accordion_title = get_sub_field('acfb_image_accordion_title');
    $acfb_image_accordion_content = get_sub_field('acfb_image_accordion_content');
    $acfb_image_accordion_button_text = get_sub_field('acfb_image_accordion_button_text');
    $acfb_image_accordion_button_url = get_sub_field('acfb_image_accordion_button_url');
    ?>

    <div class="acfb_image_accordion_item acfb_ia_order_<?php echo get_row_index(); ?>" style="background-image: url('<?php echo $acfb_image ?>'); flex: 1 1 0%;">
      <div class="acfb_image_accordion_overlay">
        <div class="acfb_image_accordion_wrap">
          <div class="acfb_image_accordion_content">

          <?php if( $acfb_image_accordion_title ): ?>
              <h2 class="acfb_image_accordion_title"><?php echo $acfb_image_accordion_title; ?></h2>   
          <?php endif; ?>

          <?php if( $acfb_image_accordion_content ): ?>
              <div class="acfb_image_accordion_description"><?php echo $acfb_image_accordion_content; ?></div>   
          <?php endif; ?>

          <?php if( $acfb_image_accordion_button_text && $acfb_image_accordion_button_url ): ?>
              <a href="<?php echo $acfb_image_accordion_button_url; ?>" class="acfb_image_accordion_button"><?php echo $acfb_image_accordion_button_text; ?></a>  
          <?php endif; ?>

        </div>
       </div>
      </div>
    </div>

  <?php endwhile; ?>

  </div>

<?php endif; ?>
</div><!-- Uid -->