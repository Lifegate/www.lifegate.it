<?php 
echo parse_link(
    array(
        get_field('acfb_ah_typo')
    )
);

$acfb_ah_padding = acfb_padding_name('acfb_ah_padding');
$acfb_ah_margin = acfb_margin_name('acfb_ah_margin');
$acfb_ah_typo = acfb_ffaimly_name('acfb_ah_typo');

$uid = $block['id'];

$className = 'acfb_animated_headline_block';
if( !empty($block['className']) ) {
   $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
   $className .= ' align' . $block['align'];
}

?>
<div class="<?php echo $uid; ?> <?php echo esc_attr($className); ?>">
<style type="text/css">
.<?php echo $uid; ?> {
  <?php echo get_padding_field($acfb_ah_padding); ?>
  <?php echo get_margin_field($acfb_ah_margin); ?>
}

.<?php echo $uid; ?> .acfb_ah_wrapper .cd-headline:not(.type) .anim-spacebar{
  	margin: 0px 5px;
}

.<?php echo $uid; ?> .acfb_ah_wrapper .cd-headline{
	color: <?php the_field('acfb_ah_headline_color'); ?>;
	<?php echo get_typo_field($acfb_ah_typo); ?>
	text-align: <?php the_field('acfb_ah_alignment'); ?>;
}

.<?php echo $uid; ?> .acfb_ah_wrapper .cd-headline .cd-words-wrapper{
	color: <?php the_field('acfb_ah_rotating_color'); ?>;
}

.<?php echo $uid; ?> .acfb_ah_wrapper .cd-headline.loading-bar .cd-words-wrapper::after{
	background: <?php the_field('acfb_ah_loading_bar_color'); ?>;
}


</style>

<div class="acfb_ah_wrapper">

<section class="cd-intro">
	<h1 class="cd-headline <?php the_field('acfb_ah_animations'); ?>">
		<span><?php the_field('acfb_ah_before_text'); ?></span>
		<span class="cd-words-wrapper">
			<?php if( have_rows('acfb_ah_rotating_text') ): ?>
				<?php while( have_rows('acfb_ah_rotating_text') ): the_row(); 
					$acfb_ah_rotating_item = get_sub_field('acfb_ah_rotating_item');
					?>

					<b>
						<?php if( $acfb_ah_rotating_item ): ?>
							<?php echo $acfb_ah_rotating_item; ?>
						<?php endif; ?>
					</b>

				<?php endwhile; ?>
			<?php endif; ?>
		</span>
		<span><?php the_field('acfb_ah_after_text'); ?></span>
	</h1>
</section>

</div>


</div><!-- Uid -->