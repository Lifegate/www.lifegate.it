<?php 
echo parse_link(
    array(
        get_field('acfb_flipbox_front_title_typo'),
        get_field('acfb_flipbox_front_content_typo'),
        get_field('acfb_flipbox_back_title_typo'),
        get_field('acfb_flipbox_back_content_typo'),
        get_field('acfb_flipbox_back_button_typo')
    )
);

$acfb_flipbox_inner_padding = acfb_padding_name('acfb_flipbox_inner_padding');
$acfb_flipbox_padding = acfb_padding_name('acfb_flipbox_padding');
$acfb_flipbox_margin = acfb_margin_name('acfb_flipbox_margin');
$acfb_flipbox_front_title_typo = acfb_ffaimly_name('acfb_flipbox_front_title_typo');
$acfb_flipbox_front_content_typo = acfb_ffaimly_name('acfb_flipbox_front_content_typo');
$acfb_flipbox_back_title_typo = acfb_ffaimly_name('acfb_flipbox_back_title_typo');
$acfb_flipbox_back_content_typo = acfb_ffaimly_name('acfb_flipbox_back_content_typo');
$acfb_flipbox_back_button_typo = acfb_ffaimly_name('acfb_flipbox_back_button_typo');

$uid = $block['id'];

$className = 'acfb_flip_box_block';
if( !empty($block['className']) ) {
   $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
   $className .= ' align' . $block['align'];
}

$ffoverlay = '';
if(get_field('acfb_flipbox_front_background_image_overlay_yesno') == 'true'){
  $ffoverlay = 'linear-gradient(' . hex2rgba(get_field("acfb_flipbox_front_background_overlay"), get_field('acfb_flipbox_front_background_overlay_opacity')) .', '. hex2rgba(get_field("acfb_flipbox_front_background_overlay"), get_field('acfb_flipbox_front_background_overlay_opacity')) . '),';
}

$fboverlay = '';
if(get_field('acfb_flipbox_back_background_image_overlay_yesno') == 'true'){
  $fboverlay = 'linear-gradient(' . hex2rgba(get_field("acfb_flipbox_back_background_overlay"), get_field('acfb_flipbox_back_background_overlay_opacity')) .', '. hex2rgba(get_field("acfb_flipbox_back_background_overlay"), get_field('acfb_flipbox_back_background_overlay_opacity')) . '),';
}
?>

<div class="<?php echo $uid; ?> <?php echo esc_attr($className); ?>">

<style type="text/css">
.<?php echo $uid; ?> {
  <?php echo get_padding_field($acfb_flipbox_padding); ?>
  <?php echo get_margin_field($acfb_flipbox_margin); ?>
}

.<?php echo $uid; ?> .acfb_box_inner .acfb_flipbox_front,
.<?php echo $uid; ?> .acfb_box_inner .acfb_flipbox_back{
  padding-top: <?php echo $acfb_flipbox_inner_padding['padding_top']; ?>px;
  padding-bottom: <?php echo $acfb_flipbox_inner_padding['padding_bottom']; ?>px;
  padding-left: <?php echo $acfb_flipbox_inner_padding['padding_left']; ?>px;
  padding-right: <?php echo $acfb_flipbox_inner_padding['padding_right']; ?>px;
}

.<?php echo $uid; ?> .acfb_box_inner{
	min-height: <?php the_field('acfb_flipbox_height'); ?>px;
}

.<?php echo $uid; ?> .acfb_box_inner .acfb_flipbox_front{
  <?php if(get_field('acfb_flipbox_front_background_select') === 'acfb_flipbox_bg_color'): ?>
  background-color: <?php the_field('acfb_flipbox_front_background_color'); ?>;	
  <?php else: ?>
  background-image: <?php echo $ffoverlay; ?> url(<?php the_field('acfb_flipbox_front_background_image'); ?>);
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center center;
  <?php endif; ?>
  display: flex;
  align-items: <?php the_field('acfb_flipbox_front_content_horizontal_align'); ?>;
  justify-content: <?php the_field('acfb_flipbox_front_content_vertical_align'); ?>;
  flex-direction: column; 

  <?php if(get_field('acfb_flipbox_front_content_horizontal_align') == 'flex-start'): ?>
  	text-align: left;
  <?php endif; ?>

  <?php if(get_field('acfb_flipbox_front_content_horizontal_align') == 'center'): ?>
  	text-align: center;
  <?php endif; ?>

  <?php if(get_field('acfb_flipbox_front_content_horizontal_align') == 'flex-end'): ?>
  	text-align: right;
  <?php endif; ?>
		
}

.<?php echo $uid; ?> .acfb_box_inner .acfb_flipbox_front img{
	width: <?php the_field('acfb_flipbox_front_image_size'); ?>%;
}

.<?php echo $uid; ?> .acfb_box_inner .acfb_flipbox_front h3{
	<?php echo get_typo_field($acfb_flipbox_front_title_typo); ?>
	color: <?php the_field('acfb_flipbox_front_title_color'); ?>;
}

.<?php echo $uid; ?> .acfb_box_inner .acfb_flipbox_front p{
	<?php echo get_typo_field($acfb_flipbox_front_content_typo); ?>
	color: <?php the_field('acfb_flipbox_front_content_color'); ?>;
}

.<?php echo $uid; ?> .acfb_box_inner .acfb_flipbox_back{
  <?php if(get_field('acfb_flipbox_back_background_select') === 'acfb_flipbox_bg_color'): ?>
  background-color: <?php the_field('acfb_flipbox_back_background_color'); ?>; 
  <?php else: ?>
  background-image: <?php echo $fboverlay; ?> url(<?php the_field('acfb_flipbox_back_background_image'); ?>);
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center center;
  <?php endif; ?>

  display: flex;
  align-items: <?php the_field('acfb_flipbox_back_content_horizontal_align'); ?>;
  justify-content: <?php the_field('acfb_flipbox_back_content_vertical_align'); ?>;
  flex-direction: column; 

  <?php if(get_field('acfb_flipbox_back_content_horizontal_align') == 'flex-start'): ?>
  	text-align: left;
  <?php endif; ?>

  <?php if(get_field('acfb_flipbox_back_content_horizontal_align') == 'center'): ?>
  	text-align: center;
  <?php endif; ?>	

  <?php if(get_field('acfb_flipbox_back_content_horizontal_align') == 'flex-end'): ?>
  	text-align: right;
  <?php endif; ?>
}

.<?php echo $uid; ?> .acfb_box_inner .acfb_flipbox_back img{
  width: <?php the_field('acfb_flipbox_back_image_size'); ?>%;
}

.<?php echo $uid; ?> .acfb_box_inner .acfb_flipbox_back h3{
	<?php echo get_typo_field($acfb_flipbox_back_title_typo); ?>
	color: <?php the_field('acfb_flipbox_back_title_color'); ?>;
}

.<?php echo $uid; ?> .acfb_box_inner .acfb_flipbox_back p{
	<?php echo get_typo_field($acfb_flipbox_back_content_typo); ?>
	color: <?php the_field('acfb_flipbox_back_content_color'); ?>;
}

.<?php echo $uid; ?> .acfb_box_inner .acfb_flipbox_back a{
	<?php echo get_typo_field($acfb_flipbox_back_button_typo); ?>
	background-color: <?php the_field('acfb_flipbox_back_button_background'); ?>;
	color: <?php the_field('acfb_flipbox_back_button_text_color'); ?> !important; 
}

.<?php echo $uid; ?> .acfb_box_inner .acfb_flipbox_back a:hover{
	background-color: <?php the_field('acfb_flipbox_back_button_background_hover_color'); ?>;
	color: <?php the_field('acfb_flipbox_back_button_text_hover_color'); ?> !important; 
}


<?php if(get_field('acfb_flip_3d_depth') == 'true'): ?>
.<?php echo $uid; ?> .acfb_flip_overlay{
-webkit-transform: translateZ(90px) scale(.91);
    transform: translateZ(90px) scale(.91);
}
<?php endif; ?>



</style>


<div class="<?php the_field('acfb_flip_effects'); ?> <?php the_field('acfb_flip_direction'); ?>">
	<div class="acfb_box_inner">
		<div class="acfb_flipbox_front">
        <div class="acfb_flip_overlay">

        <?php if(get_field('acfb_flipbox_front_image')): ?>
  		  <img src="<?php the_field('acfb_flipbox_front_image'); ?>" alt="<?php the_field('acfb_flipbox_front_image_alt'); ?>">
        <?php endif; ?>

        <?php if(get_field('acfb_flipbox_front_title')): ?>
        <h3><?php the_field('acfb_flipbox_front_title'); ?></h3>
        <?php endif; ?>

        <?php if(get_field('acfb_flipbox_front_content')): ?>
        <p><?php the_field('acfb_flipbox_front_content'); ?></p>
        <?php endif; ?>
  	      
        </div>
	    </div>
	    <div class="acfb_flipbox_back">
        <div class="acfb_flip_overlay">

        <?php if(get_field('acfb_flipbox_back_image')): ?>
        <img src="<?php the_field('acfb_flipbox_back_image'); ?>" alt="<?php the_field('acfb_flipbox_back_image_alt'); ?>">
        <?php endif; ?>

        <?php if(get_field('acfb_flipbox_back_title')): ?>
        <h3><?php the_field('acfb_flipbox_back_title'); ?></h3>
        <?php endif; ?>

        <?php if(get_field('acfb_flipbox_back_content')): ?>
        <p><?php the_field('acfb_flipbox_back_content'); ?></p>
        <?php endif; ?>

        <?php if(get_field('acfb_flipbox_back_button_text')): ?>
        <a href="<?php the_field('acfb_flipbox_back_button_url'); ?>"><?php the_field('acfb_flipbox_back_button_text'); ?></a>
        <?php endif; ?>

        </div>
	    </div>
    </div>
</div>

</div><!-- Uid -->