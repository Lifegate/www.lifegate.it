<?php 
echo parse_link(
    array(
        get_field('acfb_timeline_title_typo'),
        get_field('acfb_timeline_date_typo'),
        get_field('acfb_timeline_content_typo'),
        get_field('acfb_timeline_button_typo')
    )
);

$acfb_timeline_inner_padding = acfb_padding_name('acfb_timeline_inner_padding');
$acfb_timeline_padding = acfb_padding_name('acfb_timeline_padding');
$acfb_timeline_margin = acfb_margin_name('acfb_timeline_margin');
$acfb_timeline_title_typo = acfb_ffaimly_name('acfb_timeline_title_typo');
$acfb_timeline_date_typo = acfb_ffaimly_name('acfb_timeline_date_typo');
$acfb_timeline_content_typo = acfb_ffaimly_name('acfb_timeline_content_typo');
$acfb_timeline_button_typo = acfb_ffaimly_name('acfb_timeline_button_typo');


$uid = $block['id'];

$className = 'acfb_timeline_block';
if( !empty($block['className']) ) {
   $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
   $className .= ' align' . $block['align'];
}

?>
<div class="<?php echo $uid; ?> <?php echo esc_attr($className); ?>">

<style type="text/css">
.<?php echo $uid; ?> {
  <?php echo get_padding_field($acfb_timeline_padding); ?>
  <?php echo get_margin_field($acfb_timeline_margin); ?>
}

.<?php echo $uid; ?> .acfb_timeline .acfb_timeline_inner .acfb_timeline_content{
   background: <?php the_field('acfb_timeline_background'); ?>;
   padding-top: <?php echo $acfb_timeline_inner_padding['padding_top']; ?>px;
   padding-bottom: <?php echo $acfb_timeline_inner_padding['padding_bottom']; ?>px;
   padding-left: <?php echo $acfb_timeline_inner_padding['padding_left']; ?>px;
   padding-right: <?php echo $acfb_timeline_inner_padding['padding_right']; ?>px;
}

.<?php echo $uid; ?> .acfb_left_timeline .acfb_timeline_inner:nth-child(odd)::before, .<?php echo $uid; ?> .acfb_left_timeline .acfb_timeline_inner:nth-child(even)::before{
   border-color: transparent <?php the_field('acfb_timeline_background'); ?> transparent transparent;
}

.<?php echo $uid; ?> .acfb_right_timeline .acfb_timeline_inner:nth-child(odd)::before, .<?php echo $uid; ?> .acfb_right_timeline .acfb_timeline_inner:nth-child(even)::before{
   border-color: transparent transparent transparent <?php the_field('acfb_timeline_background'); ?>;
}

.<?php echo $uid; ?> .acfb_center_timeline .acfb_timeline_inner:nth-child(odd)::before{
   border-color: transparent transparent transparent <?php the_field('acfb_timeline_background'); ?>;
}

.<?php echo $uid; ?> .acfb_center_timeline .acfb_timeline_inner:nth-child(even)::before{
   border-color: transparent <?php the_field('acfb_timeline_background'); ?> transparent transparent;
}


/* Media queries - Responsive timeline on screens less than 767px wide */
@media screen and (max-width: 767px) {
.<?php echo $uid; ?> .acfb_center_timeline .acfb_timeline_inner:nth-child(odd)::before, .<?php echo $uid; ?> .acfb_center_timeline .acfb_timeline_inner:nth-child(even)::before{
   border-color: transparent <?php the_field('acfb_timeline_background'); ?> transparent transparent ;
}
}

.<?php echo $uid; ?> .acfb_timeline .acfb_timeline_inner .acfb_timeline_content .acfb_timeline_image{
   width: <?php the_field('acfb_timeline_image_size'); ?>%;
}

.<?php echo $uid; ?> .acfb_timeline .acfb_timeline_inner .acfb_timeline_content .acfb_timeline_date{
   <?php echo get_typo_field($acfb_timeline_date_typo); ?>
   color: <?php the_field('acfb_timeline_date_color'); ?>;
}

.<?php echo $uid; ?> .acfb_timeline .acfb_timeline_inner .acfb_timeline_content .acfb_timeline_title{
   <?php echo get_typo_field($acfb_timeline_title_typo); ?>
   color: <?php the_field('acfb_timeline_title_color'); ?>;
}

.<?php echo $uid; ?> .acfb_timeline .acfb_timeline_inner .acfb_timeline_content .acfb_timeline_desc p{
   <?php echo get_typo_field($acfb_timeline_content_typo); ?>
   color: <?php the_field('acfb_timeline_content_color'); ?>;
}

.<?php echo $uid; ?> .acfb_timeline .acfb_timeline_inner .acfb_timeline_content .acfb_timeline_button{
   background: <?php the_field('acfb_timeline_button_background_color'); ?>;
   <?php echo get_typo_field($acfb_timeline_button_typo); ?>
   color: <?php the_field('acfb_timeline_button_text_color'); ?>;
}

.<?php echo $uid; ?> .acfb_timeline .acfb_timeline_inner .acfb_timeline_content .acfb_timeline_button:hover{
   background: <?php the_field('acfb_timeline_button_background_hover_color'); ?>;
   color: <?php the_field('acfb_timeline_button_text_hover_color'); ?>;
}

.<?php echo $uid; ?> .acfb_timeline::after{
   background: <?php the_field('acfb_timeline_divider_color'); ?>;
}

.<?php echo $uid; ?> .acfb_timeline .acfb_timeline_inner::after{
   background: <?php the_field('acfb_timeline_circle_color'); ?>;
   border: 4px solid <?php the_field('acfb_timeline_circle_border_color'); ?>;
}



</style>


<?php if( have_rows('acfb_timeline_items') ): ?>

	<div class="acfb_timeline <?php the_field('acfb_timeline_style'); ?>">

	<?php while( have_rows('acfb_timeline_items') ): the_row(); ?>


	<?php if( have_rows('acfb_timeline_flexible') ): ?>

			<div class="acfb_timeline_inner">
			<div class="acfb_timeline_content">

	    <?php while ( have_rows('acfb_timeline_flexible') ) : the_row(); ?>

		<?php
			$acfb_image = '';
			if(!get_sub_field('acfb_timeline_image')){
				$acfb_image = plugins_url() . '/acf-blocks/img/placeholder-image.jpg';
			} else{
				$acfb_image = get_sub_field('acfb_timeline_image');
			}
		?>

	   	<?php if( get_row_layout() == 'acfb_timeline_image_layout' ): ?>
	    	<img src="<?php echo $acfb_image; ?>" class="acfb_timeline_image">
	    <?php endif; ?>

	   	<?php if( get_row_layout() == 'acfb_timeline_date_layout' ): ?>
	    	<span class="acfb_timeline_date"><?php the_sub_field('acfb_timeline_date'); ?></span>
	    <?php endif; ?>

	   	<?php if( get_row_layout() == 'acfb_timeline_title_layout' ): ?>
	    	<h2 class="acfb_timeline_title"><?php the_sub_field('acfb_timeline_title'); ?></h2>
	    <?php endif; ?>

	    <?php if( get_row_layout() == 'acfb_timeline_content_layout' ): ?>
	    	<div class="acfb_timeline_desc">
	    		<p><?php the_sub_field('acfb_timeline_content'); ?></p>
	    	</div>
	    <?php endif; ?>

	    <?php if( get_row_layout() == 'acfb_timeline_button_layout' ): ?>
	    	<a href="<?php the_sub_field('acfb_timeline_button_url'); ?>" class="acfb_timeline_button"><?php the_sub_field('acfb_timeline_button_text'); ?></a>
	    <?php endif; ?>


	<?php endwhile; ?>
			</div>
			</div>
	<?php endif; ?>

	<?php endwhile; ?>
	</div>
<?php endif; ?>

</div><!-- Uid -->