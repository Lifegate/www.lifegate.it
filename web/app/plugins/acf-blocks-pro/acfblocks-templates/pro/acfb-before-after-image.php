<?php 
echo parse_link(
    array(
        get_field('acfb_ba_labels_typo')
    )
);

$acfb_ba_padding = acfb_padding_name('acfb_ba_padding');
$acfb_ba_margin = acfb_margin_name('acfb_ba_margin');
$acfb_ba_labels_typo = acfb_ffaimly_name('acfb_ba_labels_typo');


$uid = $block['id'];

$className = 'acfb_before_after_block';
if( !empty($block['className']) ) {
   $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
   $className .= ' align' . $block['align'];
}

?>
<div class="<?php echo $uid; ?> <?php echo esc_attr($className); ?>">
<style type="text/css">
.<?php echo $uid; ?> {
  <?php echo get_padding_field($acfb_ba_padding); ?>
  <?php echo get_margin_field($acfb_ba_margin); ?>
}

.<?php echo $uid; ?> .twentytwenty-overlay:hover{
    background-color: <?php echo hex2rgba(get_field('acfb_ba_overlay_color'), get_field('acfb_ba_overlay_opacity')); ?>;
}


.<?php echo $uid; ?> .twentytwenty-handle{
  border-color: <?php the_field('acfb_ba_handler_color'); ?>;
}

.<?php echo $uid; ?> .twentytwenty-handle .twentytwenty-left-arrow{
  border-right-color: <?php the_field('acfb_ba_handler_color'); ?>;
}


.<?php echo $uid; ?> .twentytwenty-handle .twentytwenty-right-arrow{
   border-left-color: <?php the_field('acfb_ba_handler_color'); ?>;
}

.<?php echo $uid; ?> .twentytwenty-handle::before{
  background: <?php the_field('acfb_ba_handler_color'); ?> !important;
}

.<?php echo $uid; ?> .twentytwenty-handle::after{
  background: <?php the_field('acfb_ba_handler_color'); ?> !important;
}

.<?php echo $uid; ?> .twentytwenty-before-label:before, .<?php echo $uid; ?> .twentytwenty-after-label:before{
	background: <?php echo hex2rgba(get_field('acfb_ba_labels_background_color'), 0.2); ?>;
	color: <?php the_field("acfb_ba_labels_color"); ?>;
	<?php echo get_typo_field($acfb_ba_labels_typo); ?>
}
</style>

<?php
$acfb_before_image = '';
if(get_field('acfb_ba_before_image')){
  $acfb_before_image = get_field('acfb_ba_before_image');
} else{
   $acfb_before_image = plugins_url() . '/acf-blocks/img/placeholder-image.jpg';
}

$acfb_after_image = '';
if(get_field('acfb_ba_after_image')){
  $acfb_after_image = get_field('acfb_ba_after_image');
} else{
  $acfb_after_image = plugins_url() . '/acf-blocks/img/placeholder-image.jpg';
}

$acfb_ba_overlay = '';
if(get_field('acfb_ba_overlay') == '1'){
  $acfb_ba_overlay = 'false';
} else{
  $acfb_ba_overlay = 'true';
}

$acfb_ba_move_on_hover = '';
if(get_field('acfb_ba_move_slider_on_hover') == '0'){
  $acfb_ba_move_on_hover = 'false';
} else{
  $acfb_ba_move_on_hover = 'true';
}

$acfb_ba_move_on_click = '';
if(get_field('acfb_ba_move_slider_on_click') == '0'){
  $acfb_ba_move_on_click = 'false';
} else{
  $acfb_ba_move_on_click = 'true';
}
?>


<div class="twentytwenty-container" data-uid="<?php echo $uid; ?>" data-before-label="<?php the_field('acfb_ba_before_label'); ?>"  data-after-label="<?php the_field('acfb_ba_after_label'); ?>" data-offset="<?php the_field('acfb_ba_default_offset'); ?>" data-orientation="<?php the_field('acfb_ba_orientation'); ?>" data-overlay="<?php echo $acfb_ba_overlay; ?>" 
	data-move-on-hover="<?php echo $acfb_ba_move_on_hover; ?>" data-move-on-click="<?php echo $acfb_ba_move_on_click; ?>">
  <img src="<?php echo $acfb_before_image; ?>" alt="<?php the_field('acfb_ba_before_image_alt'); ?>" />
  <img src="<?php echo $acfb_after_image; ?>" alt="<?php the_field('acfb_ba_after_image_alt'); ?>" />
</div>

</div><!-- Uid -->