<?php 
echo parse_link(
    array(
        get_field('acfb_db_typo'),
    )
);

$acfb_db_button_padding = acfb_padding_name('acfb_db_button_padding');
$acfb_db_padding = acfb_padding_name('acfb_db_padding');
$acfb_db_margin = acfb_margin_name('acfb_db_margin');
$acfb_db_typo = acfb_padding_name('acfb_db_typo');


$uid = $block['id'];

$className = 'acfb_dual_button_block';
if( !empty($block['className']) ) {
   $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
   $className .= ' align' . $block['align'];
}

$acfb_db_radius = get_field('acfb_db_radius');
?>

<div class="<?php echo $uid; ?> <?php echo esc_attr($className); ?>">
<style type="text/css">
.<?php echo $uid; ?> {
  <?php echo get_padding_field($acfb_db_padding); ?>
  <?php echo get_margin_field($acfb_db_margin); ?>
}

.<?php echo $uid; ?>{
	display: flex;
	justify-content: <?php the_field('acfb_db_alignment') ?>;
}

.<?php echo $uid; ?> .acfb_db_wrapper{
	width: <?php the_field('acfb_db_width') ?>%;
}

.<?php echo $uid; ?> .acfb_db_wrapper .acfb_db_one{
	background: <?php the_field('acfb_db_lb_bg_color'); ?>;
}

.<?php echo $uid; ?> .acfb_db_wrapper .acfb_db_one:hover{
	background: <?php the_field('acfb_db_lb_hover_bg_color'); ?>;
}

.<?php echo $uid; ?> .acfb_db_wrapper .acfb_db_one .acfb_db{
	color: <?php the_field('acfb_db_lb_color'); ?>;
}

.<?php echo $uid; ?> .acfb_db_wrapper .acfb_db_one .acfb_db:hover{
	color: <?php the_field('acfb_db_lb_hover_color'); ?>;
}

.<?php echo $uid; ?> .acfb_db_wrapper .acfb_db_two{
	background: <?php the_field('acfb_db_rb_bg_color'); ?>;
}

.<?php echo $uid; ?> .acfb_db_wrapper .acfb_db_two:hover{
	background: <?php the_field('acfb_db_rb_hover_bg_color'); ?>;
}

.<?php echo $uid; ?> .acfb_db_wrapper .acfb_db_two .acfb_db{
	color: <?php the_field('acfb_db_rb_color'); ?>;
}

.<?php echo $uid; ?> .acfb_db_wrapper .acfb_db_two .acfb_db:hover{
	color: <?php the_field('acfb_db_rb_hover_color'); ?>;
}


.<?php echo $uid; ?> .acfb_db_wrapper .acfb_db{
	<?php echo get_typo_field($acfb_db_typo); ?>
	padding-top: <?php echo $acfb_db_button_padding['padding_top']; ?>px !important;
	padding-bottom: <?php echo $acfb_db_button_padding['padding_bottom']; ?>px !important;
	padding-left: <?php echo $acfb_db_button_padding['padding_left']; ?>px !important;
	padding-right: <?php echo $acfb_db_button_padding['padding_right']; ?>px !important;
}

.<?php echo $uid; ?> .acfb_db_wrapper .acfb_db_seprator{
	background: <?php the_field('acfb_db_bc_bg_color'); ?>;
	<?php if(get_field('acfb_db_bc_img_text') == 'acfb_db_bc_text'): ?>
	color: <?php the_field('acfb_db_bc_text_color'); ?>;
	<?php endif; ?>
	border-radius: <?php the_field('acfb_db_bc_radius'); ?>%; 
}


.<?php echo $uid; ?> .acfb_db_wrapper .acfb_db_one,
.<?php echo $uid; ?> .acfb_db_wrapper .acfb_db_two{
	flex-grow: 1;
    flex-basis: 0;
    display: flex;
}

.<?php echo $uid; ?> .acfb_db_wrapper .acfb_db_one, .<?php echo $uid; ?> .acfb_db_wrapper .acfb_db_two{
	border-top-left-radius: <?php echo $acfb_db_radius['acfb_db_tl_radius']; ?>px;
	border-top-right-radius: <?php echo $acfb_db_radius['acfb_db_tr_radius']; ?>px;
	border-bottom-left-radius: <?php echo $acfb_db_radius['acfb_db_bl_radius']; ?>px;
	border-bottom-right-radius: <?php echo $acfb_db_radius['acfb_db_br_radius']; ?>px;
}


<?php if(get_field('acfb_db_stack') == '1'): ?>

.<?php echo $uid; ?> .acfb_db_wrapper{
  	flex-direction: column;
}

.<?php echo $uid; ?> .acfb_db_wrapper .acfb_db{
	justify-content: center;
	padding-top: 15px !important;
	padding-bottom: 15px !important; 
}

.<?php echo $uid; ?> .acfb_db_wrapper .acfb_db_seprator{
    transform: none;
    top: calc(100% - 17.5px);
    left: calc(50% - 17.5px);
}
<?php endif; ?>	

</style>

<?php
$acfb_image = '';
if(!get_field('acfb_db_bc_img')){
	$acfb_image = plugins_url() . '/acf-blocks/img/placeholder-image.jpg';
} else{
	$acfb_image = get_field('acfb_db_bc_img');
}

$acfb_db_bc_img_text = "";
if(get_field('acfb_db_bc_img_text') == 'acfb_db_bc_img'){
	$acfb_db_bc_img_text = '<img src="' . $acfb_image . '">';
} else{
	$acfb_db_bc_img_text = get_field('acfb_db_bc_text');
}

?>

<div class="acfb_db_wrapper">

	<div class="acfb_db_one">
		<a class="acfb_db" href="<?php the_field('acfb_db_lb_url'); ?>">
			<span class="btn-text"><?php the_field('acfb_db_lb_text'); ?></span>
		</a>
		<?php if(get_field('acfb_db_bc') == 'true'): ?>
			<div class="acfb_db_seprator"><span><?php echo $acfb_db_bc_img_text; ?></span></div>
		<?php endif; ?>
	</div>

	<div class="acfb_db_two">
		<a class="acfb_db" href="<?php the_field('acfb_db_rb_url'); ?>">
			<span class="btn-text"><?php the_field('acfb_db_rb_text'); ?></span>
		</a>
	</div>
	
</div>


</div><!-- Uid -->