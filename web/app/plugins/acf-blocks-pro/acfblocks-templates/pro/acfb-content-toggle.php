<?php 
$uid = $block['id'];

$className = 'acfb_content_toggle_block';
if( !empty($block['className']) ) {
   $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
   $className .= ' align' . $block['align'];
}

$template = [
	[
		"acf/acfb-content-toggle-child",
		[]
	],
	[
		"acf/acfb-content-toggle-child",
		[]
	]
];

?>

<div class="<?php echo $uid; ?> <?php echo esc_attr($className); ?>">
<style type="text/css">

</style>



<div class="acfb_ct_btn_wrap">
	<h3>Content A</h3>
		<label class="acfb_ct_switch">
		  <input type="checkbox" id="switch-id">
		  <span class="acfb_ct_slider acfb_ct_round"></span>
		</label>
	<h3>Content B</h3>
</div>

<div class="acfb_ct_content_wrap one">
	<InnerBlocks template="<?php echo esc_attr( wp_json_encode( $template ) ) ?>" />
</div>

</div><!-- Uid -->