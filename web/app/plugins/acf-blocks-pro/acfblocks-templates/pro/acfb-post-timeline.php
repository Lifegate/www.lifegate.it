<?php 
echo parse_link(
    array(
        get_field('acfb_post_timeline_title_typo'),
        get_field('acfb_post_timeline_meta_typo'),
        get_field('acfb_post_timeline_excerpt_typo'),
        get_field('acfb_post_timeline_button_typo'),
        get_field('acfb_post_timeline_opposite_title_typo'),
        get_field('acfb_post_timeline_opposite_meta_typo'),
        get_field('acfb_post_timeline_opposite_excerpt_typo'),
        get_field('acfb_post_timeline_opposite_button_typo')
    )
);


$acfb_post_timeline_padding = acfb_padding_name('acfb_post_timeline_padding');
$acfb_post_timeline_margin = acfb_margin_name('acfb_post_timeline_margin');
$acfb_post_timeline_title_typo = acfb_ffaimly_name('acfb_post_timeline_title_typo');
$acfb_post_timeline_meta_typo = acfb_ffaimly_name('acfb_post_timeline_meta_typo');
$acfb_post_timeline_excerpt_typo = acfb_ffaimly_name('acfb_post_timeline_excerpt_typo');
$acfb_post_timeline_button_typo = acfb_ffaimly_name('acfb_post_timeline_button_typo');

$acfb_post_timeline_opposite_title_typo = acfb_ffaimly_name('acfb_post_timeline_opposite_title_typo');
$acfb_post_timeline_opposite_meta_typo = acfb_ffaimly_name('acfb_post_timeline_opposite_meta_typo');
$acfb_post_timeline_opposite_excerpt_typo = acfb_ffaimly_name('acfb_post_timeline_opposite_excerpt_typo');
$acfb_post_timeline_opposite_button_typo = acfb_ffaimly_name('acfb_post_timeline_opposite_button_typo');

$uid = $block['id'];

$className = 'acfb_post_timeline_block';
if( !empty($block['className']) ) {
   $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
   $className .= ' align' . $block['align'];
}

$acfb_post_timeline_excerpt_length = get_field('acfb_post_timeline_excerpt_length');
?>

<div class="<?php echo $uid; ?> <?php echo esc_attr($className); ?>">
<style type="text/css">
.<?php echo $uid; ?> {
	<?php echo get_padding_field($acfb_post_timeline_padding); ?>
	<?php echo get_margin_field($acfb_post_timeline_margin); ?>
}

.<?php echo $uid; ?> .acfb_post_timeline .acfb_post_timeline_content{
	background: <?php the_field('acfb_post_timeline_background'); ?>;
}

.<?php echo $uid; ?> .acfb_post_timeline .acfb_post_timeline_content img{
	width: <?php the_field('acfb_post_timeline_image_size'); ?>%;
}

.<?php echo $uid; ?> .acfb_post_timeline_center .acfb_post_timeline_inner:nth-child(odd)::before{
	border-color: transparent transparent transparent <?php the_field('acfb_post_timeline_background'); ?>;
}

.<?php echo $uid; ?> .acfb_post_timeline_center .acfb_post_timeline_inner:nth-child(even)::before{
	border-color: transparent <?php the_field('acfb_post_timeline_background'); ?> transparent transparent;
}

.<?php echo $uid; ?> .acfb_post_timeline_left .acfb_post_timeline_inner::before{
	border-color: transparent <?php the_field('acfb_post_timeline_background'); ?> transparent transparent;
}

.<?php echo $uid; ?> .acfb_post_timeline_right .acfb_post_timeline_inner::before{
	border-color: transparent transparent transparent <?php the_field('acfb_post_timeline_background'); ?>;
}


.<?php echo $uid; ?> .acfb_post_timeline:after{
	background: <?php the_field('acfb_post_timeline_divider_color'); ?>;
}

.<?php echo $uid; ?> .acfb_post_timeline .acfb_post_timeline_inner::after{
	background: <?php the_field('acfb_post_timeline_circle_color'); ?>;
	border-color: <?php the_field('acfb_post_timeline_circle_border_color'); ?>;
}

<?php if( get_field('acfb_post_timeline_title_custom_typography') == '1' ): ?>
.<?php echo $uid; ?> .acfb_post_timeline .acfb_post_timeline_content .acfb_post_timeline_title a{
 <?php echo get_typo_field($acfb_post_timeline_title_typo); ?>
}
<?php endif; ?>

<?php if( get_field('acfb_post_timeline_title_custom_color') == '1' ): ?>
.<?php echo $uid; ?> .acfb_post_timeline .acfb_post_timeline_content .acfb_post_timeline_title a{
	color: <?php the_field('acfb_post_timeline_title_color'); ?> !important;
}

.<?php echo $uid; ?> .acfb_post_timeline .acfb_post_timeline_content .acfb_post_timeline_title a:hover{
	color: <?php the_field('acfb_post_timeline_title_hover_color'); ?> !important;
}
<?php endif; ?>



<?php if( get_field('acfb_post_timeline_meta_custom_typography') == '1' ): ?>
.<?php echo $uid; ?> .acfb_post_timeline .acfb_post_timeline_content .acfb_post_timeline_meta{
	<?php echo get_typo_field($acfb_post_timeline_meta_typo); ?>
}
<?php endif; ?>


<?php if( get_field('acfb_post_timeline_meta_custom_color') == '1' ): ?>
.<?php echo $uid; ?> .acfb_post_timeline .acfb_post_timeline_content .acfb_post_timeline_meta{
	color: <?php the_field('acfb_post_timeline_meta_color'); ?>;
}
<?php endif; ?>



<?php if( get_field('acfb_post_timeline_meta_excerpt_typography') == '1' ): ?>
.<?php echo $uid; ?> .acfb_post_timeline .acfb_post_timeline_content .acfb_post_timeline_excerpt{
	<?php echo get_typo_field($acfb_post_timeline_excerpt_typo); ?>
}
<?php endif; ?>


<?php if( get_field('acfb_post_timeline_excerpt_custom_color') == '1' ): ?>
.<?php echo $uid; ?> .acfb_post_timeline .acfb_post_timeline_content .acfb_post_timeline_excerpt{
	color: <?php the_field('acfb_post_timeline_excerpt_color'); ?>;
}
<?php endif; ?>

<?php if( get_field('acfb_post_timeline_meta_button_typography') == '1' ): ?>
.<?php echo $uid; ?> .acfb_post_timeline .acfb_post_timeline_content .acfb_post_timeline_button a{
	<?php echo get_typo_field($acfb_post_timeline_button_typo); ?>
}

<?php endif; ?>

<?php if( get_field('acfb_post_timeline_button_custom_color') == '1' ): ?>
.<?php echo $uid; ?> .acfb_post_timeline .acfb_post_timeline_content .acfb_post_timeline_button a{
	background-color: <?php the_field('acfb_post_timeline_button_background_color'); ?>;
	color: <?php the_field('acfb_post_timeline_button_text_color'); ?> !important;
	padding: 10px;
	display: inline-block;
}

.<?php echo $uid; ?> .acfb_post_timeline .acfb_post_timeline_content .acfb_post_timeline_button a:hover{
	background-color: <?php the_field('acfb_post_timeline_button_background_hover_color'); ?>;
	color: <?php the_field('acfb_post_timeline_button_text_hover_color'); ?> !important;
}
<?php endif; ?>



/**** opposite css  ****/
.<?php echo $uid; ?> .acfb_post_timeline .acfb_posts_opposite_elements_inner{
	background: <?php the_field('acfb_post_timeline_opposite_background'); ?>;
}

.<?php echo $uid; ?> .acfb_post_timeline_center .acfb_post_timeline_inner:nth-child(odd) .acfb_posts_opposite_elements .acfb_posts_opposite_elements_inner::before{
	border-color: transparent <?php the_field('acfb_post_timeline_opposite_background'); ?> transparent transparent;
}

.<?php echo $uid; ?> .acfb_post_timeline_center .acfb_post_timeline_inner:nth-child(even) .acfb_posts_opposite_elements .acfb_posts_opposite_elements_inner::before{
	 border-color: transparent transparent transparent <?php the_field('acfb_post_timeline_opposite_background'); ?>;
}

.<?php echo $uid; ?> .acfb_post_timeline_left .acfb_post_timeline_inner .acfb_posts_opposite_elements .acfb_posts_opposite_elements_inner::before{
	border-color: transparent <?php the_field('acfb_post_timeline_opposite_background'); ?> transparent transparent;
}

.<?php echo $uid; ?> .acfb_post_timeline_right .acfb_post_timeline_inner .acfb_posts_opposite_elements .acfb_posts_opposite_elements_inner::before{
	border-color: transparent transparent transparent <?php the_field('acfb_post_timeline_opposite_background'); ?>;
}


<?php if( get_field('acfb_post_timeline_opposite_title_custom_typography') == '1' ): ?>
.<?php echo $uid; ?> .acfb_post_timeline .acfb_posts_opposite_elements .acfb_post_timeline_title a{
	<?php echo get_typo_field($acfb_post_timeline_opposite_title_typo); ?>
}

<?php endif; ?>

<?php if( get_field('acfb_post_timeline_opposite_title_custom_color') == '1' ): ?>
.<?php echo $uid; ?> .acfb_post_timeline .acfb_posts_opposite_elements .acfb_post_timeline_title a{
	color: <?php the_field('acfb_post_timeline_opposite_title_color'); ?> !important;
}

.<?php echo $uid; ?> .acfb_post_timeline .acfb_posts_opposite_elements .acfb_post_timeline_title a:hover{
	color: <?php the_field('acfb_post_timeline_opposite_title_hover_color'); ?> !important;
}
<?php endif; ?>



<?php if( get_field('acfb_post_timeline_opposite_meta_custom_typography') == '1' ): ?>
.<?php echo $uid; ?> .acfb_post_timeline .acfb_posts_opposite_elements .acfb_post_timeline_meta{
	<?php echo get_typo_field($acfb_post_timeline_opposite_meta_typo); ?>
}
<?php endif; ?>

<?php if( get_field('acfb_post_timeline_opposite_meta_custom_color') == '1' ): ?>
.<?php echo $uid; ?> .acfb_post_timeline .acfb_posts_opposite_elements .acfb_post_timeline_meta{
	color: <?php the_field('acfb_post_timeline_opposite_meta_color'); ?>;
}
<?php endif; ?>




<?php if( get_field('acfb_post_timeline_opposite_excerpt_custom_typography') == '1' ): ?>
.<?php echo $uid; ?> .acfb_post_timeline .acfb_posts_opposite_elements .acfb_post_timeline_excerpt{
	<?php echo get_typo_field($acfb_post_timeline_opposite_excerpt_typo); ?>
}
<?php endif; ?>

<?php if( get_field('acfb_post_timeline_opposite_excerpt_custom_color') == '1' ): ?>
.<?php echo $uid; ?> .acfb_post_timeline .acfb_posts_opposite_elements .acfb_post_timeline_excerpt{
	color: <?php the_field('acfb_post_timeline_opposite_excerpt_color'); ?>;
}
<?php endif; ?>





<?php if( get_field('acfb_post_timeline_opposite_button_custom_typography') == '1' ): ?>
.<?php echo $uid; ?> .acfb_post_timeline .acfb_posts_opposite_elements .acfb_post_timeline_button a{
	<?php echo get_typo_field($acfb_post_timeline_opposite_button_typo); ?>
}
<?php endif; ?>

<?php if( get_field('acfb_post_timeline_opposite_button_custom_color') == '1' ): ?>
.<?php echo $uid; ?> .acfb_post_timeline .acfb_posts_opposite_elements .acfb_post_timeline_button a{
	background-color: <?php the_field('acfb_post_timeline_opposite_button_background_color'); ?>;
	color: <?php the_field('acfb_post_timeline_opposite_button_text_color'); ?> !important;
	padding: 10px;
	display: inline-block;
}

.<?php echo $uid; ?> .acfb_post_timeline .acfb_posts_opposite_elements .acfb_post_timeline_button a:hover{
	background-color: <?php the_field('acfb_post_timeline_opposite_button_background_hover_color'); ?>;
	color: <?php the_field('acfb_post_timeline_opposite_button_text_hover_color'); ?> !important;
}
<?php endif; ?>




@media screen and (max-width: 767px) {
<?php if(get_field('acfb_post_timeline_opposite_elements')): ?>	
.<?php echo $uid; ?> .acfb_post_timeline_center .acfb_post_timeline_inner:nth-child(odd)::before,
.<?php echo $uid; ?> .acfb_post_timeline_center .acfb_post_timeline_inner:nth-child(even)::before{
	border-color: transparent <?php the_field('acfb_post_timeline_opposite_background'); ?> transparent transparent;
}
<?php else: ?>
.<?php echo $uid; ?> .acfb_post_timeline_center .acfb_post_timeline_inner:nth-child(odd)::before,
.<?php echo $uid; ?> .acfb_post_timeline_center .acfb_post_timeline_inner:nth-child(even)::before{
	border-color: transparent <?php the_field('acfb_post_timeline_background'); ?> transparent transparent;
}
<?php endif; ?>
}
</style>

<div class="acfb_post_timeline <?php the_field('acfb_post_timeline_layout'); ?>">
	<?php

	$post_timeline_args = array(
	'post_type' => get_field('acfb_post_type'),
	'post_status' => 'publish',
	'posts_per_page' => get_field('acfb_post_timeline_number_of_posts'), 
	);

	// the query
	$timeline_post_query = new WP_Query( $post_timeline_args ); ?>

	<?php if ( $timeline_post_query->have_posts() ) : ?>

		<?php while ( $timeline_post_query->have_posts() ) : $timeline_post_query->the_post(); ?>
			<div class="acfb_post_timeline_inner">
			
				<div class="acfb_post_timeline_content">

				<!-- Top Elements -->
				<?php if( have_rows('acfb_post_timeline_top_elements') ):?>
				<div class="acfb_posts_timeline_top_elements">
				<?php while ( have_rows('acfb_post_timeline_top_elements') ) : the_row(); ?>

				    <?php if( get_row_layout() == 'post_timeline_image' ): ?>
				        <div class="acfb_post_timeline_thumbnail">	
				        	<?php the_post_thumbnail(); ?>
				        </div>
				    <?php endif; ?>

				    <?php if( get_row_layout() == 'post_timeline_title' ): ?>
				    	<div class="acfb_post_timeline_title">	
				       		 <<?php the_field('acfb_post_timeline_title_html_tag') ?>><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></<?php the_field('acfb_post_timeline_title_html_tag') ?>>
				   		</div>
				    <?php endif; ?>

				    <?php if( get_row_layout() == 'post_timeline_meta_data' ): ?>
				    	<div class="acfb_post_timeline_meta">
				    		<span class="acfb_post_timeline_author"><?php the_author(); ?></span> -
							<span class="acfb_post_timeline_date"><?php the_time('F jS, Y') ?></span>
						</div>
				    <?php endif; ?>
				      
				    <?php if( get_row_layout() == 'post_timeline_content' ): ?>
				    	<div class="acfb_post_timeline_excerpt">
				        	<?php echo acfb_excerpt($acfb_post_timeline_excerpt_length); ?>
				    	</div>
				    <?php endif; ?>

					<?php if( get_row_layout() == 'post_timeline_read_more_button' ): ?>
						<div class="acfb_post_timeline_button">
							<a href="<?php the_permalink(); ?>" class="acfb_post_timeline_btn">Read More</a>
						</div>
				    <?php endif; ?>

				<?php endwhile; ?>
				</div>
				<?php endif; ?>


				<!-- Center Elements -->
				<?php if( have_rows('acfb_post_timeline_center_elements') ):?>
				<div class="acfb_posts_timeline_center_elements">
				<?php while ( have_rows('acfb_post_timeline_center_elements') ) : the_row(); ?>

				    <?php if( get_row_layout() == 'post_timeline_image' ): ?>
				        <div class="acfb_post_timeline_thumbnail">	
				        	<?php the_post_thumbnail(); ?>
				        </div>
				    <?php endif; ?>

				     <?php if( get_row_layout() == 'post_timeline_title' ): ?>
				    	<div class="acfb_post_timeline_title">	
				       		 <<?php the_field('acfb_post_timeline_title_html_tag') ?>><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></<?php the_field('acfb_post_timeline_title_html_tag') ?>>
				   		</div>
				    <?php endif; ?>

				    <?php if( get_row_layout() == 'post_timeline_meta_data' ): ?>
				    	<div class="acfb_post_timeline_meta">
				    		<span class="acfb_post_timeline_author"><?php the_author(); ?></span> -
							<span class="acfb_post_timeline_date"><?php the_time('F jS, Y') ?></span>
						</div>
				    <?php endif; ?>
				      
				    <?php if( get_row_layout() == 'post_timeline_content' ): ?>
				    	<div class="acfb_post_timeline_excerpt">
				        	<?php echo acfb_excerpt($acfb_post_timeline_excerpt_length); ?>
				    	</div>
				    <?php endif; ?>

					<?php if( get_row_layout() == 'post_timeline_read_more_button' ): ?>
						<div class="acfb_post_timeline_button">
							<a href="<?php the_permalink(); ?>" class="acfb_post_timeline_btn">Read More</a>
						</div>
				    <?php endif; ?>

				<?php endwhile; ?>
				</div>
				<?php endif; ?>


				<!-- Bottom Elements -->
				<?php if( have_rows('acfb_post_timeline_bottom_elements') ):?>
				<div class="acfb_posts_timeline_bottom_elements">
				<?php while ( have_rows('acfb_post_timeline_bottom_elements') ) : the_row(); ?>

					<?php if( get_row_layout() == 'post_timeline_image' ): ?>
				        <div class="acfb_post_timeline_thumbnail">	
				        	<?php the_post_thumbnail(); ?>
				        </div>
				    <?php endif; ?>

				    <?php if( get_row_layout() == 'post_timeline_title' ): ?>
				    	<div class="acfb_post_timeline_title">	
				       		 <<?php the_field('acfb_post_timeline_title_html_tag') ?>><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></<?php the_field('acfb_post_timeline_title_html_tag') ?>>
				   		</div>
				    <?php endif; ?>

				    <?php if( get_row_layout() == 'post_timeline_meta_data' ): ?>
				    	<div class="acfb_post_timeline_meta">
				    		<span class="acfb_post_timeline_author"><?php the_author(); ?></span> -
							<span class="acfb_post_timeline_date"><?php the_time('F jS, Y') ?></span>
						</div>
				    <?php endif; ?>
				      
				    <?php if( get_row_layout() == 'post_timeline_content' ): ?>
				    	<div class="acfb_post_timeline_excerpt">
				        	<?php echo acfb_excerpt($acfb_post_timeline_excerpt_length); ?>
				    	</div>
				    <?php endif; ?>

					<?php if( get_row_layout() == 'post_timeline_read_more_button' ): ?>
						<div class="acfb_post_timeline_button">
							<a href="<?php the_permalink(); ?>" class="acfb_post_timeline_btn">Read More</a>
						</div>
				    <?php endif; ?>

				<?php endwhile; ?>
				</div>
				<?php endif; ?>

				</div>	


				<?php if( have_rows('acfb_post_timeline_opposite_elements') ):?>
				<div class="acfb_posts_opposite_elements">
					<div class="acfb_posts_opposite_elements_inner">
				<?php while ( have_rows('acfb_post_timeline_opposite_elements') ) : the_row(); ?>

					<?php if( get_row_layout() == 'post_timeline_image' ): ?>
				        <div class="acfb_post_timeline_thumbnail">	
				        	<?php the_post_thumbnail(); ?>
				        </div>
				    <?php endif; ?>
				    
				     <?php if( get_row_layout() == 'post_timeline_title' ): ?>
				    	<div class="acfb_post_timeline_title">	
				       		 <<?php the_field('acfb_post_timeline_title_html_tag') ?>><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></<?php the_field('acfb_post_timeline_title_html_tag') ?>>
				   		</div>
				    <?php endif; ?>

				    <?php if( get_row_layout() == 'post_timeline_meta_data' ): ?>
				    	<div class="acfb_post_timeline_meta">
				    		<span class="acfb_post_timeline_author"><?php the_author(); ?></span> -
							<span class="acfb_post_timeline_date"><?php the_time('F jS, Y') ?></span>
						</div>
				    <?php endif; ?>
				      
				    <?php if( get_row_layout() == 'post_timeline_content' ): ?>
				    	<div class="acfb_post_timeline_excerpt">
				        	<?php echo acfb_excerpt($acfb_post_timeline_excerpt_length); ?>
				    	</div>
				    <?php endif; ?>

					<?php if( get_row_layout() == 'post_timeline_read_more_button' ): ?>
						<div class="acfb_post_timeline_button">
							<a href="<?php the_permalink(); ?>" class="acfb_post_timeline_btn">Read More</a>
						</div>
				    <?php endif; ?>

				<?php endwhile; ?>
					</div>
				</div>
				<?php endif; ?>

			</div>
		<?php endwhile; ?>

	<?php wp_reset_postdata(); ?>

	<?php else : ?>
		<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
	<?php endif; ?>

</div>

</div><!-- Uid -->