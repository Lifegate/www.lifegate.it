<?php 
echo parse_link(
  array(
    get_field('acfb_tilt_title_typo'),
    get_field('acfb_tilt_content_typo'),
    get_field('acfb_tilt_content_button_typo')
  )
);

$acfb_tilt_title_typo = acfb_ffaimly_name('acfb_tilt_title_typo');
$acfb_tilt_content_typo = acfb_ffaimly_name('acfb_tilt_content_typo');
$acfb_tilt_content_button_typo = acfb_ffaimly_name('acfb_tilt_content_button_typo');
$acfb_tilt_image_padding = acfb_padding_name('acfb_tilt_image_padding');
$acfb_tilt_image_inner_padding = acfb_padding_name('acfb_tilt_image_inner_padding');
$acfb_tilt_image_margin = acfb_margin_name('acfb_tilt_image_margin');


$acfb_tilt_image_overlay = '';
if(get_field('acfb_tilt_image_overlay_switch') == 'true'){
  $acfb_tilt_image_overlay = 'linear-gradient(' . hex2rgba(get_field("acfb_tilt_image_overlay_color"), get_field('acfb_tilt_image_overlay_opacity')) .', '. hex2rgba(get_field("acfb_tilt_image_overlay_color"), get_field('acfb_tilt_image_overlay_opacity')) . '),';
}


$acfb_image = '';
if(!get_field('acfb_tilt_image')){
  $acfb_image = plugins_url() . '/acf-blocks/img/placeholder-image.jpg';
} else{
  $acfb_image = get_field('acfb_tilt_image');
}


$uid = $block['id'];

$className = 'acfb_tilt_card_block';
if( !empty($block['className']) ) {
   $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
   $className .= ' align' . $block['align'];
}
?>

<div class="<?php echo $uid; ?> <?php echo esc_attr($className); ?>">
<style>
.<?php echo $uid; ?> {
  <?php echo get_padding_field($acfb_tilt_image_padding); ?>
  <?php echo get_margin_field($acfb_tilt_image_margin); ?>
}

.<?php echo $uid; ?> .acfb_tilt_wrapper .acfb_tilt_card{
    display: flex;
    flex-direction: column;
    align-items: <?php the_field('acfb_tilt_content_halign'); ?>;
    justify-content: <?php the_field('acfb_tilt_content_valign'); ?>;
    height: <?php the_field('acfb_tilt_height'); ?>px;
    <?php echo get_padding_field($acfb_tilt_image_inner_padding); ?>
}

.<?php echo $uid; ?> .acfb_tilt_wrapper .acfb_tilt_card .acfb_tilt_bg{
    <?php if(get_field('acfb_tilt_imgorcolor') === 'image'): ?>
    background-image: <?php echo $acfb_tilt_image_overlay; ?> url('<?php echo $acfb_image; ?>');
    <?php elseif (get_field("acfb_tilt_imgorcolor") === 'color'): ?>
    background: <?php the_field('acfb_tilt_bg_color'); ?>;
    <?php endif;?>  
    border-radius: <?php the_field('acfb_tilt_image_radius'); ?>px;
}

.<?php echo $uid; ?> .acfb_tilt_wrapper .acfb_tilt_card .acfb_tilt_content_img{
    width: <?php the_field('acfb_tilt_content_image_size'); ?>px;
}

.<?php echo $uid; ?> .acfb_tilt_wrapper .acfb_tilt_card .acfb_tilt_title{
    <?php echo get_typo_field($acfb_tilt_title_typo); ?>
    color: <?php the_field('acfb_tilt_title_color'); ?>; 
}

.<?php echo $uid; ?> .acfb_tilt_wrapper .acfb_tilt_card .acfb_tilt_content{
    <?php echo get_typo_field($acfb_tilt_content_typo); ?>
    color: <?php the_field('acfb_tilt_content_color'); ?>;
     <?php if(get_field('acfb_tilt_content_halign') === 'flex-start'): ?>
        text-align: left;
    <?php elseif (get_field("acfb_tilt_content_halign") === 'center'): ?>
        text-align: center;
    <?php elseif (get_field("acfb_tilt_content_halign") === 'flex-end'): ?>
        text-align: right;
    <?php endif;?>   
}

.<?php echo $uid; ?> .acfb_tilt_wrapper .acfb_tilt_card .acfb_tilt_btn{
    background: <?php the_field('acfb_tilt_content_button_background_color'); ?>;
    color: <?php the_field('acfb_tilt_content_button_text_color'); ?>;
    <?php echo get_typo_field($acfb_tilt_content_button_typo); ?>
}

.<?php echo $uid; ?> .acfb_tilt_wrapper .acfb_tilt_card .acfb_tilt_btn:hover{
    background: <?php the_field('acfb_tilt_content_button_background_hover_color'); ?>;
    color: <?php the_field('acfb_tilt_content_button_text_hover_color'); ?>;
}

.editor-styles-wrapper .<?php echo $uid; ?> .acfb_tilt_wrapper .acfb_tilt_card .acfb_tilt_content{
    margin-top: 0; 
}
</style>


<div class="acfb_tilt_wrapper">
    <div class="acfb_tilt_card" data-max="<?php the_field('acfb_tilt_max'); ?>" data-perspective="<?php the_field('acfb_tilt_perspective'); ?>" data-speed="<?php the_field('acfb_tilt_speed'); ?>" data-scale="<?php the_field('acfb_tilt_scale'); ?>" data-glare="<?php the_field('acfb_tilt_glare'); ?>" data-tilt>

      <?php if(!empty( get_field('acfb_tilt_content_image'))): ?>  
        <img src="<?php the_field("acfb_tilt_content_image"); ?>" class="acfb_tilt_content_img">
      <?php endif; ?>

      <?php if( !empty(get_field('acfb_tilt_title'))): ?>  
        <h1 class="acfb_tilt_title"><?php the_field("acfb_tilt_title"); ?></h1>
      <?php endif; ?>

      <?php if( !empty(get_field('acfb_tilt_content'))): ?>  
        <p class="acfb_tilt_content"><?php the_field("acfb_tilt_content"); ?></p>
      <?php endif; ?>

      <?php if( !empty(get_field('acfb_tilt_content_button_text'))): ?>  
        <a href="<?php the_field("acfb_tilt_content_button_url"); ?>" class="acfb_tilt_btn"><?php the_field("acfb_tilt_content_button_text"); ?></a>
      <?php endif; ?>

      <div class="acfb_tilt_bg"></div>
    </div>
</div>

</div><!-- Uid -->