<?php 
$uid = $block['id'];

$className = 'acfb_lottie_block';
if( !empty($block['className']) ) {
   $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
   $className .= ' align' . $block['align'];
}

$acfb_lottie_padding = acfb_padding_name('acfb_lottie_padding');
$acfb_lottie_margin = acfb_margin_name('acfb_lottie_margin');

$acfb_lottie_hover = "";
if(get_field("acfb_lottie_hover") == 'true'){
	$acfb_lottie_hover = "hover";
} 

$acfb_lottie_loop = "";
if(get_field("acfb_lottie_loop") == 'true'){
	$acfb_lottie_loop = "loop";
} 

$acfb_lottie_autoplay = "";
if(get_field("acfb_lottie_autoplay") == 'true'){
	$acfb_lottie_autoplay = "autoplay";
} 

$acfb_lottie_src = "";
if(get_field("acfb_lottie_json_file")){
	$acfb_lottie_src = get_field("acfb_lottie_json_file");
} else{
	$acfb_lottie_src = plugins_url() . '/acf-blocks/img/location-pin.json';
}

$acfb_lottie_url = "";
if(get_field("acfb_lottie_json_url")){
	$acfb_lottie_url = get_field("acfb_lottie_json_url");
} else{
	$acfb_lottie_url = plugins_url() . '/acf-blocks/img/location-pin.json';
}

$acfb_lottie_src_select = "";
if(get_field("acfb_lottie_src") === "media"){
	$acfb_lottie_src_select = $acfb_lottie_src;
} else{
	$acfb_lottie_src_select = $acfb_lottie_url;
}

$loop_time = '';
if(get_field("acfb_lottie_loop_time_switch") == 'true'){
	$loop_time = (int)get_field("acfb_lottie_loop_times") - 1;
}
?>

<div class="<?php echo $uid; ?> <?php echo esc_attr($className); ?>">
<style>
.<?php echo $uid; ?> {
  <?php echo get_padding_field($acfb_lottie_padding); ?>
  <?php echo get_margin_field($acfb_lottie_margin); ?>
}

.<?php echo $uid; ?> .acfb_lottie {
	display: flex;
	justify-content: <?php the_field('acfb_lottie_alignment') ?>;
}
</style>

<div class="acfb_lottie">
	<lottie-player src="<?php echo $acfb_lottie_src_select; ?>" background="<?php the_field("acfb_lottie_bg_color"); ?>" speed="<?php the_field("acfb_lottie_speed"); ?>" style="max-width: <?php the_field("acfb_lottie_width"); ?>%;" <?php echo $acfb_lottie_hover; ?> <?php echo $acfb_lottie_loop; ?> <?php echo $acfb_lottie_autoplay; ?> count="<?php echo $loop_time ?>"></lottie-player>
</div>
</div><!-- Uid -->