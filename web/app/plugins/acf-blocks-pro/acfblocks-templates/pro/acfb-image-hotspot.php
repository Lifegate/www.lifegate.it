<?php 
echo parse_link(
    array(
        get_field('acfb_ih_tooltip_title_typo'),
        get_field('acfb_ih_tooltip_content_typo'),
        get_field('acfb_ih_tooltip_button_typo')
    )
);

$acfb_ih_padding = acfb_padding_name('acfb_ih_padding');
$acfb_ih_margin = acfb_margin_name('acfb_ih_margin');
$acfb_ih_tooltip_title_typo = acfb_ffaimly_name('acfb_ih_tooltip_title_typo');
$acfb_ih_tooltip_content_typo = acfb_ffaimly_name('acfb_ih_tooltip_content_typo');
$acfb_ih_tooltip_button_typo = acfb_ffaimly_name('acfb_ih_tooltip_button_typo');

$uid = $block['id'];

$className = 'acfb_image_hotspot_block';
if( !empty($block['className']) ) {
   $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
   $className .= ' align' . $block['align'];
}

?>
<div class="<?php echo $uid; ?> <?php echo esc_attr($className); ?>">
<style type="text/css">
.<?php echo $uid; ?> {
	<?php echo get_padding_field($acfb_ih_padding); ?>
	<?php echo get_margin_field($acfb_ih_margin); ?>
}	

.<?php echo $uid; ?> .acfb_ih_wrapper .acfb_ih_item .acfb_ih_tooltip, .<?php echo $uid; ?> .acfb_ih_wrapper .acfb_ih_item .acfb_ih_tooltip:after{
	background: <?php the_field("acfb_ih_tooltip_background_color"); ?>;
}

.<?php echo $uid; ?> .acfb_ih_wrapper .acfb_ih_item .acfb_ih_tooltip{
	width: <?php the_field("acfb_ih_tooltip_width"); ?>px;
}

.<?php echo $uid; ?> .acfb_ih_wrapper .acfb_ih_pointer{
	background: <?php the_field("acfb_ih_icon_color"); ?>;
	border: 3px solid <?php the_field("acfb_ih_icon_border_color"); ?>;
	width: <?php the_field("acfb_ih_icon_size"); ?>px;
	height: <?php the_field("acfb_ih_icon_size"); ?>px;
}

.<?php echo $uid; ?> .acfb_ih_wrapper .acfb_ih_pointer:after{
   border: 1px solid <?php the_field("acfb_ih_icon_color"); ?>;
   height: <?php the_field("acfb_ih_icon_size"); ?>.4px;
   width: <?php the_field("acfb_ih_icon_size"); ?>.4px;
}

.<?php echo $uid; ?> .acfb_ih_wrapper .acfb_ih_tooltip h4{
	<?php echo get_typo_field($acfb_ih_tooltip_title_typo); ?>
	color: <?php the_field("acfb_ih_tooltip_title_color"); ?>;
}

.<?php echo $uid; ?> .acfb_ih_wrapper .acfb_ih_tooltip p{
	<?php echo get_typo_field($acfb_ih_tooltip_content_typo); ?>
	color: <?php the_field("acfb_ih_tooltip_content_color"); ?>;
}

.<?php echo $uid; ?> .acfb_ih_wrapper .acfb_ih_tooltip a{
	background: <?php the_field("acfb_ih_tooltip_button_background_color"); ?>;
	<?php echo get_typo_field($acfb_ih_tooltip_button_typo); ?>
	color: <?php the_field("acfb_ih_tooltip_button_text_color"); ?> !important;
}

.<?php echo $uid; ?> .acfb_ih_wrapper .acfb_ih_tooltip a:hover{
	background: <?php the_field("acfb_ih_tooltip_button_background_hover_color"); ?>;
	color: <?php the_field("acfb_ih_tooltip_button_text_hover_color"); ?> !important;
}
</style>

<?php
$acfb_image = '';
if(get_field('acfb_ih_image')){
	$acfb_image = get_field('acfb_ih_image');
} else{
	$acfb_image = plugins_url() . '/acf-blocks/img/placeholder-image.jpg';
}
?>

<div class="acfb_ih_wrapper">

<img class="acfb_ih_image" src="<?php echo $acfb_image; ?>" alt="<?php the_field("acfb_ih_image_alt_text"); ?>">

<?php if( have_rows('acfb_ih_image_spots') ): ?>

	<?php while( have_rows('acfb_ih_image_spots') ): the_row(); ?>

		<div style="top: calc( <?php the_sub_field('acfb_ih_spot_vertical_position'); ?>% - <?php the_field("acfb_ih_icon_size"); ?>px); left: calc(<?php the_sub_field('acfb_ih_spot_horizontal_position'); ?>% - <?php the_field("acfb_ih_icon_size"); ?>px);" class="acfb_ih_item_<?php echo get_row_index(); ?> acfb_ih_item">
			<div class="acfb_ih_pointer"></div>
			<div class="acfb_ih_tooltip <?php the_sub_field('acfb_ih_tooltip_position'); ?>">
				
				<?php if( get_sub_field('acfb_ih_spot_image') ): ?>
					<img src="<?php the_sub_field('acfb_ih_spot_image'); ?>" style="width: <?php the_sub_field('acfb_ih_spot_image_size'); ?>%;">
				<?php endif; ?>

				<?php if( get_sub_field('acfb_ih_spot_title') ): ?>
					<h4><?php the_sub_field('acfb_ih_spot_title'); ?></h4>
				<?php endif; ?>

				<?php if( get_sub_field('acfb_ih_spot_content') ): ?>
					<p><?php the_sub_field('acfb_ih_spot_content'); ?></p>
				<?php endif; ?>

				<?php if( get_sub_field('acfb_ih_spot_button_text') && get_sub_field('acfb_ih_spot_button_url') ): ?>
					<a href="<?php the_sub_field('acfb_ih_spot_button_url'); ?>"><?php the_sub_field('acfb_ih_spot_button_text'); ?></a>
				<?php endif; ?>
	      	</div>
		</div>

	<?php endwhile; ?>

<?php endif; ?>

</div>

</div><!-- Uid -->