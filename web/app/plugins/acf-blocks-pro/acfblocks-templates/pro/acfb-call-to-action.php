<?php 
echo parse_link(
    array(
        get_field('acfb_cta_title_typo'),
        get_field('acfb_cta_description_typo'),
        get_field('acfb_cta_button_typo'),
        get_field('acfb_cta_ribbon_typo')
    )
);

$acfb_cta_content_padding = acfb_padding_name('acfb_cta_content_padding');
$acfb_cta_padding = acfb_padding_name('acfb_cta_padding');
$acfb_cta_margin = acfb_margin_name('acfb_cta_margin');
$acfb_cta_title_typo = acfb_ffaimly_name('acfb_cta_title_typo');
$acfb_cta_description_typo = acfb_ffaimly_name('acfb_cta_description_typo');
$acfb_cta_button_typo = acfb_ffaimly_name('acfb_cta_button_typo');
$acfb_cta_ribbon_typo = acfb_ffaimly_name('acfb_cta_ribbon_typo');


$uid = $block['id'];

$className = 'acfb_call_to_action_block';
if( !empty($block['className']) ) {
   $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
   $className .= ' align' . $block['align'];
}

?>
<div class="<?php echo $uid; ?> <?php echo esc_attr($className); ?>">
<style type="text/css">
.<?php echo $uid; ?> {
  <?php echo get_padding_field($acfb_cta_padding); ?>
  <?php echo get_margin_field($acfb_cta_margin); ?>
}

.<?php echo $uid; ?> .acfb_cta:hover .acfb_cta_bg_overlay{
	background-color: <?php echo hex2rgba(get_field('acfb_cta_image_overlay_color'), get_field('acfb_cta_image_overlay_opacity')); ?>!important;
}

.<?php echo $uid; ?> .acfb_cta_skin_classic .acfb_cta_content{
	background: <?php the_field("acfb_cta_content_background_color"); ?>;
}

.<?php echo $uid; ?> .acfb_cta_content{
	padding-top: <?php echo $acfb_cta_content_padding['padding_top']; ?>px;
 	padding-bottom: <?php echo $acfb_cta_content_padding['padding_bottom']; ?>px;
  	padding-left: <?php echo $acfb_cta_content_padding['padding_left']; ?>px;
 	padding-right: <?php echo $acfb_cta_content_padding['padding_right']; ?>px;
	align-items: <?php the_field("acfb_cta_content_horizontal_align"); ?>;
	justify-content: <?php the_field("acfb_cta_content_vertical_align"); ?>;
	min-height: <?php the_field("acfb_cta_content_height"); ?>px;
	<?php if(get_field("acfb_cta_content_horizontal_align") === 'flex-start'): ?>
		text-align: left;
	<?php elseif (get_field("acfb_cta_content_horizontal_align") === 'center'): ?>
		text-align: center;
	<?php elseif (get_field("acfb_cta_content_horizontal_align") === 'flex-end'): ?>
		text-align: right;
	<?php endif;?>	
}


.<?php echo $uid; ?> .acfb_cta_content .acfb_cta_title{
	<?php echo get_typo_field($acfb_cta_title_typo); ?>
	color: <?php the_field("acfb_cta_title_color"); ?>;
}

.<?php echo $uid; ?> .acfb_cta_content .acfb_cta_description p{
	<?php echo get_typo_field($acfb_cta_description_typo); ?>
	color: <?php the_field("acfb_cta_description_color"); ?>;
}

.<?php echo $uid; ?> .acfb_cta_content .acfb_cta_button_wrap .acfb_cta_button{
	background: <?php the_field("acfb_cta_button_background_color"); ?>;
	<?php echo get_typo_field($acfb_cta_button_typo); ?>
	color: <?php the_field("acfb_cta_button_text_color"); ?> !important;
}

.<?php echo $uid; ?> .acfb_cta_content .acfb_cta_button_wrap .acfb_cta_button:hover{
	background: <?php the_field("acfb_cta_button_background_hover_color"); ?>; 
	color: <?php the_field("acfb_cta_button_text_hover_color"); ?> !important;
}


.<?php echo $uid; ?> .acfb_cta_ribbon_wrap  .acfb_cta_ribbon{
	background: <?php the_field("acfb_cta_ribbon_background_color"); ?>; 
	color: <?php the_field("acfb_cta_ribbon_text_color"); ?>;
	<?php echo get_typo_field($acfb_cta_ribbon_typo); ?>
}
</style>

<?php
$acfb_image = '';
if(!get_field('acfb_cta_image')){
	$acfb_image = plugins_url() . '/acf-blocks/img/placeholder-image.jpg';
} else{
	$acfb_image = get_field('acfb_cta_image');
}
?>

<div class="<?php the_field('acfb_cta_skins'); ?> <?php the_field('acfb_cta_image_position'); ?>">
<div class="acfb_cta">
	<div class="acfb_cta_bg_wrapper">
		<div class="acfb_cta_bg" style="background-image: url(<?php echo $acfb_image; ?>);"></div>
		<div class="acfb_cta_bg_overlay"></div>
	</div>

	<div class="acfb_cta_content">

	  <?php if( get_field('acfb_cta_title') ): ?>
          <h2 class="acfb_cta_title acfb_cta_item"><?php the_field('acfb_cta_title'); ?></h2>
      <?php endif; ?>

      <?php if( get_field('acfb_cta_description') ): ?>
          <div class="acfb_cta_description acfb_cta_item">
			<p><?php the_field('acfb_cta_description'); ?></p>
		  </div>
      <?php endif; ?>

      <?php if( get_field('acfb_cta_button_text') ): ?>
          <div class="acfb_cta_button_wrap acfb_cta_item">
			<a href="<?php the_field('acfb_cta_button_url'); ?>" class="acfb_cta_button"><?php the_field('acfb_cta_button_text'); ?></a>
		  </div>
      <?php endif; ?>

	</div>

	<?php if( get_field('acfb_cta_ribbon_text') ): ?>
	     <div class="acfb_cta_ribbon_wrap <?php the_field('acfb_cta_ribbon_position'); ?>">
			<div class="acfb_cta_ribbon"><?php the_field('acfb_cta_ribbon_text'); ?></div>
		 </div>
    <?php endif; ?>

</div>
</div>



</div><!-- Uid -->