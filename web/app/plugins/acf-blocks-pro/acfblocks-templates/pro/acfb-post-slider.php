<?php 
echo parse_link(
    array(
        get_field('acfb_post_slider_title_typo'),
        get_field('acfb_post_slider_meta_typo'),
        get_field('acfb_post_slider_excerpt_typo'),
        get_field('acfb_post_slider_button_typo')
    )
);

$acfb_post_slider_inner_padding = acfb_padding_name('acfb_post_slider_inner_padding');
$acfb_post_slider_padding = acfb_padding_name('acfb_post_slider_padding');
$acfb_post_slider_margin = acfb_margin_name('acfb_post_slider_margin');
$acfb_post_slider_title_typo = acfb_ffaimly_name('acfb_post_slider_title_typo');
$acfb_post_slider_meta_typo = acfb_ffaimly_name('acfb_post_slider_meta_typo');
$acfb_post_slider_excerpt_typo = acfb_ffaimly_name('acfb_post_slider_excerpt_typo');
$acfb_post_slider_button_typo = acfb_ffaimly_name('acfb_post_slider_button_typo');


$uid = $block['id'];

$className = 'acfb_posts_slider_block';
if( !empty($block['className']) ) {
   $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
   $className .= ' align' . $block['align'];
}
?>

<div class="<?php echo $uid; ?> <?php echo esc_attr($className); ?>">

<style type="text/css">
.<?php echo $uid; ?> {
	<?php echo get_padding_field($acfb_post_slider_padding); ?>
	<?php echo get_margin_field($acfb_post_slider_margin); ?>
}	

<?php if(get_field('acfb_post_slider_autoheight') === false): ?>
.<?php echo $uid; ?> .acfb_post_slider_item{
	min-height: <?php the_field('acfb_post_slider_height'); ?>px;
	padding-top: <?php echo $acfb_post_slider_inner_padding['padding_top']; ?>px;
    padding-bottom: <?php echo $acfb_post_slider_inner_padding['padding_bottom']; ?>px;
    padding-left: <?php echo $acfb_post_slider_inner_padding['padding_left']; ?>px;
    padding-right: <?php echo $acfb_post_slider_inner_padding['padding_right']; ?>px;

}
<?php endif; ?>


.<?php echo $uid; ?> .acfb_posts_slider_top_elements{
	display: flex;
	align-items: <?php the_field('acfb_post_slider_top_elements_h_align'); ?>;
	justify-content: <?php the_field('acfb_post_slider_top_elements_v_align'); ?>;
	flex-direction: column;
	<?php if(get_field('acfb_post_slider_top_elements_h_align') == 'center'){ ?>
		text-align: center;
	<?php } ?>
}

.<?php echo $uid; ?> .acfb_posts_slider_center_elements{
	display: flex;
	align-items: <?php the_field('acfb_post_slider_center_elements_h_align'); ?>;
	justify-content: <?php the_field('acfb_post_slider_center_elements_v_align'); ?>;
	flex-direction: column;
	<?php if(get_field('acfb_post_slider_center_elements_h_align') == 'center'){ ?>
		text-align: center;
	<?php } ?>
}

.<?php echo $uid; ?> .acfb_posts_slider_bottom_elements{
	display: flex;
	align-items: <?php the_field('acfb_post_slider_bottom_elements_h_align'); ?>;
	justify-content: <?php the_field('acfb_post_slider_bottom_elements_v_align'); ?>;
	flex-direction: column;
	<?php if(get_field('acfb_post_slider_bottom_elements_h_align') == 'center'){ ?>
		text-align: center;
	<?php } ?>
}



<?php if( get_field('acfb_post_slider_title_custom_typography') == '1' ): ?>
.<?php echo $uid; ?> .acfb_post_slider_title a{
	<?php echo get_typo_field($acfb_post_slider_title_typo); ?>
}
<?php endif; ?>


<?php if( get_field('acfb_post_slider_title_custom_color') == '1' ): ?>
.<?php echo $uid; ?> .acfb_post_slider_title a{
	color: <?php the_field('acfb_post_slider_title_color'); ?> !important;
}

.<?php echo $uid; ?> .acfb_post_slider_title a:hover{
	color: <?php the_field('acfb_post_slider_title_hover_color'); ?> !important;
}
<?php endif; ?>




<?php if( get_field('acfb_post_slider_meta_custom_typography') == '1' ): ?>
.<?php echo $uid; ?> .acfb_post_slider_meta{
	<?php echo get_typo_field($acfb_post_slider_meta_typo); ?>
}
<?php endif; ?>


<?php if( get_field('acfb_post_slider_meta_custom_color') == '1' ): ?>
.<?php echo $uid; ?> .acfb_post_slider_meta{
	color: <?php the_field('acfb_post_slider_meta_color'); ?>;
}
<?php endif; ?>




<?php if( get_field('acfb_post_slider_excerpt_custom_typography') == '1' ): ?>
.<?php echo $uid; ?> .acfb_post_slider_excerpt{
	<?php echo get_typo_field($acfb_post_slider_excerpt_typo); ?>
}
<?php endif; ?>

<?php if( get_field('acfb_post_slider_excerpt_custom_color') == '1' ): ?>
.<?php echo $uid; ?> .acfb_post_slider_excerpt{
	color: <?php the_field('acfb_post_slider_excerpt_color'); ?>;
}
<?php endif; ?>





<?php if( get_field('acfb_post_slider_button_custom_typography') == '1' ): ?>
.<?php echo $uid; ?> .acfb_post_slider_button a{
	<?php echo get_typo_field($acfb_post_slider_button_typo); ?>
}
<?php endif; ?>

<?php if( get_field('acfb_post_slider_button_custom_color') == '1' ): ?>
.<?php echo $uid; ?> .acfb_post_slider_button a{
	background-color: <?php the_field('acfb_post_slider_button_background_color'); ?>;
	color: <?php the_field('acfb_post_slider_button_text_color'); ?> !important;
	padding: 10px;
}

.<?php echo $uid; ?> .acfb_post_slider_button a:hover{
	background-color: <?php the_field('acfb_post_slider_button_background_hover_color'); ?>;
	color: <?php the_field('acfb_post_slider_button_text_hover_color'); ?> !important;
}
<?php endif; ?>


.<?php echo $uid; ?> .swiper-pagination .swiper-pagination-bullet{
	background: <?php the_field('acfb_post_slider_pagination_unactive_color_copy'); ?>;
}

.<?php echo $uid; ?> .swiper-pagination .swiper-pagination-bullet-active{
	background: <?php the_field('acfb_post_slider_pagination_active_color'); ?>;
}

.<?php echo $uid; ?> .acfb_post_slider_navigation .acfb_button_prev, .<?php echo $uid; ?> .acfb_post_slider_navigation .acfb_button_next{
	background: <?php echo hex2rgba(get_field('acfb_post_slider_navigation_background_color'), 0.7); ?>;
}

.<?php echo $uid; ?> .acfb_post_slider_navigation .acfb_button_prev svg path, .<?php echo $uid; ?> .acfb_post_slider_navigation .acfb_button_next svg path{
	fill: <?php the_field('acfb_post_slider_navigation_color'); ?>;
}
</style>


<div class="swiper-container" data-psslidesperview="<?php the_field("acfb_post_slider_spv"); ?>" data-psdelay="<?php the_field("acfb_post_slider_delay"); ?>" data-pseffect="<?php the_field("acfb_post_slider_teffect"); ?>" data-pscenterslide="<?php the_field("acfb_post_slider_centerslide"); ?>" data-psgutter="<?php the_field("acfb_post_slider_gutter"); ?>" data-psautoplay="<?php the_field("acfb_post_slider_autoplay"); ?>" data-psautoheight="<?php the_field("acfb_post_slider_autoheight"); ?>">
    <div class="swiper-wrapper">

<?php
$acfb_post_slider_args = array(
	'post_type' => get_field('acfb_post_type'),
	'post_status' => 'publish',
	'posts_per_page' => get_field('acfb_post_slider_nop'), 
);


// the query
$acfb_post_slider_query = new WP_Query( $acfb_post_slider_args ); ?>

<?php if ( $acfb_post_slider_query->have_posts() ) : ?>


	<?php while ( $acfb_post_slider_query->have_posts() ) : $acfb_post_slider_query->the_post(); ?>

		<div class="swiper-slide">
			
		<?php
		$acfb_post_slider_excerpt_length = get_field('acfb_post_slider_excerpt_length');

		$acfb_tile_overlay = hex2rgba(get_field('acfb_post_slider_tile_overlay_color'), get_field('acfb_post_slider_tile_overlay_opacity'));

		$acfb_pslider_itembg = '';
		if(get_field('acfb_post_slider_layout') === 'acfb_post_slider_card'){
		    $acfb_pslider_itembg = get_field('acfb_post_slider_bg_color');
		} else{
		    $acfb_pslider_itembg = 'linear-gradient(' . $acfb_tile_overlay . ', ' . $acfb_tile_overlay . '), url(' . get_the_post_thumbnail_url() . ')';
		}
		?>

		<div class="<?php the_field('acfb_post_slider_layout'); ?> acfb_post_slider_item" style="background: <?php echo $acfb_pslider_itembg; ?>;">

		<!-- Top Elements -->
		<?php if( have_rows('acfb_post_slider_top_elements') ):?>
		<div class="acfb_posts_slider_top_elements">
		<?php while ( have_rows('acfb_post_slider_top_elements') ) : the_row(); ?>

		    <?php if( get_row_layout() == 'post_slider_image' ): ?>
		        <div class="acfb_post_slider_thumbnail">  
		            <?php the_post_thumbnail(); ?>
		        </div>
		    <?php endif; ?>

		    <?php if( get_row_layout() == 'post_slider_title' ): ?>
		    	<div class="acfb_post_slider_title">	
		       		 <<?php the_field('acfb_post_slider_title_html_tag'); ?>><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></<?php the_field('acfb_post_slider_title_html_tag'); ?>>
		   		</div>
		    <?php endif; ?>

		    <?php if( get_row_layout() == 'post_slider_meta_data' ): ?>
		    	<div class="acfb_post_slider_meta">
		    		<span class="acfb_post_slider_author"><?php the_author(); ?></span> -
					<span class="acfb_post_slider_date"><?php the_time('F jS, Y') ?></span>
				</div>
		    <?php endif; ?>
		      
		    <?php if( get_row_layout() == 'post_slider_content' ): ?>
		    	<div class="acfb_post_slider_excerpt">
		        	<?php echo acfb_excerpt($acfb_post_slider_excerpt_length); ?>
		    	</div>
		    <?php endif; ?>

			<?php if( get_row_layout() == 'post_slider_read_more_button' ): ?>
				<div class="acfb_post_slider_button">
					<a href="<?php the_permalink(); ?>" class="acfb_post_slider_btn">Read More</a>
				</div>
		    <?php endif; ?>

		<?php endwhile; ?>
		</div>
		<?php endif; ?>


		<!-- Center Elements -->
		<?php if( have_rows('acfb_post_slider_center_elements') ):?>
		<div class="acfb_posts_slider_center_elements">
		<?php while ( have_rows('acfb_post_slider_center_elements') ) : the_row(); ?>

		    <?php if( get_row_layout() == 'post_slider_image' ): ?>
		        <div class="acfb_post_slider_thumbnail">  
		            <?php the_post_thumbnail(); ?>
		        </div>
		    <?php endif; ?>

		    <?php if( get_row_layout() == 'post_slider_title' ): ?>
		    	<div class="acfb_post_slider_title">	
		       		 <<?php the_field('acfb_post_slider_title_html_tag'); ?>><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></<?php the_field('acfb_post_slider_title_html_tag'); ?>>
		   		</div>
		    <?php endif; ?>

		    <?php if( get_row_layout() == 'post_slider_meta_data' ): ?>
		    	<div class="acfb_post_slider_meta">
		    		<span class="acfb_post_slider_author"><?php the_author(); ?></span> -
					<span class="acfb_post_slider_date"><?php the_time('F jS, Y') ?></span>
				</div>
		    <?php endif; ?>
		      
		    <?php if( get_row_layout() == 'post_slider_content' ): ?>
		    	<div class="acfb_post_slider_excerpt">
		        	<?php echo acfb_excerpt($acfb_post_slider_excerpt_length); ?>
		    	</div>
		    <?php endif; ?>

			<?php if( get_row_layout() == 'post_slider_read_more_button' ): ?>
				<div class="acfb_post_slider_button">
					<a href="<?php the_permalink(); ?>" class="acfb_post_slider_btn">Read More</a>
				</div>
		    <?php endif; ?>

		<?php endwhile; ?>
		</div>
		<?php endif; ?>


		<!-- Bottom Elements -->
		<?php if( have_rows('acfb_post_slider_bottom_elements') ):?>
		<div class="acfb_posts_slider_bottom_elements">
		<?php while ( have_rows('acfb_post_slider_bottom_elements') ) : the_row(); ?>

		    <?php if( get_row_layout() == 'post_slider_image' ): ?>
		        <div class="acfb_post_slider_thumbnail">  
		            <?php the_post_thumbnail(); ?>
		        </div>
		    <?php endif; ?>

		    <?php if( get_row_layout() == 'post_slider_title' ): ?>
		    	<div class="acfb_post_slider_title">	
		       		 <<?php the_field('acfb_post_slider_title_html_tag'); ?>><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></<?php the_field('acfb_post_slider_title_html_tag'); ?>>
		   		</div>
		    <?php endif; ?>

		    <?php if( get_row_layout() == 'post_slider_meta_data' ): ?>
		    	<div class="acfb_post_slider_meta">
		    		<span class="acfb_post_slider_author"><?php the_author(); ?></span> -
					<span class="acfb_post_slider_date"><?php the_time('F jS, Y') ?></span>
				</div>
		    <?php endif; ?>
		      
		    <?php if( get_row_layout() == 'post_slider_content' ): ?>
		    	<div class="acfb_post_slider_excerpt">
		        	<?php echo acfb_excerpt($acfb_post_slider_excerpt_length); ?>
		    	</div>
		    <?php endif; ?>

			<?php if( get_row_layout() == 'post_slider_read_more_button' ): ?>
				<div class="acfb_post_slider_button">
					<a href="<?php the_permalink(); ?>" class="acfb_post_slider_btn">Read More</a>
				</div>
		    <?php endif; ?>

		<?php endwhile; ?>
		</div>
		<?php endif; ?>

		</div>
			
		</div>

	<?php endwhile; ?>

<?php endif; ?>

	</div>

<?php if(get_field("acfb_post_slider_dots") == 'true'): ?><div class="swiper-pagination"></div><?php endif; ?>

 <?php if(get_field("acfb_post_slider_arrow") == 'true'): ?>
	<div class="acfb_post_slider_navigation">
		<div class="acfb_button_prev"><svg enable-background="new 0 0 477.175 477.175" version="1.1" viewBox="0 0 477.18 477.18" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
				<path d="m145.19 238.58 215.5-215.5c5.3-5.3 5.3-13.8 0-19.1s-13.8-5.3-19.1 0l-225.1 225.1c-5.3 5.3-5.3 13.8 0 19.1l225.1 225c2.6 2.6 6.1 4 9.5 4s6.9-1.3 9.5-4c5.3-5.3 5.3-13.8 0-19.1l-215.4-215.5z"/>
		</svg>
		</div>
		<div class="acfb_button_next">
			<svg enable-background="new 0 0 477.175 477.175" version="1.1" viewBox="0 0 477.18 477.18" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
			<path d="m360.73 229.08-225.1-225.1c-5.3-5.3-13.8-5.3-19.1 0s-5.3 13.8 0 19.1l215.5 215.5-215.5 215.5c-5.3 5.3-5.3 13.8 0 19.1 2.6 2.6 6.1 4 9.5 4s6.9-1.3 9.5-4l225.1-225.1c5.3-5.2 5.3-13.8 0.1-19z"/>
		</svg>
		</div>
	</div>
<?php endif; ?>


</div>

</div><!-- Uid -->