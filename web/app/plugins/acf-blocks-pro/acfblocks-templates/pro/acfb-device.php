<?php 

$acfb_device_padding = acfb_padding_name('acfb_device_padding');
$acfb_device_margin = acfb_margin_name('acfb_device_margin');

$uid = $block['id'];

$className = 'acfb_device_block';
if( !empty($block['className']) ) {
   $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
   $className .= ' align' . $block['align'];
}

?>
<div class="<?php echo $uid; ?> <?php echo esc_attr($className); ?>">
<style type="text/css">
.<?php echo $uid; ?> {
  <?php echo get_padding_field($acfb_device_padding); ?>
  <?php echo get_margin_field($acfb_device_margin); ?>
}

.<?php echo $uid; ?>.acfb_device_block{
	justify-content: <?php the_field('acfb_device_alignment'); ?>;
}

.<?php echo $uid; ?> .acfb_device_type_<?php the_field('acfb_device_type'); ?>{
	width: <?php the_field('acfb_device_width'); ?>%;
}

.<?php echo $uid; ?> .acfb_device_type_<?php the_field('acfb_device_type'); ?> .acfb_device_media img{
	object-position: <?php the_field('acfb_device_image_vposition'); ?>% <?php the_field('acfb_device_image_hposition'); ?>%;
}
</style>

<?php
$acfb_image = '';
if(!get_field('acfb_device_image')){
	$acfb_image = plugins_url() . '/acf-blocks/img/placeholder-image.jpg';
} else{
	$acfb_image = get_field('acfb_device_image');
}
?>

<div class="acfb_device_type_<?php the_field('acfb_device_type'); ?>">
	<div class="acfb_device">
		<div class="acfb_device_body">
			<?php if(get_field('acfb_device_type') == 'tablet'){ ?>
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 430 595" class="pp-devices-svg"> <rect class="back-shape frame" fill="#000000" x="0" y="0" width="430" height="595" rx="20" ry="20" vector-effect="non-scaling-stroke"></rect> <circle class="overlay-shape home-button" fill="#6b6a6a" cx="215" cy="574" r="10" vector-effect="non-scaling-stroke"></circle> <circle class="overlay-shape camera" fill="#6b6a6a" cx="211" cy="20" r="4" vector-effect="non-scaling-stroke"></circle> </svg>
			<?php } elseif (get_field('acfb_device_type') == 'mobile'){ ?>
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 277 574" class="pp-devices-svg"> <rect class="back-shape frame" fill="#000000" x="3" y="0" width="271" height="574" rx="35" ry="35" vector-effect="non-scaling-stroke"></rect> <circle class="overlay-shape home-button" fill="#6b6a6a" cx="139" cy="538" r="20" vector-effect="non-scaling-stroke"></circle> <circle class="overlay-shape camera" fill="#6b6a6a" cx="95" cy="37" r="4" vector-effect="non-scaling-stroke"></circle> <rect class="overlay-shape speaker" fill="#6b6a6a" x="116" y="35" width="45" height="5" rx="4.5" vector-effect="non-scaling-stroke"></rect> <rect class="side-shape button-sound" fill="#000000" x="1" y="71" width="2" height="18" vector-effect="non-scaling-stroke"></rect> <rect class="side-shape button-plus" fill="#000000" x="0" y="120" width="3" height="32" vector-effect="non-scaling-stroke"></rect> <rect class="side-shape button-minus" fill="#000000" x="0" y="163" width="3" height="32" vector-effect="non-scaling-stroke"></rect> <rect class="side-shape color-normal button-on" fill="#000000" x="274" y="120" width="3" height="32" vector-effect="non-scaling-stroke"></rect> </svg>
			<?php } else{ ?>
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 645 370" class="pp-devices-svg"> <rect class="back-shape frame" fill="#000000" x="59" y="0" width="527" height="357" rx="20"></rect> <rect class="back-shape base" fill="#242424" x="0" y="350" width="645" height="11"></rect> <rect class="overlay-shape base-tint" fill="#6b6a6a" x="0" y="350" width="645" height="11"></rect> <path class="overlay-shape dent" d="M277,350 L368,350 L368,354 C368,356.209139 366.202696,358 364.001926,358 L280.998074,358 C278.789999,358 277,356.204644 277,354 L277,350 Z" fill="#383838"></path> <circle class="overlay-shape camera" fill="#383838" cx="323" cy="11" r="3"></circle> <path class="back-shape bottom" d="M0,361 C0,361 3.91836515,370 37.3210755,370 L320.849959,370 L324.150041,370 L607.694382,370 C641.097092,370 645,361 645,361 L0,361 Z" fill="#000000"></path></svg>
			<?php } ?>	
		</div>
		<div class="acfb_device_media">
			<img src="<?php echo $acfb_image; ?>" alt="<?php the_field('acfb_device_image_alt'); ?>">	
		</div>
	</div>
</div>

</div><!-- Uid -->