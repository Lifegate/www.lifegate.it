<div class="wrap admin-banner-options">
  <h2><?php _e( 'Banner Options', TBM_ADVERTISING_LANGUAGE_PLUGIN ) ?></h2>
  <div class='form-tbm-advertising' >
    <?php
	    wp_nonce_field( 'tbm-advertising-options', 'tbm-advertising-options' );
	    settings_fields('tbm-advertising-options-group');

    	global $wpdb;
    	$banners = $wpdb->get_results("
			SELECT *
			FROM " . TBM_Advertising::$table_banners . "
			ORDER BY banner_label
		");
    $tbm_advertising_v_1_7=get_option('tbm_advertising_v_1_7');
    ?>
    <div class="behavior-type">
      <p>
        <span><?php _e( 'Enable new behavior', TBM_ADVERTISING_LANGUAGE_PLUGIN ) ?></span>
        <span class="switch_on_off">
          <input id="switch_on_off_newmpadv" class="cmn-toggle toggle_switch_on_off cmn-toggle-round"
          type="checkbox" <?= ($tbm_advertising_v_1_7)? "checked" : null; ?>>
          <label for="switch_on_off_newmpadv"></label>
        </span>
      </p>

      <p>
        <small><?php _e( 'Categories archive banners and Taxonomies archive banners not used "is_single()". You can use POST BY TAXONOMIES.', TBM_ADVERTISING_LANGUAGE_PLUGIN ) ?></small>
      </p>
    </div>
    
    <?php 
      $taxonomies = get_taxonomies();
      $disabled_taxonomies=array('nav_menu','link_category','post_format','post_status','ef_usergroup','following_users');
      if ( !is_wp_error( $taxonomies ) ) {
    ?>
      <div class="behavior-type">
        <p><span><?php _e( 'Topic tax', TBM_ADVERTISING_LANGUAGE_PLUGIN ) ?></span></p>
        <?php 
          $tbm_advertising_topic_tax=get_option('tbm_advertising_topic_tax');
          if(empty($tbm_advertising_topic_tax) || false==$tbm_advertising_topic_tax){
            $tbm_advertising_topic_tax=array('speciale','category','post_tag');
          }
        ?>
        <select multiple="multiple" id="topic-tax" name="topic-tax[]" style='width:100%'>
          <?php 
          foreach( $tbm_advertising_topic_tax as $k=>$taxonomy ) {
             echo sprintf( '<option value="%s" selected>%s</option>', $taxonomy, $taxonomy );
             $disabled_taxonomies[]=$taxonomy;
          }
          foreach( $taxonomies as $k=>$taxonomy ) {
                if(!in_array($taxonomy,$disabled_taxonomies)){
                    echo sprintf( '<option value="%s">%s</option>', $k, $taxonomy );
                }
            } ?>
        </select>
        <span type='button' class='btn save-topic-tax'><?php _e( 'Save Topic tax', TBM_ADVERTISING_LANGUAGE_PLUGIN ) ?></span>
        <script>
          jQuery(document).ready(function(){    
            jQuery('#topic-tax').chosen();
          });
        </script>
      </div>
    <?php } ?>

    <p for="list-allowed"><?php _e( 'List allowed banners', TBM_ADVERTISING_LANGUAGE_PLUGIN ) ?></p>
    <table class='list-allowed' >
    	<thead>
    		<tr>
    			<th></th>
    			<th><?php _e( 'Banner label', TBM_ADVERTISING_LANGUAGE_PLUGIN ) ?></th>
    			<th class='max_width_td'><?php _e( 'Banner description', TBM_ADVERTISING_LANGUAGE_PLUGIN ) ?></th>
    			<th><?php _e( 'Banner type', TBM_ADVERTISING_LANGUAGE_PLUGIN ) ?></th>
    		</tr>
    	</thead>
    	<tbody>
    		<?php
    			foreach( $banners as $banner ) {
				?>
					<tr data-label='<?php echo $banner->banner_label ?>' data-message='<?php echo sprintf(__( 'Do you want to delete "%s" banner?', TBM_ADVERTISING_LANGUAGE_PLUGIN ),$banner->banner_label )?>'>
		    			<td class='delete_this_banner' <?php _e( 'Delete banner', TBM_ADVERTISING_LANGUAGE_PLUGIN ) ?>><span class="dashicons dashicons-no"></span></td>
		    			<td><?php echo $banner->banner_label ?></td>
		    			<td class='max_width_td'><?php echo (!empty($banner->banner_desc))? $banner->banner_desc : "/" ?></td>
		    			<td><?php echo $banner->banner_type ?></td>
		    		</tr>
				<?php
    			}
    		?>
    	</tbody>
    	<tfoot>
    		<tr>
    			<td class='add_this_banner' data-message='<?php echo __( 'Are you sure you want to add this banner?', TBM_ADVERTISING_LANGUAGE_PLUGIN ) ?>' title='<?php _e( 'Add new banner', TBM_ADVERTISING_LANGUAGE_PLUGIN ) ?>'><span class="dashicons dashicons-yes"></span></td>
    			<td><input type='text' id='input_banner_label' placeholder='<?php _e( 'Banner label', TBM_ADVERTISING_LANGUAGE_PLUGIN ) ?>' /></td>
    			<td class='max_width_td'><input type='text' id='input_banner_desc' placeholder='<?php _e( 'Banner description', TBM_ADVERTISING_LANGUAGE_PLUGIN ) ?>'/></td>
    			<td>
    			<select id='input_banner_type'>
    				<option value='code'>code</option>
    				<option value='banner'>banner</option>
    				<option value='premium'>premium</option>
    			</select>
    			</td>
    		</tr>
    	</tfoot>
    </table>
	<script>
		jQuery(document).ready(function(){
			jQuery('input#input_banner_label').keyup(function () {
			jQuery(this).val(banner_label_uppercase(jQuery(this).val()));
			})
		});
		function banner_label_uppercase(value){
			var new_value=value.toUpperCase().replace(" ", "_").trim().replace(/([~!@#$%^&*()-+=`{}\[\]\|\\:;'<>,.\/?])+/g, '_');
			return new_value;
		}
	</script>
	<div class="language-php">
	<p><?php _e( 'Example of usage', TBM_ADVERTISING_LANGUAGE_PLUGIN ) ?></p>
<pre>/**
  * Outputs a banner content
  *
  * @param name, string, optional, The banner name
  * @param before, string, optional, An optional text to prepend to banner content
  * @param after, string, optional, An optional text to append to banner content
  * @param echo, boolean, optional, True to echo result
  * @param disabled_before_after, boolean, optional, True to print only the banner content
**/</pre>
<div class="textarea language-php" readonly="readonly">&lt;?php
	if ( class_exists('TBM_Advertising') ) {
		$banner = TBM_Advertising::output("BANNER_LABEL", "&lt;p&gt;BEFORE&lt;/p&gt;","&lt;p&gt;AFTER&lt;/p&gt;", false, false);
		if ( false !== $banner ) { echo $banner; }
	}

	/*****  OR  *****/

	if ( class_exists('TBM_Advertising') ) {
		TBM_Advertising::output("BANNER_LABEL");
	}
?&gt;</div>
	</div>
  </div>

</div>