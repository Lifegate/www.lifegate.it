<div class="wrap admin-banner-options">
  <h2><?php _e( 'Banner Management', TBM_ADVERTISING_LANGUAGE_PLUGIN ) ?></h2>
  <div class='form-tbm-advertising' method="post" action="options-general.php?page=tbm-advertising">
    <?php
    wp_nonce_field( 'tbm-advertising-options', 'tbm-advertising-options' );
    settings_fields('tbm-advertising-options-group');
    ?>

    <p for="select-zone"><?php _e( 'Select the area where you can set the banner', TBM_ADVERTISING_LANGUAGE_PLUGIN ) ?></p>
    <div class='select-zone-container'>
      <div class='select-zone-content'>
        <div class='select-zone' data-value="page"><?php _e( 'Pages', TBM_ADVERTISING_LANGUAGE_PLUGIN ) ?></div>
        <div class='select-zone' data-value="tax"><?php _e( 'Taxonomies', TBM_ADVERTISING_LANGUAGE_PLUGIN ) ?></div>
      </div>
      <div class='select-zone global' data-value="global"><?php _e( 'Global', TBM_ADVERTISING_LANGUAGE_PLUGIN ) ?></div>
      <div class='select-zone-content right'>
        <div class='select-zone' data-value="types"><?php _e( 'Post', TBM_ADVERTISING_LANGUAGE_PLUGIN ) ?></div>
        <div class='select-zone' data-value="link"><?php _e( 'Link', TBM_ADVERTISING_LANGUAGE_PLUGIN ) ?></div>
      </div>
    </div>
    <div class='zone-options-container'></div>
    <div class='zone-secondary-container'></div>
    <div class='zone-tertiary-container'></div>
    <div class='zone-container'></div>
    <p class="submit">
    <input type="submit" id='tbm-advertising-submit' class="button-primary" value="<?php _e( 'Save options', TBM_ADVERTISING_LANGUAGE_PLUGIN ) ?>" />
    </p>
  </div>
</div>
<input type='hidden' id='message_saved_succesfully' value="<?php _e( 'Data saved successfully', TBM_ADVERTISING_LANGUAGE_PLUGIN ) ?>">
<input type='hidden' id='message_confirm_delete_page' value="<?php _e( 'Do you want to delete all banners for this url?', TBM_ADVERTISING_LANGUAGE_PLUGIN ) ?>">