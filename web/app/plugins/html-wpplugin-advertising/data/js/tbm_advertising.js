tbm_advertising = new Object();
tbm_advertising.init = function() {
  tbm_advertising.adminBannerOptions = jQuery('.admin-banner-options');

  jQuery('.select-zone', tbm_advertising.adminBannerOptions).on('click', tbm_advertising.selectZoneChange);
  jQuery('#tbm-advertising-submit', tbm_advertising.adminBannerOptions).on('click', tbm_advertising.saveBannersPerZone);

  jQuery('.page-select', tbm_advertising.adminBannerOptions).live('change', tbm_advertising.pageSelectHandler);
  jQuery('.taxonomy-select', tbm_advertising.adminBannerOptions).live('change', tbm_advertising.taxonomySelectHandler);
  jQuery('.term-select', tbm_advertising.adminBannerOptions).live('change', tbm_advertising.termOfTaxonomySelectHandler);

  jQuery('.post-post-select', tbm_advertising.adminBannerOptions).live('change', tbm_advertising.postSelectHandler);
  jQuery('.types-select', tbm_advertising.adminBannerOptions).live('change', tbm_advertising.typesSelectHandler);
  jQuery('.taxonomy-type-select', tbm_advertising.adminBannerOptions).live('change', tbm_advertising.taxsSelectHandler);
  jQuery('.type-term-select', tbm_advertising.adminBannerOptions).live('change', tbm_advertising.termsSelectHandler);
  jQuery('.submit_post', tbm_advertising.adminBannerOptions).live('click', tbm_advertising.typeSelectHandler);
  jQuery('.save-topic-tax', tbm_advertising.adminBannerOptions).live('click', tbm_advertising.saveTopicTax);
  jQuery('.post-select', tbm_advertising.adminBannerOptions).live('change', tbm_advertising.updateSingleSelectTypeView);

  jQuery('.toggle_switch_on_off', tbm_advertising.adminBannerOptions).live('click', tbm_advertising.toggleSwitchOnOffBanner);

  jQuery('.link-add', tbm_advertising.adminBannerOptions).live('click', tbm_advertising.addLinkClickHandler);
  jQuery('.link-mod', tbm_advertising.adminBannerOptions).live('click', tbm_advertising.modLinkClickHandler);
  jQuery('.link-del', tbm_advertising.adminBannerOptions).live('click', tbm_advertising.deleteLinkBannersHandler);
  jQuery('.link-all', tbm_advertising.adminBannerOptions).live('click', tbm_advertising.allLinkClickHandler);

  jQuery('.delete_this_banner', tbm_advertising.adminBannerOptions).live('click', tbm_advertising.optionsPageDeleteBanners);
  jQuery('.add_this_banner', tbm_advertising.adminBannerOptions).live('click', tbm_advertising.optionsPageSetBanners);
};

// live/on 
tbm_advertising.selectZoneChange = function() {
  var zone = jQuery(this).data('value');
  jQuery('.select-zone').removeClass('selected');
  jQuery(this).addClass('selected');
  tbm_advertising.ajaxStart();
  switch (zone) {
    case 'cat':
      jQuery.post(ajaxurl, {
        action: 'tbm_advertising.updateCatView'
      }, function(response) {
        if (response.match(/^Error:/) != null) {
          tbm_advertising.log(response.replace('Error:', ''));
        }
        else {
          options_container = jQuery('.form-tbm-advertising .zone-options-container', tbm_advertising.adminBannerOptions);
          secondary_container = jQuery('.form-tbm-advertising .zone-secondary-container', tbm_advertising.adminBannerOptions);
          tertiary_container = jQuery('.form-tbm-advertising .zone-tertiary-container', tbm_advertising.adminBannerOptions);
          zone_container = jQuery('.form-tbm-advertising .zone-container', tbm_advertising.adminBannerOptions);
          jQuery(options_container).html(response);
          jQuery(zone_container).html("");
          jQuery(secondary_container).html("");
          jQuery(tertiary_container).html("");
        }
      }).always(tbm_advertising.ajaxStop2);
      break;
    case 'page':
      jQuery.post(ajaxurl, {
        action: 'tbm_advertising.updatePageView'
      }, function(response) {
        if (response.match(/^Error:/) != null) {
          tbm_advertising.log(response.replace('Error:', ''));
        }
        else {
          options_container = jQuery('.form-tbm-advertising .zone-options-container', tbm_advertising.adminBannerOptions);
          secondary_container = jQuery('.form-tbm-advertising .zone-secondary-container', tbm_advertising.adminBannerOptions);
          tertiary_container = jQuery('.form-tbm-advertising .zone-tertiary-container', tbm_advertising.adminBannerOptions);
          zone_container = jQuery('.form-tbm-advertising .zone-container', tbm_advertising.adminBannerOptions);
          jQuery(options_container).html(response);
          jQuery(zone_container).html("");
          jQuery(secondary_container).html("");
          jQuery(tertiary_container).html("");
        }
      }).always(tbm_advertising.ajaxStop2);
      break;
    case 'tax':
      jQuery.post(ajaxurl, {
        action: 'tbm_advertising.updateTaxView'
      }, function(response) {
        if (response.match(/^Error:/) != null) {
          tbm_advertising.log(response.replace('Error:', ''));
        }
        else {
          options_container = jQuery('.form-tbm-advertising .zone-options-container', tbm_advertising.adminBannerOptions);
          secondary_container = jQuery('.form-tbm-advertising .zone-secondary-container', tbm_advertising.adminBannerOptions);
          tertiary_container = jQuery('.form-tbm-advertising .zone-tertiary-container', tbm_advertising.adminBannerOptions);
          zone_container = jQuery('.form-tbm-advertising .zone-container', tbm_advertising.adminBannerOptions);
          jQuery(options_container).html(response);
          jQuery(zone_container).html("");
          jQuery(secondary_container).html("");
          jQuery(tertiary_container).html("");
        }
      }).always(tbm_advertising.ajaxStop2);
      break;
    case 'tag':
      jQuery.post(ajaxurl, {
        action: 'tbm_advertising.updateTagView'
      }, function(response) {
        if (response.match(/^Error:/) != null) {
          tbm_advertising.log(response.replace('Error:', ''));
        }
        else {
          options_container = jQuery('.form-tbm-advertising .zone-options-container', tbm_advertising.adminBannerOptions);
          secondary_container = jQuery('.form-tbm-advertising .zone-secondary-container', tbm_advertising.adminBannerOptions);
          tertiary_container = jQuery('.form-tbm-advertising .zone-tertiary-container', tbm_advertising.adminBannerOptions);
          zone_container = jQuery('.form-tbm-advertising .zone-container', tbm_advertising.adminBannerOptions);
          jQuery(options_container).html(response);
          jQuery(zone_container).html("");
          jQuery(secondary_container).html("");
          jQuery(tertiary_container).html("");
        }
      }).always(tbm_advertising.ajaxStop2);
      break;
    case 'types':
      jQuery.post(ajaxurl, {
        action: 'tbm_advertising.updatePostView'
      }, function(response) {
        if (response.match(/^Error:/) != null) {
          tbm_advertising.log(response.replace('Error:', ''));
        }
        else {
          options_container = jQuery('.form-tbm-advertising .zone-options-container', tbm_advertising.adminBannerOptions);
          secondary_container = jQuery('.form-tbm-advertising .zone-secondary-container', tbm_advertising.adminBannerOptions);
          tertiary_container = jQuery('.form-tbm-advertising .zone-tertiary-container', tbm_advertising.adminBannerOptions);
          zone_container = jQuery('.form-tbm-advertising .zone-container', tbm_advertising.adminBannerOptions);
          jQuery(options_container).html(response);
          jQuery(zone_container).html("");
          jQuery(secondary_container).html("");
          jQuery(tertiary_container).html("");
        }
      }).always(tbm_advertising.ajaxStop2);
      break;
    case 'link':
      jQuery.post(ajaxurl, {
        action: 'tbm_advertising.updateLinkView'
      }, function(response) {
        if (response.match(/^Error:/) != null) {
          tbm_advertising.log(response.replace('Error:', ''));
        }
        else {
          options_container = jQuery('.form-tbm-advertising .zone-options-container', tbm_advertising.adminBannerOptions);
          secondary_container = jQuery('.form-tbm-advertising .zone-secondary-container', tbm_advertising.adminBannerOptions);
          tertiary_container = jQuery('.form-tbm-advertising .zone-tertiary-container', tbm_advertising.adminBannerOptions);
          zone_container = jQuery('.form-tbm-advertising .zone-container', tbm_advertising.adminBannerOptions);
          jQuery(options_container).html(response);
          jQuery(zone_container).html("");
          jQuery(secondary_container).html("");
          jQuery(tertiary_container).html("");
          jQuery('#tbm-advertising-submit').hide();
        }
      }).always(tbm_advertising.ajaxStop2);
      break;
    default:
      tbm_advertising.updateZoneView(zone, '', true, true, null);
  }
};
tbm_advertising.saveBannersPerZone = function() {
  var zone = tbm_advertising.adminBannerOptions.data('tbm-advertising-zone'),
    real_zone = zone,
    codes = new Array();
  zone = real_zone === 'link-add' ? jQuery('#link-page').val() : zone;

  jQuery('textarea', tbm_advertising.adminBannerOptions).each(function() {
    codes.push(jQuery(this).attr('name') + "<{[SEP]}>" + jQuery(this).val().replace(/&/g, '<[EAMP]>').replace(/\+/g, '<[PLUS]>').replace(' ', '<[WHITESPACE]>'));
  });
  var data = {
    zone: zone,
    codes: Base64.encode(codes.join('<[{SEP}]>')),
    action: 'tbm_advertising.saveBannersPerZone'
  };
  tbm_advertising.ajaxStart();
  jQuery.post(ajaxurl, data, function(response) {
    if (response.match(/^Error:/) != null) {
      tbm_advertising.log(response.replace('Error:', ''));
    } else {
      if (real_zone === 'link-add') {
        tbm_advertising.selectZoneChange.apply(jQuery('.select-zone.selected'));
      }
      alert(response == 'ok' ? jQuery('#message_saved_succesfully').val() : response);
    }
  }).always(tbm_advertising.ajaxStop);

  return false;
};

tbm_advertising.pageSelectHandler = function() {
  if(jQuery(this).val()=='homepage'){
    tbm_advertising.updateZoneView("home", '', false, true, null);
  } else {
    tbm_advertising.updateZoneView('page', jQuery(this).val(), false, true, null);
  }
};

tbm_advertising.taxonomySelectHandler = function() {
  if (jQuery(this).val() == "0") {
    tbm_advertising.updateZoneView('tax', jQuery(this).val(), false, true, null);
  }
  else {
    tbm_advertising.updateTermViewFromTax('tax', jQuery(this).val(), false, true);
  }
};

tbm_advertising.termOfTaxonomySelectHandler = function() {
  if(jQuery(this).data('term')=='category-term'){
    tbm_advertising.updateZoneView('cat', jQuery(this).val(), false, false, null);
  } else if(jQuery(this).data('term')=='post_tag-term'){
    tbm_advertising.updateZoneView('tag', jQuery(this).val(), false, false, null);
  } else {
    tbm_advertising.updateZoneView('tax', jQuery(this).data('term') + "-" + jQuery(this).val(), false, false, jQuery(this).data('term'));
  }
};

tbm_advertising.postSelectHandler = function() {
  if (jQuery(this).val() == "0") {
    tbm_advertising.updateZoneView('type', jQuery(this).val(), false, true, null);
  } else if(jQuery(this).val() == "by-post-type") {
    tbm_advertising.updatePostPostType('type', jQuery(this).val(), false, true);
  } else if(jQuery(this).val() == "by-tax") {
    tbm_advertising.updatePostTaxType('type', jQuery(this).val(), false, true);
  }
};

tbm_advertising.typesSelectHandler = function() {
    tbm_advertising.updateSearchPostType('type', jQuery(this).val(), false, true);
};

tbm_advertising.taxsSelectHandler = function() {
    tbm_advertising.updateSearchPostTaxType('type', 'tax-'+jQuery(this).val(), false, true);
};

tbm_advertising.termsSelectHandler = function() {
  tbm_advertising.updateZoneView('type', jQuery(this).data('term') + "-" + jQuery(this).val(), false, false, jQuery(this).data('term'));
};

tbm_advertising.saveTopicTax = function() {
  var ret=[];
  jQuery('#topic_tax_chosen').find('.search-choice').each(function(){
    var selectedValue = jQuery(this).find('span').text();
    ret.push(selectedValue);  
  })
  var data = {
    tax: ret,
    action: 'tbm_advertising.saveTopicTax'
  };
  jQuery('.behavior-type').css('opacity','0.4').css('pointer-events','none');
  jQuery.post(ajaxurl, data, function(response) {
    if (response.match(/^Error:/) != null) {
      tbm_advertising.log(response.replace('Error:', ''));
    } else {}
  }).always(function() {
    jQuery('.behavior-type').css('opacity',"").css('pointer-events','');
  });
};

tbm_advertising.typeSelectHandler = function() {
  var search_post = jQuery('#search_post').val();
  var type_post = jQuery('#search_post').data('type');
  if (typeof search_post !== "undefined" && search_post != "") {
    tbm_advertising.searchGetPostsList(search_post, type_post);
  }
};

tbm_advertising.updateSingleSelectTypeView = function(event) {
  event.preventDefault();
  event.stopPropagation();
  var post = jQuery(this);
  var post_val = post.data('type') + "-" + post.val();
  tbm_advertising.updateZoneView('type', post_val, false, false, jQuery(this).data('type'));
  return false;
};

tbm_advertising.toggleSwitchOnOffBanner = function(event) {
  var _switch = jQuery(this);
  var zone = _switch.data('zone');
  var banner = _switch.data('banner');
  var checked = _switch.is(':checked') ? 1 : 0;
  _switch.parent().addClass('disabled');
  jQuery('textarea[name="' + banner + '"]').attr('disabled', 'disabled');
  /*
  console.log("zone: "+ zone);
  console.log("banner: "+ banner);
  console.log("checked: "+ checked);
  console.log("=======");*/

  var data = {
    zone: zone,
    banner: banner,
    checked: checked,
    action: 'tbm_advertising.toggleSwitchOnOffBanner'
  };
  if(_switch.attr('id')=='switch_on_off_newmpadv'){
    data['action']='tbm_advertising.toggleSwitchOnOffNewMPAdv';
  }
  jQuery.post(ajaxurl, data, function(response) {
    if (response.match(/^Error:/) != null) {
      tbm_advertising.log(response.replace('Error:', ''));
    } else {}
  }).always(function() {
    _switch.parent().removeClass('disabled');
    jQuery('textarea[name="' + banner + '"]').removeAttr('disabled');
    if (checked == 0) {
      jQuery('textarea[name="' + banner + '"]').attr('disabled', 'disabled');
    }
  });
};

tbm_advertising.addLinkClickHandler = function() {
  jQuery('.page-links-div').hide();
  tbm_advertising.updateZoneView('link-add', '', false, true, null);
  return false;
};

tbm_advertising.modLinkClickHandler = function() {
  jQuery('.page-links-div').hide();
  tbm_advertising.updateZoneView(jQuery(this).parent().parent().find('input').val(), '', false, true, null);
  return false;
};

tbm_advertising.deleteLinkBannersHandler = function() {
  if (window.confirm(jQuery('#message_confirm_delete_page').val())) {
    tbm_advertising.ajaxStart();
    jQuery.post(ajaxurl, {
      zone: jQuery(this).parent().parent().find('input').val(),
      action: 'tbm_advertising.deleteLinkBanners'
    }, function(response) {
      if (response.match(/^Error:/) != null) {
        tbm_advertising.log(response.replace('Error:', ''));
      }
      else {
        tbm_advertising.ajaxStart();
        tbm_advertising.selectZoneChange.apply(jQuery('.select-zone.selected'));
      }
    });
  }
  return false;
};

tbm_advertising.allLinkClickHandler = function() {
  tbm_advertising.ajaxStart();
  tbm_advertising.selectZoneChange.apply(jQuery('.select-zone.selected'));
  return false;
};

tbm_advertising.optionsPageDeleteBanners = function() {
  var tr = jQuery(this).parent();
  if (window.confirm(tr.data('message'))) {
    tbm_advertising.ajaxStart();
    jQuery.post(ajaxurl, {
      banner: tr.data('label'),
      action: 'tbm_advertising.optionsPageDeleteBanners'
    }, function(response) {
      if (response.match(/^Error:/) != null) {
        tbm_advertising.log(response.replace('Error:', ''));
      }
      else {
        tr.remove();
      }
    }).always(tbm_advertising.ajaxStop);
  }
  return false;
};

tbm_advertising.optionsPageSetBanners = function() {
  var submit = jQuery(this);

  jQuery('#input_banner_label').val(banner_label_uppercase(jQuery('#input_banner_label').val()));
  var input_banner_label = jQuery('#input_banner_label');
  var input_banner_desc = jQuery('#input_banner_desc');
  var input_banner_type = jQuery('#input_banner_type');

  input_banner_label.css('background', '');

  if (input_banner_label.val() == '') {
    input_banner_label.css('background', 'rgb(255, 220, 220)');
  }
  else if (window.confirm(submit.data('message'))) {
    tbm_advertising.ajaxStart();
    jQuery.post(ajaxurl, {
      input_banner_label: input_banner_label.val(),
      input_banner_desc: input_banner_desc.val(),
      input_banner_type: input_banner_type.val(),
      action: 'tbm_advertising.optionsPageSetBanners'
    }, function(response) {
      if (response.match(/^Error:/) != null) {
        tbm_advertising.log(response.replace('Error:', ''));
        tbm_advertising.ajaxStop();
      }
      else {
        location.reload();
      }
    });
  }
  return false;
};


// Functions
tbm_advertising.updatePostTaxType = function(zone, info, hide_options) {

  var table,
    temp = zone + (info === '' ? '' : '-' + info),
    data = {
      action: 'tbm_advertising.updatePostTaxTypeView',
      zone: temp
    };

  tbm_advertising.adminBannerOptions.data('tbm-advertising-zone', temp);
  tbm_advertising.ajaxStart();
  jQuery.post(ajaxurl, data, function(response) {
    if (response.match(/^Error:/) != null) {
      tbm_advertising.log(response.replace('Error:', ''));
    } else {
      zone_container = jQuery('.form-tbm-advertising .zone-container', tbm_advertising.adminBannerOptions);
      secondary_container = jQuery('.form-tbm-advertising .zone-secondary-container', tbm_advertising.adminBannerOptions);
      tertiary_container = jQuery('.form-tbm-advertising .zone-tertiary-container', tbm_advertising.adminBannerOptions);
      options_container = jQuery('.form-tbm-advertising .zone-options-container', tbm_advertising.adminBannerOptions);
      jQuery(secondary_container).html(response);
      jQuery(tertiary_container).html("");
      jQuery(zone_container).html("");
      if (hide_options) {
        jQuery(options_container).html("");
      }
    }
  }).always(tbm_advertising.ajaxStop2);
};
tbm_advertising.updatePostPostType = function(zone, info, hide_options) {

  var table,
    temp = zone + (info === '' ? '' : '-' + info),
    data = {
      action: 'tbm_advertising.updatePostPostTypeView',
      zone: temp
    };

  tbm_advertising.adminBannerOptions.data('tbm-advertising-zone', temp);
  tbm_advertising.ajaxStart();
  jQuery.post(ajaxurl, data, function(response) {
    if (response.match(/^Error:/) != null) {
      tbm_advertising.log(response.replace('Error:', ''));
    } else {
      zone_container = jQuery('.form-tbm-advertising .zone-container', tbm_advertising.adminBannerOptions);
      secondary_container = jQuery('.form-tbm-advertising .zone-secondary-container', tbm_advertising.adminBannerOptions);
      tertiary_container = jQuery('.form-tbm-advertising .zone-tertiary-container', tbm_advertising.adminBannerOptions);
      options_container = jQuery('.form-tbm-advertising .zone-options-container', tbm_advertising.adminBannerOptions);
      jQuery(secondary_container).html(response);
      jQuery(tertiary_container).html("");
      jQuery(zone_container).html("");
      if (hide_options) {
        jQuery(options_container).html("");
      }
    }
  }).always(tbm_advertising.ajaxStop2);
};

tbm_advertising.updateSearchPostType = function(zone, info, hide_options) {

  var table,
    temp = zone + (info === '' ? '' : '-' + info),
    data = {
      action: 'tbm_advertising.updateSearchPostType',
      zone: temp
    };

  tbm_advertising.adminBannerOptions.data('tbm-advertising-zone', temp);
  tbm_advertising.ajaxStart();
  jQuery.post(ajaxurl, data, function(response) {
    if (response.match(/^Error:/) != null) {
      tbm_advertising.log(response.replace('Error:', ''));
    } else {
      zone_container = jQuery('.form-tbm-advertising .zone-container', tbm_advertising.adminBannerOptions);
      secondary_container = jQuery('.form-tbm-advertising .zone-secondary-container', tbm_advertising.adminBannerOptions);
      tertiary_container = jQuery('.form-tbm-advertising .zone-tertiary-container', tbm_advertising.adminBannerOptions);
      options_container = jQuery('.form-tbm-advertising .zone-options-container', tbm_advertising.adminBannerOptions);
      jQuery(tertiary_container).html(response);
      jQuery(zone_container).html("");
      if (hide_options) {
        jQuery(options_container).html("");
      }
    }
  }).always(tbm_advertising.ajaxStop2);
};

tbm_advertising.updateSearchPostTaxType = function(zone, info, hide_options) {

  var table,
    temp = zone + (info === '' ? '' : '-' + info),
    data = {
      action: 'tbm_advertising.updateSearchPostTaxTypeView',
      zone: temp
    };

  tbm_advertising.adminBannerOptions.data('tbm-advertising-zone', temp);
  tbm_advertising.ajaxStart();
  jQuery.post(ajaxurl, data, function(response) {
    if (response.match(/^Error:/) != null) {
      tbm_advertising.log(response.replace('Error:', ''));
    } else {
      zone_container = jQuery('.form-tbm-advertising .zone-container', tbm_advertising.adminBannerOptions);
      secondary_container = jQuery('.form-tbm-advertising .zone-secondary-container', tbm_advertising.adminBannerOptions);
      tertiary_container = jQuery('.form-tbm-advertising .zone-tertiary-container', tbm_advertising.adminBannerOptions);
      options_container = jQuery('.form-tbm-advertising .zone-options-container', tbm_advertising.adminBannerOptions);
      jQuery(tertiary_container).html(response);
      jQuery(zone_container).html("");
      if (hide_options) {
        jQuery(options_container).html("");
      }
    }
  }).always(tbm_advertising.ajaxStop2);
};

tbm_advertising.updateTermViewFromTax = function(zone, info, hide_options) {

  var table,
    temp = zone + (info === '' ? '' : '-' + info),
    data = {
      action: 'tbm_advertising.updateTermViewFromTax',
      zone: temp
    };

  tbm_advertising.adminBannerOptions.data('tbm-advertising-zone', temp);
  tbm_advertising.ajaxStart();
  jQuery.post(ajaxurl, data, function(response) {
    if (response.match(/^Error:/) != null) {
      tbm_advertising.log(response.replace('Error:', ''));
    } else {
      zone_container = jQuery('.form-tbm-advertising .zone-container', tbm_advertising.adminBannerOptions);
      secondary_container = jQuery('.form-tbm-advertising .zone-secondary-container', tbm_advertising.adminBannerOptions);
      tertiary_container = jQuery('.form-tbm-advertising .zone-tertiary-container', tbm_advertising.adminBannerOptions);
      options_container = jQuery('.form-tbm-advertising .zone-options-container', tbm_advertising.adminBannerOptions);
      jQuery(secondary_container).html(response);
      jQuery(zone_container).html("");
      jQuery(tertiary_container).html("");
      if (hide_options) {
        jQuery(options_container).html("");
      }
    }
  }).always(tbm_advertising.ajaxStop2);
};

tbm_advertising.searchGetPostsList = function(search, zone) {

  var data = {
    action: 'tbm_advertising.searchGetPostsList',
    search: search,
    zone: zone
  };

  tbm_advertising.ajaxStart();
  jQuery.post(ajaxurl, data, function(response) {
    if (response.match(/^Error:/) != null) {
      tbm_advertising.log(response.replace('Error:', ''));
    } else {
      zone_container = jQuery('.form-tbm-advertising .zone-container', tbm_advertising.adminBannerOptions);
      secondary_container = jQuery('.form-tbm-advertising .zone-secondary-container', tbm_advertising.adminBannerOptions);
      tertiary_container = jQuery('.form-tbm-advertising .zone-tertiary-container', tbm_advertising.adminBannerOptions);
      options_container = jQuery('.form-tbm-advertising .zone-options-container', tbm_advertising.adminBannerOptions);
      post_container = jQuery('.post-select-div', tbm_advertising.adminBannerOptions);
      jQuery(post_container).html(response);
      jQuery(zone_container).html("");
    }
  }).always(tbm_advertising.ajaxStop2);
};

tbm_advertising.updateZoneView = function(zone, info, hide_options, hide_secondary, opt) {

  var table,
    temp = zone + (info === '' ? '' : '-' + info),
    tempOpt = zone + (opt === '' ? '' : '-' + opt),
    data = {
      action: 'tbm_advertising.updateZoneView',
      zone: temp,
      type: zone,
      opt: tempOpt
    };

  tbm_advertising.adminBannerOptions.data('tbm-advertising-zone', temp);
  tbm_advertising.ajaxStart();
  jQuery.post(ajaxurl, data, function(response) {
    if (response.match(/^Error:/) != null) {
      tbm_advertising.log(response.replace('Error:', ''));
    } else {
      zone_container = jQuery('.form-tbm-advertising .zone-container', tbm_advertising.adminBannerOptions);
      options_container = jQuery('.form-tbm-advertising .zone-options-container', tbm_advertising.adminBannerOptions);
      secondary_container = jQuery('.form-tbm-advertising .zone-secondary-container', tbm_advertising.adminBannerOptions);
      tertiary_container = jQuery('.form-tbm-advertising .zone-tertiary-container', tbm_advertising.adminBannerOptions);
      jQuery(zone_container).html(response);
      if (hide_secondary) {
        jQuery(secondary_container).html("");
        jQuery(tertiary_container).html("");
      }
      if (hide_options) {
        jQuery(options_container).html("");
      }

      if (zone == 'link-add') {
        jQuery('.switch_on_off label').addClass('not_available_parent');
      }
    }
  }).always(tbm_advertising.ajaxStop);
};

tbm_advertising.log = function(message) {
  if (window.console) {
    window.console.debug(message);
  }
  else {
    alert(message);
  }
};

tbm_advertising.ajaxStart = function() {
  jQuery(tbm_advertising.adminBannerOptions).css('pointer-events', 'none').css("opacity", "0.5");
  jQuery('#tbm-advertising-submit').hide();
};

tbm_advertising.ajaxStop = function() {
  jQuery(tbm_advertising.adminBannerOptions).css('pointer-events', '').css("opacity", "");
  jQuery('#tbm-advertising-submit').show();
};

tbm_advertising.ajaxStop2 = function() {
  jQuery(tbm_advertising.adminBannerOptions).css('pointer-events', '').css("opacity", "");
};

jQuery(document).ready(tbm_advertising.init);