<?php

class TBM_Advertising {
	/**
	 * The global db prefix
	 **/
	private static $dbPrefix;

	/**
	 * The global tables
	 **/
	private static $table_banners_contents;
	private static $table_banners_contents_not_available;
	private static $table_banners;

	/**
	 * The current banners list
	 **/
	private static $banner_zones;
	private static $currentBanners;
	private static $disabled_taxonomies = array(
		'nav_menu',
		'link_category',
		'post_format',
		'post_status',
		'ef_usergroup',
		'following_users'
	);

	/**
	 * The allowed banners list
	 **/
	private static $allowedBanners = array();
	private static $initialAllowedBanners = array(
		'CODE_HEAD'                  => 'code',
		'CODE_FOOT'                  => 'code',
		'SKIN'                       => 'code',
		'OOP'                        => 'code',
		'LEADERBOARD'                => 'code',
		'MEDIUM_RECTANGLE_UP'        => 'code',
		'MEDIUM_RECTANGLE_UP_MOBILE' => 'code',
		'MEDIUM_RECTANGLE_DW'        => 'code',
		'MEDIUM_RECTANGLE_DW_MOBILE' => 'code',
		'BEFORE_ARTICLE'             => 'code',
		'INNER_ARTICLE'              => 'code',
		'END_ARTICLE'                => 'code',
		'PREMIUM_NATIVE'             => 'code',
		'PROMOBOX_1'                 => 'code',
		'PROMOBOX_2'                 => 'code'
	);

	/************************
	 * PUBLIC STATIC METHODS *
	 ************************/

	/**
	 * Initialization method
	 **/
	public static function init() {
		# set db prefix and tables
		TBM_Advertising::setDbPrefix();
		TBM_Advertising::setDbTables();

		# set admin actions callback
		add_action( 'admin_menu', array( __CLASS__, 'adminMenu' ) );
		add_action( 'admin_init', array( __CLASS__, 'adminInit' ) );
		# set page actions
		add_action( 'wp', array( __CLASS__, 'extractBanners' ) );

		# set page actions
		add_action( 'wp_footer', array( __CLASS__, 'saveAllZones' ) );

		# set ajax actions callback
		add_action( 'wp_ajax_tbm_advertising.updateZoneView', array( __CLASS__, 'updateZoneView' ) );
		add_action( 'wp_ajax_tbm_advertising.updatePageView', array( __CLASS__, 'updatePageView' ) );
		add_action( 'wp_ajax_tbm_advertising.updateTaxView', array( __CLASS__, 'updateTaxView' ) );
		add_action( 'wp_ajax_tbm_advertising.updateTermViewFromTax', array( __CLASS__, 'updateTermViewFromTax' ) );
		add_action( 'wp_ajax_tbm_advertising.updatePostView', array( __CLASS__, 'updatePostView' ) );
		add_action( 'wp_ajax_tbm_advertising.updatePostPostTypeView', array( __CLASS__, 'updatePostPostTypeView' ) );
		add_action( 'wp_ajax_tbm_advertising.updatePostTaxTypeView', array( __CLASS__, 'updatePostTaxTypeView' ) );
		add_action( 'wp_ajax_tbm_advertising.updateSearchPostType', array( __CLASS__, 'updateSearchPostType' ) );
		add_action( 'wp_ajax_tbm_advertising.updateSearchPostTaxTypeView', array(
			__CLASS__,
			'updateSearchPostTaxTypeView'
		) );
		add_action( 'wp_ajax_tbm_advertising.searchGetPostsList', array( __CLASS__, 'searchGetPostsList' ) );
		add_action( 'wp_ajax_tbm_advertising.updateLinkView', array( __CLASS__, 'updateLinkView' ) );
		add_action( 'wp_ajax_tbm_advertising.deleteLinkBanners', array( __CLASS__, 'deleteLinkBanners' ) );
		add_action( 'wp_ajax_tbm_advertising.saveBannersPerZone', array( __CLASS__, 'saveBannersPerZone' ) );
		add_action( 'wp_ajax_tbm_advertising.toggleSwitchOnOffBanner', array( __CLASS__, 'toggleSwitchOnOffBanner' ) );
		add_action( 'wp_ajax_tbm_advertising.toggleSwitchOnOffNewMPAdv', array(
			__CLASS__,
			'toggleSwitchOnOffNewMPAdv'
		) );
		add_action( 'wp_ajax_tbm_advertising.saveTopicTax', array( __CLASS__, 'saveTopicTax' ) );
		add_action( 'wp_ajax_tbm_advertising.optionsPageSetBanners', array( __CLASS__, 'optionsPageSetBanners' ) );
		add_action( 'wp_ajax_tbm_advertising.optionsPageDeleteBanners', array(
			__CLASS__,
			'optionsPageDeleteBanners'
		) );
	}

	/**
	 * Invoked on admin_init action
	 **/
	public static function adminInit() {
		if ( isset( $_GET['page'] ) && ( $_GET['page'] === 'tbm-advertising' || $_GET['page'] === 'tbm-advertising-options' ) && ( current_user_can( 'manage_options' ) ) ) {
			# enqueue scripts
			wp_enqueue_script(
				'tbm-advertising-javascripts',
				plugins_url( "data/js/tbm_advertising.js", dirname( __FILE__ ) ),
				array( 'jquery' ),
				TBM_ADVERTISING_VERSION
			);
			wp_enqueue_script(
				'webtoolkit-base64',
				plugins_url( "data/js/webtoolkit.base64.js", dirname( __FILE__ ) ),
				array( 'jquery' ),
				TBM_ADVERTISING_VERSION
			);
			wp_enqueue_script(
				'multi-select',
				plugins_url( "data/js/chosen.jquery.min.js", dirname( __FILE__ ) ),
				array( 'jquery' ),
				TBM_ADVERTISING_VERSION
			);
			# enqueue styles
			wp_enqueue_style(
				'tbm-advertising-stylesheets',
				plugins_url( "data/css/tbm_advertising.css", dirname( __FILE__ ) ),
				array(),
				TBM_ADVERTISING_VERSION,
				'screen'
			);
		} // end if( isset( $_GET['page'] ) && ( $_GET['page'] === 'tbm-advertising' || $_GET['page'] === 'tbm-advertising-options') )
	} // end function adminInit()

	/**
	 * Invoked on admin_menu action
	 * Create admin menu Banner Management and Banner Options
	 **/
	public static function adminMenu() {
		add_menu_page( __( 'Banner Management', TBM_ADVERTISING_LANGUAGE_PLUGIN ), __( 'Banner Management', TBM_ADVERTISING_LANGUAGE_PLUGIN ), 'manage_options', 'tbm-advertising', array(
			__CLASS__,
			'adminPageOutput'
		) );
		add_submenu_page( 'tbm-advertising', __( 'Banner Management', TBM_ADVERTISING_LANGUAGE_PLUGIN ), __( 'Banner Management', TBM_ADVERTISING_LANGUAGE_PLUGIN ), 'manage_options', 'tbm-advertising', array(
			__CLASS__,
			'adminPageOutput'
		) );
		add_submenu_page( 'tbm-advertising', __( 'Banner Options', TBM_ADVERTISING_LANGUAGE_PLUGIN ), __( 'Banner Options', TBM_ADVERTISING_LANGUAGE_PLUGIN ), 'manage_options', 'tbm-advertising-options', array(
			__CLASS__,
			'adminPageOptionsOutput'
		) );
	} // end function adminMenu()

	/**
	 * Print Banner Management page if have correct permission
	 **/
	public static function adminPageOutput() {
		if ( ! current_user_can( 'manage_options' ) ) {
			wp_die( __( 'You do not have permission to access this page.', TBM_ADVERTISING_LANGUAGE_PLUGIN ) );
		} // end if ( !current_user_can( 'manage_options' ) )

		TBM_Advertising::createTables();
		require_once TBM_ADVERTISING_DIR . '/tpl/options.form.php';
	} // end function adminPageOutput()

	/**
	 * Print Banner Options page if have correct permission
	 **/
	public static function adminPageOptionsOutput() {
		if ( ! current_user_can( 'manage_options' ) ) {
			wp_die( __( 'You do not have permission to access this page.', TBM_ADVERTISING_LANGUAGE_PLUGIN ) );
		} // end if ( !current_user_can( 'manage_options' ) )

		require_once TBM_ADVERTISING_DIR . '/tpl/options.page.php';
	} // end function adminPageOptionsOutput()

	/**
	 * Updates the zone view with banner textareas
	 **/
	public static function updateZoneView() {
		# use global db object
		global $wpdb;
		if ( ! isset( $_POST['zone'] ) || ! trim( $_POST['zone'] ) ) {
			wp_die( 'Error: ' . __( 'Incorrect parameters', TBM_ADVERTISING_LANGUAGE_PLUGIN ) );
		} // end if( !isset( $_POST['zone'] ) || !trim( $_POST['zone'] ) )
		if ( ! isset( $_POST['type'] ) || ! trim( $_POST['type'] ) ) {
			wp_die( 'Error: ' . __( 'Incorrect parameters', TBM_ADVERTISING_LANGUAGE_PLUGIN ) );
		} // end if( !isset( $_POST['type'] ) || !trim( $_POST['type'] ) )

		$zone = trim( $_POST['zone'] );
		$type = trim( $_POST['type'] );
		// var_dump($zone);
		// var_dump($type);

		if ( $zone === 'none' ) {
			wp_die( '' );
		} // end if( $zone === 'none' )
		# set answer
		$output = array();
		if ( $zone === 'link-add' ) {
			$output[] = (
				'<div class="link-container"><div class="banner-type"><h4 for="link-page">'
				. __( 'Page url', TBM_ADVERTISING_LANGUAGE_PLUGIN )
				. '</h4></div><div><input type="text" id="link-page" class="link-page" /></div></div>'
			);
		} elseif ( substr( $zone, 0, 7 ) === 'http://' || substr( $zone, 0, 8 ) === 'https://' ) {
			$output[] = (
				'<div class="link-container"><div class="banner-type"><h4 for="link-page">'
				. __( 'Page url', TBM_ADVERTISING_LANGUAGE_PLUGIN )
				. '</h4></div><div><input type="text" id="link-page" class="link-page" readonly="readonly" value="'
				. $zone . '" /></div></div>'
			);
		} else {
			$output[] = '<h3>' . __( 'Banners availables', TBM_ADVERTISING_LANGUAGE_PLUGIN ) . '</h3>';
		} // end if( $zone === 'link-add' )

		$banners         = $wpdb->get_results( "
            SELECT *
            FROM " . TBM_Advertising::$table_banners_contents . "," . TBM_Advertising::$table_banners . "
            WHERE  " . TBM_Advertising::$table_banners_contents . ".cont_banner_label=" . TBM_Advertising::$table_banners . ".banner_label
            AND cont_site_zone = '{$zone}'
        " );
		$banner_contents = array();
		foreach ( $banners as $banner ) {
			$banner_contents[ $banner->cont_banner_label ] = stripslashes( $banner->cont_banner_content );
		} // end foreach( $banners as $banner )

		// check blocked not available
		// GLOBAL
		$not_available_global         = $wpdb->get_results( "
            SELECT cont_banner_label
            FROM " . TBM_Advertising::$table_banners_contents_not_available . ", " . TBM_Advertising::$table_banners . "
            WHERE " . TBM_Advertising::$table_banners_contents_not_available . ".cont_banner_label=" . TBM_Advertising::$table_banners . ".banner_label
            AND cont_site_zone = 'global'
        " );
		$not_available_global_banners = array();
		foreach ( $not_available_global as $na ) {
			$not_available_global_banners[] = $na->cont_banner_label;
		}

		// specific ALL parent type
		$not_available_specific_type_banners = array();
		if ( $type == "type" || $type == "page" || $type == 'cat' || $type == 'tag' || $type == "tax" ) {
			$not_available_specific_type = $wpdb->get_results( "
                SELECT cont_banner_label
                FROM " . TBM_Advertising::$table_banners_contents_not_available . ", " . TBM_Advertising::$table_banners . "
                WHERE " . TBM_Advertising::$table_banners_contents_not_available . ".cont_banner_label=" . TBM_Advertising::$table_banners . ".banner_label
                AND cont_site_zone = '{$type}-0'
            " );
			foreach ( $not_available_specific_type as $na ) {
				$not_available_specific_type_banners[] = $na->cont_banner_label;
			}
		}
		$not_available_specific_type_term_banners     = array();
		$not_available_specific_type_term_banners_tax = array();

		// specific ALL sub-parent type
		// terms (from taxonomy) and specific post (from post type)
		if ( ( $type == "tax" || $type == "type" ) && isset( $_POST['opt'] ) && ! empty( $_POST['opt'] ) && $zone != $_POST["opt"] . "-0" ) {
			$not_available_specific_type_term = $wpdb->get_results( "
                SELECT cont_banner_label
                FROM " . TBM_Advertising::$table_banners_contents_not_available . ", " . TBM_Advertising::$table_banners . "
                WHERE " . TBM_Advertising::$table_banners_contents_not_available . ".cont_banner_label=" . TBM_Advertising::$table_banners . ".banner_label
                AND cont_site_zone = '{$_POST["opt"]}-0'
            " );
			foreach ( $not_available_specific_type_term as $na ) {
				$not_available_specific_type_term_banners[] = $na->cont_banner_label;
			}
			// var_dump($not_available_specific_type_term_banners);
			if ( strpos( $zone, 'type-tax' ) !== false ) {
				$args   = array( 'public' => true );
				$_types = get_post_types( $args );
				foreach ( $_types as $_post_type ) {
					$not_available_specific_type_term = $wpdb->get_results( "
                        SELECT cont_banner_label
                        FROM " . TBM_Advertising::$table_banners_contents_not_available . ", " . TBM_Advertising::$table_banners . "
                        WHERE " . TBM_Advertising::$table_banners_contents_not_available . ".cont_banner_label=" . TBM_Advertising::$table_banners . ".banner_label
                        AND cont_site_zone = 'type-{$_post_type}-0'
                    " );
					foreach ( $not_available_specific_type_term as $na ) {
						$not_available_specific_type_term_banners_tax[ $_post_type ] = $na->cont_banner_label;
					}
				}
			}
			// var_dump($not_available_specific_type_term_banners_tax);
		}
		// specific zone
		$not_available         = $wpdb->get_results( "
            SELECT cont_banner_label
            FROM " . TBM_Advertising::$table_banners_contents_not_available . ", " . TBM_Advertising::$table_banners . "
            WHERE " . TBM_Advertising::$table_banners_contents_not_available . ".cont_banner_label=" . TBM_Advertising::$table_banners . ".banner_label
            AND cont_site_zone = '{$zone}'
        " );
		$not_available_banners = array();
		foreach ( $not_available as $na ) {
			$not_available_banners[] = $na->cont_banner_label;
		}

		//get allowed banners
		TBM_Advertising::allowedBannersFunction();

		# loop results
		foreach ( TBM_Advertising::$allowedBanners as $banner_label => $banner_type ) {
			$row_template          = TBM_Advertising::getBannerRowTemplate( 'zone' );
			$checked_switch_on_off = "checked";
			$class_switch_on_off   = null;
			$disabled_textarea     = null;
			$label_switch_on_off   = null;

			# disabled from global zone
			if ( in_array( $banner_label, $not_available_global_banners ) && $zone != 'global' ) {
				$checked_switch_on_off = null;
				$class_switch_on_off   = "not_available_parent";
				$disabled_textarea     = "disabled";
				$label_switch_on_off   = __( 'disabled from "global" zone', TBM_ADVERTISING_LANGUAGE_PLUGIN );
			}

			# disabled from ALL parent zone
			if ( ! empty( $not_available_specific_type_banners ) && in_array( $banner_label, $not_available_specific_type_banners ) && $zone != $type . '-0' ) {
				$checked_switch_on_off = null;
				$class_switch_on_off   = "not_available_parent";
				$disabled_textarea     = "disabled";
				$real_zone_name        = null;
				if ( "type" == $type ) {
					$real_zone_name = __( 'Post', TBM_ADVERTISING_LANGUAGE_PLUGIN );
				} elseif ( 'cat' == $type ) {
					$real_zone_name = __( 'Categories', TBM_ADVERTISING_LANGUAGE_PLUGIN );
				} elseif ( 'tag' == $type ) {
					$real_zone_name = __( 'Tags', TBM_ADVERTISING_LANGUAGE_PLUGIN );
				} elseif ( 'tax' == $type ) {
					$real_zone_name = __( 'Taxonomies', TBM_ADVERTISING_LANGUAGE_PLUGIN );
				} elseif ( 'page' == $type ) {
					$real_zone_name = __( 'Pages', TBM_ADVERTISING_LANGUAGE_PLUGIN );
				}
				$label_switch_on_off = ( empty( $label_switch_on_off ) ) ? sprintf( __( 'disabled from "%s" option ALL', TBM_ADVERTISING_LANGUAGE_PLUGIN ), strtolower( $real_zone_name ) ) : $label_switch_on_off . "  " . sprintf( __( 'and from "%s" option ALL', TBM_ADVERTISING_LANGUAGE_PLUGIN ), strtolower( $real_zone_name ) );
			}

			# disabled from ALL (sub)parent zone
			if ( in_array( $banner_label, $not_available_specific_type_term_banners ) ) {
				$checked_switch_on_off = null;
				$class_switch_on_off   = "not_available_parent";
				$disabled_textarea     = "disabled";
				if ( 'tax' == $type || ( strpos( $zone, 'type-tax' ) !== false ) ) {
					$label_switch_on_off = ( empty( $label_switch_on_off ) ) ? __( 'disabled from the selected term option ALL', TBM_ADVERTISING_LANGUAGE_PLUGIN ) : $label_switch_on_off . "  " . __( 'and from the selected term option ALL', TBM_ADVERTISING_LANGUAGE_PLUGIN );
				} else {
					$label_switch_on_off = ( empty( $label_switch_on_off ) ) ? __( 'disabled from the selected post_type option ALL', TBM_ADVERTISING_LANGUAGE_PLUGIN ) : $label_switch_on_off . "  " . __( 'and from the selected post_type option ALL', TBM_ADVERTISING_LANGUAGE_PLUGIN );
				}
			}

			if ( in_array( $banner_label, $not_available_specific_type_term_banners_tax ) ) {
				$label_switch_on_off = ( empty( $label_switch_on_off ) ) ? __( 'Disabled posts type: ', TBM_ADVERTISING_LANGUAGE_PLUGIN ) : $label_switch_on_off . "  " . __( 'and disabled posts type: ', TBM_ADVERTISING_LANGUAGE_PLUGIN );
				foreach ( $not_available_specific_type_term_banners_tax as $k => $val ) {
					if ( $banner_label == $val ) {
						$label_switch_on_off .= "<span class='disabled_post_type'>" . $k . "</span>";
					}
				}
			}

			# zone disabled
			if ( in_array( $banner_label, $not_available_banners ) ) {
				$checked_switch_on_off = null;
				$disabled_textarea     = 'disabled';
				$label_switch_on_off   = ( empty( $label_switch_on_off ) ) ? null : $label_switch_on_off . '  ' . __( 'and from itself', TBM_ADVERTISING_LANGUAGE_PLUGIN );
			}

			#output switch
			$switch_on_off = '<input id="switch_on_off_' . $banner_label . '" data-banner="' . $banner_label . '" data-zone="' . $zone . '"
             class="cmn-toggle toggle_switch_on_off cmn-toggle-round"
             type="checkbox" ' . $checked_switch_on_off . '>
             <label for="switch_on_off_' . $banner_label . '" class="' . $class_switch_on_off . '"></label>
             <div class="switch_on_off_not_available">' . $label_switch_on_off . '</div>';

			$output[] = sprintf(
				$row_template,
				strtoupper( $banner_label ),
				$switch_on_off,
				$banner_label,
				$disabled_textarea,
				( isset( $banner_contents[ $banner_label ] ) ) ? $banner_contents[ $banner_label ] : ''
			);
		} // end foreach( $banners as $banner )
		# return string
		wp_die( implode( '', $output ) );
	} // end function updateZoneView()

	/**
	 * Show informations about zones published
	 *
	 * @param string $name The zone name
	 * @param bool $echo True to echo, false to return
	 *
	 * @return string The box
	 */
	public static function showCheckZones( $name, $echo ) {
		$print_zone       = get_query_var( 'banner_check_zones', '' );
		$zone             = implode( ' > ', TBM_Advertising::$banner_zones );
		$tbm_banner_array = TBM_Advertising::$currentBanners;
		$id               = uniqid( 'a' );
		$trigger          = '<span class="adv_button" onclick="document.getElementById(\'%s\').style.display = \'block\'">+</span>';
		$value            = '<span class="adv_hacker" id="%s">%s</span>';
		$codes            = isset( $tbm_banner_array[ $name ] ) ? $tbm_banner_array[ $name ] : array();

		add_filter( 'tbm_disable_tms', '__return_true' );

		if ( ! $codes ) {
			return '';
		}

		foreach ( $codes as $key => $rawcode ) {
			//overwrite code at every loop so we get the last one
			$code = '<span class="badge">' . $key . '</span> ' . htmlspecialchars( $rawcode, ENT_QUOTES, 'UTF-8' );
		}

		/**
		 * Print CSS only in the first loop
		 */
		if ( empty( $print_zone ) ) {
			wp_enqueue_style(
				'tbm-advertising-zone',
				plugins_url( "data/css/tbm_adv_checker.css", dirname( __FILE__ ) ),
				array(),
				TBM_ADVERTISING_VERSION,
				'screen'
			);

			add_action( 'amp_post_template_css', function ( $amp_template ) {
				// only CSS here please...
				include( TBM_ADVERTISING_DIR . 'data/css/tbm_adv_checker.css' );
			} );
		}

		/**
		 * Print zones only in the first loop
		 */
		if ( empty( $print_zone ) ) {
			$tag = '<div class="adv_zone adv_head"><div><small style="font-size: 90%;">' . $zone . '</small></div></div><div class="adv_zone"><div class="zone_name">' . $name . sprintf( $trigger, $id ) . '</div>' . sprintf( $value, $id, $code ) . '</div>';
			set_query_var( 'banner_check_zones', '1' );
		} else {
			$tag = '<div class="adv_zone"><div class="zone_name">' . $name . sprintf( $trigger, $id ) . '</div>' . sprintf( $value, $id, $code ) . '</div>';
		}

		/**
		 * Echo or return the value
		 */
		if ( $echo ) {
			echo $tag;
		} else {
			return $tag;
		}

	}

	public static function saveAllZones() {
		$active_zones = get_query_var( 'active_zones', array() );
	}


	/************************
	 *         PAGES         *
	 ************************/

	/**
	 * Updates the page view with pages select
	 **/
	public static function updatePageView() {
		# use global db object
		global $wpdb;

		// get ids of pages "edited"
		$ids         = array();
		$pageids     = $wpdb->get_results( "
            (
                SELECT DISTINCT( cont_site_zone )
                FROM " . TBM_Advertising::$table_banners_contents . "
                WHERE cont_site_zone LIKE 'page-%'
            )
            UNION
            (
                SELECT DISTINCT( cont_site_zone )
                FROM " . TBM_Advertising::$table_banners_contents_not_available . "
                WHERE cont_site_zone LIKE 'page-%'
            )
        " );
		$pageidsHome = $wpdb->get_results( " SELECT DISTINCT( cont_site_zone ) FROM " . TBM_Advertising::$table_banners_contents . " WHERE cont_site_zone LIKE 'home'" );

		#loops pageids
		foreach ( $pageids as $id ) {
			$ids[] = intval( str_replace( 'page-', '', $id->cont_site_zone ) );
		} // end foreach( $pageids as $id )

		// query
		$args  = array(
			"posts_per_page" => - 1,
			"s"              => $_POST['search'],
			"orderby"        => "title",
			"post_type"      => array( "page" ),
			"post_status"    => array( 'publish' )
		);
		$pages = get_posts( $args );

		// max 15 elements min 5 elements in select
		$count_ = ( count( $pages ) >= 15 ) ? 15 : count( $pages ) + 1;
		$count_ = ( $count_ < 5 ) ? 5 : $count_;

		# set answer
		$output       = array();
		$output[]     = '<h3>' . __( 'Select a page (* page edited)', TBM_ADVERTISING_LANGUAGE_PLUGIN ) . '</h3>';
		$output[]     = '<select id="select-page" class="page-select" size="' . $count_ . '">';
		$output[]     = sprintf( '<option value="%d">%s</option>', 0, strtoupper( __( 'all', TBM_ADVERTISING_LANGUAGE_PLUGIN ) . ( in_array( "0", $ids ) ? ' *' : '' ) ) );
		$output[]     = sprintf( '<option value="%s">%s</option>', "homepage", strtoupper( __( 'Home page', TBM_ADVERTISING_LANGUAGE_PLUGIN ) . ( ! empty( $pageidsHome ) ? " *" : "" ) ) );
		$frontpage_id = get_option( 'page_on_front' );
		# loop pages
		foreach ( $pages as $page ) {
			if ( $frontpage_id != $page->ID ) {
				$output[] = sprintf( '<option value="%d">%s</option>', $page->ID, strtoupper( $page->post_title . ( in_array( $page->ID, $ids ) ? ' *' : '' ) ) );
			}
		} // end foreach( $categories as $category )
		$output[] = '</select>';
		# return string
		wp_die( implode( '', $output ) );
	} // end function updatePageView()


	/************************
	 *      TAXONOMIES       *
	 ************************/

	/**
	 * Updates the tassonomies view with tassonimia select
	 * The first view clicking on the button "taxonomies"
	 **/
	public static function updateTaxView() {
		# use global db object
		global $wpdb;

		// get ids of taxonomies "edited"
		$ids    = array();
		$taxids = $wpdb->get_results( "
            (
                SELECT DISTINCT( cont_site_zone )
                FROM " . TBM_Advertising::$table_banners_contents . "
                WHERE cont_site_zone LIKE 'tax-%'
            )
            UNION
            (
                SELECT DISTINCT( cont_site_zone )
                FROM " . TBM_Advertising::$table_banners_contents_not_available . "
                WHERE cont_site_zone LIKE 'tax-%'
            )
        " );

		# loop taxids
		foreach ( $taxids as $id ) {
			$ids[] = str_replace( 'tax-', '', $id->cont_site_zone );
		} // end foreach( $taxids as $id )

		// get taxonomies
		$taxonomies          = get_taxonomies();
		$disabled_taxonomies = array(
			'nav_menu',
			'link_category',
			'post_format',
			'post_status',
			'ef_usergroup',
			'following_users'
		);

		# set answer
		$output   = array();
		$output[] = '<h3>' . __( 'Select a taxonomy', TBM_ADVERTISING_LANGUAGE_PLUGIN ) . '</h3>';
		$output[] = '<select id="select-tax" class="taxonomy-select" size="5">';
		$output[] = sprintf( '<option value="%d">%s</option>', 0, strtoupper( __( 'all', TBM_ADVERTISING_LANGUAGE_PLUGIN ) ) . ( in_array( 0, $ids ) ? ' *' : '' ) );
		if ( ! is_wp_error( $taxonomies ) ) {
			foreach ( $taxonomies as $k => $taxonomy ) {
				if ( ! in_array( $taxonomy, TBM_Advertising::$disabled_taxonomies ) ) {
					$tax_rep  = str_replace( 'tax-', '', $taxonomy );
					$output[] = sprintf( '<option value="%s">%s</option>', $k, $taxonomy . ( in_array( $tax_rep . "-term-0", $ids ) ? ' *' : '' ) );
				}
			}
		}
		$output[] = '</select>';
		# return string
		wp_die( implode( '', $output ) );
	} // end function updateTaxView()

	/**
	 * Updates the terms view with taxonomy select
	 * view clicking a taxonomy name
	 **/
	public static function updateTermViewFromTax() {
		# use global db object
		global $wpdb;

		// get taxonomy id
		$tax      = str_replace( 'tax-', "", $_POST['zone'] );
		$tax      = substr( $_POST['zone'], 4 );
		$type_tax = "term";
		if ( $tax == 'category' ) {
			$type_tax = 'cat';
		} else if ( $tax == 'post_tag' ) {
			$type_tax = 'tag';
		}

		// get ids of terms "edited"
		$ids = array();
		$arg = "(
                SELECT DISTINCT( cont_site_zone )
                FROM " . TBM_Advertising::$table_banners_contents . "
                WHERE cont_site_zone LIKE 'tax-" . $tax . "-term-%'
            )
            UNION
            (
                SELECT DISTINCT( cont_site_zone )
                FROM " . TBM_Advertising::$table_banners_contents_not_available . "
                WHERE cont_site_zone LIKE 'tax-" . $tax . "-term-%'
            )";
		if ( $type_tax == 'cat' ) {
			$arg = "(
                SELECT DISTINCT( cont_site_zone )
                FROM " . TBM_Advertising::$table_banners_contents . "
                WHERE cont_site_zone LIKE 'cat-%'
            )
            UNION
            (
                SELECT DISTINCT( cont_site_zone )
                FROM " . TBM_Advertising::$table_banners_contents_not_available . "
                WHERE cont_site_zone LIKE 'cat-%'
            )";
		} else if ( $type_tax == 'tag' ) {
			$arg = "(
                SELECT DISTINCT( cont_site_zone )
                FROM " . TBM_Advertising::$table_banners_contents . "
                WHERE cont_site_zone LIKE 'tag-%'
            )
            UNION
            (
                SELECT DISTINCT( cont_site_zone )
                FROM " . TBM_Advertising::$table_banners_contents_not_available . "
                WHERE cont_site_zone LIKE 'tag-%'
            )";
		}
		$taxids = $wpdb->get_results( $arg );

		# loop taxids
		foreach ( $taxids as $id ) {
			if ( $type_tax == 'term' ) {
				$ids[] = intval( str_replace( 'tax-' . $tax . '-term-', '', $id->cont_site_zone ) );
			} else if ( $type_tax == 'cat' ) {
				$ids[] = intval( str_replace( 'cat-', '', $id->cont_site_zone ) );
			} else if ( $type_tax == 'tag' ) {
				$ids[] = intval( str_replace( 'tag-', '', $id->cont_site_zone ) );
			}

		} // end foreach( $catids as $id )

		// get terms
		$terms = get_terms( $tax, array( 'hide_empty' => 0 ) );

		// max 15 elements min 5 elements in select
		$count_ = ( count( $terms ) >= 15 ) ? 15 : count( $terms ) + 1;
		$count_ = ( $count_ < 5 ) ? 5 : $count_;

		# set answer
		$output   = array();
		$output[] = '<h3>' . __( 'Select a term of this taxonomy (* term edited)', TBM_ADVERTISING_LANGUAGE_PLUGIN ) . '</h3>';
		$output[] = '<select id="select-term" class="term-select" size="' . $count_ . '" data-term="' . $tax . '-term">';
		$output[] = sprintf( '<option value="%s">%s</option>', "0", strtoupper( __( 'all', TBM_ADVERTISING_LANGUAGE_PLUGIN ) . ( in_array( "0", $ids ) ? ' *' : '' ) ) );
		if ( ! is_wp_error( $terms ) ) {
			foreach ( $terms as $term ) {
				$output[] = sprintf( '<option value="%s">%s</option>', $term->term_id, $term->name . ( in_array( $term->term_id, $ids ) ? ' *' : '' ) );
			} // end foreach( $taxonomies as $taxonomy )
		}
		$output[] = '</select>';
		# return string
		wp_die( implode( '', $output ) );
	} // end function updateTaxView()


	/************************
	 *      POST TYPES       *
	 ************************/

	/**
	 * Updates the post view with post select
	 * The first view clicking on the button "POST"
	 **/
	public static function updatePostView() {
		# use global db object
		global $wpdb;

		$ids      = array();
		$typesids = $wpdb->get_results( "
            (
                SELECT DISTINCT( cont_site_zone )
                FROM " . TBM_Advertising::$table_banners_contents . "
                WHERE cont_site_zone LIKE 'type-%'
            )
            UNION
            (
                SELECT DISTINCT( cont_site_zone )
                FROM " . TBM_Advertising::$table_banners_contents_not_available . "
                WHERE cont_site_zone LIKE 'type-%'
            )
        " );
		# loop typesids
		foreach ( $typesids as $id ) {
			$id    = str_replace( 'type-', '', $id->cont_site_zone );
			$id    = preg_replace( '/-[0-9]+/', '', $id );
			$ids[] = $id;
		} // end foreach( $catids as $id )

		$output   = array();
		$output[] = '<h3>' . __( 'Select a post', TBM_ADVERTISING_LANGUAGE_PLUGIN ) . '</h3>';
		$output[] = '<select id="select-post" class="post-post-select" size="3">';
		$output[] = sprintf( '<option value="%d">%s</option>', 0, strtoupper( __( 'all', TBM_ADVERTISING_LANGUAGE_PLUGIN ) . ( in_array( "0", $ids ) ? ' *' : '' ) ) );
		$output[] = sprintf( '<option value="%s">%s</option>', "by-post-type", strtoupper( __( "by post type", TBM_ADVERTISING_LANGUAGE_PLUGIN ) ) );
		$output[] = sprintf( '<option value="%s">%s</option>', "by-tax", strtoupper( __( "by taxonomy", TBM_ADVERTISING_LANGUAGE_PLUGIN ) ) );
		// $output[] = sprintf( '<option value="%s">%s</option>', "by-link", strtoupper(__("by link", TBM_ADVERTISING_LANGUAGE_PLUGIN )) );
		$output[] = '</select>';
		# return string
		wp_die( implode( '', $output ) );
	} // end function updatePostView()

	/**
	 * Updates the post_types view with post types select
	 * The view clicking on the button "BY POST TYPES"
	 **/
	public static function updatePostPostTypeView() {
		# use global db object
		global $wpdb;

		// get ids of post types "edited"
		$ids      = array();
		$typesids = $wpdb->get_results( "
            (
                SELECT DISTINCT( cont_site_zone )
                FROM " . TBM_Advertising::$table_banners_contents . "
                WHERE cont_site_zone LIKE 'type-%'
            )
            UNION
            (
                SELECT DISTINCT( cont_site_zone )
                FROM " . TBM_Advertising::$table_banners_contents_not_available . "
                WHERE cont_site_zone LIKE 'type-%'
            )
        " );
		# loop typesids
		foreach ( $typesids as $id ) {
			$id    = str_replace( 'type-', '', $id->cont_site_zone );
			$id    = preg_replace( '/-[0-9]+/', '', $id );
			$ids[] = $id;
		} // end foreach( $catids as $id )

		# get types
		$args  = array( 'public' => true );
		$types = get_post_types( $args );

		// max 15 elements min 5 elements in select
		$count_ = ( count( $types ) >= 15 ) ? 15 : count( $types ) - 1;
		$count_ = ( $count_ < 5 ) ? 5 : $count_;

		# set answer
		$output   = array();
		$output[] = '<h3>' . __( 'Select a post type (* posts types edited)', TBM_ADVERTISING_LANGUAGE_PLUGIN ) . '</h3>';
		$output[] = '<select id="select-types" class="types-select" size="' . $count_ . '">';
		# loop types //pre(types);
		foreach ( $types as $type ) {
			if ( 'page' != $type ) {
				$output[] = sprintf( '<option value="%s">%s</option>', $type, strtoupper( $type . ( in_array( $type, $ids ) ? ' *' : '' ) ) );
			}
		} // end foreach( $types as $type )
		$output[] = '</select>';
		# return string
		wp_die( implode( '', $output ) );
	} // end function updatePostPostTypeView()

	/**
	 * Updates the post-ID view with post ID select
	 * The view clicking on the button "BY POST ID"
	 **/
	public static function updatePostPostIDView( $post_type ) {
		# use global db object
		global $wpdb;
		$post_type = str_replace( 'type-', "", $post_type );
		$ids       = $post__in = array();
		$postsIDS  = $wpdb->get_results( "
            (
                SELECT DISTINCT( cont_site_zone )
                FROM " . TBM_Advertising::$table_banners_contents . "
                WHERE cont_site_zone LIKE 'type-" . $post_type . "-%'
            )
            UNION
            (
                SELECT DISTINCT( cont_site_zone )
                FROM " . TBM_Advertising::$table_banners_contents_not_available . "
                WHERE cont_site_zone LIKE 'type-" . $post_type . "-%'
            )
        " );
		# loop posts
		foreach ( $postsIDS as $id ) {
			$my_id = str_replace( 'type-' . $post_type . "-", '', $id->cont_site_zone );;
			$ids[] = $my_id;
			if ( is_numeric( $my_id ) && (int) $my_id != 0 ) {
				$post__in[] = (int) $my_id;
			}
		} // end foreach( $postsIDS as $id )

		$posts = array();
		if ( ! empty( $post__in ) ) {
			$args  = array(
				"posts_per_page" => - 1,
				"orderby"        => "title",
				'post__in'       => $post__in,
				"post_type"      => array( $post_type ),
				"post_status"    => array( 'any' )
			);
			$posts = get_posts( $args );
		}

		// max 15 elements min 5 elements in select
		$count_ = ( count( $posts ) >= 15 ) ? 15 : count( $postsIDS ) + 1;
		$count_ = ( $count_ < 5 ) ? 5 : $count_;

		# set answer
		$output   = array();
		$output[] = '<h3 style="float:left">' . __( 'All post customized', TBM_ADVERTISING_LANGUAGE_PLUGIN ) . '</h3>';
		$output[] = '<select id="select-post" class="post-select" size="' . $count_ . '" data-type="' . $post_type . '">';
		$output[] = sprintf( '<option data-post="%s" value="%s">%s</option>', 0, 0, strtoupper( __( 'all', TBM_ADVERTISING_LANGUAGE_PLUGIN ) ) . ( in_array( "0", $ids ) ? ' *' : '' ) );
		if ( ! is_wp_error( $posts ) ) {
			foreach ( $posts as $post ) {
				$output[] = sprintf( '<option data-post="%s" value="%s">%s</option>', $post->ID, $post->ID, $post->post_title . ( in_array( $post->ID, $ids ) ? ' *' : '' ) );
			} // end foreach( $taxonomies as $taxonomy )
		}
		$output[] = '</select>';

		# return string
		return ( implode( '', $output ) );
	} // end function updatePostPostIDView()

	/**
	 * Updates the tax type view with taxs select
	 * The view clicking on the button "BY TAXONOMY"
	 **/
	public static function updatePostTaxTypeView() {
		# use global db object
		global $wpdb;

		// get ids of taxonomies "edited"
		$ids    = array();
		$taxids = $wpdb->get_results( "
            (
                SELECT DISTINCT( cont_site_zone )
                FROM " . TBM_Advertising::$table_banners_contents . "
                WHERE cont_site_zone LIKE 'type-tax-%'
            )
            UNION
            (
                SELECT DISTINCT( cont_site_zone )
                FROM " . TBM_Advertising::$table_banners_contents_not_available . "
                WHERE cont_site_zone LIKE 'type-tax-%'
            )
        " );

		# loop taxids
		foreach ( $taxids as $id ) {
			$ids[] = str_replace( 'type-tax-', '', $id->cont_site_zone );
		} // end foreach( $taxids as $id )

		// get taxonomies
		$taxonomies = get_taxonomies();

		# set answer
		$output   = array();
		$output[] = '<h3>' . __( 'Select a taxonomy', TBM_ADVERTISING_LANGUAGE_PLUGIN ) . '</h3>';
		$output[] = '<select id="select-tax-type" class="taxonomy-type-select" size="5">';
		if ( ! is_wp_error( $taxonomies ) ) {
			foreach ( $taxonomies as $k => $taxonomy ) {
				if ( ! in_array( $taxonomy, TBM_Advertising::$disabled_taxonomies ) ) {
					$tax_rep  = str_replace( 'tax-', '', $taxonomy );
					$output[] = sprintf( '<option value="%s">%s</option>', $k, $taxonomy . ( in_array( $tax_rep . "-term-0", $ids ) ? ' *' : '' ) );
				}
			}
		}
		$output[] = '</select>';
		# return string
		wp_die( implode( '', $output ) );
	} // end function updatePostTaxTypeView()

	/**
	 * Updates the post_types view
	 * Print an input with the scope to search posts with the post type previously selected.
	 * The result will be input in the select#select-post
	 **/
	public static function updateSearchPostType() {
		# use global db object
		global $wpdb;
		# get post type selected
		$type_post = str_replace( 'type-', "", $_POST['zone'] );
		# set answer
		$output   = array();
		$output[] = '<div class="div_select_tag"><label for="select-post">' . sprintf( __( 'Search %s', TBM_ADVERTISING_LANGUAGE_PLUGIN ), $type_post ) . '</label> ';
		$output[] = '<input type="text" name="post" id="search_post" data-type="' . $_POST['zone'] . '"/><input type="button" class="submit_post button" value="' . sprintf( __( 'Search %s', TBM_ADVERTISING_LANGUAGE_PLUGIN ), $type_post ) . '"></div>';
		$output[] = '<div class="post-select-div">';
		$output[] = TBM_Advertising::updatePostPostIDView( $_POST['zone'] );
		$output[] = '</div>';
		# return string
		wp_die( implode( '', $output ) );
	} // end function updateSearchPostType()

	/**
	 * Updates the post tax view
	 * Print an input with the scope to search posts with the post type previously selected.
	 * The result will be input in the select#select-post
	 **/
	public static function updateSearchPostTaxTypeView() {
		# use global db object
		global $wpdb;

		// get taxonomy id
		$tax      = str_replace( 'type-tax-', "", $_POST['zone'] );
		$tax      = substr( $_POST['zone'], 9 );
		$type_tax = "term";
		if ( $tax == 'category' ) {
			$type_tax = 'cat';
		} else if ( $tax == 'post_tag' ) {
			$type_tax = 'tag';
		}

		// get ids of terms "edited"
		$ids    = array();
		$taxids = $wpdb->get_results( "
            (
                SELECT DISTINCT( cont_site_zone )
                FROM " . TBM_Advertising::$table_banners_contents . "
                WHERE cont_site_zone LIKE 'type-tax-" . $tax . "-term-%'
            )
            UNION
            (
                SELECT DISTINCT( cont_site_zone )
                FROM " . TBM_Advertising::$table_banners_contents_not_available . "
                WHERE cont_site_zone LIKE 'type-tax-" . $tax . "-term-%'
            )"
		);

		# loop taxids type-tax-TAX-term-
		foreach ( $taxids as $id ) {
			$ids[] = intval( str_replace( 'type-tax-' . $tax . '-term-', '', $id->cont_site_zone ) );
		} // end foreach( $catids as $id )

		// get terms
		$terms = get_terms( $tax, array( 'hide_empty' => 0 ) );

		// max 15 elements min 5 elements in select
		$count_ = ( count( $terms ) >= 15 ) ? 15 : count( $terms ) + 1;
		$count_ = ( $count_ < 5 ) ? 5 : $count_;

		# set answer
		$output   = array();
		$output[] = '<h3>' . __( 'Select a term of this taxonomy (* term edited)', TBM_ADVERTISING_LANGUAGE_PLUGIN ) . '</h3>';
		$zone     = str_replace( 'type-', '', $_POST['zone'] );
		$output[] = '<select id="select-term-type" class="type-term-select" size="' . $count_ . '" data-term="' . $zone . '-term">';
		$output[] = sprintf( '<option value="%s">%s</option>', "0", strtoupper( __( 'all', TBM_ADVERTISING_LANGUAGE_PLUGIN ) . ( in_array( "0", $ids ) ? ' *' : '' ) ) );
		if ( ! is_wp_error( $terms ) ) {
			foreach ( $terms as $term ) {
				$output[] = sprintf( '<option value="%s">%s</option>', $term->term_id, $term->name . ( in_array( $term->term_id, $ids ) ? ' *' : '' ) );
			} // end foreach( $taxonomies as $taxonomy )
		}
		$output[] = '</select>';
		# return string
		wp_die( implode( '', $output ) );
	} // end function updateSearchPostTaxType()

	/**
	 * Updates list (select#select-post)
	 * send in post method a variable called "search"
	 **/
	public static function searchGetPostsList() {
		# use global db object
		global $wpdb;
		# get post type selected
		$post_type = str_replace( 'type-', "", $_POST['zone'] );

		// get ids of post ( that have this post_types ) "edited"
		$ids      = array();
		$postsIDS = $wpdb->get_results( "
            (
                SELECT DISTINCT( cont_site_zone )
                FROM " . TBM_Advertising::$table_banners_contents . "
                WHERE cont_site_zone LIKE 'type-" . $post_type . "-%'
            )
            UNION
            (
                SELECT DISTINCT( cont_site_zone )
                FROM " . TBM_Advertising::$table_banners_contents_not_available . "
                WHERE cont_site_zone LIKE 'type-" . $post_type . "-%'
            )
        " );
		# loop posts
		foreach ( $postsIDS as $id ) {
			$ids[] = str_replace( 'type-' . $post_type . "-", '', $id->cont_site_zone );
		} // end foreach( $postsIDS as $id )

		// max 15 elements min 5 elements in select
		$count_ = ( count( $postsIDS ) >= 15 ) ? 15 : count( $postsIDS ) + 1;
		$count_ = ( $count_ < 5 ) ? 5 : $count_;

		# query get posts
		$args  = array(
			"posts_per_page" => - 1,
			"s"              => $_POST['search'],
			"orderby"        => "title",
			"post_type"      => array( $post_type ),
			"post_status"    => array( 'any' )
		);
		$posts = get_posts( $args );
		if ( is_numeric( $_POST['search'] ) ) {
			$post_numeric = get_post( (int) $_POST['search'] );
		}

		# set answer
		$output   = array();
		$output[] = '<h3 style="float:left">' . sprintf( __( 'Select a %s', TBM_ADVERTISING_LANGUAGE_PLUGIN ), $post_type ) . '</h3>';
		$output[] = '<select id="select-post" class="post-select" size="' . $count_ . '" data-type="' . $post_type . '">';
		$output[] = sprintf( '<option data-post="%s" value="%s">%s</option>', 0, 0, strtoupper( __( 'all', TBM_ADVERTISING_LANGUAGE_PLUGIN ) ) . ( in_array( "0", $ids ) ? ' *' : '' ) );

		$post_id_already_write = array();
		if ( ! is_wp_error( $posts ) ) {
			foreach ( $posts as $post ) {
				$output[]                = sprintf( '<option data-post="%s" value="%s">%s</option>', $post->ID, $post->ID, $post->post_title . ( in_array( $post->ID, $ids ) ? ' *' : '' ) );
				$post_id_already_write[] = $post->ID;
			} // end foreach( $taxonomies as $taxonomy )
		}
		if ( isset( $post_numeric->ID ) && ! in_array( $post_numeric->ID, $post_id_already_write ) ) {
			$output[] = sprintf( '<option data-post="%s" value="%s">%s</option>', $post_numeric->ID, $post_numeric->ID, $post_numeric->post_title . ( in_array( $post_numeric->ID, $ids ) ? ' *' : '' ) );
		}
		$output[] = '</select>';
		# return string
		wp_die( implode( '', $output ) );
	} // end function searchGetPostsList()


	/************************
	 *         LINKS         *
	 ************************/

	/**
	 * Updates the page view with the setted pages list
	 * The first view clicking on the button "links"
	 **/
	public static function updateLinkView() {
		# use global db object
		global $wpdb;

		$row = TBM_Advertising::getBannerRowTemplate( 'url' );

		# gets already setted pages
		$pages = $wpdb->get_results( "
            SELECT DISTINCT( cont_site_zone )
            FROM " . TBM_Advertising::$table_banners_contents . "
            WHERE cont_banner_content != ''
            AND cont_banner_url_based = '1'
        " );

		# set answer
		$output   = array();
		$output[] = '<div class="page-link-div">';
		$output[] = '<strong>' . sprintf( __( '%s links found', TBM_ADVERTISING_LANGUAGE_PLUGIN ), count( $pages ) ) . '</strong>';
		$output[] = '<a href="#" class="link-all" title="' . __( 'Show links', TBM_ADVERTISING_LANGUAGE_PLUGIN ) . '">' . __( 'Show links', TBM_ADVERTISING_LANGUAGE_PLUGIN ) . '</a>';
		$output[] = '<a href="#" class="link-add" title="' . __( 'Add new link', TBM_ADVERTISING_LANGUAGE_PLUGIN ) . '">' . __( 'Add new link', TBM_ADVERTISING_LANGUAGE_PLUGIN ) . '</a>';
		$output[] = '</div>';
		# loop pages
		$output[] = '<div class="page-links-div">';
		foreach ( $pages as $page ) {
			$output[] = sprintf( $row, $page->cont_site_zone, $page->cont_site_zone );
		} // end foreach( $pages as $page )
		$output[] = '</div>';
		# return string
		wp_die( implode( '', $output ) );
	} // end function updateLinkView()

	/**
	 * Delete links banners according to posted data
	 **/
	public static function deleteLinkBanners() {
		# use global db object
		global $wpdb;
		# check params
		if ( ! isset( $_POST['zone'] ) || ! trim( $_POST['zone'] ) ) {
			wp_die( 'Error: ' . __( 'Incorrect parameters', TBM_ADVERTISING_LANGUAGE_PLUGIN ) );
		} // end if( !isset( $_POST['zone'] ) || !trim( $_POST['zone'] ) )
		$zone = trim( $_POST['zone'] );
		# deletes db info
		$wpdb->query( "
            DELETE FROM " . TBM_Advertising::$table_banners_contents . "
            WHERE cont_site_zone = '" . esc_sql( $zone ) . "'
            AND cont_banner_url_based = '1'
        " );
		wp_die( 'ok' );
	} // end function deleteLinkBanners()


	/**************************
	 *  SAVE BANNERS PER ZONE  *
	 **************************/

	/**
	 * Set banners according to posted data
	 **/
	public static function saveBannersPerZone() {
		# use global db object
		global $wpdb;

		# check params
		if ( ! isset( $_POST['zone'] ) || ! trim( $_POST['zone'] ) ) {
			wp_die( 'Error: ' . __( 'Incorrect parameters', TBM_ADVERTISING_LANGUAGE_PLUGIN ) );
		} // end if( !isset( $_POST['zone'] ) || !trim( $_POST['zone'] ) )

		if ( ! isset( $_POST['codes'] ) || ! trim( $_POST['codes'] ) ) {
			wp_die( 'Error: ' . __( 'Incorrect parameters', TBM_ADVERTISING_LANGUAGE_PLUGIN ) );
		} // end if( !isset( $_POST['codes'] ) || !trim( $_POST['codes'] ) )

		$zone = trim( $_POST['zone'] );

		$codes = explode( '<[{SEP}]>', utf8_decode( trim( base64_decode( $_POST['codes'] ) ) ) );

		$url_based = ( substr( $zone, 0, 7 ) === 'http://' || substr( $zone, 0, 8 ) === 'https://' ) ? '1' : '0';

		# delete previous entries
		// $wpdb->query("DELETE FROM " . TBM_Advertising::$table_banners_contents . " WHERE cont_site_zone = '" . esc_sql( $zone ) . "'");

		// controllo per cancellare tutte le zone per tipologia
		$emptyAllAdv = true;
		# loop codes
		foreach ( $codes as $code ) {
			# extract values
			$info    = explode( '<{[SEP]}>', $code );
			$trimmed = trim( $info[1] );

			if ( ! empty( $trimmed ) ) {
				$emptyAllAdv = false;
			}
			# writes db info
			$wpdb->query( "
                INSERT INTO " . TBM_Advertising::$table_banners_contents . "
                (`cont_site_zone`, `cont_banner_label`, `cont_banner_url_based`, `cont_banner_content`)
                VALUES
                    (
                        '" . esc_sql( $zone ) . "',
                        '" . esc_sql( $info[0] ) . "',
                        '{$url_based}',
                        '" . esc_sql( str_replace( array(
					'<[EAMP]>',
					'<[PLUS]>',
					'<[WHITESPACE]>',
					'<[EMPTY]>'
				), array( '&', '+', ' ', ' ' ), $info[1] ) ) . "'
                    ) ON DUPLICATE KEY UPDATE `cont_banner_url_based` = '{$url_based}', `cont_banner_content` = '" . esc_sql( str_replace( array(
					'<[EAMP]>',
					'<[PLUS]>',
					'<[WHITESPACE]>',
					'<[EMPTY]>'
				), array( '&', '+', ' ', ' ' ), $info[1] ) ) . "'
            " );
			if ( $emptyAllAdv ) {

				$wpdb->query( "DELETE FROM " . TBM_Advertising::$table_banners_contents . " WHERE `cont_site_zone` = '" . esc_sql( $zone ) . "'" );

				$wpdb->query( "DELETE FROM " . TBM_Advertising::$table_banners_contents_not_available . " WHERE `cont_site_zone` = '" . esc_sql( $zone ) . "'" );
			}

		} // end foreach( $codes as $code )
		# return result
		wp_die( 'ok' );
	} // end function saveBannersPerZone()


	/***************************
	 *  DISABLE/ENABLE BANNERS  *
	 ***************************/

	/**
	 * Enable or disable banners
	 **/
	public static function toggleSwitchOnOffBanner() {
		# use global db object
		global $wpdb;

		# check params
		if ( ! isset( $_POST['zone'] ) || ! trim( $_POST['zone'] ) ) {
			wp_die( 'Error: ' . __( 'Incorrect parameters', TBM_ADVERTISING_LANGUAGE_PLUGIN ) );
		} // end if( !isset( $_POST['zone'] ) || !trim( $_POST['zone'] ) )
		if ( ! isset( $_POST['banner'] ) || ! trim( $_POST['banner'] ) ) {
			wp_die( 'Error: ' . __( 'Incorrect parameters', TBM_ADVERTISING_LANGUAGE_PLUGIN ) );
		} // end if( !isset( $_POST['banner'] ) || !trim( $_POST['banner'] ) )
		if ( ! isset( $_POST['checked'] ) || ! is_numeric( $_POST['checked'] ) ) {
			wp_die( 'Error: ' . __( 'Incorrect parameters', TBM_ADVERTISING_LANGUAGE_PLUGIN ) );
		} // end if( !isset( $_POST['checked'] ) || !trim( $_POST['checked'] ) )

		$zone    = trim( $_POST['zone'] );
		$banner  = trim( $_POST['banner'] );
		$checked = trim( $_POST['checked'] );

		if ( (bool) $checked ) {
			# deletes db info
			$wpdb->query( "
                DELETE FROM " . TBM_Advertising::$table_banners_contents_not_available . "
                WHERE cont_site_zone = '" . esc_sql( $zone ) . "'
                AND cont_banner_label = '" . esc_sql( $banner ) . "'
            " );

		} else {
			$wpdb->query( "
                DELETE FROM " . TBM_Advertising::$table_banners_contents_not_available . "
                WHERE cont_site_zone = '" . esc_sql( $zone ) . "'
                AND cont_banner_label = '" . esc_sql( $banner ) . "'
            " );

			# add db info
			$wpdb->query( "
                REPLACE INTO `" . TBM_Advertising::$table_banners_contents_not_available . "`
                (`cont_site_zone`, `cont_banner_label`)
                VALUES
                ('" . esc_sql( $zone ) . "','" . esc_sql( $banner ) . "')
            " );
		}
		wp_die( 'ok' );
	} // end function toggleSwitchOnOffBanner()

	/**
	 * Enable or disable new MP ADV
	 **/
	public static function toggleSwitchOnOffNewMPAdv() {
		# use global db object
		global $wpdb;

		# check params
		if ( ! isset( $_POST['checked'] ) || ! is_numeric( $_POST['checked'] ) ) {
			wp_die( 'Error: ' . __( 'Incorrect parameters', TBM_ADVERTISING_LANGUAGE_PLUGIN ) );
		} // end if( !isset( $_POST['checked'] ) || !trim( $_POST['checked'] ) )

		$checked = trim( $_POST['checked'] );
		update_option( 'tbm_advertising_v_1_7', $checked );

		wp_die( 'ok' );
	} // end function toggleSwitchOnOffNewMPAdv()

	/**
	 * save topic tax
	 **/
	public static function saveTopicTax() {
		# use global db object
		global $wpdb;

		update_option( 'tbm_advertising_topic_tax', $_POST['tax'] );

		wp_die( 'ok' );
	} // end function saveTopicTax()


	/**************************
	 *       OPTIONS PAGE      *
	 **************************/

	/**
	 * delete banners
	 **/
	public static function optionsPageDeleteBanners() {
		# use global db object
		global $wpdb;

		if ( ! isset( $_POST['banner'] ) || ! trim( $_POST['banner'] ) ) {
			wp_die( 'Error: ' . __( 'Incorrect parameters', TBM_ADVERTISING_LANGUAGE_PLUGIN ) );
		} // end if( !isset( $_POST['banner'] ) || !trim( $_POST['banner'] ) )

		$banner = trim( $_POST['banner'] );

		$wpdb->query( "
            DELETE FROM " . TBM_Advertising::$table_banners . "
            WHERE banner_label = '" . esc_sql( $banner ) . "'
        " );

		wp_die( 'ok' );
	} // end function optionsPageDeleteBanners()

	/**
	 * Set new banners
	 **/
	public static function optionsPageSetBanners() {
		# use global db object
		global $wpdb;

		# check params
		if ( ! isset( $_POST['input_banner_label'] ) || ! trim( $_POST['input_banner_label'] ) ) {
			wp_die( 'Error: ' . __( 'Incorrect parameters', TBM_ADVERTISING_LANGUAGE_PLUGIN ) );
		} // end if( !isset( $_POST['input_banner_label'] ) || !trim( $_POST['input_banner_label'] ) )
		if ( ! isset( $_POST['input_banner_type'] ) || ! trim( $_POST['input_banner_type'] ) ) {
			wp_die( 'Error: ' . __( 'Incorrect parameters', TBM_ADVERTISING_LANGUAGE_PLUGIN ) );
		} // end if( !isset( $_POST['input_banner_type'] ) || !trim( $_POST['input_banner_type'] ) )

		$input_banner_label = trim( $_POST['input_banner_label'] );
		$input_banner_desc  = trim( $_POST['input_banner_desc'] );
		$input_banner_type  = trim( $_POST['input_banner_type'] );

		# add db info
		$wpdb->query( "
            REPLACE INTO `" . TBM_Advertising::$table_banners . "`
            (`banner_label`, `banner_desc`, `banner_type`)
            VALUES
            ('" . esc_sql( $input_banner_label ) . "','" . esc_sql( $input_banner_desc ) . "','" . esc_sql( $input_banner_type ) . "')
        " );

		wp_die( 'ok' );
	} // end function optionsPageSetBanners()

	/**
	 * Outputs a banner content
	 *
	 * @param name , string, optional, The banner name
	 * @param before , string, optional, An optional text to prepend to banner content
	 * @param after , string, optional, An optional text to append to banner content
	 * @param echo , boolean, optional, True to echo result
	 * @param disabled_before_after , boolean, optional, True to print only the banner content
	 **/
	public static function output( $name = '', $before = '', $after = '', $echo = true, $disabled_before_after = false ) {
		# use global db object
		global $wpdb;

		$result    = false;
		$new_zones = array();

		if ( isset( $_GET['check_zones'] ) ) {

			/**
			 * Returns zone information
			 */
			if ( $_GET['check_zones'] == 1 ) {
				$output = TBM_Advertising::showCheckZones( $name, $echo );

				return $output;
			}
			/**
			 * Die and print Bookmarklet setup
			 */
			$output = '<p style="padding: 20px">Trascina la Bookmarklet nella barra dei browser: <a
            alt="Bookmarklet - Trascina l\'elemento sulla toolbar del browser"
            style="border:2px groove black; padding: 0 5px 2px; background-color: #eee; color: black; "
            href="javascript:void((function(){var loc = location.href; loc.indexOf(\'?\') == -1 ? (location.href = loc+\'?check_zones=1\') : (location.href = loc+\'&check_zones=1\');})());">Check Zones</a>. Non sai come fare? Chiedi a Matteo</p>';
			die( $output );
		}


		#reverse array order
		$banners_available_reversed = ! empty( TBM_Advertising::$banner_zones ) ? array_reverse( TBM_Advertising::$banner_zones ) : null;
		if ( ! empty( $banners_available_reversed ) && false !== $banners_available_reversed ) {
			foreach ( $banners_available_reversed as $zone ) {
				if ( isset( TBM_Advertising::$currentBanners[ $name ][ $zone ] ) ) {
					$result = TBM_Advertising::$currentBanners[ $name ][ $zone ];
					break;
				}
			}
		}

		/**
		 * Save Zones in query_var
		 */
		$active_zones = get_query_var( 'active_zones', array() );
		$tbm_banner   = preg_replace( '/(?=<!--)([\s\S]*?)-->/', '', $result );
		preg_match_all( '/id="div-gpt-ad-([^"]+)/', $tbm_banner, $matches );
		foreach ( $matches[1] as $match ) {
			$new_zones[] = $match;
		}
		$active_zones = array_merge( $active_zones, $new_zones );
		set_query_var( 'active_zones', $active_zones );

		if ( $before . $after === '' && false !== $result ) {

			$type   = TBM_Advertising::$allowedBanners[ $name ];
			$before = "<div class=\"tbm-" . $type . " tbm-" . $type . "-container " . str_replace( '_', '-', strtolower( $name ) ) . "\">\n\t<div class=\"banner-content\">";
			$after  = "\t</div>\n</div>";
		} // end if( $before . $after === '' && $result !== '' )

		if ( is_user_logged_in() ) {
			$before = "<!-- " . $name . " BY TBM_ADVERTISING -->" . $before;
			$after  = $after . "<!-- /" . $name . " BY TBM_ADVERTISING -->";
		}
		if ( ! $disabled_before_after && false !== $result && ! empty( $result ) ) {
			$result = $before . stripslashes( $result ) . $after;
		}

		// Force HTTPS connection on HTTPS sites
		if ( stripos( get_bloginfo( 'url' ), 'https://' ) === 0 ) {
			$result = str_replace( 'http://', 'https://', $result );
		}

		// Force App directory on bedrock sites
		if ( stripos( wp_get_upload_dir()['baseurl'], 'app/uploads' ) !== false ) {
			$result = str_replace( '/wp-content/', '/app/', $result );
		};

		if ( $echo ) {
			echo $result;
		} else {
			return $result;
		} // end if( $echo )
	} // end function output()

	/**
	 * set Allowed banners
	 **/
	public static function allowedBannersFunction() {

		# use global db object
		global $wpdb;
		# get banners
		$banners = $wpdb->get_results( "SELECT * FROM " . TBM_Advertising::$table_banners . " ORDER BY banner_label" );
		foreach ( $banners as $banner ) {
			TBM_Advertising::$allowedBanners[ $banner->banner_label ] = $banner->banner_type;
		}

	}

	/**
	 * Extract current banners
	 * Only in frontend
	 **/
	public static function extractBanners() {

		if ( isset( $_GET['check_banner'] ) && $_GET['check_banner'] == 1 ) {
			echo '<pre>';
		}

		if ( is_admin() ) {
			return true;
		}
		# check for banners array
		if ( is_array( TBM_Advertising::$banner_zones ) || isset( $_GET['no_banners'] ) ) {
			return true;
		}

		# use global db object
		global $wpdb;

		$queried_object = get_queried_object();
		$post_id        = @get_the_ID();

		TBM_Advertising::allowedBannersFunction();
		# create banners array
		TBM_Advertising::$banner_zones = array();

		# global
		TBM_Advertising::$banner_zones[] = 'global';

		/**
		 * Homepage
		 */
		if ( is_home() || is_front_page() ) {
			TBM_Advertising::$banner_zones[] = 'home';

			// reset $post_id
			$post_id = false;
		} // end if( is_home() || is_front_page() )

		/**
		 * Page
		 */
		if ( ! is_front_page() && ( is_page() || is_404() ) ) {
			TBM_Advertising::$banner_zones[] = 'page-0';
			if ( is_page() ) {
				TBM_Advertising::$banner_zones[] = 'page-' . $post_id;
			}
		} // end if( is_page() || is_404() )

		/**
		 * Category
		 */
		if ( is_category() ) {
			TBM_Advertising::$banner_zones[] = 'cat-0';
			if ( $queried_object->parent != 0 ) {
				TBM_Advertising::$banner_zones[] = 'cat-' . $queried_object->parent;
			}
			TBM_Advertising::$banner_zones[] = 'cat-' . $queried_object->term_id;

			// reset $post_id
			$post_id = false;
		} // end if( is_category())

		/**
		 * Tag
		 */
		if ( is_tag() ) {
			TBM_Advertising::$banner_zones[] = 'tag-0';
			if ( $queried_object->parent != 0 ) {
				TBM_Advertising::$banner_zones[] = 'tag-' . $queried_object->parent;
			}
			TBM_Advertising::$banner_zones[] = 'tag-' . $queried_object->term_id;

			// reset $post_id
			$post_id = false;
		} // end if( is_category())

		/**
		 * Taxonomy
		 */
		if ( is_tax() ) {
			TBM_Advertising::$banner_zones[] = 'tax-0';
			TBM_Advertising::$banner_zones[] = 'tax-' . $queried_object->taxonomy . "-term-0";
			if ( $queried_object->parent != 0 ) {
				TBM_Advertising::$banner_zones[] = 'tax-' . $queried_object->taxonomy . '-term-' . $queried_object->parent;
			}
			TBM_Advertising::$banner_zones[] = 'tax-' . $queried_object->taxonomy . '-term-' . $queried_object->term_id;

			// reset $post_id
			$post_id = false;
		} // end if( is_tax() )

		/**
		 * Archive of post-type
		 */
		if ( is_post_type_archive() && ! ( is_home() || is_front_page() ) ) {
			TBM_Advertising::$banner_zones[] = 'type-0';
			TBM_Advertising::$banner_zones[] = 'type-' . get_post_type() . '-0';

			// reset $post_id
			$post_id = false;
		} // end if( is_post_type_archive() )

		/**
		 * Single
		 */
		$post_id = apply_filters( 'tbm_advertising_filter_post_id', $post_id );
		if ( is_numeric( $post_id ) && ( is_single() || ( $post_id !== false && ! empty( $post_id ) ) ) && ! ( is_home() || is_front_page() ) ) {
			$post_type  = get_post_type( $post_id );
			$categories = get_the_category( $post_id );
			$taxonomies = get_taxonomies();


			$start_taxonomies = array( 'trend', 'storia', "speciale", "category", "post_tag" );
			foreach ( $taxonomies as $k => $tax ) {
				if ( in_array( $k, TBM_Advertising::$disabled_taxonomies ) || in_array( $tax, TBM_Advertising::$disabled_taxonomies ) ) {
					unset( $taxonomies[ $k ] );
				}
			}
			$tbm_advertising_topic_tax = get_option( 'tbm_advertising_topic_tax' );
			if ( empty( $tbm_advertising_topic_tax ) || false == $tbm_advertising_topic_tax ) {
				$tbm_advertising_topic_tax = $start_taxonomies;
			}

			$tbm_advertising_v_1_7 = (bool) get_option( 'tbm_advertising_v_1_7' );
			if ( ! $tbm_advertising_v_1_7 ) {
				# if it have a category (page)
				if ( ! ( 'gallery' == get_post_type() ) ) {
					if ( ! empty( $categories ) ) {
						TBM_Advertising::$banner_zones[] = 'cat-0';
					}
					foreach ( $categories as $category ) {
						if ( $category->category_parent == 0 ) {
							TBM_Advertising::$banner_zones[] = 'cat-' . $category->cat_ID;
						} // end if( intval( $category->parent ) === 0 )
					} // end foreach( $categories as $category )
				}
				# if it have a tax (page)
				$tax_initial = false;
				foreach ( $taxonomies as $taxonomy ) {
					$term_initial = false;
					if ( $taxonomy != 'post_tag' && $taxonomy != 'category' ) {
						$tts = wp_get_post_terms( $post_id, $taxonomy );
						if ( ! empty( $tts ) && false !== $tts ) {
							foreach ( $tts as $tt ) {
								if ( isset( $tt->term_id ) ) {
									if ( ! $tax_initial ) {
										TBM_Advertising::$banner_zones[] = 'tax-0';
										$tax_initial                     = true;
									}
									if ( ! $term_initial ) {
										TBM_Advertising::$banner_zones[] = 'tax-' . $taxonomy . "-term-0";
										$term_initial                    = true;
									}
									if ( $tt->parent != 0 ) {
										TBM_Advertising::$banner_zones[] = 'tax-' . $taxonomy . '-term-' . $tt->parent;
									}
									TBM_Advertising::$banner_zones[] = 'tax-' . $taxonomy . '-term-' . $tt->term_id;
								}
							}
						}
					}
				} // end foreach( $taxonomies as $taxonomy )
			}

			TBM_Advertising::$banner_zones[] = 'type-0';
			TBM_Advertising::$banner_zones[] = 'type-' . $post_type . '-0';
			TBM_Advertising::$banner_zones[] = 'type-' . $post_type . '-' . $post_id;

			# if it have a type other tags
			foreach ( $taxonomies as $k => $tax ) {
				if ( in_array( $k, $tbm_advertising_topic_tax ) || in_array( $tax, $tbm_advertising_topic_tax ) ) {
					unset( $taxonomies[ $k ] );
				}
			}
			foreach ( $taxonomies as $taxonomy ) {
				$term_initial = false;
				$tts          = wp_get_post_terms( $post_id, $taxonomy );
				if ( ! empty( $tts ) && false !== $tts ) {
					foreach ( $tts as $tt ) {
						if ( isset( $tt->term_id ) ) {
							if ( ! $term_initial ) {
								TBM_Advertising::$banner_zones[] = 'type-tax-' . $taxonomy . "-term-0";
								$term_initial                    = true;
							}
							if ( $tt->parent != 0 ) {
								TBM_Advertising::$banner_zones[] = 'type-tax-' . $taxonomy . '-term-' . $tt->parent;
							}
							TBM_Advertising::$banner_zones[] = 'type-tax-' . $taxonomy . '-term-' . $tt->term_id;
						}
					}
				}
			} // end foreach( $taxonomies as $taxonomy )
			# if it have a type tax speciale,post_tag,category
			foreach ( array_reverse( $tbm_advertising_topic_tax ) as $taxonomy ) {
				$term_initial = false;
				$tts          = wp_get_post_terms( $post_id, $taxonomy );
				if ( ! empty( $tts ) && false !== $tts ) {
					foreach ( $tts as $tt ) {
						if ( isset( $tt->term_id ) ) {
							if ( ( 'gallery' == get_post_type() && $valid_url !== false ) && ! ( $tt->taxonomy === 'post_tag' || $tt->taxonomy === 'category' ) ) {
								if ( ! $term_initial ) {
									TBM_Advertising::$banner_zones[] = 'type-tax-' . $taxonomy . "-term-0";
									$term_initial                    = true;
								}
								if ( $tt->parent != 0 ) {
									TBM_Advertising::$banner_zones[] = 'type-tax-' . $taxonomy . '-term-' . $tt->parent;
								}
								TBM_Advertising::$banner_zones[] = 'type-tax-' . $taxonomy . '-term-' . $tt->term_id;
							} else if ( ! ( 'gallery' == get_post_type() && $valid_url !== false ) ) {
								if ( ! $term_initial ) {
									TBM_Advertising::$banner_zones[] = 'type-tax-' . $taxonomy . "-term-0";
									$term_initial                    = true;
								}
								if ( $tt->parent != 0 ) {
									TBM_Advertising::$banner_zones[] = 'type-tax-' . $taxonomy . '-term-' . $tt->parent;
								}
								TBM_Advertising::$banner_zones[] = 'type-tax-' . $taxonomy . '-term-' . $tt->term_id;
							}
						}
					}
				}


			} // end foreach( $taxonomies as $taxonomy )

		}// end if( is_single() )

		// current page url
		TBM_Advertising::$banner_zones[] = TBM_Advertising::getCurrentPageUrl();
		TBM_Advertising::$banner_zones[] = TBM_Advertising::getCurrentPageUrl() . "*";

		// check banner zone GET
		if ( isset( $_GET['check_banner'] ) && $_GET['check_banner'] == 1 ) {
			var_dump( TBM_Advertising::$banner_zones );
		}

// create site_zone condition to use in future queries
		$_site_zones        = null;
		$total_banner_zones = count( TBM_Advertising::$banner_zones );
		if ( is_array( TBM_Advertising::$banner_zones ) && $total_banner_zones > 0 ) {
			$i = 0;
			foreach ( TBM_Advertising::$banner_zones as $cont_banner_label ) {
				$i ++;
				$_site_zones .= "`cont_site_zone` LIKE '" . esc_sql( $cont_banner_label ) . "'";
				if ( $i < $total_banner_zones ) {
					$_site_zones .= " OR ";
				}
			}
		}

// get banners not available
		$not_availables = $wpdb->get_results( "
            SELECT *
            FROM `" . TBM_Advertising::$table_banners_contents_not_available . "`, " . TBM_Advertising::$table_banners . "
            WHERE " . TBM_Advertising::$table_banners_contents_not_available . ".cont_banner_label=" . TBM_Advertising::$table_banners . ".banner_label
            AND (" . $_site_zones . ")
            GROUP BY `cont_banner_label`" );

// delete banners not available from allowerdBanners lists
		foreach ( $not_availables as $not_available ) {
			unset( TBM_Advertising::$allowedBanners[ $not_available->cont_banner_label ] );
		}

		if ( isset( $_GET['check_banner'] ) && $_GET['check_banner'] == 1 ) {
			echo '_________________________________<br>';
			echo '<br><strong>NOT AVAILABLES</strong><br>';
			echo '_________________________________<br><br>';
			echo "
            SELECT *
            FROM `" . TBM_Advertising::$table_banners_contents_not_available . "`, " . TBM_Advertising::$table_banners . "
            WHERE " . TBM_Advertising::$table_banners_contents_not_available . ".cont_banner_label=" . TBM_Advertising::$table_banners . ".banner_label
            AND (" . $_site_zones . ")
            GROUP BY `cont_banner_label`
            ";
			//die;
		}

// extract all "allowed" banners not empty
		$extract_banners = $wpdb->get_results( "
            SELECT *
            FROM `" . TBM_Advertising::$table_banners_contents . "`, " . TBM_Advertising::$table_banners . "
            WHERE " . TBM_Advertising::$table_banners_contents . ".cont_banner_label=" . TBM_Advertising::$table_banners . ".banner_label
            AND  `cont_banner_content` != ''
            AND (" . $_site_zones . " )
            AND `cont_banner_label` IN('" . implode( "','", array_keys( TBM_Advertising::$allowedBanners ) ) . "') 
        " );
//set currentBanners
		if ( ! empty( $extract_banners ) ) {
			foreach ( $extract_banners as $extract_banner ) {
				TBM_Advertising::$currentBanners[ strtoupper( $extract_banner->cont_banner_label ) ][ $extract_banner->cont_site_zone ] = $extract_banner->cont_banner_content;
			}
		}
		if ( isset( $_GET['check_banner'] ) && $_GET['check_banner'] == 1 ) {
			echo '<br>_________________________________<br>';
			echo '<br><strong>EXTRACT BANNERS</strong><br>';
			echo '_________________________________<br><br>';
			echo "
            SELECT *
            FROM `" . TBM_Advertising::$table_banners_contents . "`, " . TBM_Advertising::$table_banners . "
            WHERE " . TBM_Advertising::$table_banners_contents . ".cont_banner_label=" . TBM_Advertising::$table_banners . ".banner_label
            AND  `cont_banner_content` != ''
            AND (" . $_site_zones . " )
            AND `cont_banner_label` IN('" . implode( "','", array_keys( TBM_Advertising::$allowedBanners ) ) . "') 
        	";
			//die;
		}
// extract all "cont_banner_url_based" banners not empty
		$extract_banners_url = $wpdb->get_results( "
            SELECT *
            FROM `" . TBM_Advertising::$table_banners_contents . "`, " . TBM_Advertising::$table_banners . "
            WHERE " . TBM_Advertising::$table_banners_contents . ".cont_banner_label=" . TBM_Advertising::$table_banners . ".banner_label
            AND  `cont_banner_content` != ''
            AND  `cont_banner_url_based` = '1'
            AND  `cont_site_zone` LIKE '%*'
            AND `cont_banner_label` IN('" . implode( "','", array_keys( TBM_Advertising::$allowedBanners ) ) . "')
        " );

		if ( isset( $_GET['check_banner'] ) && $_GET['check_banner'] == 1 ) {

			echo '<br>_________________________________<br>';
			echo '<br><strong>EXTRACT BANNERS URLS</strong><br>';
			echo '_________________________________<br><br>';
			echo "
            SELECT *
            FROM `" . TBM_Advertising::$table_banners_contents . "`, " . TBM_Advertising::$table_banners . "
            WHERE " . TBM_Advertising::$table_banners_contents . ".cont_banner_label=" . TBM_Advertising::$table_banners . ".banner_label
            AND  `cont_banner_content` != ''
            AND  `cont_banner_url_based` = '1'
            AND  `cont_site_zone` LIKE '%*'
            AND `cont_banner_label` IN('" . implode( "','", array_keys( TBM_Advertising::$allowedBanners ) ) . "') 
        ";

		}
		//set currentBanners
		if ( ! empty( $extract_banners_url ) ) {
			foreach ( $extract_banners_url as $extract_banner ) {
				# if $extract_banner->cont_site_zone match TBM_Advertising::getCurrentPageUrl()
				$cont_size_zone = rtrim( $extract_banner->cont_site_zone, "*" );
				if ( substr( TBM_Advertising::getCurrentPageUrl(), 0, strlen( $cont_size_zone ) ) === $cont_size_zone ) {
					TBM_Advertising::$currentBanners[ strtoupper( $extract_banner->cont_banner_label ) ][ TBM_Advertising::getCurrentPageUrl() ] = $extract_banner->cont_banner_content;
					break;
				}
			}
		}

		if ( isset( $_GET['check_banner'] ) && $_GET['check_banner'] == 1 ) {
			$tbm_banner_array = TBM_Advertising::$currentBanners;

			array_walk_recursive( $tbm_banner_array, function ( &$value ) {
				$id    = uniqid( 'a' );
				$value = '<span class="adv_button" onclick="document.getElementById(\'' . $id . '\').style.display = \'block\'">+</span><span class="adv_hacker" id="' . $id . '">' . htmlspecialchars( $value, ENT_QUOTES, 'UTF-8' ) . '</span>';
			} );


			echo '<br>_________________________________<br>';
			echo '<style>.adv_hacker {display: none;background-color: black;padding: 10px;color: #20C20E;margin: 10px 0 0 10px}</style>';
			echo '<style>.adv_button {cursor: pointer; background-color: #1e8cc1;color: #fff;padding: 4px 6px;font-weight: 800;}</style>';
			echo '<br><strong>ARRAY BANNERS</strong><br>';
			echo '_________________________________<br><br>';
			print_r( $tbm_banner_array );
			echo '</pre>';
			die;
		}

	} // end function extractBanners()


	/*************************
	 * PRIVATE STATIC METHODS *
	 *************************/

	/**
	 * Install plugin if required
	 **/
	private
	static function createTables() {
		# use global db object
		# check if tables exists
		global $wpdb;
		if (
			$wpdb->get_var( "SHOW TABLES LIKE '" . TBM_Advertising::$table_banners . "'" ) != TBM_Advertising::$table_banners
			||
			$wpdb->get_var( "SHOW TABLES LIKE '" . TBM_Advertising::$table_banners_contents_not_available . "'" ) != TBM_Advertising::$table_banners_contents_not_available
			||
			$wpdb->get_var( "SHOW TABLES LIKE '" . TBM_Advertising::$table_banners_contents . "'" ) != TBM_Advertising::$table_banners_contents
		) {

			$wpdb->query( "
                    CREATE TABLE IF NOT EXISTS `" . TBM_Advertising::$table_banners_contents_not_available . "`
                    (
                        `cont_site_zone` text NOT NULL default '',
                        `cont_banner_label` varchar(64) NOT NULL default ''
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;
                " );

			$wpdb->query( "
                    CREATE TABLE IF NOT EXISTS `" . TBM_Advertising::$table_banners_contents . "`
                    (
                        `cont_site_zone` varchar(255) NOT NULL default '',
                        `cont_banner_label` varchar(64) NOT NULL default '',
                        `cont_banner_url_based` enum('0','1') NOT NULL default '0',
                        `cont_banner_content` text NULL,
 						PRIMARY KEY (`cont_site_zone`, `cont_banner_label`)
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;
                " );

			$wpdb->query( "
                    CREATE TABLE IF NOT EXISTS `" . TBM_Advertising::$table_banners . "`
                    (
                        `banner_label` varchar(64) NOT NULL default '',
                        `banner_desc` varchar(255) NOT NULL default '',
                        `banner_type` varchar(64) NOT NULL default 'banner',
                        PRIMARY KEY  (`banner_label`)
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;
                " );

			$values       = null;
			$i            = 0;
			$total_banner = count( TBM_Advertising::$initialAllowedBanners );
			foreach ( TBM_Advertising::$initialAllowedBanners as $k => $banner ) {
				$desc   = str_replace( '_', ' ', strtolower( $k ) );
				$values .= "('" . $k . "', '" . $desc . "', '" . $banner . "')";
				$i ++;
				if ( $total_banner > $i ) {
					$values .= ",";
				}
			}

			$wpdb->query( "
                    REPLACE INTO `" . TBM_Advertising::$table_banners . "`
                        (`banner_label`, `banner_desc`, `banner_type`)
                    VALUES
                        " . $values . "
                    ;" );
		}
	} // end function createTables()

	/**
	 * Set the db prefix
	 **/
	private
	static function setDbPrefix() {
		# use global db object
		global $wpdb;
		// TBM_Advertising::$dbPrefix = preg_replace( '#_[0-9]+_#', '_', $wpdb->prefix );
		TBM_Advertising::$dbPrefix = $wpdb->prefix;

	} // end function setDbPrefix()

	/**
	 * Set the db tables
	 **/
	private
	static function setDbTables() {
		TBM_Advertising::$table_banners_contents               = ( defined( "TBM_ADVERTISING_TABLE_BANNERS_CONTENTS" ) ) ? TBM_Advertising::$dbPrefix . TBM_ADVERTISING_TABLE_BANNERS_CONTENTS : TBM_Advertising::$dbPrefix . "advertising_banners_contents";
		TBM_Advertising::$table_banners_contents_not_available = ( defined( "TBM_ADVERTISING_TABLE_BANNERS_CONTENTS" ) ) ? TBM_Advertising::$dbPrefix . TBM_ADVERTISING_TABLE_BANNERS_CONTENTS_NOT_AVAILABLE : TBM_Advertising::$dbPrefix . "advertising_banners_contents_not_available";
		TBM_Advertising::$table_banners                        = ( defined( "TBM_ADVERTISING_TABLE_BANNERS" ) ) ? TBM_Advertising::$dbPrefix . TBM_ADVERTISING_TABLE_BANNERS : TBM_Advertising::$dbPrefix . "advertising_banners";
	} // end function setDbTables()

	/**
	 * Returns the current page url
	 **/
	private
	static function getCurrentPageUrl() {
		// if( is_single() ) {
		//  return get_permalink();
		// }
		return implode( '', array(
				( is_ssl() ) ? 'https' : 'http',
				'://',
				$_SERVER['SERVER_NAME'],
				! in_array( $_SERVER['SERVER_PORT'], array( 80, 443 ) ) ? ':' . $_SERVER['SERVER_PORT'] : '',
				strtok( $_SERVER["REQUEST_URI"], '?' )
			)
		);
	} // end function getCurrentPageUrl()

	/**
	 * Returns the banner row template
	 *
	 * @param type , string, required, The banner row type
	 *
	 * @return string
	 **/
	private
	static function getBannerRowTemplate(
		$type
	) {
		$row_template = '';
		switch ( $type ) {
			case 'zone':
				$row_template = '
                    <div class="banner-div banner-zone">
                        <div class="banner-type">
                            <span>%s</span>
                            <div class="switch_on_off">%s</div>
                        </div>
                        <div class="banner-content">
                            <textarea rows="8" cols="50" name="%s" %s>%s</textarea>
                        </div>
                    </div>';
				break;
			case 'url':
				$row_template = '
                    <div class="banner-div link-zone">
                        <div class="url-buttons">
                            <input type="hidden" name="pkey" value="%s" />
                            <a href="#" class="link-mod" title="' . __( 'Edit banner inserted', TBM_ADVERTISING_LANGUAGE_PLUGIN ) . '">
                                <span class="hidden">' . __( 'Edit banner inserted', TBM_ADVERTISING_LANGUAGE_PLUGIN ) . '</span>
                            </a>
                            <a href="#" class="link-del" title="' . __( 'Delete banner inserted', TBM_ADVERTISING_LANGUAGE_PLUGIN ) . '">
                                <span class="hidden">' . __( 'Delete banner inserted', TBM_ADVERTISING_LANGUAGE_PLUGIN ) . '</span>
                            </a>
                        </div>
                        <div colspan="2" class="full-url">%s</div>
                    </div>';
				break;
		} // end switch( $type )

		return $row_template;
	} // end function getBannerRowTemplate()

} // end class TBM_Advertising
