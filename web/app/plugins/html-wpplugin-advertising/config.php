<?php
/* LANGUAGES */
define('TBM_ADVERTISING_LANGUAGE_PLUGIN',		"tbm_advertising");

/* TABLES */
define('TBM_ADVERTISING_TABLE_BANNERS_CONTENTS_NOT_AVAILABLE',	"advertising_banners_contents_not_available");
define('TBM_ADVERTISING_TABLE_BANNERS_CONTENTS',	"advertising_banners_contents");
define('TBM_ADVERTISING_TABLE_BANNERS',			"advertising_banners");
