<?php
/*
Plugin Name: TBM Advertising
Plugin URI: http://www.triboomedia.it
Description: Plugin for managing banner on Triboomedia websites
Version: 4.8.0
Author: HTML.it
Author URI: http://www.triboomedia.it/
License: LGPL
*/

$version = 'version:4.8.0';

define( "TBM_ADVERTISING_DIR", plugin_dir_path( __FILE__ ) );
define( "TBM_ADVERTISING_URL", plugin_dir_url( __FILE__ ) );
define( 'TBM_ADVERTISING_VERSION', str_replace( 'version:', '', $version ) );

require_once TBM_ADVERTISING_DIR . '/config.php';
require_once TBM_ADVERTISING_DIR . '/php/class.advertising.php';

function loadLanguageMpAdvertising() {
  load_plugin_textdomain( TBM_ADVERTISING_LANGUAGE_PLUGIN, false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
}
add_action('plugins_loaded', 'loadLanguageMpAdvertising');

# init script
TBM_Advertising::init();
