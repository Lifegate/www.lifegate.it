/*global module:false*/
module.exports = function (grunt) {
    require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);

    grunt.initConfig({
        gitinfo: {
            commands: {
                'plugin_tag': ['describe', '--tags', 'HEAD']
            }
        },
        banner: '/*! TBM Advertising - v<%= gitinfo.plugin_tag %> - ' +
            '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
            '* Copyright (c) <%= grunt.template.today("yyyy") %> ' +
            'Triboo; Unlicensed; Francesco Caccavella  */\n',
        bump: {
            options: {
                files: ['package.json', 'html-wpplugin-advertising.php'],
                updateConfigs: [],
                commit: true,
                commitMessage: 'Release v%VERSION%',
                commitFiles: ['package.json', 'html-wpplugin-advertising.php'],
                createTag: true,
                tagName: '%VERSION%',
                tagMessage: 'Version %VERSION%',
                push: true,
                pushTo: 'origin',
                gitDescribeOptions: '--tags --always --abbrev=1 --dirty=-d',
                globalReplace: true,
                prereleaseName: false,
                regExp: false
            }
        }
    });

    grunt.registerTask('default', []);
    grunt.registerTask('docs', ['phpdocumentor:dist']);
};
