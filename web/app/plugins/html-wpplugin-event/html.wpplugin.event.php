<?php
/*
Plugin Name: TBM Events
Description: Plugin to handle events
Version: 3.3.1
*/

DEFINE( "HTML_EVENT_PLUGIN_DIR", plugin_dir_path( __FILE__ ) );

// configuration params
require_once( HTML_EVENT_PLUGIN_DIR . 'inc/define.php' );

// check activation dependencies
require_once( HTML_EVENT_PLUGIN_DIR . 'inc/activate.php' );

// custom post type
require_once( HTML_EVENT_PLUGIN_DIR . 'inc/custom-post-type.php' );

// acf fields
require_once( HTML_EVENT_PLUGIN_DIR . 'inc/acf.php' );

// templating rules
require_once( HTML_EVENT_PLUGIN_DIR . 'inc/template.php' );

// functions
require_once( HTML_EVENT_PLUGIN_DIR . 'inc/functions.php' );

