<?php
/**
 * Custom Post Type
 */

function html_event_custom_post_type() {
	register_post_type( HTML_EVENT_POST_TYPE,
		[
			'labels'      => [
				'name' => ucfirst( HTML_EVENT_POST_TYPE_NAME )
			],
			'public'      => true,
			'has_archive' => true,
			'rewrite'     => [ 'slug' => HTML_EVENT_POST_TYPE_SLUG ],
			'menu_icon'   => 'dashicons-tickets-alt',
			'supports'    => [ 'title', 'author', 'editor', 'thumbnail', 'excerpt' ],
			'description' => HTML_EVENT_POST_TYPE_DESCRIPTION
		]
	);

	// category & tags
	register_taxonomy_for_object_type( 'category', HTML_EVENT_POST_TYPE );

	register_taxonomy_for_object_type( 'post_tag', HTML_EVENT_POST_TYPE );

}

add_action( 'init', 'html_event_custom_post_type' );

