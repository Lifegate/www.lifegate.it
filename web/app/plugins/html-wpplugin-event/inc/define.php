<?php
// configuration
defined( 'HTML_EVENT_POST_TYPE' ) or define( 'HTML_EVENT_POST_TYPE', 'event' );
defined( 'HTML_EVENT_POST_TYPE_NAME' ) or define( 'HTML_EVENT_POST_TYPE_NAME', 'Eventi' );
defined( 'HTML_EVENT_POST_TYPE_SLUG' ) or define( 'HTML_EVENT_POST_TYPE_SLUG', 'eventi' );
defined( 'HTML_EVENT_POST_TYPE_DESCRIPTION' ) or define( 'HTML_EVENT_POST_TYPE_DESCRIPTION', 'Post Type per la pubblicazione di eventi' );