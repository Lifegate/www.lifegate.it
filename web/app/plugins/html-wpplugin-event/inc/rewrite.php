<?php
/**
 * Gestione struttura custom dei permalink
 */

/**
 * Personalizzo il permalink della domanda
 *
 * @param $url
 * @param $post
 *
 * @return string|void
 */
function html_event_single_event_permalink( $url, $post ) {
	if ( $post->post_type == HTML_EVENT_POST_TYPE ) {
		// recupero cat e subcat
		$cats_qa = wp_get_object_terms( $post->ID, 'category' );
		$cat     = "cat";
		$subcat  = "subcat";
		if ( ! empty( $cats_qa ) ) {
			if ( ! is_wp_error( $cats_qa ) ) {
				foreach ( $cats_qa as $term ) {
					if ( $term->parent == 0 ) {
						$cat = $term->slug;
					} else {
						$subcat = $term->slug;
					}
				}
			}
		}

		return home_url( '/' . $cat . '/' . $subcat . '/eventi/' . $post->ID . '/' . $post->post_name . ".html" );
	}

	return $url;
}

add_filter( 'post_type_link', 'html_event_single_event_permalink', 10, 2 );

/**
 * regexp per la gestione della rewrite custom del post type domanda
 */
function html_event_single_event_rewrite() {
	add_rewrite_rule(
		'^([^\/]+)\/([^\/]+)\/eventi\/([0-9]+)\/(.+?)?.html',
		'index.php?post_type=' . HTML_EVENT_POST_TYPE . '&p=$matches[3]',
		'top' );
}

add_action( 'init', 'html_event_single_event_rewrite' );