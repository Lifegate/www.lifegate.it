<?php
/**
 * Choose plugin template if single not exist
 */

/**
 * Checks to see if appropriate templates are present in active template directory.
 * Otherwises uses templates present in plugin's template directory.
 */
add_filter( 'template_include', 'html_event_set_template' );
function html_event_set_template( $template ) {
	if ( is_singular( HTML_EVENT_POST_TYPE ) ) {
		if ( function_exists( 'App\locate_template' ) ) {
			$located = App\locate_template( 'single-' . HTML_EVENT_POST_TYPE . '.php' );
		} else {
			$located = locate_template( 'single-' . HTML_EVENT_POST_TYPE . '.php' );
		}
		if ( empty( $located ) ) {
			//WordPress couldn't find an 'question' template. Use plug-in instead:
			$template = HTML_EVENT_PLUGIN_DIR . '/templates/single-' . HTML_EVENT_POST_TYPE . '.php';
		}
	}
	if ( is_post_type_archive( HTML_EVENT_POST_TYPE ) ) {
		if ( function_exists( 'App\locate_template' ) ) {
			$located = App\locate_template( 'single-' . HTML_EVENT_POST_TYPE . '.php' );
		} else {
			$located = locate_template( 'archive-' . HTML_EVENT_POST_TYPE . '.php' );
		}
		if ( empty( $located ) ) {
			//WordPress couldn't find an 'question' template. Use plug-in instead:
			$template = HTML_EVENT_PLUGIN_DIR . '/templates/archive-' . HTML_EVENT_POST_TYPE . '.php';
		}
	}

	return $template;
}


/**
 * Add plugin template from list
 *
 * @param $templates
 *
 * @return mixed
 *
 */
function html_event_add_page_template( $templates ) {
	$templates[ 'archive-old-' . HTML_EVENT_POST_TYPE . '.php' ] = 'Archivio Eventi Passati';

	return $templates;
}

add_filter( 'theme_page_templates', 'html_event_add_page_template' );


/**
 * Redirect page template to plugin
 *
 * @param $template
 *
 * @return string
 */
function html_event_redirect_page_template( $template ) {
	if ( is_page_template( 'old-' . HTML_EVENT_POST_TYPE . '.php' ) ) {
		$located = locate_template( 'archive-old-' . HTML_EVENT_POST_TYPE . '.php' );
		if ( empty( $located ) ) {
			$template = HTML_EVENT_PLUGIN_DIR . '/templates/archive-old-' . HTML_EVENT_POST_TYPE . '.php';
		}
	}

	return $template;
}

add_filter( 'page_template', 'html_event_redirect_page_template' );
