<?php
/**
 * Created by PhpStorm.
 * User: Francesco
 * Date: 10/11/2018
 * Time: 20:14
 */

/**
 * Formats all the event data
 *
 * @param int|WP_Post $post Optional. Post ID or post object. Default is the global `$post`.
 */

function tbm_get_event_data( $post = null ) {

	$post = get_post( $post );
	if ( empty( $post ) ) {
		return '';
	}
	$data = array();

	$meta = get_post_meta( $post->ID );
	if ( ! $meta ) {
		return '';
	}

	if ( ! isset( $meta['dateFrom'] ) || empty( $meta['dateFrom'] ) ) {
		$meta['dateFrom'][0] = '19700101';
	}

	if ( ! isset( $meta['dateTo'] ) || empty( $meta['dateTo'] ) ) {
		$meta['dateTo'][0] = '19700101';
	}

	$fromDateObject = DateTime::createFromFormat( 'Ymd', $meta['dateFrom'][0] );

	$toDateObject = DateTime::createFromFormat( 'Ymd', $meta['dateTo'][0] );

	$fromMyDate   = $fromDateObject ? $fromDateObject->getTimestamp() : 0;
	$toMyDate     = $toDateObject ? $toDateObject->getTimestamp() : $fromMyDate;
	$data['from'] = $fromMyDate;
	$data['to']   = $toMyDate;
	// FROM
	$data['d'] = date_i18n( 'd', $fromMyDate ); //02
	$data['j'] = date_i18n( 'j', $fromMyDate ); //2
	$data['f'] = date_i18n( 'F', $fromMyDate ); //Gen
	$data['m'] = date_i18n( 'M', $fromMyDate ); //Gennaio
	$data['y'] = date_i18n( 'Y', $fromMyDate ); //2018
	// TO
	$data['td']             = date_i18n( 'd', $toMyDate );
	$data['tj']             = date_i18n( 'j', $toMyDate );
	$data['tf']             = date_i18n( 'F', $toMyDate );
	$data['tm']             = date_i18n( 'M', $toMyDate );
	$data['ty']             = date_i18n( 'Y', $toMyDate ); //2018
	$data['location']       = isset( $meta['location'][0] ) ? $meta['location'][0] : '';
	$data['price']          = isset( $meta['price2'][0] ) ? $meta['price2'][0] : '';
	$data['euro']           = isset( $meta['price'][0] ) ? $meta['price'][0] : '';
	$data['organizzazione'] = isset( $meta['organizzation'][0] ) ? $meta['organizzation'][0] : '';
	$data['telefono']       = isset( $meta['phone'][0] ) ? $meta['phone'][0] : '';
	$data['email']          = isset( $meta['email'][0] ) ? $meta['email'][0] : '';
	$data['gmaps']          = isset( $meta['address'][0] ) ? $meta['address'][0] : '';
	$data['tipologia']      = isset( $meta['tipology'][0] ) ? $meta['tipology'][0] : '';
	$data['url']            = isset( $meta['url'][0] ) ? $meta['url'][0] : '';

	return $data;
}


add_action('wp_head',  function (){ ?>
	<?php
	// evento schema
	if(is_singular("event")){
		$data = tbm_get_event_data( get_the_ID() );
		$tipoevento = $data["tipologia"];
		$quando  = date("c", $data["from"]);
		$dove = $data["location"];
		$url = $data["url"];
		$gmaps = $data["gmaps"];
		$arraymap = unserialize($gmaps);
		$sede = $arraymap["name"];
		$description = get_the_excerpt(get_the_ID());
		// escludo gli eventi digitali a monte e in caso non sia valorizzato il luogo
		if($tipoevento != "" && (($tipoevento != "Evento digitale" && $dove != "") || ($tipoevento == "Evento digitale" && $url != ""))){
			?>
			<script type="application/ld+json"> { "@context": "https://schema.org","@type": "Event" <?php
				if($tipoevento != "Evento digitale" && $dove != ""){
					?>, "location": {"@type": "Place","address": {"@type": "PostalAddress" <?php
					if($dove){ ?>, "addressLocality": "<?php echo $dove; ?>"<?php }
					if($arraymap["state"]){ ?>, "addressRegion": "<?php echo $arraymap["state"]; ?>"<?php }
					if($arraymap["post_code"]){ ?>, "postalCode": "<?php echo $arraymap["post_code"]; ?>"<?php }
					if($arraymap["address"]){ ?> ,"streetAddress": "<?php echo $arraymap["address"]; ?>"<?php }
					?>} <?php if($sede){ ?>, "name": "<?php echo $sede; ?>" <?php } ?> } <?php
				}else if($tipoevento == "Evento digitale" && $url != ""){
					?>, "eventAttendanceMode": "https://schema.org/OnlineEventAttendanceMode", "location": {"@type": "VirtualLocation", "url": "<?php echo $url; ?>"}<?php
				}  ?>, "name": "<?php echo get_the_title(); ?>" <?php
				if($quando){ ?>, "startDate": "<?php echo $quando; ?>" <?php }
				if($description){ ?>, "description": "<?php echo addcslashes($description,  '"\\'); ?>" <?php
					$image = tbm_get_the_post_thumbnail_url(get_the_ID(),array(1920,1920));
					$image2 = tbm_get_the_post_thumbnail_url(get_the_ID(),array(1920,1080));
					$image3 = tbm_get_the_post_thumbnail_url(get_the_ID(),array(1920,1280));
					if($image){
						?>, "image": [ "<?php echo $image; ?>","<?php echo $image2; ?>","<?php echo $image3; ?>" ]<?php
					}
				}
				?> } </script>
			<?php
		}
	}
});
