<?php
/**
 * ACF fields & functions
 */
if ( function_exists( 'acf_add_local_field_group' ) ):

	acf_add_local_field_group( array(
		'key'                   => 'group_5a7473e123afb',
		'title'                 => 'Events Options',
		'fields'                => array(
			array(
				'key'               => 'field_5a7474520ad94',
				'label'             => 'Dal',
				'name'              => 'dateFrom',
				'type'              => 'date_picker',
				'instructions'      => '',
				'required'          => 1,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '50',
					'class' => '',
					'id'    => '',
				),
				'display_format'    => 'd/m/Y',
				'return_format'     => 'd/m/Y',
				'first_day'         => 1,
			),
			array(
				'key'               => 'field_5a7474a00ad95',
				'label'             => 'al',
				'name'              => 'dateTo',
				'type'              => 'date_picker',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '50',
					'class' => '',
					'id'    => '',
				),
				'display_format'    => 'd/m/Y',
				'return_format'     => 'd/m/Y',
				'first_day'         => 1,
			),
			array(
				'key'               => 'field_5a7473f00ad92',
				'label'             => 'Tipologia',
				'name'              => 'tipology',
				'type'              => 'select',
				'instructions'      => 'Tipo di evento',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '25',
					'class' => '',
					'id'    => '',
				),
				'choices'           => array(
					'Fiera'        => 'Fiera',
					'Concorso'     => 'Concorso',
					'Conf. Stampa' => 'Conf. Stampa',
					'Convegno'     => 'Convegno',
					'Roadshow'     => 'Roadshow',
					'Seminario'    => 'Seminario',
					'Corso'        => 'Corso',
					'Master'       => 'Master',
					'Concerto'     => 'Concerto',
					'Evento'     => 'Evento',
					'Evento diffuso'     => 'Evento diffuso',
					'Evento digitale'     => 'Evento digitale',
					'Evento diffuso e digitale'     => 'Evento diffuso e digitale',
					'Mostra'     => 'Mostra',
					'Cinema e TV'  => 'Cinema e TV'
				),
				'default_value'     => array(),
				'allow_null'        => 0,
				'multiple'          => 0,
				'ui'                => 1,
				'ajax'              => 0,
				'return_format'     => 'value',
				'placeholder'       => '',
			),
			array(
				'key'               => 'field_5a7474360ad93',
				'label'             => 'Organizzazione',
				'name'              => 'organizzation', // sic (from compatibility)
				'type'              => 'text',
				'instructions'      => 'Nome dell\'ente organizzatore',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '25',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
			),
			array(
				'key'               => 'field_5a7474bc0ad96',
				'label'             => 'Luogo',
				'name'              => 'location',
				'type'              => 'text',
				'instructions'      => 'Per esempio: <span syle="font-style: normal;font-weight: 700;">Verona</span>',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '25',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
			),
			array(
				'key'               => 'field_5a7474bc0xs72',
				'label'             => 'Sede',
				'name'              => 'tbm_event_venue',
				'type'              => 'text',
				'instructions'      => 'Per esempio:<span syle="font-style: normal;font-weight: 700;">Palafiera</span>',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '25',
					'class' => '',
					'id'    => '',
				),

			),

			array(
				'key'               => 'field_5a7474d60ad97',
				'label'             => 'Indirizzo e mappa',
				'name'              => 'address',
				'type'              => 'google_map',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'center_lat'        => '41.8919300',
				'center_lng'        => '12.5113300',
				'zoom'              => '',
				'height'            => '',
			),
			array(
				'key'               => 'field_5a7474ef0ad98',
				'label'             => 'Indirizzo e-mail',
				'name'              => 'email',
				'type'              => 'email',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '25',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
			),
			array(
				'key'               => 'field_5a7475050ad99',
				'label'             => 'Numero di telefono',
				'name'              => 'phone',
				'type'              => 'text',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '25',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
			),
			array(
				'key'               => 'field_5a7475130ad9a',
				'label'             => 'Approfondimenti(url)',
				'name'              => 'url',
				'type'              => 'url',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '25',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
			),
			array(
				'key'               => 'field_5a7475210ad9b',
				'label'             => 'Lingua',
				'name'              => 'language',
				'type'              => 'select',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '25',
					'class' => '',
					'id'    => '',
				),
				'choices'           => array(
					'Afrikaans'        => 'Afrikaans',
					'Albanese'         => 'Albanese',
					'Amharico'         => 'Amharico',
					'Arabo'            => 'Arabo',
					'Armeno'           => 'Armeno',
					'Basco'            => 'Basco',
					'Bengalese'        => 'Bengalese',
					'Bielorusso'       => 'Bielorusso',
					'Birmano'          => 'Birmano',
					'Bulgaro'          => 'Bulgaro',
					'Catalano'         => 'Catalano',
					'Ceco'             => 'Ceco',
					'Cinese'           => 'Cinese',
					'Croato'           => 'Croato',
					'Curdo'            => 'Curdo',
					'Danese'           => 'Danese',
					'Dari'             => 'Dari',
					'Dzongkha'         => 'Dzongkha',
					'Ebraico'          => 'Ebraico',
					'Esperanto'        => 'Esperanto',
					'Estone'           => 'Estone',
					'Faroese'          => 'Faroese',
					'Farsi'            => 'Farsi',
					'Finlandese'       => 'Finlandese',
					'Francese'         => 'Francese',
					'Gaelico'          => 'Gaelico',
					'Gallego'          => 'Gallego',
					'Gallese'          => 'Gallese',
					'Giapponese'       => 'Giapponese',
					'Greco'            => 'Greco',
					'Hindi'            => 'Hindi',
					'Indonesiano'      => 'Indonesiano',
					'Inglese'          => 'Inglese',
					'Inuit (Eskimo)'   => 'Inuit (Eskimo)',
					'Islandese'        => 'Islandese',
					'Italiano'         => 'Italiano',
					'Khmer'            => 'Khmer',
					'Koreano'          => 'Koreano',
					'Laotiano'         => 'Laotiano',
					'Lappone'          => 'Lappone',
					'Lettone'          => 'Lettone',
					'Lituano'          => 'Lituano',
					'Macedone'         => 'Macedone',
					'Malay'            => 'Malay',
					'Maltese'          => 'Maltese',
					'Nepalese'         => 'Nepalese',
					'Norvegese'        => 'Norvegese',
					'Olandese'         => 'Olandese',
					'Pashtu'           => 'Pashtu',
					'Polacco'          => 'Polacco',
					'Portoghese'       => 'Portoghese',
					'Rumeno'           => 'Rumeno',
					'Russo'            => 'Russo',
					'Scozzese'         => 'Scozzese',
					'Serbo'            => 'Serbo',
					'Slovacco'         => 'Slovacco',
					'Sloveno'          => 'Sloveno',
					'Somalo'           => 'Somalo',
					'Spagnolo'         => 'Spagnolo',
					'Svedese'          => 'Svedese',
					'Swahili'          => 'Swahili',
					'Tagalog-Filipino' => 'Tagalog-Filipino',
					'Tagiko'           => 'Tagiko',
					'Tamil'            => 'Tamil',
					'Tedesco'          => 'Tedesco',
					'Thai'             => 'Thai',
					'Tibetano'         => 'Tibetano',
					'Tigrinya'         => 'Tigrinya',
					'Tongan'           => 'Tongan',
					'Turco'            => 'Turco',
					'Turkmeno'         => 'Turkmeno',
					'Ucraino'          => 'Ucraino',
					'Ungherese'        => 'Ungherese',
					'Urdu'             => 'Urdu',
					'Uzbeco'           => 'Uzbeco',
					'Vietnamita'       => 'Vietnamita',
				),
				'default_value'     => array(
					0 => 'Italiano',
				),
				'allow_null'        => 0,
				'multiple'          => 0,
				'ui'                => 1,
				'ajax'              => 1,
				'return_format'     => 'value',
				'placeholder'       => '',
			),
			array(
				'key'               => 'field_5a7476030ad9c',
				'label'             => 'Costo',
				'name'              => 'price',
				'type'              => 'text',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '50',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
			),
			array(
				'key'               => 'field_5a7476170ad9d',
				'label'             => 'Tipo Costo',
				'name'              => 'price2',
				'type'              => 'select',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '50',
					'class' => '',
					'id'    => '',
				),
				'choices'           => array(
					'a pagamento'                  => 'a pagamento',
					'gratuito'                     => 'gratuito',
					'gratuito/previo invito'       => 'gratuito/previo invito',
					'gratuito/previa prenotazione' => 'gratuito/previa prenotazione',
				),
				'default_value'     => array(),
				'allow_null'        => 1,
				'multiple'          => 0,
				'ui'                => 1,
				'ajax'              => 0,
				'return_format'     => 'value',
				'placeholder'       => '',
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'post_type',
					'operator' => '==',
					'value'    => HTML_EVENT_POST_TYPE,
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => 1,
		'description'           => '',
	) );

endif;
