<?php
/**
 * Question Post Type Archive
 */

get_header(); ?>
<?php $obj = get_post_type_object( HTML_EVENT_POST_TYPE ); ?>
    <div class="wrap">
        <h1><?php echo $obj->label; ?></h1>
        <p><?php echo $obj->description; ?></p>
        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">
				<?php echo facetwp_display( 'template', 'event_query' ); ?>

				<?php echo facetwp_display( 'pager' ); ?>

            </main><!-- #main -->
        </div><!-- #primary -->
        <aside id="secondary" class="widget-area" role="complementary">
            <ul>
				<?php echo facetwp_display( 'facet', 'event_categories' ); ?>
				<?php echo facetwp_display( 'facet', 'event_location' ); ?>
				<?php echo facetwp_display( 'facet', 'event_text' ); ?>
            </ul>

        </aside><!-- #secondary -->
    </div><!-- .wrap -->

<?php get_footer();
