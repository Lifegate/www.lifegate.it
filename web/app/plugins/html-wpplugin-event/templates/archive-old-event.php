<?php
/**
 * Archivio Eventi Passati
 */
$today = current_time( 'Ymd' );

$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

$args = array(
	"post_type"   => "event",
	"post_status" => "publish",
	"paged"       => $paged
	"meta_query" => array(
	array(
		"key"     => "dateTo",
		"compare" => "<",
		"value"   => $today,
	)
)
);

get_header(); ?>
<?php $obj = get_post_type_object( HTML_EVENT_POST_TYPE ); ?>
    <div class="wrap">
        <h1><?php echo $obj->label; ?></h1>
        <p><?php echo $obj->description; ?></p>
        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">
				<?php
				$wp_query = new WP_Query( $args );

				//use the query for paging
				$wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;

				//set the "paginate_links" array to do what we would like it it. Check the codex for examples http://codex.wordpress.org/Function_Reference/paginate_links
				$pagination = array(
					'base'     => @add_query_arg( 'paged', '%#%' ),
					//'format' => '',
					'showall'  => false,
					'end_size' => 4,
					'mid_size' => 4,
					'total'    => $wp_query->max_num_pages,
					'current'  => $current,
					'type'     => 'plain'
				);

				//build the paging links
				if ( $wp_rewrite->using_permalinks() ) {
					$pagination['base'] = user_trailingslashit( trailingslashit( remove_query_arg( 's', get_pagenum_link( 1 ) ) ) . 'page/%#%/', 'paged' );
				}

				//more paging links
				if ( ! empty( $wp_query->query_vars['s'] ) ) {
					$pagination['add_args'] = array( 's' => get_query_var( 's' ) );
				}

				//run the query
				if ( $wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
					<?php echo get_field( "dateFrom" ); ?> - <?php echo get_field( "dateTo" ); ?>
                    <div><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>

				<?php endwhile; ?>
				<?php endif; ?>
				<?php
				echo '<div class="nav">' . paginate_links( $pagination ) . '</div>';
				?>
            </main><!-- #main -->
        </div><!-- #primary -->
        <aside id="secondary" class="widget-area" role="complementary">
            <ul>
				<?php
				$terms = get_terms( array(
					'taxonomy'   => 'category',
					'hide_empty' => false,
				) );

				foreach ( $terms as $term ) {
					echo '<li><a href="' . get_term_link( $term ) . '?post_type=' . HTML_EVENT_POST_TYPE . '">' . $term->name . '</a></li>';
				}
				?>
            </ul>

        </aside><!-- #secondary -->
    </div><!-- .wrap -->

<?php get_footer();
