<?php
/*
Plugin Name: Lifegate Blockchain
Description: Add post in blockchain
Version: 1.0.0
Author: Marco Buttarini
Author URI: https://bititup.it/
*/

// aggiungo campi all'autore
require_once("inc/acf.php");

DEFINE("LIFEGATE_BLOCKCHAIN_ENDPOINT", "https://public-api.timesafe.io");

add_action( 'save_post', 'lifegate_set_blockchain', 11, 2 );

function lifegate_set_blockchain( $post_id, $post ) {

	//error_log("entro in lifegate_set_blockchain");


	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
		return;

	// Only set for post publisher
	if ( 'publish' !== $post->post_status ) {
		return;
	}

	// Only set for post publisher
	if ( 'post' !== $post->post_type ) {
		return;
	}


	// recupero l'autore per vedere se è in blockchain
	$authors = get_field( 'autore_news' , $post_id);
	if ( ! $authors ) {
		return;
	}


	 // Old custom fields are strings. Cast to array
	if ( ! is_array( $authors ) ) {
		$authors = (array) $authors;
	}
	$author = $authors[0];

	// recupero il post autore
	$post_author = get_post( $author );
	if ( empty( $post_author ) ) {
		return;
	}

	// blockchain non abilitata
	$abilita_blockchain = get_field("abilita_blockchain", $post_author->ID);
	if(!$abilita_blockchain){
		return;
	}

	$username_blockchain = get_field("username_blockchain", $post_author->ID);
	if(!$username_blockchain){
		error_log("blockchain: missing username");
		return;
	}

	$password_blockchain = get_field("password_blockchain", $post_author->ID);
	if(!$password_blockchain){
		error_log("blockchain: missing pwd");
		return;
	}

	// ora ho tutti i dati per inviare in blockchain il post
	lifegate_send_to_chain($post, $username_blockchain, $password_blockchain);

}


function lifegate_get_hash_eth($post){

	$post_id = $post->ID;

	$blockchain_data = get_field("blockchain_data", $post_id);
	$datas = json_decode($blockchain_data, true);
	$certificate_id = $datas["certificate_id"];

	if(!$certificate_id){
		return false;
	}

	// recupero l'autore per vedere se è in blockchain
	$authors = get_field( 'autore_news' , $post_id);
	if ( ! $authors ) {
		return false;
	}


	// Old custom fields are strings. Cast to array
	if ( ! is_array( $authors ) ) {
		$authors = (array) $authors;
	}
	$author = $authors[0];

	// recupero il post autore
	$post_author = get_post( $author );
	if ( empty( $post_author ) ) {
		return false;
	}

	// blockchain non abilitata
	$abilita_blockchain = get_field("abilita_blockchain", $post_author->ID);
	if(!$abilita_blockchain){
		return false;
	}

	$username_blockchain = get_field("username_blockchain", $post_author->ID);
	if(!$username_blockchain){
		error_log("blockchain: missing username");
		return false;
	}

	$password_blockchain = get_field("password_blockchain", $post_author->ID);
	if(!$password_blockchain){
		error_log("blockchain: missing pwd");
		return false;
	}

	;

// faccio login per recuperare il token
	$url = LIFEGATE_BLOCKCHAIN_ENDPOINT."/user/login";

	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $url,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => '{ "email": "'.$username_blockchain.'", "secret": "'.$password_blockchain.'"}',
	));

	$response = curl_exec($curl);
	if (curl_errno($curl)) {
		error_log(curl_error($curl));
		return false;
	}
	curl_close($curl);

	$json = json_decode($response, true);

	if($json["status"] != "OK"){
		error_log("blockchain: status NOT OK");
		return false;
	}

	$token = $json["data"]["token"];
	if(!$token){
		error_log("blockchain: no token");
		return false;
	}

	// ho il token, chiamo la get per recuperare il dato

	$url = LIFEGATE_BLOCKCHAIN_ENDPOINT."/certificate/".$certificate_id;

	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $url,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'GET',
		CURLOPT_HTTPHEADER => array(
			'token: '.$token.'',
			'Authorization: Bearer qwerty',
			'Content-Type: application/json'
		),
	));

	$response = curl_exec($curl);
	if (curl_errno($curl)) {
		error_log(curl_error($curl));
		return false;
	}
	curl_close($curl);

	$json_put = json_decode($response, true);

	if($json_put["status"] != "OK"){
		error_log("blockchain: status NOT OK");
		return false;
	}


	$data = $json_put["data"];
	$hash_eth = $data["items"][0]["hash_eth"];
	if(!$hash_eth){
		return false;
	}

	update_field("blockchain_hash_eth", $hash_eth, $post);
	return $hash_eth;
}


function lifegate_send_to_chain($post, $username, $password){


	error_log("entro in lifegate_send_to_chain");

	// recupero se c'è un precedente id da settare come parent
	$parent = get_field("blockchain_main_id", $post);

	// creo lo sha1 del contenuto
	// todo: valutare se salvarlo pulito da html
	$hash = sha1(wpautop($post->post_content));

	// faccio login per recuperare il token
	$url = LIFEGATE_BLOCKCHAIN_ENDPOINT."/user/login";

	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $url,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => '{ "email": "'.$username.'", "secret": "'.$password.'"}',
	));

	$response = curl_exec($curl);
	if (curl_errno($curl)) {
		error_log(curl_error($curl));
		return false;
	}
	curl_close($curl);

	$json = json_decode($response, true);

	if($json["status"] != "OK"){
		error_log("blockchain: status NOT OK");
		return false;
	}

	$token = $json["data"]["token"];
	if(!$token){
		error_log("blockchain: no token");
		return false;
	}

	// ora che ho il token posso fare la push
	$url = LIFEGATE_BLOCKCHAIN_ENDPOINT."/certificate/put";

	$curl = curl_init();

	$strparent = "";
	if($parent){
		$strparent .= ' "parent" : '.$parent.',';
	}

	curl_setopt_array($curl, array(
		CURLOPT_URL => $url,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS =>'{
    "blockchain" : "Algorand",
    "hash_file_and_cf" : "0",
    "note" : "Lifegate PostID '.$post->ID.'",
    "fileName" : "'.$post->post_name.'",
    "url" : "'.get_permalink($post).'",
    '.$strparent.' "hash" : "'.$hash.'"
}',
		CURLOPT_HTTPHEADER => array(
			'token: '.$token.'',
			'Authorization: Bearer qwerty',
			'Content-Type: application/json'
		),
	));

	$response = curl_exec($curl);
	if (curl_errno($curl)) {
		error_log(curl_error($curl));
		return false;
	}
	curl_close($curl);

	$json_put = json_decode($response, true);

	if($json_put["status"] != "OK"){
		error_log("blockchain: status NOT OK");
		return false;
	}

	$data = $json_put["data"];

	// solo se ancora non c'era aggiorno id parent
	if(!$parent){
		update_field("blockchain_main_id", $data["certificate_id"] , $post);
	}

	update_field("blockchain_data", json_encode($data), $post);
	update_field("blockchain_sha1", $hash, $post);

	// svuoto il vecchio id transazione, o ne creo uno vuoto se è la prima
	update_field("blockchain_hash_eth", "", $post);


}
