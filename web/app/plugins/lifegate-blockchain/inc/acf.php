<?php

if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_60196dd518bef',
		'title' => 'Autore in Blockchain',
		'fields' => array(
			array(
				'key' => 'field_60196e0830dd1',
				'label' => 'Abilita blockchain',
				'name' => 'abilita_blockchain',
				'type' => 'true_false',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'message' => 'Abilitando l\'autore al blockchain, e inserendo le credenziali, i suoi articoli verranno automaticamente salvati in blockchain in fase di pubblicazione	e successivi aggiornamenti.',
				'default_value' => 0,
				'ui' => 1,
				'ui_on_text' => '',
				'ui_off_text' => '',
				'translations' => 'copy_once',
			),
			array(
				'key' => 'field_60196e6730dd2',
				'label' => 'Username Blockchain',
				'name' => 'username_blockchain',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => array(
					array(
						array(
							'field' => 'field_60196e0830dd1',
							'operator' => '==',
							'value' => '1',
						),
					),
				),
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => 'email as username',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'translations' => 'translate',
			),
			array(
				'key' => 'field_60196f3d30dd3',
				'label' => 'Password Blockchain',
				'name' => 'password_blockchain',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => array(
					array(
						array(
							'field' => 'field_60196e0830dd1',
							'operator' => '==',
							'value' => '1',
						),
					),
				),
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'translations' => 'translate',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'autori',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));

endif;

if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_6019b3ee950ab',
		'title' => 'Blockchain Data',
		'fields' => array(
			array(
				'key' => 'field_6019b419dff97',
				'label' => 'Blockchain Main ID',
				'name' => 'blockchain_main_id',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'translations' => 'translate',
			),
			array(
				'key' => 'field_6019b441dff98',
				'label' => 'Blockchain Data',
				'name' => 'blockchain_data',
				'type' => 'textarea',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'new_lines' => '',
				'translations' => 'translate',
			),
			array(
				'key' => 'field_6019b441dff11',
				'label' => 'Blockchain Sha1',
				'name' => 'blockchain_sha1',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'new_lines' => '',
				'translations' => 'translate',
			),
			array(
				'key' => 'field_6019b4416446',
				'label' => 'Blockchain hash_eth',
				'name' => 'blockchain_hash_eth',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'new_lines' => '',
				'translations' => 'translate',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
				),
			),
		),
		'menu_order' => 1033,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));

endif;
