<?php
add_filter( 'big_image_size_threshold', '__return_false' );

function html_gallery_add_plugin_scripts() {
	global $post;

	if ( is_singular( HTML_GALLERY_POST_TYPE ) || ( is_singular() && has_shortcode( $post->post_content, 'htmlgallery' ) ) ) {
		wp_enqueue_style( 'swiper-min', HTML_GALLERY_PLUGIN_URL . 'dist/css/swiper.min.css' );
		wp_enqueue_script( 'swiper-js', HTML_GALLERY_PLUGIN_URL . 'dist/js/swiper.js', array( 'jquery' ), "4.1.0" );
	}
}

// Commented script inclusion
//add_action( 'wp_enqueue_scripts', 'html_gallery_add_plugin_scripts' );

/**
 * Redirect to 404 if a page over count is requested
 */
add_action( 'template_redirect', function () {

	// If we aren't in a gallery post type
	if ( ! is_singular( 'gallery' ) ) {
		return;
	}

	$page = ( get_query_var( 'page' ) ) ?: 1;

	// If page requested is over the last photo, return 404
	if ( $page > tbm_get_gallery_photo_number( get_the_ID() ) ) {
		add_filter( 'template_include', 'tbm_404_template_filter', 99 );
	}
} );

/**
 * When publish gallery, save the gallery meta
 */
add_action( 'publish_' . HTML_GALLERY_POST_TYPE, 'save_gallery_meta', 10, 2 );