<?php
/**
 * Custom Post Type
 */

function html_gallery_custom_post_type() {
	register_post_type( HTML_GALLERY_POST_TYPE,
		[
			'labels'      => [
				'name' => ucfirst( HTML_GALLERY_POST_TYPE_NAME )
			],
			'public'      => true,
			'has_archive' => true,
			'rewrite'     => [ 'slug' => HTML_GALLERY_POST_TYPE_SLUG ],
			'menu_icon'   => 'dashicons-format-gallery',
			'supports'    => [ 'title', 'editor', 'author', 'thumbnail' ,'excerpt'],
			'description' => HTML_GALLERY_POST_TYPE_DESCRIPTION
		]
	);

	// category & tags
	register_taxonomy_for_object_type( 'category',HTML_GALLERY_POST_TYPE );

	register_taxonomy_for_object_type( 'post_tag',HTML_GALLERY_POST_TYPE );

}

add_action( 'init', 'html_gallery_custom_post_type' );
