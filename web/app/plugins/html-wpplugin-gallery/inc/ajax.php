<?php
/**
 * Import tracking IDs
 */
add_action( 'wp_ajax_tbm_save_gallery', function () {

	foreach ( get_intermediate_image_sizes() as $size ) {
		remove_image_size( $size );
	}

	add_filter( 'intermediate_image_sizes_advanced', function ( $sizes ) {

		unset( $sizes['medium'] );       // disable medium size
		unset( $sizes['large'] ); // disable large size
		unset( $sizes['medium_large'] ); // disable 768px size images
		unset( $sizes['1536x1536'] ); // disable 2x medium-large size
		unset( $sizes['2048x2048'] ); // disable 2x large size

		return $sizes;

	} );

	$gump = new GUMP();

	$_GET = $gump->sanitize( $_GET );

	$gump->validation_rules( array(
		'mynonce'   => 'required',
		'post_id'   => 'required',
		'tbm_image' => 'required'
	) );

	$gump->filter_rules( array(
		'mynonce'   => 'trim|sanitize_string',
		'post_id'   => 'trim|sanitize_string',
		'tbm_image' => 'trim'
	) );

	$validated_data = $gump->run( $_GET );

	if ( $validated_data === false ) {
		tbm_products_send_error_response( $gump );
	}

	//Verifiche varie: nonce
	if ( ! wp_verify_nonce( $validated_data['mynonce'], "tbm_video_import_gallery_images" ) ) {
		tbm_products_send_error_response( 'not authorized' );
	}

	/**
	 * Set variables
	 */
	$post_id = $validated_data['post_id'];
	$image   = $validated_data['tbm_image'];

	/**
	 * First: check if we have gallery
	 */
	$gallery = get_field( "tbm_gallery", $post_id, false ) ? get_field( "tbm_gallery", $post_id, false ) : array();


	if ( ! filter_var( $image['image_url'], FILTER_VALIDATE_URL ) ) {
		tbm_products_send_error_response( 'image is not a valid url' );
	}

	$image_id = tbm_process_gallery( $image['image_url'], $image['description'], $post_id );

	if ( $image_id ) {
		$gallery[] = $image_id;
		update_field( 'tbm_gallery', $gallery, $post_id );
	}

	// Set response
	tbm_products_send_success_response( 'success' );

} );
