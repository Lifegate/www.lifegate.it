<?php
/**
 *  Estensione wp-cli per processare i video
 */
if ( class_exists( "WP_CLI" ) ) {
	function html_gallery_convert( $args ) {
		global $wpdb;
		$debug = false;
		// ciclo su tutte le gallery

		if ( $debug ) :
			$galleries = get_posts(
				array(
					'post_type' => 'any',
					'post__in'  => array( 232726 )
				)
			);
		else :
			$galleries = get_posts(
				array(
					'post_type' => HTML_GALLERY_GALLERYIMPORT_POST_TYPE,
					'posts_per_page' => -1
				)
			);
		endif;


		foreach ( $galleries as $gallery ) {
			$gallery_id        = $gallery->ID;
			$post_thumbnail_id = get_post_thumbnail_id( $gallery_id );

			$attachments_array = array();

			WP_CLI::success( "Processo " . $gallery->post_title . ' #' . $gallery_id . ' con thumb #' . $post_thumbnail_id);

			// controllo se nel content c'è uno shortcode gallery nativo
			if ( get_post_meta( $gallery_id, 'tbm_gallery_processed', true ) !== '1' ) {
				WP_CLI::success( "Trovata Gallery " . get_post_meta( $gallery_id, 'tbm_gallery_processed', true ) );
				$images = $attachments = get_children( array(
					'post_parent'    => $gallery->ID,
					'post_status'    => 'inherit',
					'post_type'      => 'attachment',
					'post_mime_type' => 'image',
					'order'          => 'ASC',
					'orderby'        => 'menu_order ID'
				) );

				unset( $images[ $post_thumbnail_id ] );

				foreach ( $images as $image ) {
					$image_id            = $image->ID;
					$image_guid          = $image->guid;
					$image_count         = count( $images );
					$attachments_array[] = $image_id;
//					WP_CLI::success( $image_id . "--" . $image_guid );
				}

				WP_CLI::success( "Aggiorno Gallery" );
				update_field( 'tbm_gallery', $attachments_array, $gallery_id );

				// Update fields
				$photos = array_slice( $images, 0, 5 );

				foreach ( $photos as $photo ) {
					$out[] = $photo->ID;
				}


				WP_CLI::success( "Aggiorno Thumb" );
				update_post_meta( $gallery_id, 'tbm_gallery_thumb', $out );

				WP_CLI::success( "Aggiorno conteggio gallery " . $image_count );
				update_post_meta( $gallery_id, 'tbm_gallery_count', $image_count );

				WP_CLI::success( "Gallery Processato" );
				update_post_meta( $gallery_id, 'tbm_gallery_processed', '1' );

			} else {
				WP_CLI::warning( "Già processata #" . $gallery_id );
			}

		}

	}

	WP_CLI::add_command( 'galleryimport', 'html_gallery_convert' );
}


/**
 * Recupera id attachment from image url
 *
 * @param $url
 *
 * @return int
 */
function html_gallery_get_attachment_id( $url ) {
	$attachment_id = 0;
	$dir           = wp_upload_dir();
	if ( false !== strpos( $url, $dir['baseurl'] . '/' ) ) { // Is URL in uploads directory?
		$file       = basename( $url );
		$query_args = array(
			'post_type'   => 'attachment',
			'post_status' => 'inherit',
			'fields'      => 'ids',
			'meta_query'  => array(
				array(
					'value'   => $file,
					'compare' => 'LIKE',
					'key'     => '_wp_attachment_metadata',
				),
			)
		);

		$query = new WP_Query( $query_args );
		if ( $query->have_posts() ) {
			foreach ( $query->posts as $post_id ) {
				$meta          = wp_get_attachment_metadata( $post_id );
				$original_file = basename( $meta['file'] );
				if ( isset( $meta['sizes'] ) ) {
					$cropped_image_files = wp_list_pluck( $meta['sizes'], 'file' );
					if ( $original_file === $file || in_array( $file, $cropped_image_files ) ) {
						$attachment_id = $post_id;
						break;
					}
				}
			}
		}
	}

	return $attachment_id;
}

/**
 * Attaches images to Photogallery
 *
 * @param array
 *
 * @return bool
 */
if ( class_exists( "WP_CLI" ) ) {
	function html_attach_images_to_gallery($args) {
		$ok = false;
		if(!empty($args)){
			$post_id = $args[0];
			if($args[2] == 'onlyurls'){
				$arr_image_urls = unserialize($args[1]);
				add_post_meta( $post_id, 'tbm_gallery_urls', $arr_image_urls, true );
				$ok = true;
			} else {	
				$arr_image_ids = unserialize($args[1]);
				$arr_thumb_ids = unserialize($args[2]);
				$count_images = count($arr_image_ids);
				if($count_images > 0){
					update_field( 'tbm_gallery', $arr_image_ids, $post_id );
					update_field( 'tbm_gallery_thumb', $arr_thumb_ids, $post_id );
					update_post_meta( $post_id, 'tbm_gallery_count', $count_images );
					$ok = true;
				}
			}
		}
		return $ok;
	}
	WP_CLI::add_command( 'attachimagestogallery', 'html_attach_images_to_gallery' );
}
