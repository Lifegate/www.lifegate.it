<?php
/**
 * Activation tools
 */

add_action( 'admin_init', 'html_gallery_has_parent_plugin' );

/**
 * check if ACF plugin exist and is active
 */
function html_gallery_has_parent_plugin() {
	if ( is_admin() && current_user_can( 'activate_plugins' ) && ( ! class_exists( 'acf' ) ) ) {
		add_action( 'admin_notices', 'html_gallery_child_plugin_notice' );
		deactivate_plugins( HTML_GALLERY__PLUGIN_DIR . "/html.wpplugin.gallery.php" );
		if ( isset( $_GET['activate'] ) ) {
			unset( $_GET['activate'] );
		}
	}
}

/**
 * add notice to notify ACF dependencies
 */
function html_gallery_child_plugin_notice() {
	?>
    <div class="error">
    <p><?php _e( "Sorry, but Gallery plugin require <b>Advanced Custom Fields PRO</b> AND <b>FacetWP</b> plugin to be installed and active." ); ?></p>
    </div><?php
}