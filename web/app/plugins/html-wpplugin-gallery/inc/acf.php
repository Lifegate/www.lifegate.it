<?php
/**
 * ACF fields & functions
 */


if ( function_exists( 'acf_add_local_field_group' ) ):

	acf_add_local_field_group( array(
		'key'                   => 'group_5ace1593b7dc9',
		'title'                 => 'Gallery',
		'fields'                => array(
			array(
				'key'               => 'field_5e2b057c49a62',
				'label'             => 'Attivo Slider?',
				'name'              => 'tbm_gallery_slider',
				'type'              => 'true_false',
				'instructions'      => 'Se spuntato, la gallery verrà navigata a slider',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => 0,
				'ui'                => 1,
				'ui_on_text'        => 'Sì',
				'ui_off_text'       => 'No',
			),
			array(
				'key'               => 'field_5ace15cb4bdb1',
				'label'             => 'Seleziona le foto',
				'name'              => 'tbm_gallery',
				'type'              => 'gallery',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'min'               => '',
				'max'               => '',
				'insert'            => 'append',
				'library'           => 'all',
				'min_width'         => '',
				'min_height'        => '',
				'min_size'          => '',
				'max_width'         => '',
				'max_height'        => '',
				'max_size'          => '',
				'mime_types'        => '',
			),

		),
		'location'              => array(
			array(
				array(
					'param'    => 'post_type',
					'operator' => '==',
					'value'    => HTML_GALLERY_POST_TYPE,
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => 1,
		'description'           => '',
	) );

	acf_add_local_field_group( array(
		'key'                   => 'group_3a68b58abb2fa',
		'title'                 => 'Opzioni Paginazione gallery',
		'fields'                => array(
			array(
				'key'               => 'field_3a68b5987db2d',
				'label'             => 'Opzioni di paginazione',
				'name'              => 'post_pagination',
				'type'              => 'radio',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'choices'           => array(
					'general_option' => 'Utilizza le opzioni di default',
					'change_page'    => 'Cambio Pagina',
					'same_page'      => 'Paginazione in pagina',
				),
				'allow_null'        => 0,
				'other_choice'      => 0,
				'save_other_choice' => 0,
				'default_value'     => 'general_option',
				'layout'            => 'vertical',
				'return_format'     => 'value',
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'post_type',
					'operator' => '==',
					'value'    => HTML_GALLERY_POST_TYPE,
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'side',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => 1,
		'description'           => '',
	) );

endif;

/**
 * Personalizzo il layout del file video esponendone le info
 *
 * @param $arg
 */
add_action( 'acf/render_field/type=gallery', function ( $arg ) {

//	tbm_log( get_field( "tbm_gallery", $_GET['post']));


	/**
	 * Check post id
	 */
	if ( ! isset( $_GET['post'] ) || empty( $_GET['post'] ) ) {
		return;
	}

	/**
	 * Check field
	 */
	if ( $arg["key"] !== "field_5ace15cb4bdb1" ) {
		return;
	}

	/**
	 * Check images
	 */
	$images_old = get_field( "tbm_gallery_urls", $_GET['post'] );

	if ( ! $images_old ) {
		return;
	}

	/**
	 * Add Javascript to save correlator data
	 */
	$nonce  = wp_create_nonce( 'tbm_video_import_gallery_images' );
	$images = json_encode( $images_old );
	$count  = count( $images_old );
	$text   = '<button class="button button-info button-large button-save-gallery" id="save-gallery" data-spinner-color="black" data-count="' . $count . '">Recupera le <span class="badge badge-light">' . $count . '</span> immagini della gallery</button>';
	$text   .= '<div class="save-gallery-label instructions">Clicca per rendere visibili nel backend le immagini delle vecchie gallerie di Blogo. Le immagini vengono <strong>aggiunte</strong> a quelle attualmente presenti.</div>';
	$text   .= '<div class="save-gallery-label message"></div>';
	$text   .= "<script>var ajaxurl = '" . admin_url( 'admin-ajax.php' ) . "', tbmPostId = '" . $_GET['post'] . "', galleryNonce = '" . $nonce . "', tbmImages = " . $images . ";</script>";

	echo $text;

	/**
	 * Enqueue styles and scripts
	 */
	wp_enqueue_style( 'ladda', 'https://cdnjs.cloudflare.com/ajax/libs/Ladda/1.0.6/ladda.min.css' );
	wp_enqueue_script( 'tbm-search-spin', 'https://cdnjs.cloudflare.com/ajax/libs/Ladda/1.0.6/spin.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'tbm-search-ladda', 'https://cdnjs.cloudflare.com/ajax/libs/Ladda/1.0.6/ladda.min.js', array( 'tbm-search-spin' ), '', true );
	wp_enqueue_script( 'tbm-search-ladda-jquery', 'https://cdnjs.cloudflare.com/ajax/libs/Ladda/1.0.6/ladda.jquery.min.js', array( 'tbm-search-ladda' ), '', true );
	wp_enqueue_script( 'tbm-save_gallery', HTML_GALLERY_PLUGIN_URL . 'dist/js/gallery_correlation.min.js', array( 'jquery' ), HTML_GALLERY_PLUGIN_VERSION, true );
	wp_enqueue_style( 'tbm-save_gallery', HTML_GALLERY_PLUGIN_URL . 'dist/css/gallery_correlation.min.css', array(), HTML_GALLERY_PLUGIN_VERSION );


}, 20 );

