<?php
/**
 * Created by PhpStorm.
 * User: Francesco
 * Date: 15/12/2018
 * Time: 10:58
 */

/**
 * Return first five photo iDs from gallery
 *
 * @param int $gallery_id The id of the post gallery
 * @param int $offset The sequence will start from this element
 * @param int $num The numbers of ids to return
 *
 * @return array
 */
function tbm_get_gallery_thumbs( $gallery_id = 0, $offset = 0, $num = 1 ) {

	if ( empty( $gallery_id ) ) {
		return;
	}

	// Check if we have the IDs array saved in postmeta
	$photos_thumbs = get_field( "tbm_gallery_thumb", $gallery_id );

	if ( $photos_thumbs ) {
		return array_slice( $photos_thumbs, $offset, $num );
	}

	$photos_full = get_field( "tbm_gallery", $gallery_id );

	if ( ! is_array( $photos_full ) ) {
		return array();
	}

	// First save the meta for future access
	save_gallery_meta( $gallery_id );

	$photos = array_slice( $photos_full, $offset, $num );

	foreach ( $photos as $photo ) {
		$out[] = $photo['ID'];
	}

	return $out;
}

/**
 * Return the numbers of photos in gallery
 *
 * @param int $gallery_id The id of the post gallery
 *
 * @return int|void
 */
function tbm_get_gallery_photo_number( $gallery_id = 0 ) {
	if ( empty( $gallery_id ) ) {
		return;
	}

	// Check if we have the count saved in postmeta
	$photos_count = get_field( "tbm_gallery_count", $gallery_id );

	if ( $photos_count ) {
		return $photos_count;
	}

	$photos_full = get_field( "tbm_gallery", $gallery_id );

	if ( ! is_array( $photos_full ) || empty( $photos_full ) ) {
		return false;
	}

	// Save the meta for future access
	save_gallery_meta( $gallery_id );

	return count( get_field( "tbm_gallery", $gallery_id ) );
}

/**
 * Save the first five thumb ID and the images count in postmeta
 * Makes the DB query faster in front-page and archive gallery page
 */
function save_gallery_meta( $post_id ) {

	$photos_full = get_field( "tbm_gallery", $post_id );

	if ( ! is_array( $photos_full ) ) {
		return;
	}

	$photos = array_slice( $photos_full, 0, 5 );

	foreach ( $photos as $photo ) {
		$out[] = $photo['ID'];
	}

	update_post_meta( $post_id, 'tbm_gallery_thumb', $out );
	update_post_meta( $post_id, 'tbm_gallery_count', count( $photos_full ) );
}

/**
 * Processes a gallery
 *
 * @param int $post_id The ID for a post
 *
 * @return void
 **/
function tbm_process_gallery( $image_url = '', $image_description = '', $post_id ) {

	if ( ! $image_url ) {
		tbm_products_send_error_response( 'image is not a valid url' );
	}

	// Let's use a hash trick here to find our attachment post after it's been sideloaded.
	$hash = md5( 'attachment-hash' . $image_url . time() . rand( 1, 999 ) );

	$result = media_sideload_image( $image_url, $post_id, $hash );

	if ( is_wp_error( $result ) ) {
		tbm_products_send_error_response( 'error in upload image' );
	}

	$attachments = get_posts( array(
		'post_parent'    => $post_id,
		's'              => $hash,
		'post_type'      => 'attachment',
		'posts_per_page' => - 1,
	) );

	if ( ! $attachments || ! is_array( $attachments ) || count( $attachments ) != 1 ) {
		tbm_products_send_error_response( 'error in attachment' );
	}

	$attachment = $attachments[0];

	if ( $image_description ) {
		$attachment->post_content = $image_description;
		wp_update_post( $attachment );
	}

	return $attachment->ID;
}