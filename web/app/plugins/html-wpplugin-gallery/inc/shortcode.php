<?php

/**
 * Funzione per la generazione dello shortcode htmlgallery
 * example: [htmlgallery id="123"]
 * @param $atts
 *
 * @return string|void
 */
function htmlgallery_shortcode( $atts ) {
	if(!$atts['id'])
		return;

	$ret = ' <div class="swiper-container swiper-container-'.$atts['id'].'">';
	$ret .= ' <div class="swiper-wrapper">';
	$galleries = get_field( "tbm_gallery" , $atts['id']);
	foreach ( $galleries as $image ) {
		$ret .=  '<div class="swiper-slide"><img src="'.$image["sizes"]["large"].'" /></div>';
	}
	$ret .= '</div>';
	$ret .= '<div class="swiper-pagination"></div>';
	$ret .= '</div>';
	$ret .= '<style>.swiper-pagination-bullet {width: 20px;height: 20px;text-align: center;line-height: 20px;font-size: 12px;color:#000;opacity: 1;background: rgba(0,0,0,0.2);} .swiper-pagination-bullet-active {color:#fff;background: #007aff;}</style>';
	$ret .= '<script>var mySwiper'.$atts['id'].' = new Swiper (\'.swiper-container-'.$atts['id'].'\', {autoHeight: true,loop: true,pagination: { el: \'.swiper-pagination\', clickable: true, renderBullet: function (index, className) {return \'<span class="\' + className + \'">\' + (index + 1) + \'</span>\';},},}) </script>';

	return $ret;
}
add_shortcode( 'htmlgallery', 'htmlgallery_shortcode' );