<?php
/**
 * Gallery Post Type Archive
 */
get_header(); ?>
<?php $obj = get_post_type_object(HTML_GALLERY_POST_TYPE); ?>
    <div class="wrap">
        <h1><?php echo $obj->label; ?></h1>
        <p><?php echo $obj->description; ?></p>
        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">

				<?php
				/* Start the Loop */
				while ( have_posts() ) : the_post();

					the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
					the_excerpt();

					$gallery = get_field( "tbm_gallery" );
					$numpic = count($gallery);
					$c=0;
					foreach ($gallery as $image){
					    if($c<5){
					       echo "<a href='".get_permalink()."'><img src='".$image["sizes"]["thumbnail"]."' /></a>";
                        }
					    $c++;
                    }
                    echo $numpic." Immagini";
				endwhile; // End of the loop.
				?>
            </main><!-- #main -->
        </div><!-- #primary -->
        <aside id="secondary" class="widget-area" role="complementary">
            <ul>
				<?php
				$terms = get_terms( array(
					'taxonomy'   => 'category',
					'hide_empty' => false,
				) );

				foreach ( $terms as $term ) {
					echo '<li><a href="' . get_term_link( $term ) . '?post_type=' . HTML_GALLERY_POST_TYPE . '">' . $term->name . '</a></li>';
				}
				?>
            </ul>

        </aside><!-- #secondary -->
    </div><!-- .wrap -->

<?php get_footer();
