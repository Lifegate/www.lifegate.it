<?php
get_header();

global $post;

// option acf generali
$gallery_pagination_options = get_field("opzioni_paginazione_gallery", "options");

// opzioni acf singolo post
$gallery_pagination_post = get_field("post_pagination", $post);
if(($gallery_pagination_post != "") && ($gallery_pagination_post != "general_option"))
	$gallery_pagination_options = $gallery_pagination_post;

?>
    <div class="wrap">
        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">
				<?php while ( have_posts() ) : the_post(); ?>
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <header class="entry-header">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
							<?php edit_post_link( "Edit", '<span class="edit-link">', '</span>' ); ?>
                        </header><!-- .entry-header -->
                        <div class="entry-content">
							<?php
							$galleries = get_field( "tbm_gallery" );
							// paginazione in pagina senza reload
							if($gallery_pagination_options == "same_page") { ?>
                                <div class="swiper-container">
                                    <div class="swiper-wrapper">
										<?php foreach ( $galleries as $image ) {
											?>
                                            <div class="swiper-slide">
                                                <a href="<?php echo( $image["url"]); ?>"><img src="<?php echo( $image["sizes"]["large"] ); ?>" /></a>
                                            </div>
										<?php } ?>
                                    </div>
                                    <div class="swiper-pagination"></div>
                                </div>

							<?php }else{
								// paginazione con reload di pagina
								$page = (get_query_var('page')) ? get_query_var('page') : 1;
								?>
                                <div>
                                    <a href="<?php echo( $image["url"]); ?>"><img src="<?php echo $galleries[$page-1]["sizes"]["large"]; ?>" /></a>
                                </div>
								<?php
								// paginazione delle slide
								$totpages = count($galleries);
								echo paginate_links( array(
									'base' => get_permalink($post)."/%#%",
									'format' => '/%#%',
									'current' => $page,
									'total' => $totpages
								) );
							}

							the_content();
							?>
                        </div><!-- .entry-content -->
                    </article><!-- #post-## -->
				<?php endwhile; // End of the loop. ?>
            </main><!-- #main -->
        </div><!-- #primary -->
        <aside id="secondary" class="widget-area" role="complementary">
            <ul>
				<?php
				$terms = get_terms( array(
					'taxonomy'   => 'category',
					'hide_empty' => false,
				) );

				foreach ( $terms as $term ) {
					echo '<li><a href="' . get_term_link( $term ) . '?post_type=' . HTML_GALLERY_POST_TYPE . '">' . $term->name . '</a></li>';
				}
				?>
            </ul>
        </aside><!-- #secondary -->
    </div><!-- .wrap -->
<?php
get_footer();
// swiper
if($gallery_pagination_options == "same_page") {
	?>
    <style>
        .swiper-pagination-bullet {
            width: 20px;
            height: 20px;
            text-align: center;
            line-height: 20px;
            font-size: 12px;
            color:#000;
            opacity: 1;
            background: rgba(0,0,0,0.2);
        }
        .swiper-pagination-bullet-active {
            color:#fff;
            background: #007aff;
        }

    </style>
    <script>
        var mySwiper = new Swiper ('.swiper-container', {
            autoHeight: true,
            loop: true,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
                renderBullet: function (index, className) {
                    return '<span class="' + className + '">' + (index + 1) + '</span>';
                },
            },
        })
    </script>
	<?php
}


