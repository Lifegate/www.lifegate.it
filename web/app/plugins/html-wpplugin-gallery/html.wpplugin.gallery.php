<?php
/*
Plugin Name: Html.it Gallery
Description: Plugin to handle Gallery
Version: 2.7.4
*/

$version = 'version:2.7.4';

define( "HTML_GALLERY_PLUGIN_VERSION", str_replace( 'version:', '', $version ) );
define( "HTML_GALLERY__PLUGIN_DIR", plugin_dir_path( __FILE__ ) );
define( "HTML_GALLERY_PLUGIN_URL", plugin_dir_url( __FILE__ ) );


// configuration params
require_once( HTML_GALLERY__PLUGIN_DIR . 'inc/define.php' );

// check activation dependencies
require_once( HTML_GALLERY__PLUGIN_DIR . 'inc/activate.php' );

// custom post type
require_once( HTML_GALLERY__PLUGIN_DIR . 'inc/custom-post-type.php' );

// common functions
require_once( HTML_GALLERY__PLUGIN_DIR . 'inc/functions.php' );

// acf fields
require_once( HTML_GALLERY__PLUGIN_DIR . 'inc/acf.php' );

// templating rules
require_once( HTML_GALLERY__PLUGIN_DIR . 'inc/template.php' );

// actions & filters
require_once( HTML_GALLERY__PLUGIN_DIR . 'inc/actions-filters.php' );

// shortcode
require_once( HTML_GALLERY__PLUGIN_DIR . 'inc/shortcode.php' );

// Ajax
require_once( HTML_GALLERY__PLUGIN_DIR . 'inc/ajax.php' );

// wp-cli import
require_once( HTML_GALLERY__PLUGIN_DIR . 'inc/wp-cli.php' );

// escape ngg
require_once( HTML_GALLERY__PLUGIN_DIR . 'inc/escape-ngg-master/escape-ngg.php' );