/* globals console: false */
jQuery(document).ready(function ($) {

    var posttype = '',
        service = '',
        lookup = 'search',
        gh = document.querySelector('button.button-save-gallery') ? Ladda.create(document.querySelector('button.button-save-gallery')) : null;

    function modify_number(number) {
        var n = typeof number !== 'undefined' ? number + 1 : '0';
        jQuery("button .badge-light").text(n);
    }


    function ajaxRequest(urls, el) {
        if (urls.length > 0) {
            gh.start();
            $.ajax({
                url: ajaxurl,
                datatype: "json",
                cache: false,
                data: {
                    action: "tbm_save_gallery",
                    post_id: tbmPostId,
                    tbm_image: urls.pop(),
                    mynonce: galleryNonce
                }
            }).done(function (d) {
                ajaxRequest(urls, el);
            }).fail(function () {
                el.addClass("button-ko");
                $('.message').html('<p><strong>Errore!</strong> Abbiamo avuto un problema. Riprova e, se si dovesse ripetere, avvisa la NASA</p>');
                ajaxRequest(urls, el);
            }).always(function () {
                modify_number(urls.length);
            });
        } else {
            gh.stop();
            setTimeout(function () {
                window.location.reload(true);
            }, 1750);
            el.addClass("button-ok");
        }
    }

    /**
     * Handle save-gallery button
     */
    $('#save-gallery').click(function (e) {
        e.preventDefault();

        if ($(this).hasClass("button-ok")) {
            return;
        }

        if (typeof tbmPostId !== 'undefined' && typeof tbmImages !== 'undefined') {
            var el = $(this);
            ajaxRequest(tbmImages, el);
        } else {
            $('.message').html('<p><strong>Attenzione!</strong> Non possediamo la gallery</p>');
        }
    });

});
