# Plugin to handle Wordpress galleries

## Functions
**tbm_get_gallery_thumbs**: returns first five photo iDs from gallery
```php
tbm_get_gallery_thumbs( $gallery_id = 0, $offset = 0, $num = 1 )
```
**tbm_get_gallery_photo_number**: returns the numbers of photos in gallery
```php
tbm_get_gallery_photo_number( $gallery_id = 0 )
```

## Install packages (only first time)
First install global grunt `npm install -g grunt-cli` and then install the required packages `npm install`
### Bump the package and push
**Caution**: this command will push code to repo.

`grunt bump`  

This command will bump version in package.json and style.css, commit both files, tag them and **push** to origin master. You can use `grunt bump:patch` (0.0.1 > 0.0.2), `grunt bump:minor` (0.0.1 > 0.1.0) or `grunt bump:major` (0.0.1 > 1.0.0). Please, refer to [semantic versioning](https://semver.org/lang/it/) docs to choose wich bump use.
