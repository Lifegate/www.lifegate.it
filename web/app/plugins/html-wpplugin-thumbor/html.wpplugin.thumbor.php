<?php
/*
Plugin Name: TBM Thumbor
Description: Generate Thumbored Images
Version: 2.6.5
*/

$version = 'version:2.6.5';

define( "HTML_THUMBOR_PLUGIN_VERSION", str_replace( 'version:', '', $version ) );
define( "HTML_THUMBOR_PLUGIN_DIR", plugin_dir_path( __FILE__ ) );

// check activation dependencies
require_once( HTML_THUMBOR_PLUGIN_DIR . 'inc/activate.php' );
