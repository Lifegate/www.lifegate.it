<?php
/**
 * Activation tools
 */

/**
 * check if ACF plugin exist and is active
 */
if ( ( ! class_exists( 'acf' ) ) ) {
	// If ACF is not enabled
	add_action( 'admin_notices', function () {
		?>
		<div class="notice notice-error notice-large">
			<p>Il plugin <strong>TBM Thumbor</strong> non è abilitato. Per abilitarlo è necessario scaricare e attivare il plugin <a target="_blank" href="https://www.advancedcustomfields.com/pro/">ACF Pro</a>. Segnala questo messaggio all'amministratore di sitema.</p>
		</div>
		<?php
	} );
} else {
	// If ACF is enabled
	require_once( HTML_THUMBOR_PLUGIN_DIR . 'inc/acf.php' );
	require_once( HTML_THUMBOR_PLUGIN_DIR . 'inc/actions-filters.php' );
}