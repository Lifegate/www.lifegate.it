<?php
/**
 * ACF fields & functions
 */


/**
 * Pagina di opzioni
 */
if ( function_exists( 'acf_add_options_page' ) ) {
	$args = array(
		'page_title' => 'TBM Config',
		'menu_slug'  => 'tbm_config',
		'icon_url'   => 'dashicons-list-view',
		'capability' => 'activate_plugins',
	);
	acf_add_options_page( $args );

	acf_add_options_sub_page( array(
		'page_title'  => 'Opzioni Thumbor HTML.it plugin',
		'menu_title'  => '[Thumbor] Configurazione',
		'menu_slug'   => 'thumbor_blocks',
		'parent_slug' => 'tbm_config',
		'capability'  => 'activate_plugins',
	) );
}

/**
 * Campi ACF
 */

add_action( "init", "html_thumbor_init_acf", 11 );

function html_thumbor_init_acf() {

	if( function_exists('acf_add_local_field_group') ):

		acf_add_local_field_group(array (
			'key' => 'group_5aa96ff2d1c64',
			'title' => 'Thumbor (Recovered)',
			'fields' => array (
				array (
					'key' => 'field_5aa974e882449',
					'label' => 'Attiva Thumbor per le immagini nel contenuto',
					'name' => 'tbm_thu_content',
					'type' => 'true_false',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '50',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'default_value' => 0,
					'ui' => 1,
					'ui_on_text' => '',
					'ui_off_text' => '',
				),
				array (
					'key' => 'field_5aa974c082448',
					'label' => 'Attiva Thumbor per le immagini in primo piano',
					'name' => 'tbm_thu_featured',
					'type' => 'true_false',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '50',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'default_value' => 0,
					'ui' => 1,
					'ui_on_text' => '',
					'ui_off_text' => '',
				),
				array (
					'key' => 'field_5aa97002e615d',
					'label' => 'Endpoint URL',
					'name' => 'tbm_thu_endpoint_url',
					'type' => 'text',
					'instructions' => 'Nella forma http://&lt;thumbor&gt;:&lt;port&gt;',
					'required' => 1,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_5aa974e882449',
								'operator' => '==',
								'value' => '1',
							),
						),
						array (
							array (
								'field' => 'field_5aa974c082448',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '50',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array (
					'key' => 'field_5aa97144e615e',
					'label' => 'Secret Key',
					'name' => 'tbm_thu_secret_key',
					'type' => 'text',
					'instructions' => 'Secret key per la generazione delle URL',
					'required' => 1,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_5aa974e882449',
								'operator' => '==',
								'value' => '1',
							),
						),
						array (
							array (
								'field' => 'field_5aa974c082448',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '50',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array (
					'key' => 'field_5ab958ee2cb68',
					'label' => 'Path da sostituire',
					'name' => 'tbm_thu_replace',
					'type' => 'text',
					'instructions' => 'Indica il testo che verrà sostituito - nell\'url - dal percorso di Thumbor. Se p.es. indichi <strong>http://www.webnews.it/app/</strong>, l\'immagine <strong>http://www.webnews.it/app/</strong>uploads/2017/02/image.png verrà sostituita da https://xxx.cloudfront.net/uploads/2017/02/image.png. Il valore predefinito è <strong>' . get_bloginfo( 'url' ) . '/app/</strong>',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '100',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array (
					'key' => 'field_5ab958ee2cb67',
					'label' => 'Escludi le stringhe',
					'name' => 'tbm_thu_exclude',
					'type' => 'text',
					'instructions' => 'Indica, separati da virgola, le stringhe da cercare nell\'url per escluderla dalla riscritttura',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '65',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),

				array (
					'key' => 'field_5ab958cd2cb67',
					'label' => 'Thumbor Smart Detection',
					'name' => 'tbm_thu_smart_detection',
					'type' => 'true_false',
					'instructions' => 'Se Sì, attiva la smart detection di Thumbor',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '35',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'default_value' => 0,
					'ui' => 1,
					'ui_on_text' => '',
					'ui_off_text' => '',
				),

			),
			'location' => array (
				array (
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'thumbor_blocks',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));

	endif;

}


