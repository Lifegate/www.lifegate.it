<?php
$rewrite_thumb       = get_field( 'tbm_thu_featured', 'option' );
$rewrite_content_img = get_field( 'tbm_thu_content', 'option' );

/**
 * Remove image size pattern in file name. Is not compatible with this plugin
 */
add_action( 'wp_handle_upload_prefilter', function ( $file ) {

	if ( in_array( $file['type'], array( 'image/jpg', 'image/png', 'image/jpeg', 'image/gif' ) ) ) {
		$file['name'] = preg_replace( '/-[0-9]{2,4}x[0-9]{2,4}\.(jpg|png|jpeg|gif)/i', '.$1', $file['name'] );
	}

	return $file;
} );

/**
 * Rewrite URL in function plugin is active and we are not in admin
 */
if ( $rewrite_thumb && ! is_admin() ) {
	add_filter( 'wp_get_attachment_image_src', 'tbm_fix_thumbnail_img', 1, 2 );
	add_filter( 'wp_calculate_image_srcset', 'tbm_fix_srcset_img', 10, 3 );
}

/**
 * Rewrite URL in content if plugin is active and we are not in admin
 */
if ( $rewrite_content_img ) {
	add_filter( 'the_content', 'tbm_fix_content_img', 10 );
}

/**
 * Get wordpress image dimension from filename. P.es. http://www.site.it/wp-content/uploads/2018/02/img-1024x645.jpg
 *
 * @param $image Url of the image
 *
 * @return string The thumbor url of the image
 */
function tbm_get_thumbor_img_from_url( $image ) {
	$regex = '#(?:' . preg_quote( home_url(), "/" ) . ')?\/(?:app|wp-content)\/uploads\/[0-9]{4}\/[0-9]{2}\/[^\\" ]+[_-](\d+)x(\d+)\.(?:jpe?g|png|gif)#i';
	global $content_width;
	preg_match( $regex, $image, $match );
	if ( $match ) {
		$width  = ( isset( $content_width ) && ( $match[1] > $content_width ) ) ? $content_width : $match[1];
		$heigth = $match[2];
		$src    = preg_replace( '/[_-](?:\d+)x(?:\d+)\.(jpe?g|png|gif)/i', '.$1', $image );
		$out    = tbm_get_thumbor_img( $src, $width, $heigth );

		return $out;
	}

	return false;

}

/**
 * Filter the srcset images
 *
 * @param array $sources One or more arrays of source data to include in the 'srcset'.
 *
 * @type array $width {
 * @type string $url The URL of an image source.
 * @type string $descriptor The descriptor type used in the image candidate string,
 *                                  either 'w' or 'x'.
 * @type int $value The source width if paired with a 'w' descriptor, or a
 *                                  pixel density value if paired with an 'x' descriptor.
 *
 * @return array
 */
function tbm_fix_srcset_img( $sources ) {
	foreach ( $sources as $key => $item ) {
		if ( is_int( $key ) ) {
			$sources[ $key ]['url'] = tbm_get_thumbor_img( $item['url'], $key, '' );
		}
	}

	return $sources;

}

/**
 * Rewrite the url of the image in post content
 *
 * @param string $content The post content
 *
 * @return null|string|string[]
 */
function tbm_fix_content_img( $content ) {
	// If there's no images in $content, return $content immediately
	if ( stripos( $content, "<img" ) === false ) {
		return $content;
	}

	// Get upload host (sometimes is different from current host)
	$upload_dir  = wp_get_upload_dir()['baseurl'];
	$parsed_url  = parse_url( $upload_dir );
	$host_upload = $parsed_url['scheme'] . '://' . $parsed_url['host'];

	// Build the regex to match local images in content
	$regex = '#(?:' . preg_quote( home_url(), "/" ) . '|' . $host_upload . ')?\/(?:app|wp-content)\/uploads\/[0-9]{4}\/[0-9]{2}\/[^\\" ]+[_-](\d+)x(\d+)\.(jpe?g|png|gif)#i';

	// Match the image in $content
	preg_match_all( $regex, $content, $matches );


	// If match, build the thumbor url of the image
	if ( $matches ) {
		foreach ( $matches[0] as $match ) {
			$thumb_url = tbm_get_thumbor_img_from_url( $match );

			if ( $thumb_url ) {
				$content = str_replace( $match, $thumb_url, $content );
			}
		}
	}

	// ora cerco le immagini che NON hanno la dimensione nella url (le originali)

	$regex = "/<img(.*?)src=('|\")(.*?).(bmp|jpeg|jpg|png)(|\")(.*?)>/i";
	// Match the image in $content
	preg_match_all( $regex, $content, $matches );
	// If match, build the thumbor url of the image
	if ( $matches ) {
		$m=0;
		foreach($matches[3] as $match){

			$match = $match.".".$matches[4][$m];
			if(substr($match, 0, strlen(home_url())) === home_url()){
				$thumb_url =  tbm_get_thumbor_img( $match, 768, "" );
				$content = str_replace( 'src="'.$match, 'src="'.$thumb_url, $content );
			}

			// cerco di sostituire eventuali path incompleti /app/upload
			if(substr($match, 0, 13) === "/app/uploads/"){
				$thumb_url =  tbm_get_thumbor_img( home_url(). $match, 768, "" );
				$content = str_replace( 'src="'.$match, 'src="'.$thumb_url, $content );
			}

			if(substr($match, 0, strlen($host_upload)) === $host_upload){
				$thumb_url =  tbm_get_thumbor_img( $match ,  768, "");
				$content = str_replace( 'src="'.$match, 'src="'.$thumb_url, $content );
			}

			$m++;
		}
	}

	// ora cerco i link alle immagini che NON hanno la dimensione nella url (le originali)

	$regex = "/<a(.*?)href=('|\")([^\"]*?).(bmp|jpeg|jpg|png)('|\")(.*?)>/i";
	// Match the image in $content
	preg_match_all( $regex, $content, $matches );
	// If match, build the thumbor url of the image
	if ( $matches ) {
		$m=0;
		foreach($matches[3] as $match){

			if(in_array($matches[4][$m], array("bmp","jpeg","jpg","png")) ){
				$match = $match.".".$matches[4][$m];
				if(substr($match, 0, strlen(home_url())) === home_url()){
					$thumb_url =  tbm_get_thumbor_img( $match, 1600, "" );
					$content = str_replace( 'href="'.$match, 'href="'.$thumb_url, $content );
				}

				if(substr($match, 0, strlen($host_upload)) === $host_upload){
					$thumb_url =  tbm_get_thumbor_img( $match ,  1600, "");
					$content = str_replace( 'href="'.$match, 'href="'.$thumb_url, $content );
				}
			}


			$m++;
		}
	}



	return $content;
}

/**
 * Filter the thumbnail image
 *
 * @param string $image
 *
 * @return string
 */
function tbm_fix_thumbnail_img( $image = '' ) {

	if ( ! filter_var( $image[0], FILTER_VALIDATE_URL ) ) {
		return $image;
	}

	$src    = $image[0];
	$width  = $image[1];
	$height = $image[2];

	$image[0] = tbm_get_thumbor_img( $src, $width, $height );

	return $image;
}

/**
 * Get a thumbor URL from standard img url
 *
 * @param string $src Url of the image to rewrite
 * @param int $width Width of the returned image
 * @param int $height Height of the returned image
 * @param bool $metadata Optional. If true, returns metadata of the image and not the image. Default false.
 * @param bool $strip_dimensions Optional. If true, remove the image dimensions (p.es. 800x200) from filename. Default true.
 *
 * @return mixed Url of the image or false
 */
function tbm_get_thumbor_img( $src, $width, $height, $metadata = false, $strip_dimensions = true ) {

	$rewrite_thumb = get_field( 'tbm_thu_featured', 'option' );
	$server        = get_field( 'tbm_thu_endpoint_url', 'option' );
	$secret        = get_field( 'tbm_thu_secret_key', 'option' );
	$smart         = get_field( 'tbm_thu_smart_detection', 'option' ) ? true : false;
	$exclude       = get_field( 'tbm_thu_exclude', 'option' ) ? explode( ',', get_field( 'tbm_thu_exclude', 'option' ) ) : false;
	$replace       = get_field( 'tbm_thu_replace', 'option' ) ? get_field( 'tbm_thu_replace', 'option' ) : '';

	if ( is_array( $exclude ) ) {
		foreach ( $exclude as $item ) {
			if ( stristr( $src, $item ) != false ) {
				return $src;
			}
		}
	}

	if ( ! $server || ! $secret ) {
		return $src;
	}

	$thumbnailUrlFactory = Thumbor\Url\BuilderFactory::construct( $server, $secret );

	if ( $thumbnailUrlFactory ) {

		if ( $replace ) {
			if ( $width && $height ) {
				$src = str_replace( array( $replace, '-' . $width . 'x' . $height ), '', $src );
			} else {
				$src = str_replace( array( $replace ), '', $src );
			}
		}

		if ( $strip_dimensions ) {
			$src = preg_replace( '/-[0-9]{2,4}x[0-9]{2,4}\.(jpg|png|jpeg|gif)/i', '.$1', $src );
		}

		/**
		 * Encode the filename
		 */

		$src      = str_replace( 'http://tbm.lifegate.com/', 'https://prod.lifegate.it/', $src );
		$src      = str_replace( 'http://tbm.lifegate.it/', 'https://prod.lifegate.it/', $src );
		$src      = str_replace( 'https://tbm.lifegate.it/', 'https://prod.lifegate.it/', $src );
		$src      = str_replace( 'https://dev.lifegate.it/', 'https://prod.lifegate.it/', $src );
		$src      = str_replace( 'https://dev.lifegate.com/', 'https://prod.lifegate.it/', $src );
		$basename = basename( $src );
		$src      = str_replace( $basename, rawurlencode( $basename ), $src );


		/**
		 * The magic
		 */
		$thumborl = $thumbnailUrlFactory
			->url( $src )
			->smartCrop( $smart )
			->resize( $width, $height )
			->metadataOnly( $metadata );

		$image = $thumborl;

		return (string) $image;

	}

	return $src;
}

/**
 * Get the height and width ratio of an image attachment (rewrited with Thumbor)
 *
 * @param int $attachment_id Image attachment ID.
 * @param array $size Optional. Image size to retrieve. Accepts array of height and width dimensions. Defaults 50,50.
 *
 * @return int|false Width / Height ratio
 */
function tbm_get_thumbor_img_ratio( $attachment_id, $size = array( 50, 50 ) ) {

	if ( ! is_int( $attachment_id ) ) {
		return false;
	}

	remove_filter( 'wp_get_attachment_image_src', 'tbm_fix_thumbnail_img', 1, 2 );
	remove_filter( 'wp_calculate_image_srcset', 'tbm_fix_srcset_img', 10, 3 );
	$src = wp_get_attachment_image_src( $attachment_id, $size );

	if ( ! isset( $src['0'] ) ) {
		return false;
	}
	$width  = $size[0];
	$height = $size[1];
	add_filter( 'wp_get_attachment_image_src', 'tbm_fix_thumbnail_img', 1, 2 );
	add_filter( 'wp_calculate_image_srcset', 'tbm_fix_srcset_img', 10, 3 );

	$url = tbm_get_thumbor_img( $src['0'], $width, $height, true );

	$response = @file_get_contents( $url );

	if ( ! $response ) {
		return false;
	}

	$json = json_decode( $response );

	if ( json_last_error() !== JSON_ERROR_NONE ) {
		return false;
	}

	$height = $json->thumbor->target->height;
	$width  = $json->thumbor->target->width;

	if ( ! $height || ! $width ) {
		return false;
	}

	return ( $height / $width ) * 100;
}

/**
 * Get the original height and width of an image attachment
 *
 * @param int $attachment_id Image attachment ID.
 *
 * @return array|false Width (0) and Height (1)
 */
function tbm_get_thumbor_img_original_size( $attachment_id ) {

	if ( ! is_int( $attachment_id ) ) {
		return false;
	}

	remove_filter( 'wp_get_attachment_image_src', 'tbm_fix_thumbnail_img', 1, 2 );
	remove_filter( 'wp_calculate_image_srcset', 'tbm_fix_srcset_img', 10, 3 );
	$src = wp_get_attachment_url( $attachment_id );

	if ( ! isset( $src['0'] ) ) {
		return false;
	}

	add_filter( 'wp_get_attachment_image_src', 'tbm_fix_thumbnail_img', 1, 2 );
	add_filter( 'wp_calculate_image_srcset', 'tbm_fix_srcset_img', 10, 3 );

	$url = tbm_get_thumbor_img( $src, 0, 0, true );

	/**
	 * Get  the response
	 */
	$response = @file_get_contents( $url );

	if ( ! $response ) {
		return false;
	}

	$json = json_decode( $response );

	if ( json_last_error() !== JSON_ERROR_NONE ) {
		return false;
	}

	$width  = $json->thumbor->target->width;
	$height = $json->thumbor->target->height;

	if ( ! $height || ! $width ) {
		return false;
	}

	return array( $width, $height );
}


/**
 * filtro la sitemap yoast
 */

function filter_wpseo_sitemap_urlimages( $images, $post_id ) {
	$x=0;
	foreach ($images as $image) {

		$images[$x]["src"] =  tbm_get_thumbor_img( $image["src"], 1600, "" );
		$x++;
	}
	return $images;
};
add_filter( 'wpseo_sitemap_urlimages', 'filter_wpseo_sitemap_urlimages', 10, 2 );

/**
 * redirect attachment to cdn
 */
function lg_redirect_attachment_to_cdn() {
	if (is_attachment()) {
		$url = wp_get_attachment_image_url(get_the_ID(), "full");
		if($url){
			wp_redirect(tbm_get_thumbor_img( $url, 1600, "" ));
			exit();
		}
	}
}

// This will redirect the attachment page
add_filter('template_redirect', 'lg_redirect_attachment_to_cdn', -100);

// This will redirect to cdn instead of redirecting to attachment page when dealing with a trailing slash
add_filter('redirect_canonical', 'lg_redirect_attachment_to_cdn', -100);


// forzo la cdn sulla og:image e la riduco
add_filter( 'wpseo_opengraph_image', 'lg_change_opengraph_image_url' );

function lg_change_opengraph_image_url( $url ) {
	$maxwidth = 1200;
	$maxheight = 630;
	if(substr($url, 0, 35) == "https://www.lifegate.it/app/uploads"){
		$url = tbm_get_thumbor_img( $url, $maxwidth, $maxheight );
	} else{
		$startsWithCdn = strpos($url, "https://cdn.lifegate.it") === 0;

		if ($startsWithCdn) {
			$skip = false;
			// Estrai altezza e larghezza
			if (preg_match('/\/(\d+)x(\d+)\//', $url, $matches)) {
				$larghezza = $matches[1];
				$altezza = $matches[2];
			} else {
				$skip = true;
			}

			// Estrai URL dell'immagine originale
			if (preg_match('~https?://[^/]+/.+?/\d+x\d+/(?:[^/]+/)?(https?://.+)$~', $url, $matches)) {
				$url_immagine = $matches[1];
			}  else {
				$skip = true;
			}

			if(!$skip){
				if($larghezza > $maxwidth){
					$nuova_larghezza = $maxwidth;
					$nuova_altezza = $maxheight;
					$url = tbm_get_thumbor_img( $url_immagine, $nuova_larghezza, $nuova_altezza );
				}
			}
		}
	}

	return $url;
}

add_filter( 'wpseo_opengraph_image_size', 'lg_change_opengraph_image_size' );

function lg_change_opengraph_image_size( $size ) {

	return "thumbnail";
}

/**
 * sistemo immagine twitter fornendo un formato cdn
 */
/*
add_filter( 'wpseo_twitter_image', 'filter_wpseo_twitter_image', 100, 1 );
function filter_wpseo_twitter_image( $url) {
	if (is_singular()) {
		if (($id = get_post_thumbnail_id()) && ($img = wp_get_attachment_image_src($id, 'large')))
		{
			return $img[0];
		}
	}
	return $url;
};
*/
/*
add_action('wp_head',  function (){ ?>
	<script type="application/ld+json">{"@context": "https://schema.org", "@type": "NewsMediaOrganization", "url": "<?php echo get_home_url(); ?>", "logo": "https://cdn.lifegate.it/0nPiIdREB8IP_raN9lnx0a5yRYI=/180x180/smart/https://www.lifegate.it/app/uploads/2020/09/logo-lg.png"}	</script>
	<?php

});
*/
/*
function wpd_empty_search_fix(){
	add_rewrite_rule(
			'^search/$',
			'index.php?s=',
			'top'
	);
}
add_action( 'init', 'wpd_empty_search_fix' );
*/
