<?php
/*
Plugin Name: Draft To 410
Description: Set 410 for posts in draft
Version: 1.0
Author: Bititup srl
*/

// Hook into template_redirect to check if a post should send a 410 header
add_action('template_redirect', 'spd_check_draft_post', 99999);

/**
 * @return void
 */
function spd_check_draft_post() {
	global $is_410;
	global $wp;
//	echo "----".$wp->request;

	if (is_404()) {

		global $wp;
		$full_url = home_url($wp->request);
		$slug = get_slug_from_url($full_url);
		$post_id = get_post_id_by_slug($slug);
//		echo "----".$post_id;
		if ($post_id) {
			$post_status = get_post_status($post_id);
//			echo "----".$post_status;
//			exit();
			if ($post_status == 'draft') {
				status_header(410);
				nocache_headers();
				$is_410 = true; // Imposta la variabile globale

				if ( function_exists( '\App\locate_template' ) ) {
					return \App\locate_template( '410' );
				} else {
					return locate_template( '410' );
				}
				exit;
			}
		}
	}
}
/**
 * @param $url
 * @return false|mixed|string
 */
function get_slug_from_url($url) {
	$path = parse_url($url, PHP_URL_PATH); // Estrae il percorso dall'URL
	$path_parts = explode('/', trim($path, '/')); // Divide il percorso in parti
	$slug = end($path_parts); // Prende l'ultimo elemento che è lo slug
	return $slug;
}

function get_post_id_by_slug($slug) {
	global $wpdb;
	$post_id = $wpdb->get_var($wpdb->prepare(
		"SELECT ID FROM $wpdb->posts WHERE post_name = %s",
		$slug
	));

	return $post_id;
}
