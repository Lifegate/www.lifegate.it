<?php
/*
Plugin Name: Calcolatore CO2 Lifegate
Description: Adds shortcode for embedding
Version: 1.0.0
Author: Marco Buttarini
Author URI: https://bititup.it/
*/


add_action('init', 'lifegate_add_calculator_shortcode');

function lifegate_add_calculator_shortcode()
{
	add_shortcode('calcolatoreco2', 'calcolatoreco2_shortcode');
}


function calcolatoreco2_shortcode($atts = array())
{
	$ret = "";
	$ret .= '<iframe src="https://calcolatore.lifegate.it/" id="lifegate-calculator"  title="Calcolatore CO2 Lifegate"  frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
	$ret .= "
		<script src='" . plugin_dir_url(__FILE__) . "js/iFrameResizer.js'></script>
		<script>iFrameResize({log:true}, '#lifegate-calculator')</script>
";
	return $ret;
}
