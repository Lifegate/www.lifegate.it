function tbm_native() {
    return "<p>[ghsharethrough]</p>";
}

(function() {
    tinymce.create('tinymce.plugins.tbm_native_plugin', {
        init : function(ed, url){
            ed.addButton('tbm_native_button', {
                title : 'Includi il codice di Sharethrough',
                onclick : function() {
                    ed.execCommand(
                        'mceInsertContent',
                        false,
                        tbm_native()
                    );
                },
                image : url + '../../images/native.png'
            });
        }

    });
    tinymce.PluginManager.add('tbm_native', tinymce.plugins.tbm_native_plugin );
})();
