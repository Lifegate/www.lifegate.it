/**
 * Add a GA event when an affiliation link is clicked
 */
var count = 0;
(function waitfor() {
    count = count + 1;
    if (typeof jQuery === 'function') {
        $('[data-aid]').each(function (index) {
            $(this).on("click", function () {
                var aid = $(this).attr('data-aid').length ? $(this).attr('data-aid') : 'no-aid';
                if (window.tbmGa !== undefined) {
                    window.tbmGa.gaEvent('affiliation', 'amazon', aid)
                }
            });
        });
    } else if (count === 60) {
        return false;
    } else {
        setTimeout(function () {
            waitfor();
        }, 100);
    }
})();