var updateSn = (function () {

    window.datalytics = window.datalytics || {};

    var querySocialNetwork;

    querySocialNetwork = function () {
        var pid = (typeof dataLayer !== 'undefined' && dataLayer[0] !== 'undefined' && dataLayer[0].pagePostID) ? dataLayer[0].pagePostID : datalytics.datalytics_pid,
            nonce = datalytics.datalaytics_pid,
            url = datalytics.ajaxurl + '?action=update_sn' + '&pid=' + datalytics.datalytics_pid + '&_ajax_nonce=' + datalytics.datalytics_nonce,
            request = new XMLHttpRequest();
        request.open('GET', url, true);
        request.send();
    };

    return {
        // A public function utilizing privates
        update: function (quota) {
            if (!quota) {
                quota = Math.floor(Math.random() * 100) + 1;
            }

            if (quota <= datalytics.datalytics_quota) {
                querySocialNetwork();
            }

        }
    };

})();
updateSn.update();