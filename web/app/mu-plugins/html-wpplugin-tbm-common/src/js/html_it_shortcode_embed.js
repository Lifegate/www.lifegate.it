/*! HTML.it Shortcode Embed - v0.2.0
 * http://www.gruppohtml.it
 * Copyright (c) 2015; * Licensed GPLv2+ */

(function() {
	tinymce.create('tinymce.plugins.html_caxPlugin', {
		/**
		 * Initializes the plugin, this will be executed after the plugin has been created.
		 * This call is done before the editor instance has finished it's initialization so use the onInit event
		 * of the editor instance to intercept that event.
		 *
		 * @param {tinymce.Editor} ed Editor instance that the plugin is initialized in.
		 * @param {string} url Absolute URL to where the plugin is located.
		 */
		init : function(ed, url) {
			// Register the command so that it can be invoked by using tinyMCE.activeEditor.execCommand('ghshortcode_open');
			ed.addCommand('gh_shortcode_open', function() {
				ed.windowManager.open({
					url : ajaxurl + '?action=gh_shortcode_tinymce',
					width : 750 + ed.getLang('gh_shortcode.delta_width', 0),
					height : 500 + ed.getLang('gh_shortcode.delta_height', 0),
					inline : 1,
					title: 'Cerca e crea lo shortcode'
				}, {
					website: ""
				});
			});

			// Register gh_shortcode button
			ed.addButton('gh_shortcode', {
				title : 'Inserisci Gallerie e altri post',
				cmd : 'gh_shortcode_open',
                image: url + '../../images/triboo.png',
				stateSelector: 'img'
			});


		},

		/**
		 * Creates control instances based in the incomming name. This method is normally not
		 * needed since the addButton method of the tinymce.Editor class is a more easy way of adding buttons
		 * but you sometimes need to create more complex controls like listboxes, split buttons etc then this
		 * method can be used to create those.
		 *
		 * @param {String} n Name of the control to create.
		 * @param {tinymce.ControlManager} cm Control manager to use inorder to create new control.
		 * @return {tinymce.ui.Control} New control instance or null if no control was created.
		 */
		createControl : function(n, cm) {
			return null;
		},

		/**
		 * Returns information about the plugin as a name/value array.
		 * The current keys are longname, author, authorurl, infourl and version.
		 *
		 * @return {Object} Name/value array containing information about the plugin.
		 */
		getInfo : function() {
			return {
				longname : 'GHShortcode plugin',
				author : 'Francesco Caccavella',
				authorurl : 'http://www.caccavella.com',
				infourl : 'http://www.caccavella.com',
				version : "0.2.0"
			};
		}
	});

	// Register plugin
	tinymce.PluginManager.add('gh_shortcode', tinymce.plugins.html_caxPlugin);
})();
var link = document.createElement('link');
link.rel = 'prerender';
link.href = ajaxurl + '?action=gh_shortcode_tinymce';
document.getElementsByTagName('head')[0].appendChild(link);