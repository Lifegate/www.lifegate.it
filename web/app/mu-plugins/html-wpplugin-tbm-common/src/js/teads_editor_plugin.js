function tbm_teads() {
    return "<p>[ghteads]</p>";
}

(function() {
    tinymce.create('tinymce.plugins.tbm_teads_plugin', {
        init : function(ed, url){
            ed.addButton('tbm_teads_button', {
                title : 'Includi il codice di Sharethrough',
                onclick : function() {
                    ed.execCommand(
                        'mceInsertContent',
                        false,
                        tbm_teads()
                    );
                },
                image : url + '../../images/teads.png'
            });
        }

    });
    tinymce.PluginManager.add('tbm_teads', tinymce.plugins.tbm_teads_plugin );
})();
