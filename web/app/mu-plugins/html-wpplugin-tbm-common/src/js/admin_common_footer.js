/**
 * Debounce function
 */
function tbm_common_debounce(func, wait, immediate) {
    var timeout;
    return function () {
        var context = this, args = arguments;
        var later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        if (!timeout) timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
}

jQuery(document).on('tinymce-editor-init', function (event, editor) {
    var cce_count = tinymce.get('content').getContent({format: 'text'}).replace(/\s/g, '').length;
    jQuery('.cce_count').text(cce_count);
    tinymce.get('content').on("focus keyup paste", function () {
        var cce_count = tinymce.get('content').getContent({format: 'text'}).replace(/\s/g, '').length;
        jQuery('.cce_count').text(cce_count);
    });
});

jQuery(document).ready(function () {
    // Characters
    var ex_limit = jQuery('body.post-type-guide').length ? 'Nd' : 155;
    var ex_count = jQuery('#excerpt').val() ? jQuery('#excerpt').val().length : 0;

    // Words
    var cp_limit = jQuery('body.post-type-guide').length ? 60 : 'Nd';
    var cp_count = jQuery("#excerpt").val() ? jQuery("#excerpt").val().match(/\S+/g).length : 0;

    // Add elements to Excerpt
    jQuery('#postexcerpt .hndle').append('<span class="ccp_excerpt_counter" style="float:right">Caratteri: <span  class="ccp_count"></span> / <span class="ccp_limit"></span> </span>');
    jQuery('.ccp_count').text(ex_count);
    jQuery('.ccp_limit').text(ex_limit);

    // Add elements to Excerpt
    jQuery('#postexcerpt .hndle').append('<span class="pcp_excerpt_counter" style="float:right;padding: 0 20px 0 0;">Parole: <span  class="pcp_count"></span> / <span class="pcp_limit"></span> </span>');
    jQuery('.pcp_count').text(cp_count);
    jQuery('.pcp_limit').text(cp_limit);

    // Add content
    jQuery('#wp-word-count').after('<td class="hide-if-no-js"><span class="ccp_excerpt_counter" style="float:right">Caratteri: <span class="cce_count"></span></span></td>');

    // Calculate every keyup in Content
    jQuery('#content').keyup(tbm_common_debounce(function () {
        jQuery('.cce_count').text('-');
    }, 300));

    // Calculate every keyup in Excerpt and Title
    jQuery('#excerpt').keyup(tbm_common_debounce(function () {
        var cp_count = jQuery(this).val().match(/\S+/g).length;
        var ex_count = jQuery(this).val().length;
        if (ex_limit !== 'Nd') {
            if (ex_count > ex_limit) {
                if (jQuery('#excerpt').css('color') != 'rgb(255, 0, 0)') {
                    jQuery(this).css('color', 'red');
                    jQuery('.ccp_excerpt_counter').css('color', 'red');
                }
            } else {
                if (jQuery('#excerpt').css('color') != 'rgb(0, 0, 0)') {
                    jQuery(this).css('color', 'black');
                    jQuery('.ccp_excerpt_counter').css('color', 'black');
                }
            }
        }

        if (cp_limit !== 'Nd') {
            if (cp_count > cp_limit) {
                if (jQuery('#excerpt').css('color') != 'rgb(255, 0, 0)') {
                    jQuery(this).css('color', 'red');
                    jQuery('.pcp_excerpt_counter').css('color', 'red');
                }
            } else {
                if (jQuery('#excerpt').css('color') != 'rgb(0, 0, 0)') {
                    jQuery(this).css('color', 'black');
                    jQuery('.pcp_excerpt_counter').css('color', 'black');
                }
            }
        }

        jQuery('.ccp_count').text(ex_count);
        jQuery('.pcp_count').text(cp_count);
    }, 300));

    /**
     * Title
     */
    var hostname = location.hostname.split('.').reverse()[1] ? location.hostname.split('.').reverse()[1] : 'null';

    // Act only if lifegate is domain
    if ('lifegate' === hostname) {
        var title_limit = 80;
        var title_count = jQuery("#title").val() ? title_limit - jQuery('#title').val().length : 0;

        // Add elements to title
        jQuery('#titlewrap').append('<div style="color: #999; font-size: 14px;">Caratteri disponibili: <span class="title_count"></span> / <span class="title_limit"></span></div>');
        jQuery('.title_count').text(title_count);
        jQuery('.title_limit').text(title_limit);
        title_set_color(title_count);

        // Calculate every keyup in Excerpt and Title
        jQuery('#title').keyup(tbm_common_debounce(function () {
            var title_count = title_limit - jQuery(this).val().length;
            title_set_color(title_count);
            jQuery('.title_count').text(title_count);
        }, 300));

        function title_set_color(title_count) {
            if (title_count < 0) {
                if (jQuery('#title').css('color') != 'rgb(255, 0, 0)') {
                    jQuery('#title').css('color', 'red');
                }
            } else {
                if (jQuery('#title').css('color') != 'rgb(0, 0, 0)') {
                    jQuery('#title').css('color', 'black');
                }
            }
        }
    }
});