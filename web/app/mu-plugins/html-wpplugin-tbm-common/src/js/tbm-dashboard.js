/**
 * Change the proper icon with the loader
 *
 * @param myElement
 * @returns {boolean}
 */
function setLoader(myElement) {
    var el = myElement !== undefined ? myElement : '';

    if (!el) {
        return false;
    }

    jQuery('.fa', el).fadeOut(200, function () {
        jQuery(this).remove();
        el.addClass('is-disabled');
        el.append('<i class="fa fa-spinner fa-pulse" style="color:#f0ad4e"></i>');
    });

    return true;
}

/**
 * Set the new status of the link
 *
 * @param myElement
 * @returns {boolean}
 */
function setNewStatus(myElement, action) {
    var act = action !== undefined ? action : '',
        el = myElement !== undefined ? myElement : '',
        icon = '',
        attr = '',
        title = '',
        status = '';

    if (!act) {
        return false;
    }

    if (act === 'featuredDown') {
        icon = 'fa-star fa-tbm-off';
        attr = 'featured';
        title = 'Clic per mettere in primo piano';
        status = false;
    }
    if (act === 'featuredUp') {
        icon = 'fa-star fa-tbm-on';
        attr = 'featured';
        title = 'Clic per rimuovere dal primo piano';
        status = true;
    }
    if (act === 'pushDown') {
        icon = 'fa-bell fa-tbm-off';
        attr = 'push';
        title = 'Clic per pushare';
        status = false;
    }
    if (act === 'pushUp') {
        icon = 'fa-bell fa-tbm-on';
        attr = 'push';
        title = 'Clic per NON pushare';
        status = true;
    }
    if (act === 'twitterDown') {
        icon = 'fa-twitter fa-tbm-off';
        attr = 'twitter';
        title = 'Clic per twittare';
        status = false;
    }
    if (act === 'twitterUp') {
        icon = 'fa-twitter fa-tbm-on';
        attr = 'twitter';
        title = 'Clic per NON twittare';
        status = true;
    }
    if (act === 'facebookDown') {
        icon = 'fa-facebook fa-tbm-off';
        attr = 'facebook';
        title = 'Clic per postare';
        status = true;
    }
    if (act === 'facebookUp') {
        icon = 'fa-facebook fa-tbm-on';
        attr = 'facebook';
        title = 'Clic per NON postare';
        status = true;
    }
    if (act === 'linkedinDown') {
        icon = 'fa-linkedin fa-tbm-off';
        attr = 'linkedin';
        title = 'Clic per postare';
        status = true;
    }
    if (act === 'linkedinUp') {
        icon = 'fa-linkedin fa-tbm-on';
        attr = 'linkedin';
        title = 'Clic per NON postare';
        status = true;
    }

    el.append('<i class="fa ' + icon + ' fa-lg" style="color:#B8860B"></i>');
    el.data(attr, status);
    el.prop('title', title);

}

/**
 * Send ajax request to change post status
 *
 * @param myElement
 * @param act
 * @returns {boolean}
 */
function changePostStatus(myElement, action) {
    var act = action !== undefined ? action : '';
    var el = myElement !== undefined ? myElement : '';
    var timestamp = Math.round((new Date()).getTime() / 1000);

    if (!act) {
        return false;
    }

    id = el.data('id');


    jQuery.ajax({
        type: "POST",
        url: ajaxurl,
        data: {
            action: "change_post_status",
            status: act,
            id: id,
            date: timestamp,
            _wpnonce: window.tbmDashNonce
        }
    }).success(function () {
        el.parents("tr").css("font-weight", "bold");
        jQuery('.fa', el).stop().fadeOut(200, function () {
            jQuery(this).remove();
            el.removeClass('is-disabled');
            setNewStatus(el, act)
        });
    }).error(function () {
        jQuery('.fa', el).stop().fadeOut(200, function () {
            jQuery(this).remove();
            el.append('<i class="fa fa-exclamation-triangle fa-lg is-disabled" style="color:#a94442;"></i>');
        });

    });

}

/**
 * Act on document ready
 */
jQuery(document).ready(function ($) {
    /**
     * Bind click
     */
    jQuery("#element-filter-author, #element-filter-administrator").on("click", ".tbmSetter", function (event) {
        event.preventDefault();
        var pushEl = jQuery(this);

        var data = pushEl.data() ? pushEl.data() : '';

        console.log(data);

        if (!data) {
            return false;
        }

        var loader = setLoader(pushEl);

        console.log('start loader');

        if (!loader) {
            return false;
        }

        if (data.featured !== undefined) {
            if (data.featured === true) {
                changePostStatus(pushEl, 'featuredDown')
            }
            if (data.featured === false) {
                changePostStatus(pushEl, 'featuredUp')
            }

        }

        if (data.push !== undefined) {
            if (data.push === true) {
                changePostStatus(pushEl, 'pushDown')
            }
            if (data.push === false) {
                changePostStatus(pushEl, 'pushUp')
            }
        }

        if (data.twitter !== undefined) {
            if (data.twitter === true) {
                changePostStatus(pushEl, 'twitterDown')
            }
            if (data.twitter === false) {
                changePostStatus(pushEl, 'twitterUp')
            }
        }

        if (data.facebook !== undefined) {
            if (data.facebook === true) {
                changePostStatus(pushEl, 'facebookDown')
            }
            if (data.facebook === false) {
                changePostStatus(pushEl, 'facebookUp')
            }
        }

        if (data.linkedin !== undefined) {
            if (data.linkedin === true) {
                changePostStatus(pushEl, 'linkedinDown')
            }
            if (data.linkedin === false) {
                changePostStatus(pushEl, 'linkedinUp')
            }
        }

    });

    /**
     * Run mixitup
     */
    jQuery('#element-filter-author').mixitup({
        filterSelector: '.badge-author',
        effects: ['fade'],
        targetDisplayGrid: 'table-row',
        showOnLoad:'publish'
    });

    /**
     * Run mixitup
     */
    jQuery('#element-filter-administrator').mixitup({
        filterSelector: '.badge-administrator',
        effects: ['fade'],
        targetDisplayGrid: 'table-row',
        showOnLoad:'publish'
    });
});