function tbm_ghvideo() {
    return "<p>[ghrandvideo]</p>";
}

(function() {
    tinymce.create('tinymce.plugins.tbm_ghvideo_plugin', {
        init : function(ed, url){
            ed.addButton('tbm_ghvideo_button', {
                title : 'Includi un video correlato',
                onclick : function() {
                    ed.execCommand(
                        'mceInsertContent',
                        false,
                        tbm_ghvideo()
                    );
                },
                image : url + '../../images/ghvideo.png'
            });
        }

    });
    tinymce.PluginManager.add('tbm_ghvideo', tinymce.plugins.tbm_ghvideo_plugin );
})();
