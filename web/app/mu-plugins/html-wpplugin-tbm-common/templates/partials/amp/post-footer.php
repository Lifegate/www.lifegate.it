<?php
if ( tbm_get_the_banner( 'AMP_POST_FOOTER', ' ', ' ', false, false ) ) :
	echo ' <h2>Contenuti sponsorizzati</h2>';
	$before = '<div class="partial-amp-ad--ligatus"><div class="inner-ad">';
	$after  = '</div></div>';

	$sanitizers = amp_get_content_sanitizers();
	$results    = AMP_Content_Sanitizer::sanitize( tbm_get_the_banner( 'AMP_POST_FOOTER', $before, $after, false, false ), $sanitizers, array() );

	if ( is_array( $results ) && ! empty( $results[0] ) ) {
		echo $results[0];
	}
endif;