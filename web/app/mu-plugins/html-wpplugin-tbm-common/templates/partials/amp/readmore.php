<?php
if ( ! defined( 'TBM_SITE_NAME' ) || TBM_SITE_NAME !== 'lifegate' ) :
	?>
    <div class="partial-amp-cta--readmore">
        <a class="cta cta--readmore" href="<?php echo wp_get_canonical_url(); ?>">Leggi tutto →</a>
    </div>
<?php
endif;
?>
<?php
$before = '<div class="partial-amp-ad--standard"><div class="inner-ad">';
$after  = '</div></div>';
echo tbm_get_the_banner( 'AMP_BOX_INSIDE_TOP', $before, $after, false, false );
?>
