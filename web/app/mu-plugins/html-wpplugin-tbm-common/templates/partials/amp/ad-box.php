<?php
$before = '<div class="partial-amp-ad--box"><div class="inner-ad">';
$after  = '</div></div>';

$sanitizers = amp_get_content_sanitizers();
$results    = AMP_Content_Sanitizer::sanitize( tbm_get_the_banner( 'AMP_BOX_INSIDE_TOP', $before, $after, false, false ), $sanitizers, array() );

if ( is_array( $results ) && ! empty( $results[0] ) ) {
	echo $results[0];
}