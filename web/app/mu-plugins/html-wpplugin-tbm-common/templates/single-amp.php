<?php
/**
 * Variabili
 */
$logo  = tbm_wp_get_attachment_image_url( get_field( 'tbm_site_amp_logo', 'options' ), array( 0, 58 ) );
$fb_id = get_field( 'tbm_fb_app_id', 'options' );
$ver   = wp_get_theme()->get( 'Version' ) ?: '000';
?>
<!doctype html>
<html amp lang="it">

<?php while ( have_posts() ) :
	the_post();
	$post_id = get_the_ID();
	?>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, minimum-scale=1, initial-scale=1">
		<?php do_action( 'amp_post_template_head', $this ); ?>
        <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
        <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
        <script async custom-element="amp-social-share"
                src="https://cdn.ampproject.org/v0/amp-social-share-0.1.js"></script>
        <script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
        <script async custom-element="amp-position-observer"
                src="https://cdn.ampproject.org/v0/amp-position-observer-0.1.js"></script>
        <script async custom-element="amp-animation" src="https://cdn.ampproject.org/v0/amp-animation-0.1.js"></script>

        <style amp-custom>
            <?php do_action( 'amp_post_template_css', $this ); ?>
        </style>

    </head>

    <body class="tbm-amp">

	<?php
	/***** SIDEBAR *****/
	if ( has_nav_menu( 'amp_hamburger_navigation' ) ) : ?>
        <amp-sidebar id="sidenav1" layout="nodisplay" side="right">
            <div role="button" aria-label="close sidenav" on="tap:sidenav1.toggle" tabindex="0" class="close-sidenav">
                ✕
            </div>
			<?php
			$main_menu_args = [
				'theme_location' => 'amp_hamburger_navigation',
				'menu_class'     => 'sidenav',
				'depth'          => 1,
				'container'      => ''
			];
			wp_nav_menu( $main_menu_args );
			?>
        </amp-sidebar>
	<?php endif; ?>

	<?php
	/***** HEADER *****/
	?>
    <header class="header header--fixed">
        <div role="button" on="tap:sidenav1.toggle" tabindex="0" class="hamburger">☰</div>
		<?php if ( $logo ) : ?>
            <a class="logo" href="<?php echo get_home_url(); ?>">
                <amp-img src="<?php echo $logo; ?>"
                         height="38"
                         alt="Logo <?php echo get_bloginfo( 'name' ); ?>"
                         noloading>
                </amp-img>
            </a>
		<?php endif; ?>
    </header>

    <!-- /***** CONTENT *****/ -->
    <div class="base">
        <div class="wrapper">
            <div class="container container--nopadding">

				<?php
				/***** CAROUSEL *****/
				if ( has_nav_menu( 'amp_slider_navigation' ) ) : ?>
                    <div class="partial-amp-carousel-trend">
                        <amp-carousel layout="fixed-height" height="40" type="carousel">
							<?php
							$trend_menu_args = [
								'theme_location' => 'amp_slider_navigation',
								'container'      => '',
								'items_wrap'     => '%3$s'
							];
							wp_nav_menu( $trend_menu_args );
							?>
                        </amp-carousel>
                    </div>
				<?php endif; ?>

                <!-- /***** POST *****/ -->
                <div class="single-post">
                    <main class="single-post__main">

                        <article class="editorial">
                            <div class="container">
                                <div class="col-8">
                                    <div class="inner-padding">
                                        <h1>
											<?php echo esc_html( $this->get( 'post_title' ) ); ?>
                                            <amp-position-observer on="enter:hideAnim.start; exit:showAnim.start"
                                                                   layout="nodisplay"></amp-position-observer>
                                        </h1>

										<?php
										/***** AUTHOR *****/
										$post_author = $this->get( 'post_author' );
										if ( ! empty( $post_author ) ) : ?>
                                            <div class="single-post__author">
                                                <div>
                                                    <div class="author__info">
                                                        <div class="author__figure">
                                                            <div class="author__image-wrapper">
																<?php
																if ( function_exists( 'get_avatar_url' ) ) : ?>
                                                                    <amp-img
                                                                            src="<?php echo esc_url( get_avatar_url( $post_author->user_email, array( 'size' => 32 ) ) ); ?>"
                                                                            width="32"
                                                                            height="32"
                                                                            alt="<?php echo esc_attr( $post_author->display_name ); ?>"
                                                                            noloading>
                                                                    </amp-img>
																<?php endif; ?>
                                                            </div>
                                                        </div>
                                                        <a class="author__name"
                                                           href="<?php echo get_author_posts_url( $post_author->ID ); ?>"
                                                           rel="author"><?php echo esc_html( $post_author->display_name ); ?></a>
                                                    </div>
                                                </div>
												<?php echo tbm_get_pub_date(); ?>
                                            </div>
										<?php endif; ?>
                                    </div>

									<?php
									/***** SOCIAL *****/
									if ( has_post_thumbnail() ) : ?>
                                        <div class="single-post__social">
											<?php if ( $fb_id ) : ?>
                                                <amp-social-share type="facebook" width="640" height="32"
                                                                  data-param-app_id="<?php echo $fb_id; ?>"></amp-social-share>
											<?php endif; ?>
                                            <amp-social-share type="twitter" width="640" height="32"></amp-social-share>
                                        </div>
									<?php endif; ?>

									<?php
									/***** FEATURED IMAGE *****/
									if ( has_post_thumbnail() ) : ?>
                                        <amp-img class="single-post__featured-img"
                                                 src="<?php echo tbm_get_the_post_thumbnail_url( $post_id, array(
											         600,
											         400
										         ) ); ?>"
                                                 srcset="<?php echo tbm_get_the_post_thumbnail_url( $post_id, array(
											         600,
											         400
										         ) ); ?>"
                                                 width="600"
                                                 height="400"
                                                 layout="responsive"
                                                 alt="<?php echo tbm_get_the_post_thumbnail_alt(); ?>"
                                                 noloading>
                                        </amp-img>
									<?php endif; ?>

                                    <!-- /***** CONTENT *****/ -->
                                    <div class="inner-padding">
										<?php
										echo $this->get( 'post_amp_content' ); ?>


                                        <!-- /***** SOCIAL *****/ -->
                                        <div class="single-post__social" style="margin-bottom:30px">
											<?php if ( $fb_id ) : ?>
                                                <amp-social-share type="facebook" width="640" height="32"
                                                                  data-param-app_id="<?php echo $fb_id; ?>"></amp-social-share>
											<?php endif; ?>
                                            <amp-social-share type="twitter" width="640" height="32"></amp-social-share>
                                        </div>

                                        <!-- /***** LIGATUS *****/ -->
										<?php
										include_once( HTML_TBM_COMMON_PLUGIN_DIR . 'templates/partials/amp/post-footer.php' );
										?>

                                    </div>
                                </div>

                            </div>
                        </article>
                    </main>
                </div>

                <div class="partial-amp-footer">

					<?php if ( $logo ) : ?>
                        <a class="logo" href="<?php echo get_home_url(); ?>">
                            <amp-img src="<?php echo $logo; ?>"
                                     width="115"
                                     layout="fill"
                                     height="38"
                                     alt="Logo <?php echo get_bloginfo( 'name' ); ?>"
                                     noloading>
                            </amp-img>
                        </a>
					<?php endif; ?>

                    <p class="credits">Un canale di HTML.it, periodico telematico reg. Trib. Roma n. 309/2008 | ©
                        1997-<?php echo date( 'Y' ); ?> Triboo Media P.IVA 06933670967 | <?php echo $ver; ?></p>
                </div>

                <a href="#top" id="topBtn" title="Go to top" on="tap:top-page.scrollTo(duration=200)"></a>


            </div>
        </div>
    </div>

	<?php do_action( 'amp_post_template_footer', $this ); ?>


    <amp-animation id="showAnim"
                   layout="nodisplay">
        <script type="application/json">
            {
                "duration": "200ms",
                "fill": "both",
                "iterations": "1",
                "direction": "alternate",
                "animations": [
                    {
                        "selector": "#topBtn",
                        "keyframes": [
                            {
                                "opacity": "1",
                                "visibility": "visible"
                            }
                        ]
                    }
                ]
            }
        </script>
    </amp-animation>
    <amp-animation id="hideAnim" layout="nodisplay">
        <script type="application/json">
            {
                "duration": "200ms",
                "fill": "both",
                "iterations": "1",
                "direction": "alternate",
                "animations": [
                    {
                        "selector": "#topBtn",
                        "keyframes": [
                            {
                                "opacity": "0",
                                "visibility": "hidden"
                            }
                        ]
                    }
                ]
            }
        </script>
    </amp-animation>
    </body>
<?php endwhile; ?>
</html>
