<?php
/**
 * Created by PhpStorm.
 * User: francesco
 * Date: 07/02/2018
 * Time: 18:51
 */
?>

<aside class="related-post related-post--no-image related-post--post <?php echo $sc->layout; ?>">
    <a href="<?php echo $sc->permalink; ?>"><h3><?php echo $sc->title; ?></h3></a>
    <span><?php echo $sc->date; ?></span>
</aside>