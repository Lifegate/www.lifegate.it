<?php
/**
 * Created by PhpStorm.
 * User: francesco
 * Date: 07/02/2018
 * Time: 20:12
 */

if ( has_post_thumbnail( $post ) && get_post_type( $post ) !== 'esperto' ) :
	$class = get_post_type( $post );
	?>
    <aside class="related-post related-post--<?php echo $class; ?> <?php echo $sc->layout; ?>">
        <picture data-link="<?php echo $sc->permalink; ?>">
            <img class="lazyload" data-srcset="<?php echo $sc->image; ?>" alt="<?php echo $sc->image_alt; ?>"/>
        </picture>
        <a href="<?php echo $sc->permalink; ?>"><h3><?php echo $sc->title; ?></h3></a>
        <span><?php echo $sc->date; ?></span>
    </aside>
<?php elseif ( get_post_type( $post ) === 'esperto' ) :
	$qa_author = get_field( "html_qa_nome" ) ? get_field( "html_qa_nome" ) : "Utente di PMI"; ?>

    <aside class="related-post related-post--esperto">
        <div class="esperto-card__author">
            <img src="<?php tbm_assets_url( 'images/ico-author.svg' ); ?>"/>
            <span class="esperto-card__author-name"><?php echo $qa_author; ?></span>
            <span>chiede</span>
        </div>
        <div class="esperto-card__ballon">
            <a href="<?php echo $sc->permalink; ?>"><h2><?php echo $sc->title; ?></h2></a>
            <div class="esperto-card__ballon__author">
                risponde <?php echo pmi_author( $sc->id ); ?>
            </div>
        </div>
    </aside>

<?php endif; ?>
