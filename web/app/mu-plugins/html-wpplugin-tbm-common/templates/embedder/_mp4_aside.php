<?php
/**
 * Created by PhpStorm.
 * User: francesco
 * Date: 20/02/2018
 * Time: 00:52
 */

echo '<div class="video-post video-post--aside">';

if ( function_exists( 'html_video_generate_video_player' ) ) {
	html_video_generate_video_player( $atts['id'], '', true );
} else {
	echo '<video src="' . get_field( 'tbm_videoid', $atts['id'] )['url'] . '" controls controlsList="nodownload"></video>';
}

echo '</div>';