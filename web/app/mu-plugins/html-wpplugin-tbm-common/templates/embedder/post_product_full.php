<div id="<?php echo sanitize_title( $sc->title ); ?>" class="partial-innerproduct-affiliation--big">
    <div class="innerproduct-affiliation--big__product">

		<?php if ( $sc->badge ): ?>
            <div class="product__badge">
                <span><?php echo $sc->badge; ?></span>
            </div>
		<?php endif; ?>

        <div class="product__info">
            <div class="container">
                <div class="span-5">
                    <a href="<?php echo $sc->url; ?>" target="_blank" data-aid="<?php echo $sc->aid; ?>"
                       rel="noopener nofollow sponsored">
						<?php if ( tbm_is_amp() ) : ?>
                            <amp-img src="<?php echo $sc->image; ?>" srcset="<?php echo $sc->image; ?> 300w, <?php echo $sc->image; ?> 600w" width="420" height="420" layout="responsive" alt="<?php echo $sc->title; ?>" noloading></amp-img>
						<?php else : ?>
                            <picture>
                                <img class="lazyload" data-srcset="<?php echo $sc->image; ?>"
                                     alt="<?php echo $sc->title; ?>"/>
                            </picture>
						<?php endif; ?>
                    </a>
                </div>
                <div class="span-7">
                    <!-- link to the best price -->
                    <a href="<?php echo $sc->url; ?>" target="_blank" rel="noopener nofollow sponsored"
                       data-aid="<?php echo $sc->aid; ?>">
                        <h2><?php echo $sc->title; ?></h2></a>

					<?php if ( $sc->claim ): ?>
                        <p class="comment"><?php echo $sc->claim; ?></p>
					<?php endif; ?>
					<?php if ( $sc->excerpt ): ?>
                        <p class="abstract"><?php echo $sc->excerpt; ?></p>
					<?php endif; ?>
                    <!-- Affiliation cta -->
                    <div class="product__cta">
						<?php if ( $sc->price ) : ?>
                            <a class="cta <?php if ( $sc->prime ) {
								echo 'cta--amazonprime';
							} ?>" href="<?php echo $sc->url; ?>" target="_blank" rel="noopener nofollow sponsored"
                               data-aid="<?php echo $sc->aid; ?>"><?php echo $sc->price; ?> &euro; su Amazon</a>
						<?php else : ?>
                            <a class="cta <?php if ( $sc->prime ) {
								echo 'cta--amazonprime';
							} ?>" href="<?php echo $sc->url; ?>" target="_blank" rel="noopener nofollow sponsored"
                               data-aid="<?php echo $sc->aid; ?>">Compra su Amazon</a>
						<?php endif; ?>
						<?php if ( $sc->saving ) : ?>
                            <div class="product__offert">
                                <span class="offert"><?php echo $sc->amz_price; ?> €</span>
                                <span class="discount">risparmi <?php echo $sc->saving; ?> €</span>
                            </div>
						<?php endif; ?>
                    </div>
                </div>
            </div>
        </div>

		<?php if ( $sc->pro || $sc->cons ): ?>
            <div class="product__list">
                <div class="container">
                    <div class="col-6">
						<?php if ( $sc->pro ) : ?>
                            <p><strong>Pro</strong></p>
                            <ul class="list--pro">
                                <li><?php echo implode( '</li><li>', $sc->pro ) ?></li>
                            </ul>
						<?php endif; ?>
                    </div>
                    <div class="col-6">
						<?php if ( $sc->cons ): ?>
                            <p><strong>Contro</strong></p>
                            <ul class="list--cons">
                                <li><?php echo implode( '</li><li>', $sc->cons ) ?></li>
                            </ul>
						<?php endif; ?>
                    </div>
                </div>
            </div>
		<?php endif; ?>

    </div>


	<?php if ( $sc->content ): ?>
		<p><?php echo $sc->content; ?></p>
	<?php endif; ?>

</div>