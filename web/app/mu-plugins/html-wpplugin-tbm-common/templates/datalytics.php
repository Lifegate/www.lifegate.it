<?php
/**
 * Question Post Type Archive
 */
$widget_title = isset( $sc['instance']['title'] ) ? $sc['instance']['title'] : '';
?>

<div class="moreread">
    <div class="title-big">
        <h3><?php echo $widget_title; ?></h3>
    </div>
    <div class="moreread-container">
		<?php
		if ( $sc['posts']->have_posts() ) {
			while ( $sc['posts']->have_posts() ) {
				$sc['posts']->the_post();
				$image      = wp_get_attachment_image_src( get_post_thumbnail_id(), array( 80, 80 ) );
				$link       = get_permalink();
				$post_title = get_the_title();
				?>
                <div class="featured featured--list">
                    <a href="<?php echo $link; ?>">
						<?php if ( $image ) : ?>
                            <div class="image">
                                <img src="<?php echo $image[0]; ?>" alt="<?php echo $post_title; ?>">
                            </div>
						<?php endif; ?>
                        <div class="text">
                            <h2><?php echo $post_title; ?></h2>
                        </div>
                    </a>
                </div>
				<?php
			}
			wp_reset_postdata();
		}
		?>
    </div>
</div>
