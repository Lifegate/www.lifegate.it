<?php
//Get post type handled via wp panel
$arr_post_type = get_field('tbm_common_flipboard_post_type_shortcode', 'options');

$flipboard_query = get_posts(array('posts_per_page' => 30, 'post_type' => $arr_post_type ));

header('Content-Type: ' . feed_content_type('rss-http') . '; charset=' . get_option('blog_charset'), true);

// Remove all shortcodes from content in feed
add_filter('the_content', function ($content) {
	if (is_feed('flipboard')) {
		$content = strip_shortcodes($content);
	}
	return $content;
}
);

?>

<rss version="2.0"
     xmlns:content="http://purl.org/rss/1.0/modules/content/"
     xmlns:dc="http://purl.org/dc/elements/1.1/"
     xmlns:media="http://search.yahoo.com/mrss/"
     xmlns:atom="http://www.w3.org/2005/Atom"
     xmlns:georss="http://www.georss.org/georss">

    <channel>
        <title><?php bloginfo_rss('name');
wp_title_rss(); ?></title>
        <link><?php bloginfo_rss('url') ?></link>
        <description><?php bloginfo_rss("description") ?></description>
        <language><?php bloginfo_rss('language'); ?></language>
        <atom:link rel="hub" href="http://pubsubhubbub.appspot.com"/>

        <?php
        $duration = 'hourly';
        foreach ($flipboard_query as $post) :
            $description = get_post(get_post_thumbnail_id())->post_excerpt;
            $url = get_post(get_post_thumbnail_id())->guid;
            ?>
            <item>
                <title><?php the_title_rss() ?></title>
                <link><?php the_permalink_rss() ?></link>
                <pubDate><?php echo mysql2date('D, d M Y H:i:s +0000', get_post_time('Y-m-d H:i:s', true), false); ?></pubDate>
                <dc:creator><![CDATA[<?php the_author_meta('display_name', $post->post_author); ?>]]></dc:creator>
                <guid isPermaLink="false"><?php the_guid(); ?></guid>
                <description><![CDATA[<?php the_excerpt_rss(); ?>]]></description>
                <content:encoded><![CDATA[<hgroup><h1><?php the_title_rss(); ?></h1><h3><?php the_excerpt_rss(); ?></h3></hgroup>
<?php if ($url) { ?>
<figure><img src="<?php echo $url; ?>">
<?php if ($description) { ?>
<figcaption><?php echo $description; ?></figcaption>
<?php } ?>
</figure>
<?php } ?>
                    <?php $content = $post->post_content;
                            $content = apply_filters( 'the_content', $content );
                            echo $content;?>

                    ]]></content:encoded>
            </item>
        <?php endforeach; ?>
    </channel>
</rss>

