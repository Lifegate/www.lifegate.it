Functions
---------

### pmi\_add\_post\_type\_arg_filt()

[](#source-view)

pmi\_add\_post\_type\_arg_filt(  $add\_query\_arg,   $type,   $taxonomy) 

_Disabilito la querystring di post type dalle breadcrumb Cfr https://mtekk.us/archives/guides/how-to-remove-post_type-from-breadcrumb-navxt-urls/_

#### Parameters

$add\_query\_arg

$type

$taxonomy

File

Tags
----

package

Default

### pmi\_content\_width()

[](#source-view)

pmi\_content\_width() 

_Imposta la larghezza del contenuto_

File

Tags
----

package

Default

### pmi\_custom\_facetwp_pager()

[](#source-view)

pmi\_custom\_facetwp_pager(  $output,   $params) : string

_Modifico la paginazione del plugin FacetWP_

#### Parameters

$output

$params

#### Returns

string

File

Tags
----

package

Default

### pmi\_get\_post_related()

[](#source-view)

pmi\_get\_post\_related(integer|\\WP\_Post  $post, integer  $limit = 2) : string

_Get post related based on PMI template_

#### Parameters

integer|\\WP_Post

$post

Optional. Post ID or post object. Default is the global `$post`.

integer

$limit

Optional. Numbers of items to publish; defaults 2

#### Returns

string

File

Tags
----

package

Default

### pmi\_modify\_title()

[](#source-view)

pmi\_modify\_title(  $html_newsletter) 

_Replace custom CTA in newsletter form_

#### Parameters

$html_newsletter

File

Tags
----

package

Default

### pmi\_remove\_category\_rel\_from\_category\_list()

[](#source-view)

pmi\_remove\_category\_rel\_from\_category\_list(  $thelist) 

_Remove invalid rel attribute values in the categorylist_

#### Parameters

$thelist

File

Tags
----

package

Default

### tbm\_acf\_init()

[](#source-view)

tbm\_acf\_init() 

_Created by PhpStorm._

User: francesco Date: 23/04/2018 Time: 22:22

File

Tags
----

package

Default

### tbm\_add\_slug\_to\_body_class()

[](#source-view)

tbm\_add\_slug\_to\_body_class(  $classes) : array

_Add page slug to body class_

#### Parameters

$classes

Classes defined in body

#### Returns

array

File

Tags
----

package

Default

### tbm\_assets\_url()

[](#source-view)

tbm\_assets\_url(  $path = '',   $return = false) : string|\\empty

_Return the path the $path to theme path uri (use without slash: pmi\_assets\_url('images/logo.png'); ). The value is echoed_

#### Parameters

$path

$return

#### Returns

string|\\empty

File

Tags
----

package

Default

### tbm\_cerca\_redirect()

[](#source-view)

tbm\_cerca\_redirect() 

_Convert search results from /?s=query to /cerca/query/_

File

Tags
----

package

Default

### tbm\_cerca\_rewrite()

[](#source-view)

tbm\_cerca\_rewrite(  $url) 

#### Parameters

$url

File

Tags
----

package

Default

### tbm\_custom\_login\_logo\_url()

[](#source-view)

tbm\_custom\_login\_logo\_url() 

_Change logo url in login page_

File

Tags
----

package

Default

### tbm\_custom\_login\_name\_url()

[](#source-view)

tbm\_custom\_login\_name\_url() 

_Change logo link name in login page_

File

Tags
----

package

Default

### tbm_enqueue()

[](#source-view)

tbm_enqueue() 

_Raspistill constructor._

**Example:**

    $camera = new Raspistill([
        'rotate' => 90,
        'width' => 640,
        'height' => 480,
        'exposure' => Raspistill::EXPOSURE_NIGHT,
    ]);

File

Tags
----

package

Cvuorinen\\Raspicam

### tbm\_get\_custom\_related\_posts()

[](#source-view)

tbm\_get\_custom\_related\_posts(  $post = null, integer  $limit = 4, array  $ids\_to\_exclude = array(), string  $special = SITE\_SPECIAL\_TAXONOMY, string|array  $post_type = 'post', boolean  $simple = false) : array|boolean|mixed|object

_La funzione restituisce i contenuti correlati ad uno specifico post_

#### Parameters

$post

integer

$limit

Il numero massimo di post. Optional. Default 4

array

$ids\_to\_exclude

Id dei post da escludere. Optional.

string

$special

La tassonomia di correlazione principale. Optional. Default `trend`.

string|array

$post_type

Il post type su cui eseguire da query. Optional. Default `post`.

boolean

$simple

Se true, restituisce solo ID, titolo, url e post_type e non l'oggetto completo del $post

#### Returns

array|boolean|mixed|object

File

Tags
----

package

Default

### tbm\_get\_logo()

[](#source-view)

tbm\_get\_logo(  $img_class = '',   $a_class = '') 

#### Parameters

$img_class

$a_class

File

Tags
----

package

Default

### tbm\_get\_post_source()

[](#source-view)

tbm\_get\_post_source(  $post,   $before = '',   $after = '') 

#### Parameters

$post

$before

$after

File

Tags
----

package

Default

### tbm\_get\_post_via()

[](#source-view)

tbm\_get\_post_via(  $post,   $before = '',   $after = '') 

#### Parameters

$post

$before

$after

File

Tags
----

package

Default

### tbm\_get\_pub_date()

[](#source-view)

tbm\_get\_pub\_date(integer|\\WP\_Post  $post = null) : string

_Visualizza la data di pubblicazione del post_

#### Parameters

integer|\\WP_Post

$post

Optional. Post ID or post object. Default is the global `$post`.

#### Returns

string

File

Tags
----

package

Default

### tbm\_get\_speciale_terms()

[](#source-view)

tbm\_get\_speciale\_terms(integer|\\WP\_Post  $post = null) : string

_Get the speciale (one or more) related to the post_

#### Parameters

integer|\\WP_Post

$post

Optional. Post ID or post object. Default is the global `$post`.

#### Returns

string

File

Tags
----

package

Default

### tbm\_get\_sponsor_post()

[](#source-view)

tbm\_get\_sponsor_post(  $post = null) 

#### Parameters

$post

File

Tags
----

package

Default

### tbm\_get\_sponsor_term()

[](#source-view)

tbm\_get\_sponsor_term(  $term_id,   $taxonomy) 

#### Parameters

$term_id

$taxonomy

File

Tags
----

package

Default

### tbm\_get\_the_author()

[](#source-view)

tbm\_get\_the\_author(integer|\\WP\_Post  $post = null) : string

_Visualizza il nome dell'autore linkato alla sua pagina_

#### Parameters

integer|\\WP_Post

$post

Optional. Post ID or post object. Default is the global `$post`.

#### Returns

string

File

Tags
----

package

Default

### tbm\_get\_the_banner()

[](#source-view)

tbm\_get\_the_banner(string  $name = '', string  $_before = '', string  $_after = '', boolean  $display = true, boolean  $nodiv = false) : string

_Outputs a banner content_

#### Parameters

string

$name

The banner zone name

string

$_before

An optional text to prepend to banner content

string

$_after

An optional text to append to banner content

boolean

$display

True to echo result, false to return

boolean

$nodiv

True to print div surrounding the code, false to nothing

#### Returns

string

File

Tags
----

package

Default

### tbm\_get\_the\_post\_thumbnail_alt()

[](#source-view)

tbm\_get\_the\_post\_thumbnail\_alt(integer|\\WP\_Post  $post = null) : string|false

_Restituisce il testo ALT dell'immagine_

#### Parameters

integer|\\WP_Post

$post

Optional. Post ID or WP_Post object. Default is global `$post`.

#### Returns

string|false —

Post thumbnail ALT text or false if no ALT text is available.

File

Tags
----

package

Default

### tbm\_get\_the\_post\_thumbnail_url()

[](#source-view)

tbm\_get\_the\_post\_thumbnail\_url(integer|\\WP\_Post  $post = null, array  $size = array(50, 50)) : string|false

_Return the post thumbnail URL, rewrited with Thumbor (if available and activated)._

#### Parameters

integer|\\WP_Post

$post

Optional. Post ID or WP_Post object. Default is global `$post`.

array

$size

Optional. Image size to retrieve. Accepts array of height and width dimensions. Defaults 50,50.

#### Returns

string|false —

Post thumbnail URL rewrited with Thumbor, regular post thumbnail URL or false if no URL is available.

File

Tags
----

package

Default

### tbm\_get\_thumb_credit()

[](#source-view)

tbm\_get\_thumb_credit(  $thumbid,   $before = '',   $after = '') 

#### Parameters

$thumbid

$before

$after

File

Tags
----

package

Default

### tbm\_get\_update_date()

[](#source-view)

tbm\_get\_update\_date(integer|\\WP\_Post  $post = null) : string

_Visualizza la data di aggiornamento del post_

#### Parameters

integer|\\WP_Post

$post

Optional. Post ID or post object. Default is the global `$post`.

#### Returns

string

File

Tags
----

package

Default

### tbm\_has\_title()

[](#source-view)

tbm\_has\_title(  $id) 

#### Parameters

$id

File

Tags
----

package

Default

### tbm\_helper\_build_wpquery()

[](#source-view)

tbm\_helper\_build_wpquery(boolean  $category_in = false, boolean  $tag_in = false, boolean  $post\_not\_in = false, boolean  $tax_query = false, boolean  $limit = false, \\string/array  $post_types = array('post'), boolean  $date = false, string  $publish = 'publish', boolean  $category_and = false, boolean  $tag_and = false) : boolean|\\WP_Query

_Un helper per la creazione di query via WP\_Query. Gli argomenti sono gli stessi di WP\_Query_

#### Parameters

boolean

$category_in

boolean

$tag_in

boolean

$post\_not\_in

boolean

$tax_query

boolean

$limit

\\string/array

$post_types

boolean

$date

string

$publish

boolean

$category_and

boolean

$tag_and

#### Returns

boolean|\\WP_Query

File

Tags
----

package

Default

### tbm\_insert\_after_paragraph()

[](#source-view)

tbm\_insert\_after_paragraph(string  $insertion, integer  $paragraph_id, string  $content,   $display) : string

_Insert something after a specific <p> paragraph in some content._

#### Parameters

string

$insertion

Likely HTML markup, ad script code etc.

integer

$paragraph_id

After which paragraph should the insertion be added. Starts at 1.

string

$content

Likely HTML markup.

$display

#### Returns

string —

Likely HTML markup.

File

Tags
----

package

Default

### tbm\_insert\_before_tag()

[](#source-view)

tbm\_insert\_before_tag(string  $insertion,   $tag, integer  $position_id, string  $content, integer|boolean  $display, string  $min = 1) : string

_Insert something before a specific tag in content._

#### Parameters

string

$insertion

Likely HTML markup, ad script code etc.

$tag

integer

$position_id

Before which occurence of tag should the insertion be added. Starts at 1.

string

$content

Likely HTML markup.

integer|boolean

$display

Whether to echo or return the results. Accepts 0|false for return, 1|true for echo. Default 0|false.

string

$min

Minimun numbers of tags being present in $content (if less, no new content is inserted).

#### Returns

string —

Likely HTML markup.

File

Tags
----

package

Default

### tbm\_insert\_sharethrough()

[](#source-view)

tbm\_insert\_sharethrough(  $content) 

#### Parameters

$content

File

Tags
----

package

Default

### tbm\_insert\_video()

[](#source-view)

tbm\_insert\_video(string  $content) : string

_Insert code for video ads after second paragraph of single post content._

#### Parameters

string

$content

Post content

#### Returns

string —

Amended content

File

Tags
----

package

Default

### tbm\_is\_speciale()

[](#source-view)

tbm\_is\_speciale() : mixed

_Check if a tag is speciale_

#### Returns

mixed

File

Tags
----

package

Default

### tbm_log()

[](#source-view)

tbm_log(string|array|object  $data = null, boolean  $die = true) 

_Formatta elegantemente i messaggi di var_export_

#### Parameters

string|array|object

$data

I dati da mostrare

boolean

$die

Se interrompere l'esecuzione o no

File

Tags
----

package

Default

### tbm\_login\_logo()

[](#source-view)

tbm\_login\_logo() 

_Add PMI logo to login page_

File

Tags
----

package

Default

### tbm\_missed\_schedule()

[](#source-view)

tbm\_missed\_schedule() 

_Check timestamp from transient and published all missed posts_

File

Tags
----

package

Default

### tbm_pagination()

[](#source-view)

tbm_pagination(string  $html) 

_Format custom pagination layout with wp_pagenavi plugin_

#### Parameters

string

$html

HTML code of pagination

File

Tags
----

package

Default

### tbm\_relationship\_query()

[](#source-view)

tbm\_relationship\_query(  $args) : mixed

_Ordino il relational e il post_object per data decrescente_

#### Parameters

$args

#### Returns

mixed

File

Tags
----

package

Default

### tbm\_relationship\_result()

[](#source-view)

tbm\_relationship\_result(  $title,   $post,   $field,   $post_id) : string

_Mostra il post type nella lista del relation field_

#### Parameters

$title

$post

$field

$post_id

#### Returns

string

File

Tags
----

package

Default

### tbm\_remove\_poor_tags()

[](#source-view)

tbm\_remove\_poor_tags(  $terms,   $post_id,   $taxonomy) : mixed

_Remove tag if has a small numeber of posts_

#### Parameters

$terms

Array of terms

$post_id

ID of the post

$taxonomy

Taxonomy slug

#### Returns

mixed

File

Tags
----

package

Default

### tbm\_search\_by_title()

[](#source-view)

tbm\_search\_by_title(string  $search, \\WP_Query  $wp_query) 

_Search SQL filter for matching against post title only._

#### Parameters

string

$search

\\WP_Query

$wp_query

File

See also

[http://wordpress.stackexchange.com/a/11826/1685](http://wordpress.stackexchange.com/a/11826/1685)

Tags
----

package

Default

### tbm_setup()

[](#source-view)

tbm_setup() 

_pmi functions and definitions_

File

See also

[https://developer.wordpress.org/themes/basics/theme-functions/](https://developer.wordpress.org/themes/basics/theme-functions/)

Tags
----

package

pmi

pmi

### tbm_sponsor()

[](#source-view)

tbm_sponsor(  $sponsor) : string

_Print the html code to show sponsor_

#### Parameters

$sponsor

WP Term object relative to the sponsor term

#### Returns

string

File

Tags
----

package

Default

### tbm\_wp\_get\_attachment\_image_url()

[](#source-view)

tbm\_wp\_get\_attachment\_image_url(integer  $attachment_id, array  $size = array(50, 50)) : string|false

_Get the URL of an image attachment, rewrited with Thumbor (if available and activated)._

#### Parameters

integer

$attachment_id

Image attachment ID.

array

$size

Optional. Image size to retrieve. Accepts array of height and width dimensions. Defaults 50,50.

#### Returns

string|false —

Attachment URL rewrited with Thumbor, regular attachment URL or false if no image is available.

File

Tags
----

package

Default

### theme_init()

[](#source-view)

theme_init() 

_Rimuove un po' di schifezze dall'header_

File

Tags
----

package

Default

### tmb\_get\_card_cover()

[](#source-view)

tmb\_get\_card_cover(  $post = null) : string

_Get the cover (the first page) of the card post type_

#### Parameters

$post

#### Returns

string

File

Tags
----

package

Default

### tmb\_plural\_handle()

[](#source-view)

tmb\_plural\_handle(integer  $count = 1, string  $singular = 'nome', string  $plural = 'nomi') : string

_Restituisce una parola al plurale o singolare in base ad un numero: tmb\_plural\_handle($number,'nome','nomi');_

#### Parameters

integer

$count

Il numero su cui basare la gestione dei plurali

string

$singular

Il nome al singolare

string

$plural

Il nome al plurale

#### Returns

string

File

Tags
----

package

Default

*   [Class Hierarchy Diagram](graphs/class.html)

*   [Errors](reports/errors.html)
*   [Markers](reports/markers.html)

* * *

Documentation is powered by [phpDocumentor](http://www.phpdoc.org/) and authored on June 7th, 2018 at 09:32.