<?php
global $current_user, $wp_query;

$data = $dataspe = $dataLayer = $datacat = $datacatNew = $datacit = $datacoc = $dataric = array();

/**
 * Recupera le tassonomie di un post e le formatta in array per includerle nel datalayer
 *
 * @param integer $post_id L'id del post
 * @param string $tax_slug Lo slug della tassonomia che si desidera recuperare
 * @param string $tax_name Il nome della tassonomia da usare nell'output
 * @param int $maxparent Il numero massimo di ancestor che una tassonomia deve avere per essere mostrata
 * @param int $minparent Il numero minimo di ancestor che una tassonomia deve avere per essere mostrata
 * @param int $maxkey Il numero massimo di elementi da restituire
 * @param int $startkey Il numero da cui parte la numerazione degli elementi
 *
 * @return type Array
 */
function ad_get_taxonomy_datalayer( $post_id, $tax_slug, $tax_name, $maxparent = 10, $minparent = 10, $maxkey, $startkey ) {
	$dataLayer    = array();
	$tax_name     = $tax_name == null ? 'Non definita' : $tax_name;
	$post_taxs    = wp_get_post_terms( $post_id, $tax_slug );
	$tax_each_key = 0;
	if ( $post_taxs ) {
		$top_parent_terms = array();
		foreach ( $post_taxs as $term ) {
			$tax_key     = ( $tax_each_key + $startkey );
			$tax_parents = get_ancestors( $term->term_id, $tax_slug, 'taxonomy' );

			if ( count( $tax_parents ) <= $maxparent && count( $tax_parents ) >= $minparent ) {
				if ( ( $tax_each_key < ( $startkey + $maxkey ) ) ) {
					$dataLayer["{$tax_name}{$tax_key}"] = $term->name;
				}
				$tax_each_key ++;
			}
//            if (!in_array($term->name, $top_parent_terms)) {
//                $top_parent_terms[] = $term->name;
//            }

//            foreach (array_reverse($tax_parents) as $parent_key => $parent_tax) {
//                $term = get_term($parent_tax, $tax_slug);
//                if (!in_array($term->name, $top_parent_terms)) {
//                    $top_parent_terms[] = $term->name;
//                }
//            }
		}
//        $dataLayer["DataGlobal"] = join("/", $top_parent_terms);
	}

	return $dataLayer;
}

/**
 *
 * Recupera le tassonomie di una pagina archivio e le formatta in array per includerle nel datalayer
 *
 * @param type $tax_id L'id della tassonomia da recuperare
 * @param string $tax_slug Lo slug della tassonomia che si desidera recuperare
 * @param string $tax_name Il nome della tassonomia da usare nell'output
 * @param int $maxparent Il numero massimo di ancestor che una tassonomia deve avere per essere mostrata
 * @param int $minparent Il numero minimo di ancestor che una tassonomia deve avere per essere mostrata
 * @param int $maxkey Il numero massimo di elementi da restituire
 * @param int $startkey Il numero da cui parte la numerazione degli elementi
 *
 * @return type Array
 */

function ad_get_archive_datalayer( $tax_id, $tax_slug, $tax_name, $maxparent = 10, $minparent = 10, $maxkey, $startkey ) {
	$dataLayer = array();
	$tax_name  = $tax_name == null ? 'Non definita' : $tax_name;
	$term      = get_term( $tax_id, $tax_slug );
	if ( isset( $term->term_id ) ) {
		$top_parent_terms                    = array();
		$tax_parents                         = get_ancestors( $term->term_id, $tax_slug );
		$dataLayer["{$tax_name}{$startkey}"] = $term->name;
		$startkey ++;
		foreach ( array_reverse( $tax_parents ) as $parent_key => $parent_tax ) {
			$parent_term        = get_term( $parent_tax, $tax_slug );
			$tax_parent_parents = get_ancestors( $parent_term->term_id, $tax_slug );
			if ( count( $tax_parent_parents ) <= $maxparent && count( $tax_parent_parents ) >= $minparent ) {
				$dataLayer["{$tax_name}{$startkey}"] = $parent_term->name;
				$startkey ++;
			}
		}

//        if (!in_array($term->name, $top_parent_terms)) {
//            $top_parent_terms[] = $term->name;
//        }
//
//        foreach (array_reverse($tax_parents) as $parent_key => $parent_tax) {
//            $term = get_term($parent_tax, $tax_slug);
//            if (!in_array($term->name, $top_parent_terms)) {
//                $top_parent_terms[] = $term->name;
//            }
//        }
//        $dataLayer["DataGlobal"] = join("/", $top_parent_terms);
	}

	return $dataLayer;
}

if ( is_front_page() ) {
	$dataLayer["pagePostType"] = "Homepage";
}

if ( is_user_logged_in() ) {
	$dataLayer["visitorLoginState"] = "logged-in";
} else {
	$dataLayer["visitorLoginState"] = "logged-out";
}

if ( is_singular() && ! bp_current_component() && ! is_page() ) {
	$dataLayer["pagePostType"]      = ucfirst( get_post_type() );
	$dataLayer["pagePostWordCount"] = str_word_count( strip_tags( get_post_field( 'post_content', get_the_ID() ) ) );
	$dataLayer["pagePostAuthor"]    = esc_js( get_the_author_meta( 'display_name', $post->post_author ) );
	$dataLayer["pagePostTitle"]     = get_post_field( 'post_title', get_the_ID(), 'raw' );
	$dataLayer["pagePostDate"]      = get_the_date( '/Y/m/d/W/H/' );
	$dataLayer["pagePostDateJs"]    = get_the_time( 'Ynj' );
	$dataLayer["pagePostDateYear"]  = get_the_date( "Y" );
	$dataLayer["pagePostDateMonth"] = get_the_date( "m" );
	$dataLayer["pagePostDateDay"]   = get_the_date( "d" );
	$dataLayer["pagePostDateHour"]  = get_the_date( "H" );
	$data                           = array_merge( $data, ad_get_taxonomy_datalayer( get_the_ID(), 'category', 'Categoria', 2, 1, 6, 5 ) );
	$data                           = array_merge( $data, ad_get_taxonomy_datalayer( get_the_ID(), 'categoria_ricetta', 'Ricette', 2, 0, 4, 11 ) );
	$data                           = array_merge( $data, ad_get_taxonomy_datalayer( get_the_ID(), 'categoria_cocktail', 'Cocktail', 2, 0, 1, 15 ) );
	$data                           = array_merge( $data, ad_get_taxonomy_datalayer( get_the_ID(), 'sponsor', 'Speciale', 2, 0, 2, 18 ) );
	$data                           = array_merge( $data, ad_get_taxonomy_datalayer( get_the_ID(), 'speciale', 'Speciale', 2, 0, 2, 18 ) );
	$data                           = array_merge( $data, ad_get_taxonomy_datalayer( get_the_ID(), 'citta', 'Citta', 0, 0, 2, 16 ) );
	$data                           = array_merge( $data, ad_get_taxonomy_datalayer( get_the_ID(), 'filters', 'Filtri', 0, 0, 5, 21 ) );
	$dataLayer                      = array_merge( $dataLayer, $data );
} else if ( bp_current_component() ) {
	$dataLayer["pagePostType"] = 'BuddyPress';
	$dataLayer["Categoria5"]   = ucfirst( bp_current_component() );
	$dataLayer["Categoria6"]   = ucfirst( bp_current_action() );
} else if ( is_page() && ! bp_current_component() ) {
	$dataLayer["pagePostType"] = get_post_field( 'post_title', get_the_ID(), 'raw' );
}

if ( is_archive() || is_post_type_archive() ) {
	$name = ucfirst( get_queried_object()->name );
	if ( is_category() ) {
		$dataLayer["pagePostType"] = "Home {$name}";
	} else if ( is_tag() ) {
		$dataLayer["pagePostType"] = "Home {$name}";
	} else if ( is_tax() && ! is_tax( array( 'speciale', 'sponsor' ) ) ) {
		$dataLayer["pagePostType"] = "Home {$name}";
	} else if ( is_author() ) {
		$dataLayer["pagePostType"] = "Home Autore";
	} else if ( is_year() ) {
		$dataLayer["pagePostType"]     = "Home Anno";
		$dataLayer["pagePostDateYear"] = get_the_date( "Y" );
	} else if ( is_month() ) {
		$dataLayer["pagePostType"]      = "Home Mese";
		$dataLayer["pagePostDateYear"]  = get_the_date( "Y" );
		$dataLayer["pagePostDateMonth"] = get_the_date( "m" );
	} else if ( is_day() ) {
		$dataLayer["pagePostType"]      = "Home giorno";
		$dataLayer["pagePostDate"]      = get_the_date();
		$dataLayer["pagePostDateYear"]  = get_the_date( "Y" );
		$dataLayer["pagePostDateMonth"] = get_the_date( "m" );
		$dataLayer["pagePostDateDay"]   = get_the_date( "d" );
	} else if ( is_time() ) {
		$dataLayer["pagePostType"] = "Home ora";
	} else if ( is_date() ) {
		$dataLayer["pagePostType"]      = "Home data";
		$dataLayer["pagePostDate"]      = get_the_date();
		$dataLayer["pagePostDateYear"]  = get_the_date( "Y" );
		$dataLayer["pagePostDateMonth"] = get_the_date( "m" );
		$dataLayer["pagePostDateDay"]   = get_the_date( "d" );
	} else if ( is_post_type_archive() ) {
		$name                      = ucfirst( get_post_type() );
		$dataLayer["pagePostType"] = "Home {$name}";
	} else if ( is_tax( 'speciale' ) ) {
		$dataLayer["pagePostType"] = "Speciale";
	} else if ( is_tax( 'sponsor' ) ) {
		$dataLayer["pagePostType"] = "Speciale sponsorizzato";
	}


	if ( ( is_tax() ) ) {
		$dataric    = ad_get_archive_datalayer( get_queried_object_id(), 'categoria_ricetta', 'Ricette', 2, 0, 4, 11 );
		$datacoc    = ad_get_archive_datalayer( get_queried_object_id(), 'categoria_cocktail', 'Cocktail', 2, 0, 1, 15 );
		$datacit    = ad_get_archive_datalayer( get_queried_object_id(), 'citta', 'Citta', 0, 0, 2, 16 );
		$dataspe    = ad_get_archive_datalayer( get_queried_object_id(), array(
			'speciale',
			'sponsor'
		), 'Speciale', 2, 0, 2, 18 );
		$datacat    = ad_get_archive_datalayer( get_queried_object_id(), 'category', 'Categoria', 2, 1, 6, 5 );
		$datacatNew = ad_get_archive_datalayer( get_queried_object_id(), 'category', 'pageCategory_', 2, 1, 6, 5 );
	} else if ( is_category() ) {
		$datacat    = ad_get_archive_datalayer( get_queried_object_id(), 'category', 'Categoria', 2, 1, 6, 5 );
		$datacatNew = ad_get_archive_datalayer( get_queried_object_id(), 'category', 'pageCategory_', 2, 1, 6, 5 );
	}
	$dataLayer = array_merge( $dataLayer, $datacit, $datacat, $datacatNew, $datacoc, $dataric, $dataspe );
}

$refreshValue             = tbm_get_the_banner( 'REFRESH', '', '', false );
$refreshRate              = ( is_numeric( $refreshValue ) && ! empty( $refreshValue ) ) ? ( $refreshValue * 1000 ) : 86400000;
$dataLayer["refreshRate"] = $refreshRate;


echo '<!--';

echo $refresh;

echo '-->';


echo '<script>' . "\n";
printf( 'dataLayer = [%s];', json_encode( $dataLayer ) );
echo "\n";
echo "if (typeof console == \"object\" && typeof dataLayer[0] == \"object\") { console.log(dataLayer[0]); }";
//echo 'var _gaq=_gaq||[];_gaq.push(["_setAccount","UA-33180657-4"],["_setDomainName","www.agrodolce.it"],["_trackPageview"]);(function(){var ga=document.createElement("script");ga.type="text/javascript";ga.async=true;ga.src=("https:"==document.location.protocol?"https://ssl":"http://www")+".google-analytics.com/ga.js";var s=document.getElementsByTagName("script")[0];s.parentNode.insertBefore(ga,s)})();';
echo '</script>' . "\n";
?>