<?php
/**
 * Created by PhpStorm.
 * User: francesco
 * Date: 08/05/2015
 * Time: 20.27
 */

/**
 * Atatus
 */
$atatus_id = '';
if ( function_exists( 'get_field' ) ) {
	if ( get_field( 'tbm_common_enable_atatus', 'option' ) && ! empty( get_field( 'tbm_common_code_atatus', 'option' ) ) ) {
		$atatus_id = get_field( 'tbm_common_code_atatus', 'option' );
	}
}

/**
 * Raccoglie metadati dalle pagine WP in un array $dataLayer (cfr. http://goo.gl/Nmo1I)
 * Da includere nell'head o comunque prima del tag di Google Tag Manager
 **/
global $current_user, $wp_query;
$dataLayer = array();

/**
 * Dati per informazioni generali
 */
if ( is_multisite() ) {
	global $blog_id;
	$current_blog_details  = get_blog_details( array(
		'blog_id' => $blog_id
	) );
	$dataLayer["siteName"] = $current_blog_details->blogname;
} else {
	$dataLayer["siteName"] = get_bloginfo( 'name', 'display' );
}

/**
 * Dati per homepage
 */
if ( is_front_page() ) {
	$dataLayer["pagePostType"] = "Homepage";
}

/**
 * Dati per utenti loggati
 */
if ( is_user_logged_in() ) {
	$dataLayer["visitorLoginState"] = "logged-in";
	wp_get_current_user();
	$dataLayer["visitorType"] = ( $current_user->roles[0] == null ? "visitor-logged-out" : $current_user->roles[0] );
} else {
	$dataLayer["visitorLoginState"] = "logged-out";
}

/**
 * Dati per single
 */
if ( is_single() ) {
	$this_id                  = get_the_ID();
	$dataLayer["contentType"] = 'single';
	if ( has_post_thumbnail() ) {
		$thumb_array            = wp_get_attachment_image_src( get_post_thumbnail_id( $this_id ), array(1200,1200) );
		$thumb_url              = $thumb_array[0];
		$dataLayer["thumbnail"] = $thumb_url;
	}
}

/**
 * Dati per pagine
 */
if ( is_page() && ! is_front_page() ) {
	$dataLayer["contentType"] = 'page';
}

/**
 * Dati per allegati
 */
if ( is_attachment() ) {
	$dataLayer["contentType"] = 'attachment';
}

/**
 * Dati per single
 */
if ( is_singular() && ! is_front_page() ) {
	global $post;
	$this_id                 = get_the_ID();
	$dataLayer["pagePostID"] = $this_id;

	if ( get_post_meta( $this_id, 'liveblog', true ) != false ) {
		$dataLayer["pagePostType"] = 'liveblog';
	} else {
		$dataLayer["pagePostType"] = get_post_type();
	}

	$taxonomies = get_taxonomies( '', 'names' );
	foreach ( $taxonomies as $taxonomy ) {
		$terms = get_the_terms( $this_id, $taxonomy );
		if ( ! empty( $terms ) ) {
			$val = 0;
			foreach ( $terms as $term ) {
				$taxonomy                            = ucfirst( $taxonomy );
				$dataLayer["page{$taxonomy}_{$val}"] = $term->name;
				$val ++;
			}
			unset( $val );
		}
	}

	if ( current_user_can( 'activate_plugins' ) ) {
		$post_customs = get_post_meta( $this_id );
		if ( $post_customs ) {
			foreach ( $post_customs as $key => $post_custom ) {
				$single = get_post_meta( $this_id, $key, true );
				if ( is_array( $single ) ) {
					foreach ( $single as $val => $value ) {
						$dataLayer["pageCustom_{$val}"] = $value;
					}
				} else {
					$dataLayer["pageCustom_{$key}"] = $single;
				}
			}
		}
	}
	$dataLayer["pagePostWordCount"] = str_word_count( strip_tags( get_post_field( 'post_content', $this_id ) ) );
	$dataLayer["pagePostAuthor"]    = get_the_author_meta( 'display_name', get_post_field( 'post_author', $this_id, 'raw' ) );
	$dataLayer["pagePostTitle"]     = get_post_field( 'post_title', $this_id, 'raw' );
	$dataLayer["pagePostDate"]      = get_the_date( '/Y/m/d/W/H/' );
	$dataLayer["pagePostDateYear"]  = get_the_date( "Y" );
	$dataLayer["pagePostDateMonth"] = get_the_date( "m" );
	$dataLayer["pagePostDateDay"]   = get_the_date( "d" );
	$dataLayer["pagePostDateHour"]  = get_the_date( "H" );
}

/**
 * Dati per archivi
 */
if ( is_archive() || is_post_type_archive() ) {
	if ( is_category() ) {
		$dataLayer["pagePostType"] = "Archivio Categoria";
	} elseif ( is_tag() ) {
		$dataLayer["pagePostType"] = "Archivio Tag";
	} elseif ( is_tax() ) {
		$dataLayer["pagePostType"] = "Archivio Tassonomia";
	} elseif ( is_author() ) {
		$dataLayer["pagePostType"] = "Archivio Autore";
	} elseif ( is_year() ) {
		$dataLayer["pagePostType"]     = "Archivio Anno";
		$dataLayer["pagePostDateYear"] = get_the_date( "Y" );
	} elseif ( is_month() ) {
		$dataLayer["pagePostType"]      = "Archivio Mese";
		$dataLayer["pagePostDateYear"]  = get_the_date( "Y" );
		$dataLayer["pagePostDateMonth"] = get_the_date( "m" );
	} elseif ( is_day() ) {
		$dataLayer["pagePostType"]      = "Archivio giorno";
		$dataLayer["pagePostDate"]      = get_the_date();
		$dataLayer["pagePostDateYear"]  = get_the_date( "Y" );
		$dataLayer["pagePostDateMonth"] = get_the_date( "m" );
		$dataLayer["pagePostDateDay"]   = get_the_date( "d" );
	} elseif ( is_time() ) {
		$dataLayer["pagePostType"] = "Archivio ora";
	} elseif ( is_date() ) {
		$dataLayer["pagePostType"]      = "Archivio data";
		$dataLayer["pagePostDate"]      = get_the_date();
		$dataLayer["pagePostDateYear"]  = get_the_date( "Y" );
		$dataLayer["pagePostDateMonth"] = get_the_date( "m" );
		$dataLayer["pagePostDateDay"]   = get_the_date( "d" );
	}
	if ( ( is_tax() || is_category() || is_tag() ) ) {
		$taxonomy                     = ucfirst( get_queried_object()->taxonomy );
		$name                         = get_queried_object()->name;
		$dataLayer["page{$taxonomy}"] = $name;
	}

}

/**
 * Gestione del refresh
 */
$refreshValue = tbm_get_the_banner( 'REFRESH', '', '', false );
$refreshRate  = ( is_numeric( $refreshValue ) && ! empty( $refreshValue ) ) ? ( $refreshValue * 1000 ) : 86400000;

/**
 * Output
 */
$dataLayer["refreshRate"] = $refreshRate;

/**
 * Impostazione versione
 */
$theme_ver             = wp_get_theme()->get( 'Version' );
$dataLayer["themeVer"] = $theme_ver;
$wp_theme           = get_bloginfo( 'version' );
$dataLayer["wpVer"] = $wp_theme;

/**
 * Impostazione atatus ID
 */
$dataLayer["atatusId"] = $atatus_id;

// filtro per personalizzazioni specifiche
$dataLayer = apply_filters( "tbtms_data_layer", $dataLayer );