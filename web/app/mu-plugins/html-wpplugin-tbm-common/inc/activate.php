<?php
/**
 * Created by PhpStorm.
 * User: francesco
 * Date: 07/06/2018
 * Time: 09:47
 */


/**
 * check if ACF plugin exist and is active
 */
if ( ( ! class_exists( 'acf' ) ) ) {
	// If ACF is not enabled
	add_action( 'admin_notices', function () {
		?>
		<div class="notice notice-error notice-large">
			<p>Il plugin TBM Common non è abilitato. Per abilitarlo è necessario scaricare e attivare il plugin <a
						target="_blank" href="https://www.advancedcustomfields.com/pro/">ACF Pro</a>. Segnala questo
				messaggio all'amministratore di sitema.</p>
		</div>
		<?php
	} );
} else {
	// If ACF is enabled
	require_once( HTML_TBM_COMMON_PLUGIN_DIR . 'functions/functions.php' );
	/**
	 * Execute the CacheControl class: inject cache-control header
	 *
	 * @since    1.0.0
	 */
	include_once HTML_TBM_COMMON_PLUGIN_DIR . 'classes/System/AmpUpdate.php';
	include_once HTML_TBM_COMMON_PLUGIN_DIR . 'classes/System/CacheControl.php';
	include_once HTML_TBM_COMMON_PLUGIN_DIR . 'classes/Taxonomies/Sponsor.php';
	include_once HTML_TBM_COMMON_PLUGIN_DIR . 'classes/Templates/Header.php';

	$plugin = new \TbmCommon\System\CacheControl();

}
