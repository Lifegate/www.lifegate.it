# Changelog
### Version 10.7
- Added transient to related query

### Version 10.3
- Moved TMS to async load

### Version 10.2
- Added Active Zone in `tbm_head` function

### Version 10.1
- Added WP Cli commands

## Version 10
- Added CacheControl class: replace cache-control plugin

### Version 9.6
- Added datalayer to Wordpress admin

### Version 9.5
- Added _channel_ to Rocket Chat notification

### Version 9.4
- Added `startdate` in analytics query

### Version 9.3
- Added W3TC post flush in cache class

### Version 9.2
- Added SMTP Tbm functions

### Version 9.1
- New syntax for disabling adv

## Version 9
- Refactored `tbm_insert_after_paragraph()` function

### Version 8.3
- Added `tbm_get_first_paragraph()` function

### Version 8.2
- Added `tbm_get_post_type_in_admin()` function
- Added `Sponsor` class

### Version 8.1
- Added author link to custom dashboard

## Version 8
- Added TbmCommon namespace

### Version 7.11
- Added AMP banner sanitizer

### Version 7.10
- Added Linkedin trigger button to dashboard

### Version 7.9
- Refactored cache function

### Version 7.8
- Added `ghproductlink` shortcode

### Version 7.7
- Added editorial post type to Article Json+LD Yoast Schema

### Version 7.6
- `critical=1` querystring disable TMS and scripts

### Version 7.5
- Added viewmax new disabled options 

### Version 7.4
- Fixed template inclusion with new `get_theme_file_uri()` function

### Version 7.3
- Added Twitter trigger button to dashboard

### Version 7.2
- Added GTM in Wordpress admin

### Version 7.1
- Added products summary shortcode

## Version 7
- New custom Wordpress dashboard

## Version 6
- Added new AMP adv positions

### Version 6.2
- Added `ghproducts` shortcode

### Version 6.1
- Added push common feed

## Version 5.0
- Merged *embedder* plugin in this plugin

### Version 5.4
- Compiled and minified js

### Version 5.3
- Added tbm_send_rocket_notifications function

### Version 5.2
- Added control to disable embedder

### Version 5.1
- Added Adblocker code to footer
