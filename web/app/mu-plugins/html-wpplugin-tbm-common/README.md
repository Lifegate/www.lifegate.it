# html.wpplugin.tbm-common

Make available some common functions for TBM WP themes

#Features
- Purge homepage and post cache when updating or publishing posts
- Add `wp tbm_opcode` WP-Cli command to purge opcache
- Set cache-control to every resource
- Enable news sitemap
- Enable datalytics
- Enable datalayer and TMS
- Enable media content in rss files
- Enable AMP support for single post
- Enable TBM Smtp for all sites (you must define `SMTP_ENABLE_TBM_MAILER` as `true` in theme)

# Background code execution on request
WP_Async_Request is a class that enables php background process in Wordpress. It uses the $_POST data setted by the dispatch function. To use it, first extend the class:
```php
class TBM_XXXX extends Tbm_Plugins\WP_Async_Request {

	/**
	 * Handle the $_POST data
	 */
	protected function handle() {
		$id      = $_POST['postid'];
		// Do something with the ID
	}

}
```
Then, dispatch the code:
```php
/**
 * Instantiate class
 */
$foo = new TBM_XXXX();

/**
 * Dispatch the request. The `handle()` functioncs will receive array( 'postid' => '100' ) as `$_POST` data.
 */
$foo->data( array( 'postid' => '100' ) );
$foo->dispatch();
```
Rember to instantiate always the class to enable admin ajax endpoint
```
$foo = new TBM_XXXX();
```

#Functions
`tbm_get_paragraph()` Get a *number* of paragraphs (`<p>...</p>`) of a post.

`tbm_get_post_type_in_admin()` Get the post type in new or edit admin screen.

`tbm_critical_css()`: Print the critical css code.

`tbm_posts_are_different()`: Check if two post have some differences in slug, date, title, excerpt or content

`tbm_get_strict_related_postst()`: Get related posts strict to the taxonomy 

`tbm_set_404()`: Redirect to 404 template and set 404 header 

`tbm_head()`: Print all the TBM header (datalayer, GTM, Tealium, Atatus) 

`tbm_is_amp()`: Check if is AMP endpoint

`tbm_get_domain( $url )`: Get only the hostname part of the url 

#Features
## Send notification to TribooMedia Rocket Chat
Use `function tbm_send_rocket_notifications( $message = array(), $level = 'warning', $interval = 10, $channels = 'alerts' )` to send a notification message to Triboo Media Rocket Chat.

## SVG Upload enabler
Add `.svg` extension to know and approved file extension.

## Flipboard compatibile feed
Juts add `mrss=1` to a feed URL to add to a feed images, video and all the content.

## Error messages during publishing/updating post
Simply launch `tbm_enable_error_message()` with parameters to show, at page reload, an error message. Don't forget to set to draft the post (`$_POST['post_status']    = 'draft';`).
```php
$_POST['post_status']    = 'draft';
tbm_enable_error_message( '<ul style="list-style: inherit;padding: 0 20px;"><li>' . implode( '</li><li>', $err_msg ) . '</li></ul>' );
```
## Sanitizer
Remove accents and other non standard characters from filename during file upload

## ACF Fields
The plugin add the common ACF fields to post post type, to speciale taxonomy and to sponsor taxonomy. Take a look into `functions/tbm-acf-fields.php` file.

## Actions
**tbm_preload_url**: preaload an url. Example:
```php
do_action( 'tbm_preload_url', $url );
```

#Filters
**tbm_disable_tms**: disable the tms printing
```php
add_filter( 'tbm_disable_tms', '__return_true' );
```

**tbm_sponsor_post_types**: define the post-types where enable the sponsor taxonomy
```php
add_filter( 'tbm_sponsor_post_types', function ( $enabled_post_types ) {
	$new_post_types = array( 'video', 'lesson', 'guide', 'lesson', 'video', 'news' );

	return array_merge( $enabled_post_types, $new_post_types );
} );
```
**tbm_dashboard_post_types_admin**: set the post-types to show in the custom WP dashboard
```php
add_filter( 'tbm_dashboard_post_types_authors', function ( $enabled_post_types ) {
	unset( $enabled_post_types['guide'] );

	return $enabled_post_types;
} );
```

**tbm_transient_taxonomy_purger**:  Delete transient after saving a term. You should pass a multidimensional array of taxonomy name and transient key to this filter. *Example*:
```php
add_filter( 'tbm_transient_taxonomy_purger', function () {
    return array( 'listino' => 'search_make_items' );
} );
```  
If transient key are more then one, you should pass an array:
```php
add_filter( 'tbm_transient_taxonomy_purger', function () {
    return array( 'listino' => array( 'search_model_items', 'search_make_items' ) );
} );
```  

**tbm_transient_post_purger**:  Like above, but delete transient after saving a post. You should pass a multidimensional array of post type name and transient key to this filter. *Example*:
```php
add_filter( 'tbm_transient_post_purger', function () {
    return array( 'video' => array( 'list_video', 'get_video' ) );
} );
```  

**tbm_post_custom_field_top**: Add new custom field at the **top** of _TBM Post_ custom field group. Should be array of array:
```php
add_filter( 'tbm_post_custom_field_top', function () {
    return array(
        array(
            'key'               => 'field_5d2ae511e3x01',
            'label'             => 'Luogo di scrittura',
            [...]
        ),
        array(
            'key'               => 'field_5a5e1060e99fr',
            'label'             => 'Breaking News?',
            [...]
        )
    );
});
``` 

**tbm_post_custom_field_bottom**: Add new custom field at the **bottom** of _TBM Post_ custom field group. Look above for example;

**html_add_url_to_invalidate**: Add urls to cache-purge during post update. *Example*:
```php
add_filter('tbm_add_url_to_invalidate', 'test_function');

function test_function(array $urls) {
    $urls[] = 'http://www.html.it';

    return $urls;
}
```
**tbm_common_editorial_filter**: Change the location of the Post custom fields. Default location: 
```php
array(
    array(
        'param'    => 'post_type',
        'operator' => '==',
        'value'    => 'post',
    ),
);
```
**tbm_common_speciale_filter**: Change the location of the Speciale custom fields. Default location: 
```php
array(
    array(
        'param'    => 'taxonomy',
        'operator' => '==',
        'value'    => 'post_tag',
    ),
);
```
**tbm_common_sponsor_filter**: Change the location of the Sponsor custom fields. Default location: 
```php
array(
    array(
        'param'    => 'taxonomy',
        'operator' => '==',
        'value'    => 'sponsor',
    ),
);
```
Example
```php
add_filter('tbm_common_editorial_filter', 'my_filter', 1, 1);

function my_filter( ) {
	$location = array (
		array (
			'param' => 'post_type',
			'operator' => '==',
			'value' => 'articoli',
		),
	);

	return $location;

}
```
**tbm_insert_video**: Enable or disable the automatic embedding of the video in the content. Default is `true`. Example (for disable):
```php
add_filter('tbm_common_insert_video', '__return_false');
```
**tbm_check_constraints**: Enable or disable the category constraints checker. Default is `false`. Example (for disable):
```php
add_filter('tbm_check_constraints', '__return_false');
```
**tbm_disable_cnvid**: Enable or disable the automatic video code insertion in content. Default to `false`. Example (for disable):
```php
add_filter('tbm_disable_cnvid', '__return_true');
```
**tbm_disable_native**: Enable or disable the automatic native code insertion in content. Default to `false`. Example (for disable):
```php
add_filter('tbm_disable_native', '__return_true');
```
**tbm_disable_teads**: Enable or disable the automatic teads code insertion in content. Default to `false`. Example (for disable):
```php
add_filter('tbm_disable_teads', '__return_true');
```
# AMP
If the official [Automattic AMP plugin](https://it.wordpress.org/plugins/amp/) is enabled, AMP template will be automatically enabled for single post type (just add `?amp` to the single post URL). 

For the AMP ads, 5 new zones should be created:
- AMP_BOX_INSIDE_TOP
- AMP_STRIP
- AMP_TEADS
- AMP_POST_FOOTER
- AMP_VAST

For the AMP menu, 2 menus should be linked to the following locations:
- AMP Hamburger (main menu)
- AMP Slider (trend menu)

# Deploy

## Prerequisites
Use [NodeJs](https://nodejs.org/en/download/) and [Grunt](https://gruntjs.com/)

## Install packages (only first time)
First install global grunt `npm install -g grunt-cli` and then install the required packages `npm install`

### Bump the package and push
**Caution**: this command will push code to repo.

**Hint**: You can always test the code with `grunt bump --dry-run`.

`grunt bump:patch` or `grunt bump:minor` or `grunt bump:major`  

This command will bump version in package.json and style.css, commit both files, tag them and **push** to origin master. You can use `grunt bump:patch` (0.0.1 > 0.0.2), `grunt bump:minor` (0.0.1 > 0.1.0) or `grunt bump:major` (0.0.1 > 1.0.0). Please, refer to [semantic versioning](https://semver.org/lang/it/) docs to choose wich bump use.
