<?php
/*
Plugin Name: TBM Common functions
Description: Make available some common functions for TBM WP themes
Author: Francesco Caccavella
Author URI: https://triboo.com
Version: 10.8.4
*/

$version = 'version:10.8.4';

define( "HTML_TBM_COMMON_PLUGIN_DIR", plugin_dir_path( __FILE__ ) );
define( "HTML_TBM_COMMON_PLUGIN_URL", plugin_dir_url( __FILE__ ) );
define( 'HTML_TBM_COMMON_PLUGIN_VERSION', str_replace( 'version:', '', $version ) );
define( "NONCE_SECRET", '>eV#zp%75!&GPq8ar-w^+QsK+VO-XEc_mc}ZF%9%v,/L-QKOE#$q<$%?Anwcx/,n' );

//TODO Change all get_template_directory() with new function get_theme_file_path() (cfr. https://make.wordpress.org/core/2016/09/09/new-functions-hooks-and-behaviour-for-theme-developers-in-wordpress-4-7/)

/**
 * Define template path based on theme structure
 */
$template_path = file_exists( get_template_directory() . '/views/' ) ? get_template_directory() . '/views' : get_template_directory();
define( 'HTML_TBM_COMMON_TEMPLATE_PATH', $template_path );

$template_plugin_path = plugin_dir_path( __FILE__ ) . 'templates/';
define( 'HTML_TBM_COMMON_PLUGIN_TEMPLATE_PATH', $template_plugin_path );

// check activation dependencies
require_once( HTML_TBM_COMMON_PLUGIN_DIR . 'inc/activate.php' );

// check
if ( file_exists( get_template_directory() . '/views/embedder/templates/' ) ) {
	$template_path = get_template_directory() . '/views/embedder/templates/';
} else if ( file_exists( get_template_directory() . '/embedder/templates/' ) ) {
	$template_path = get_template_directory() . '/embedder/templates/';
} else {
	$template_path = plugin_dir_path( __FILE__ ) . 'templates/';
}

define( 'HTML_TBM_COMMON_SHORTCODE_TEMPLATE_PATH', $template_path );


