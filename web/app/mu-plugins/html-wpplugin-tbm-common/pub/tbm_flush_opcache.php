<?php
/**
 * Opcache purge during deploy
 */
if ( function_exists( 'apc_clear_cache' ) ) {
	apc_clear_cache( 'opcode' );
} else {
	$op = opcache_reset();

	if ( $op ) {
		echo "OPCode Cache is cleared";
	} else {
		echo "Opcode cache is not enabled";
	}
}
