<?php


namespace TbmCommon\System;

/**
 * Class CacheControl derived from cache-control plugin
 * @package TbmCommon\System
 */
class CacheControl {

	private $cache_control_options = array(
		'front_page'         => array(
			'id'       => 'front_page',
			'name'     => 'Front page',
			'max_age'  => 10 * MINUTE_IN_SECONDS,
			's_maxage' => 1 * DAY_IN_SECONDS,
		),
		'singles'            => array(
			'id'       => 'singles',
			'name'     => 'Posts',
			'max_age'  => 86400,
			's_maxage' => 1 * MONTH_IN_SECONDS,
			'mmulti'   => 1,
		),
		'pages'              => array(
			'id'       => 'pages',
			'name'     => 'Pages',
			'max_age'  => 1 * DAY_IN_SECONDS,
			's_maxage' => 1 * MONTH_IN_SECONDS,
		),
		'home'               => array(
			'id'       => 'home',
			'name'     => 'Main index',
			'max_age'  => 10 * MINUTE_IN_SECONDS,
			's_maxage' => 1 * DAY_IN_SECONDS,
			'paged'    => 5,
		),
		'categories'         => array(
			'id'       => 'categories',
			'name'     => 'Categories',
			'max_age'  => 1 * DAY_IN_SECONDS,
			's_maxage' => 1 * DAY_IN_SECONDS,
			'paged'    => 8,
		),
		'tags'               => array(
			'id'       => 'tags',
			'name'     => 'Tags',
			'max_age'  => 1 * DAY_IN_SECONDS,
			's_maxage' => 1 * WEEK_IN_SECONDS,
			'paged'    => 10,
		),
		'taxs'               => array(
			'id'       => 'taxs',
			'name'     => 'Taxonomies',
			'max_age'  => 1 * DAY_IN_SECONDS,
			's_maxage' => 1 * DAY_IN_SECONDS,
			'paged'    => 10,
		),
		'authors'            => array(
			'id'       => 'authors',
			'name'     => 'Authors',
			'max_age'  => 1 * DAY_IN_SECONDS,
			's_maxage' => 1 * DAY_IN_SECONDS,
			'paged'    => 10,
		),
		'dates'              => array(
			'id'       => 'dates',
			'name'     => 'Dated archives',
			'max_age'  => 1 * DAY_IN_SECONDS,
			's_maxage' => 1 * DAY_IN_SECONDS,
		),
		'feeds'              => array(
			'id'       => 'feeds',
			'name'     => 'Feeds',
			'max_age'  => 120,
			's_maxage' => 120,
		),
		'attachment'         => array(
			'id'       => 'attachment',
			'name'     => 'Attachment pages',
			'max_age'  => 1 * DAY_IN_SECONDS,
			's_maxage' => 1 * DAY_IN_SECONDS,
		),
		'search'             => array(
			'id'       => 'search',
			'name'     => 'Search results',
			'max_age'  => 1 * DAY_IN_SECONDS,
			's_maxage' => 1 * DAY_IN_SECONDS,
		),
		'notfound'           => array(
			'id'       => 'notfound',
			'name'     => '404 Not Found',
			'max_age'  => 2 * MINUTE_IN_SECONDS,
			's_maxage' => 2 * MINUTE_IN_SECONDS,
		),
		'redirect_permanent' => array(
			'id'       => 'redirect_permanent',
			'name'     => 'Permanent redirects',
			'max_age'  => 1 * DAY_IN_SECONDS,
			's_maxage' => 1 * DAY_IN_SECONDS,
		),
		'sitemap'            => array(
			'id'       => 'sitemap',
			'name'     => 'Sitemap',
			'max_age'  => 60 * MINUTE_IN_SECONDS,
			's_maxage' => 60 * MINUTE_IN_SECONDS,
			'paged'    => 10,
		),
	);

	public function __construct() {
		add_action( 'template_redirect', array( $this, 'cache_control_send_headers' ) );
		add_filter( 'wp_redirect_status', array( $this, 'cache_control_handle_redirects' ), 10, 2 );
		add_filter( 'wpseo_sitemap_http_headers', array( $this, 'cache_control_set_sitemap_header' ) );
	}

	public function cache_control_set_sitemap_header( $headers ) {
		$option = $this->cache_control_options['sitemap'];

		$max_age  = $option['max_age'];
		$s_maxage = $option['s_maxage'];

		$headers[ 'Cache-Control: max-age=' . $max_age . ', s-maxage=' . $s_maxage ] = '';

		return $headers;
	}

	public function cache_control_handle_redirects( $status, $location = null ) {
		if ( $this->cache_control_nocacheables() ) {
			$this->cache_control_send_http_header(
				$this->cache_control_build_directive_header( false, false ) );
		} elseif ( $status == 301 || $status == 308 ) {
			$this->cache_control_send_http_header(
				$this->cache_control_build_directive_from_option( 'redirect_permanent' ) );
		}

		// Include a minimal body message. Recommended by HTTP spec, required by many caching proxies.
		if ( in_array( $status, array( "301", "302", "303", "307", "308" ) ) ) {
			if ( ob_start() ) {
				$location_attr = esc_attr( $location );
				print( "<!doctype html>\n<meta charset=\"utf-8\">\n<title>Document moved</title>\n<p>Document has <a href=\"${location_attr}\">moved here</a>.</p>" );
			}
		}

		return $status;
	}

	public function cache_control_send_headers() {
		$this->cache_control_send_http_header( $this->cache_control_select_directive() );
	}

	private function cache_control_send_http_header( $directives ) {
		if ( ! empty( $directives ) ) {
			header( "Cache-Control: $directives", true );
		}
	}

	private function cache_control_stale_factorer( $factor, $max_age ) {
		if ( is_paged() && is_int( $factor ) && $factor > 0 ) {
			$multiplier = get_query_var( 'paged' ) - 1;
			if ( $multiplier > 0 ) {
				$factored_max_age = $factor * $multiplier;
				if ( $factored_max_age >= ( $max_age * 10 ) ) {
					return $max_age * 10;
				}

				return $factored_max_age;
			}
		}

		return 0;
	}

	private function cache_control_is_future_now_maxtime( $max_time_future ) {
		// trusting the database to cache this query
		$future_post = new \WP_Query( array(
			'post_status'         => 'future',
			'posts_per_page'      => 1,
			'orderby'             => 'date',
			'order'               => 'ASC',
			'ignore_sticky_posts' => 1
		) );

		if ( $future_post->have_posts() ) {
			$local_nowtime = intval( current_time( 'timestamp', 0 ) );

			while ( $future_post->have_posts() ) {
				$future_post->the_post();
				$local_futuretime = get_the_time( 'U' );
				if ( ( $local_nowtime + $max_time_future ) > $local_futuretime ) {
					$max_time_future = $local_futuretime - $local_nowtime + rand( 2, 32 );
				}
			}

			wp_reset_postdata();
		}

		return $max_time_future;
	}

	private function cache_control_build_directive_header( $max_age, $s_maxage ) {
		$directive = "";
		if ( ! empty( $max_age ) && is_int( $max_age ) && $max_age > 0 ) {
			$directive = "max-age=$max_age";
		}

		if ( ! empty( $s_maxage ) && is_int( $s_maxage ) && $s_maxage > 0 && $s_maxage != $max_age ) {
			if ( ! $directive != "" ) {
				$directive = "public";
			}

			$directive = "$directive, s-maxage=$s_maxage";
		}

		// append RFC 5861 headers only if the request is cacheable
		if ( $directive != "" ) {

			if ( ! empty( $staleerror ) && is_int( $staleerror ) && $staleerror > 0 ) {
				$directive = "$directive, stale-if-error=$staleerror";
			}

			if ( ! empty( $stalereval ) && is_int( $stalereval ) && $stalereval > 0 ) {
				$directive = "$directive, stale-while-revalidate=$stalereval";
			}

			$directive = apply_filters( 'cache_control_cachedirective', $directive );

			return $directive;

		}

		// request isn't cacheable
		return "no-cache, no-store, must-revalidate";
	}

	private function cache_control_build_directive_from_option( $option_name ) {

		$option = $this->cache_control_options[ $option_name ];

		$max_age  = $option['max_age'];
		$s_maxage = $option['s_maxage'];

		// dynamically shorten caching time when a scheduled post is imminent
		if ( $option_name != 'attachment' &&
		     $option_name != 'dates' &&
		     $option_name != 'pages' &&
		     $option_name != 'singles' &&
		     $option_name != 'notfound' ) {
			$max_age  = $this->cache_control_is_future_now_maxtime( $max_age );
			$s_maxage = $this->cache_control_is_future_now_maxtime( $s_maxage );
		}

		if ( is_paged() && isset( $option['paged'] ) ) {
			$page_factor = intval( get_option( 'cache_control_' . $option['id'] . '_paged', $option['paged'] ) );
			$max_age     += $this->cache_control_stale_factorer( $page_factor, $max_age );
			$s_maxage    += $this->cache_control_stale_factorer( $page_factor, $s_maxage );
		}

		if ( $option_name == 'singles' && get_option( 'cache_control_singles_mmulti' ) == 1 ) {
			$date_now = new \DateTime();
			$date_mod = new \DateTime( get_the_modified_date( 'c' ) );

			$date_diff    = $date_now->diff( $date_mod );
			$months_stale = $date_diff->m + ( $date_diff->y * 12 );

			if ( $months_stale > 0 ) {
				$max_age  = intval( $max_age * ( ( $months_stale + 12 ) / 12 ) );
				$s_maxage = intval( $s_maxage * ( ( $months_stale + 12 ) / 12 ) );
			}
		}

		return $this->cache_control_build_directive_header( $max_age, $s_maxage );
	}

	private function cache_control_nocacheables() {
		global $wp_query;

		$noncacheable = (
			is_preview() ||
			is_user_logged_in() ||
			is_trackback() ||
			is_admin()
		);

		// Requires post password, and post has been unlocked.
		if ( ! $noncacheable &&
		     isset( $wp_query ) &&
		     isset( $wp_query->posts ) &&
		     count( $wp_query->posts ) >= 1 &&
		     ! empty( $wp_query->posts[0]->post_password ) &&
		     ! post_password_required() ) {
			$noncacheable = true;
		} // WooCommerce support
		elseif ( ! $noncacheable && function_exists( 'is_woocommerce' ) ) {
			$noncacheable = (
				is_cart() ||
				is_checkout() ||
				is_account_page() );
		}

		$noncacheable = apply_filters( 'cache_control_nocacheables', $noncacheable );

		return $noncacheable;
	}

	private function cache_control_select_directive() {

		if ( $this->cache_control_nocacheables() ) {
			return $this->cache_control_build_directive_header( false, false );
		} elseif ( is_feed() ) {
			return $this->cache_control_build_directive_from_option( 'feeds' );
		} elseif ( is_front_page() && ! is_paged() ) {
			return $this->cache_control_build_directive_from_option( 'front_page' );
		} elseif ( is_single() ) {
			return $this->cache_control_build_directive_from_option( 'singles' );
		} elseif ( is_page() ) {
			return $this->cache_control_build_directive_from_option( 'pages' );
		} elseif ( is_home() ) {
			return $this->cache_control_build_directive_from_option( 'home' );
		} elseif ( is_category() ) {
			return $this->cache_control_build_directive_from_option( 'categories' );
		} elseif ( is_tax() ) {
			return $this->cache_control_build_directive_from_option( 'taxs' );
		} elseif ( is_tag() ) {
			return $this->cache_control_build_directive_from_option( 'tags' );
		} elseif ( is_author() ) {
			return $this->cache_control_build_directive_from_option( 'authors' );
		} elseif ( is_attachment() ) {
			return $this->cache_control_build_directive_from_option( 'attachment' );
		} elseif ( is_search() ) {
			return $this->cache_control_build_directive_from_option( 'search' );
		} elseif ( is_404() ) {
			return $this->cache_control_build_directive_from_option( 'notfound' );
		} elseif ( is_date() ) {
			if ( ( is_year() && strcmp( get_the_time( 'Y' ), date( 'Y' ) ) < 0 ) ||
			     ( is_month() && strcmp( get_the_time( 'Y-m' ), date( 'Y-m' ) ) < 0 ) ||
			     ( ( is_day() || is_time() ) && strcmp( get_the_time( 'Y-m-d' ), date( 'Y-m-d' ) ) < 0 ) ) {
				return $this->cache_control_build_directive_from_option( 'dates' );
			} else {
				return $this->cache_control_build_directive_from_option( 'home' );
			}
		}

		// cache_control_handle_redirects() is handled at an earlier stage
		return $this->cache_control_build_directive_header( false, false );
	}

}
