<?php

namespace TbmCommon\System;

/**
 *  Amp Cache Update
 *
 *  Class to request updation of Google AMP Cache. Forked from https://github.com/Ennexa/composer-amp-update-cache
 *
 */

use GuzzleHttp\Client;

class AmpUpdate {
	private $guzzleClient = null;

	private static $arContentType = [
		'c' => 'Document',
		'i' => 'Image',
		'r' => 'Resource',
	];

	private $cacheList = null;

	public function __construct( $keyFilePath ) {

		if ( ! file_exists( $keyFilePath ) ) {
			throw new \Exception( "Private key not found" );
		}

		$this->keyFilePath = $keyFilePath;
	}


	/**
	 * Base64 Encode
	 *
	 * Creates url-safe base64 encoded string
	 *
	 * @param string $string String to encode
	 *
	 * @return string
	 */
	private static function base64encode( $string ) {
		return str_replace( [ '+', '/', '=' ], [ '-', '_', '' ], base64_encode( $string ) );
	}

	/**
	 * Get Caches
	 *
	 * Get a list of AMP Caches from cdn.ampproject.org
	 *
	 * @param void
	 *
	 * @return string
	 */
	private function getCaches() {
		if ( is_null( $this->cacheList ) ) {
			$response = wp_remote_get( 'https://cdn.ampproject.org/caches.json' );

			if ( is_array( $response ) && ! is_wp_error( $response ) ) {
				$body = $response['body']; // use the content
			}

			$data = json_decode( $body );
			if ( $data ) {
				$this->cacheList = $data->caches;
			}
		}

		return $this->cacheList;
	}

	/**
	 * Purge
	 *
	 * Request AMP CDNs to purge the cache for the specified url
	 *
	 * @param string $url Updated url
	 * @param char $contentType Content type to update
	 *
	 * @return boolean
	 */
	public function purge( $url, $contentType = 'c' ) {
		$timestamp = time();
		$out       = array();
		$info      = parse_url( $url );

		$host = strtr( $info['host'], '.', '-' );

		$url = "{$info['host']}{$info['path']}" . urlencode( isset( $info['query'] ) ? "?{$info['query']}" : '' );

		$ampCachePath = "/update-cache/$contentType/" . ( $info['scheme'] === 'https' ? 's/' : '' );
		$ampCachePath .= "{$url}?amp_action=flush&amp_ts={$timestamp}";

		$privateKey = openssl_pkey_get_private( 'file://' . $this->keyFilePath );
		openssl_sign( $ampCachePath, $signature, $privateKey, OPENSSL_ALGO_SHA256 );
		openssl_free_key( $privateKey );

		$signature = self::base64encode( $signature );
		$status    = true;

		foreach ( $this->getCaches() as $cache ) {
			$ampCacheBase = "https://$host.{$cache->updateCacheApiDomainSuffix}";

			$response = wp_remote_get( "{$ampCacheBase}{$ampCachePath}&amp_url_signature={$signature}" );

			if ( is_array( $response ) && ! is_wp_error( $response ) ) {
				$body              = $response['body']; // use the content
				$out[ $cache->id ] = $body;
			}


		}

		return implode( ',', $out );
	}

	/**
	 * Purge All
	 *
	 * Convenience method to purge multiple urls
	 *
	 * @param string|array $arFile Updated urls
	 * @param char $contentType Content type to update
	 *
	 * @return boolean
	 */
	public function purgeAll( $arFile = array(), $contentType = 'c' ) {
		$out = array();

		if ( ! is_array( $arFile ) ) {
			$arFile = (array) $arFile;
		}

		foreach ( $arFile as $url ) {
			$out[ $url ] = $this->purge( $url, $contentType );
		}

		return implode( ',', $out );
	}

	/**
	 * Get the url
	 *
	 * Get the url to purge
	 *
	 * @param string|array $arFile Updated urls
	 * @param char $contentType Content type to update
	 *
	 * @return boolean
	 */
	public function getGoogleUrl( $url, $contentType = 'c' ) {
		$timestamp = time();

		$info = parse_url( $url );

		$host = strtr( $info['host'], '.', '-' );

		$url = "{$info['host']}{$info['path']}" . urlencode( isset( $info['query'] ) ? "?{$info['query']}" : '' );

		$ampCachePath = "/update-cache/$contentType/" . ( $info['scheme'] === 'https' ? 's/' : '' );
		$ampCachePath .= "{$url}?amp_action=flush&amp_ts={$timestamp}";

		$privateKey = openssl_pkey_get_private( 'file://' . $this->keyFilePath );
		openssl_sign( $ampCachePath, $signature, $privateKey, OPENSSL_ALGO_SHA256 );
		openssl_free_key( $privateKey );

		$signature = self::base64encode( $signature );

		$ampCacheBase = "https://$host.cdn.ampproject.org";

		return "{$ampCacheBase}{$ampCachePath}&amp_url_signature={$signature}";
	}
}
