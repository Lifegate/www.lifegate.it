<?php

namespace TbmCommon\Taxonomies;

class Sponsor {

	public $name;
	public $url;
	public $image;
	public $imageId;
	public $type;

	public function __construct( $field = 'post_sponsor' ) {
		if ( is_single() || is_tax() ) {
			$this->getSponsor( get_queried_object(), $field );
		}

		return false;
	}

	public function setName( $name ) {
		$this->name = $name;
	}

	public function getName() {
		return $this->name;
	}

	public function setUrl( $url ) {
		$this->url = $url;
	}

	public function getUrl() {
		return $this->url;
	}

	public function setImage( $image ) {
		$this->image = $image;
	}

	public function getImage() {
		return $this->image;
	}

	public function setImageId( $id ) {
		$this->imageId = $id;
	}

	public function getImageId() {
		return $this->imageId;
	}

	public function setType( $type ) {
		$this->type = $type;
	}

	public function getType() {
		return $this->type;
	}

	private function setSponsor( $term ) {
		$term = get_term( $term, 'sponsor' );

		if ( empty( $term ) || is_wp_error( $term ) ) {
			return '';
		}

		$id = $term->taxonomy . '_' . $term->term_id;

		$sponsor_name   = get_field( 'sponsor_name', $id );
		$sponsor_img_id = get_field( 'sponsor_img', $id );
		$sponsor_url    = get_field( 'sponsor_landing', $id );
		$sponsor_type   = get_field( 'sponsor_type', $id  ) ?: 'Sponsor';

		/**
		 * Get the image link
		 */
		if ( $sponsor_img_id ) {
			$sponsor_img_url = wp_get_attachment_url( $sponsor_img_id );
		}

		/**
		 * Set the values
		 */
		$this->setImage( $sponsor_img_url );
		$this->setName( $sponsor_name );
		$this->setUrl( $sponsor_url );
		$this->setImageId( $sponsor_img_id );
		$this->setType( $sponsor_type );

		return true;
	}

	public function getSponsor( $object = 0, $acf_field = 'post_sponsor' ) {

		if ( is_tax() ) {
			$this->getTermSponsor( $object, $acf_field );
		}

		if ( is_single() ) {
			$this->getPostSponsor( $object, $acf_field );
		}

	}

	/**
	 * Get sponsor term used in post
	 *
	 * @param \WP_Post $post Post Object or post id
	 * @param string $acf_field Name of the field. Dafaults to post_sponsor
	 *
	 * @return bool
	 */
	public function getPostSponsor( $post, $acf_field = 'post_sponsor' ) {
		$post = get_post( $post );
		if ( empty( $post ) ) {
			return false;
		}

		/**
		 * Get the sponsor term
		 */
		$sponsor_term = get_field( $acf_field, $post->ID );

		if ( empty( $sponsor_term ) ) {
			return false;
		}

		if ( ! is_a( $sponsor_term, 'WP_Term' ) ) {
			$sponsor_term = get_term( $sponsor_term );
		}

		$this->setSponsor( $sponsor_term );

		return true;
	}

	/**
	 * Get sponsor used in term
	 *
	 * @param \WP_Term $term Term Object or term_id
	 * @param string $acf_field
	 *
	 * @return bool
	 */
	public function getTermSponsor( $term, $acf_field = 'tbm_special_sponsor' ) {

		$term = get_term( $term );
		if ( empty( $term ) || is_wp_error( $term ) ) {
			return false;
		}

		$id = $term->taxonomy . '_' . $term->term_id;

		/**
		 * Get the sponsor term
		 */
		$sponsor_term = get_field( $acf_field, $id );

		if ( ! $sponsor_term ) {
			return false;
		}

		if ( ! is_a( $sponsor_term, 'WP_Term' ) ) {
			$sponsor_term = get_term( $sponsor_term );
		}

		$this->setSponsor( $sponsor_term );

		return true;

	}

}
