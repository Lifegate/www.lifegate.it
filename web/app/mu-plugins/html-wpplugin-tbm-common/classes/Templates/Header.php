<?php

namespace TbmCommon\Templates;

class Header {
	public static function getPrefetch() {
		$out = array();

		$out[] = '<link rel="preconnect" href="https://img4.juiceadv.com/" crossorigin>';
		$out[] = '<link rel="dns-prefetch" href="https://img4.juiceadv.com/">';
		$out[] = '<link rel="preconnect" href="https://cdn.triboomedia.it/" crossorigin>';
		$out[] = '<link rel="dns-prefetch" href="https://cdn.triboomedia.it/">';

		return implode( '', $out );
	}
}
