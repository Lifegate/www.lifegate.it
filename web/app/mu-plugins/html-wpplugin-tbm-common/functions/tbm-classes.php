<?php
/**
 * Created by PhpStorm.
 * User: francesco
 * Date: 19/02/2018
 * Time: 21:57
 */

namespace Tbm_Plugins;

use phpDocumentor\Reflection\DocBlock\Serializer;

class TBM_Privacy_Policy {

	public function __construct() {

		if ( 'lifegate' === tbm_get_domain() ) {
			return;
		}

		$cookie_policy  = get_page_by_title( 'Cookie policy' );
		$privacy_policy = get_page_by_title( 'Privacy policy' );
		$form_policy    = get_page_by_title( 'Policy Contatti' );
		$out            = array();

		if ( ! $cookie_policy ) {
			$out[] = '<li>Per abilitare la cookie policy devi creare una pagina intitolata esattamente \'Cookie policy\'</li>';
		}

		if ( ! $privacy_policy ) {
			$out[] = '<li>Per abilitare la privacy policy principale devi creare una pagina intitolata esattamente \'Privacy policy\'</li>';
		}

		if ( ! $form_policy ) {
			$out[] = '<li>Per abilitare la privacy policy principale devi creare una pagina intitolata esattamente \'Policy Contatti\'</li>';
		}

		add_action( 'admin_notices', function () use ( $out ) {
			if ( ! empty( $out ) ) :
				?>
                <div class="notice notice-warning">
                    <ul><?php echo implode( '', $out ); ?></ul>
                </div>
			<?php
			endif;
		} );

		add_filter( 'the_content', function ( $content ) {
			if ( is_page( 'Cookie policy' ) ) {
				return $this->filter_content( 'cookie-policy' );
			}

			if ( is_page( 'Privacy policy' ) ) {
				return $this->filter_content( 'privacy-policy' );
			}

			if ( is_page( 'Policy Contatti' ) ) {
				return $this->filter_content( 'form-policy' );
			}

			return $content;
		} );

	}

	/**
	 * Get the content and replace the formats
	 *
	 * @param $content
	 *
	 * @return false|string
	 */

	public static function filter_content( $content ) {
		return '<iframe src="' . add_query_arg( array(
				'action'          => 'tbm_policy',
				'tbm-policy'      => $content,
				'tbm-policy-site' => tbm_get_domain()
			), admin_url( 'admin-ajax.php' ) ) . '" onload=\'javascript:(function(o){o.style.height=o.contentWindow.document.body.scrollHeight+20+"px";o.style.width=o.contentWindow.document.body.scrollWidth+"px";}(this));\' style="height:600px;width:100%;border:none;overflow:hidden;"></iframe>';

	}

}

class TBM_tms {
	/**
	 * Stampa il datalayer. Va incluso prima di GTM e prima di Ensighten
	 */
	public static function include_dataLayer() {
		if ( defined( 'SITE_NAME' ) && SITE_NAME == 'agrodolce' ) {
			require_once HTML_TBM_COMMON_PLUGIN_DIR . 'inc/datalayer-agrodolce.php';
		} else {
			require_once HTML_TBM_COMMON_PLUGIN_DIR . 'inc/datalayer.php';
		}
		$dl = isset( $dataLayer ) ? sprintf( "dataLayer = [%s];", json_encode( $dataLayer ) ) : '';
		echo "<script>{$dl}</script>";
	}

	/**
	 * Include lo snippet JavaScript del Google Tag Manager
	 * Va incluso in <head> DOPO il datalayer
	 *
	 * @param string $id L'ID univoco del container fornito dal Google Tag Manager
	 */
	public static function gtm_script_tag( $config = '' ) {
		if ( ! $config ) {
			return '';
		}

		if ( isset( $_GET['critical'] ) && $_GET['critical'] === '1' ) {
			return '';
		}

		echo "<!-- @formatter:off --><!-- Google Tag Manager main --><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-{$config}');</script><!-- End Google Tag Manager --><!-- @formatter:on -->";
	}

	/**
	 * Include l'iframe del Google Tag Manager nel caso il device non supportasse JavaScript
	 * Va incluso subito al di sotto del tag <body>
	 *
	 * @param string $id L'ID univoco del container fornito dal Google Tag Manager
	 */
	public static function gtm_noscript_tag( $config = '' ) {
		if ( ! $config ) {
			return '';
		}

		if ( isset( $_GET['critical'] ) && $_GET['critical'] === '1' ) {
			return '';
		}

		echo "<!-- Google Tag Manager main --><noscript><iframe src='https://www.googletagmanager.com/ns.html?id=GTM-{$config}' height='0' width='0' style='display:none;visibility:hidden'></iframe></noscript><!-- End Google Tag Manager -->";
	}

	/**
	 * Include lo snippet di Ensighten. Va incluso subito al di sotto del Datalayer
	 *
	 * @param string $folder Il nome del folder da pubblicare
	 */
	public static function tealium_tag( $config = '' ) {

	    if (isset( $_GET['critical'] )  && $_GET['critical'] === '1') {
	    	return '';
	    }

        /**
         * If true, disable tms
         */
		$disabled = apply_filters( 'tbm_disable_tms', __return_false());

	    if ($disabled)  {
		    return '';
        }

		if ( ! $config ) {
			return '';
		}

        $tag = "<!-- TMS --><script async type=\"text/javascript\" src=\"//tms.triboomedia.it/utag/triboo/{$config}/prod/utag.js\"></script><!-- fine TMS-->";

		echo $tag;
	}

	/**
	 * Include lo snippet di Atatus.
	 *
	 * @param string $folder Il nome del folder da pubblicare
	 */
	public static function atatus_tag( $config = '' ) {
		if ( ! $config ) {
			return '';
		}
		echo "<!-- Atatus --><script>var customData = {pagePostType: dataLayer[0].pagePostType,wpVersion: dataLayer[0].wpVer,themeVersion: dataLayer[0].themeVer};
			if(Math.random()<0.033){var e = document.getElementsByTagName('script')[0];var s = document.createElement(\"script\");s.src=\"//dmc1acwvwny3.cloudfront.net/atatus.js\";s.onload=function(){atatus.config('{$config}',{anonymizeIp:true,version: dataLayer[0].themeVer,customData: customData}).install();};e.parentNode.insertBefore(s, e);}</script><!-- Atatus -->";
	}

	/**
	 * Print the javascript snippet to disable banner
	 */
	public static function print_active_zones() {
	    /**
	     * ACTIVE_ZONE is a static zone defined in advertising plugin
	     */
		$active_zones = tbm_get_the_banner( 'ACTIVE_ZONES', ' ', ' ', false, true );
		if ($active_zones) {
		    echo '<script>window.tribooAdv = window.tribooAdv || []; window.tribooAdv.push({"activeZones": '.$active_zones.'});</script>';
		}
	}

	/**
	 * Print the javascript snippet to disable banner
	 */
	public static function disable_banner() {
		echo '<script>window.tribooAdv = window.tribooAdv || []; window.tribooAdv.push({"advDisabled": true});</script>';
	}

	/**
	 * Print the javascript snippet to disable banner
	 */
	public static function disable_viewmax() {
		echo '<script>window.tribooAdv = window.tribooAdv || []; window.tribooAdv.push({"viewmaxDisabled": true});</script>';
	}

}

class TBM_News_Sitemap {

	/**
	 * Options array
	 */
	private $options = array();

	/**
	 * Post Type Enabled
	 */
	private $enabled_post_type = array();

	/**
	 * Class constructor
	 */
	public function __construct() {
		$this->enabled_post_type = get_field( 'tbm_common_news_sitemap_post_type', 'option' ) ?: array();

		add_action( 'init', array( $this, 'init' ), 10 );
		add_action( 'wpseo_head', array( $this, 'head' ) );
		add_filter( 'wpseo_sitemap_index', array( $this, 'add_to_index' ) );

	}

	public function my_wpseo_stylesheet_url( $s ) {
		return '';

		return str_replace( 'main-sitemap.xsl', 'my-sitemap.xsl', $s );
	}

	/**
	 * Register the XML News sitemap with the main sitemap class.
	 */
	public function init() {

		if ( isset( $GLOBALS['wpseo_sitemaps'] ) && method_exists( $GLOBALS['wpseo_sitemaps'], 'register_sitemap' ) ) {
			$GLOBALS['wpseo_sitemaps']->register_sitemap( 'google_news', array( $this, 'build_news_sitemap' ) );

			add_filter( 'wpseo_stylesheet_url', array( $this, 'my_wpseo_stylesheet_url' ) );

		}
	}

	/**
	 * Add the XML News Sitemap to the Sitemap Index.
	 *
	 * @param string $str String with Index sitemap content.
	 *
	 * @return string
	 */
	function add_to_index( $str ) {
		$result = strtotime( get_lastpostmodified( 'gmt', $this->enabled_post_type ) );
		$date   = date( 'c', $result );

		$str .= '<sitemap>' . "\n";
		$str .= '<loc>' . home_url( 'google_news-sitemap.xml' ) . '</loc>' . "\n";
		$str .= '<lastmod>' . $date . '</lastmod>' . "\n";
		$str .= '</sitemap>' . "\n";

		return $str;
	}

	/**
	 * Build the sitemap and push it to the XML Sitemaps Class instance for display.
	 */
	public function build_news_sitemap() {
		global $wpdb;

		// Query when Polylang is active
		if (function_exists('pll_current_language')) {

			$lang = function_exists('pll_current_language') ? pll_current_language() : '';
			$id = get_term_by( 'slug', $lang, 'language'  )->term_id;

			$items  = $wpdb->get_results(
				$wpdb->prepare(
					"SELECT ID, post_content, post_name, post_author, post_parent, post_date_gmt, post_date, post_date_gmt, post_title, post_type
				FROM {$wpdb->posts}
				INNER JOIN wp_term_relationships AS pll_tr
                ON pll_tr.object_id = wp_posts.ID
				WHERE post_status='publish'
					AND ( TIMESTAMPDIFF( MINUTE, post_date_gmt, UTC_TIMESTAMP() ) <= ( 48 * 60 ) )
					AND post_type IN ('%s')
					AND pll_tr.term_taxonomy_id = '%s'
				ORDER BY post_date_gmt DESC
				LIMIT 0, 100", $this->enabled_post_type, $id
				)
			);
        }

		if (!function_exists('pll_current_language')) {
			$items  = $wpdb->get_results(
				$wpdb->prepare(
					"SELECT ID, post_content, post_name, post_author, post_parent, post_date_gmt, post_date, post_date_gmt, post_title, post_type
				FROM {$wpdb->posts}
				WHERE post_status='publish'
					AND ( TIMESTAMPDIFF( MINUTE, post_date_gmt, UTC_TIMESTAMP() ) <= ( 48 * 60 ) )
					AND post_type IN ('%s')
				ORDER BY post_date_gmt DESC
				LIMIT 0, 100", $this->enabled_post_type
				)
			);
        }

		$output = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
		xmlns:news="http://www.google.com/schemas/sitemap-news/0.9"
		xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">' . "\n";

//		echo '<!--'.print_r($items,1).'-->';

		if ( ! empty( $items ) ) {
			foreach ( $items as $item ) {
				$item->post_status = 'publish';
				$print_images      = false;


				$publication_name = ! empty( $this->options['newssitemapname'] ) ? $this->options['newssitemapname'] : get_bloginfo( 'name' );
				$publication_lang = function_exists('pll_get_post_language') ? pll_get_post_language($item->ID) : substr( get_locale(), 0, 2 );

				$output .= '<url>' . "\n";
				$output .= "\t<loc>" . get_permalink( $item ) . '</loc>' . "\n";
				$output .= "\t<news:news>\n";
				$output .= "\t\t<news:publication>" . "\n";
				$output .= "\t\t\t<news:name>" . htmlspecialchars( $publication_name ) . '</news:name>' . "\n";
				$output .= "\t\t\t<news:language>" . htmlspecialchars( $publication_lang ) . '</news:language>' . "\n";
				$output .= "\t\t</news:publication>\n";
				$output .= "\t\t<news:publication_date>" . mysql2date( 'c', $item->post_date_gmt ) . '</news:publication_date>' . "\n";
				$output .= "\t\t<news:title>" . htmlspecialchars( $item->post_title ) . '</news:title>' . "\n";
				$output .= "\t</news:news>\n";


				/**
				 * Here we should check if we want to show images in post.
				 * For now, we disable it
				 */
				if ( $print_images == 'ok' ) {
					$images = array();
					if ( preg_match_all( '/<img [^>]+>/', $item->post_content, $matches ) ) {
						foreach ( $matches[0] as $img ) {
							if ( preg_match( '/src=("|\')([^"|\']+)("|\')/', $img, $match ) ) {
								$src = $match[2];
								if ( strpos( $src, 'http' ) !== 0 ) {
									if ( $src[0] != '/' ) {
										continue;
									}
									$src = get_bloginfo( 'url' ) . $src;
								}

								if ( $src != esc_url( $src ) ) {
									continue;
								}

								if ( isset( $url['images'][ $src ] ) ) {
									continue;
								}

								$image = array();
								if ( preg_match( '/title=("|\')([^"\']+)("|\')/', $img, $match ) ) {
									$image['title'] = str_replace( array( '-', '_' ), ' ', $match[2] );
								}

								if ( preg_match( '/alt=("|\')([^"\']+)("|\')/', $img, $match ) ) {
									$image['alt'] = str_replace( array( '-', '_' ), ' ', $match[2] );
								}

								$images[ $src ] = $image;
							}
						}
					}

					// Also check if the featured image value is set.
					$post_thumbnail_id = get_post_thumbnail_id( $item->ID );

					if ( $post_thumbnail_id ):
						$post_thumbnail_url            = wp_get_attachment_url( $post_thumbnail_id );
						$images[ $post_thumbnail_url ] = $item->post_title;
					endif;

					if ( isset( $images ) && count( $images ) > 0 ) {
						foreach ( $images as $src => $img ) {
							$output .= "\t\t<image:image>\n";
							$output .= "\t\t\t<image:loc>" . htmlspecialchars( $src ) . "</image:loc>\n";
							if ( isset( $img['title'] ) ) {
								$output .= "\t\t\t<image:title>" . htmlspecialchars( $img['title'] ) . "</image:title>\n";
							}
							if ( isset( $img['alt'] ) ) {
								$output .= "\t\t\t<image:caption>" . htmlspecialchars( $img['alt'] ) . "</image:caption>\n";
							}
							$output .= "\t\t</image:image>\n";
						}
					}
				}
				$output .= '</url>' . "\n";
			}

		}

		$output .= '</urlset>';
		$GLOBALS['wpseo_sitemaps']->set_sitemap( $output );

	}

	/**
	 * Display the optional sources link elements in the <code>&lt;head&gt;</code>.
	 */
	public function head() {

	}
}

// Credits: https://github.com/TimeIncUK/flipboard-rss-feed
class Flipboard_RSS_Feed {

	/**
	 * Plugin version, used for cache-busting of style and script file references.
	 *
	 * @since   1.0.0
	 *
	 * @var     string
	 */
	const VERSION = '1.1.0';

	/**
	 * Instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @var      object
	 */
	protected static $instance = null;

	/**
	 * Array of sizes
	 *
	 * @since    1.0.0
	 *
	 * @var      array
	 */
	protected $image_sizes = array();

	/**
	 * Initialize the plugin by setting localization and loading public scripts
	 * and styles.
	 *
	 * @since     1.0.0
	 */
	public function __construct() {

	    // Define SITE_FEATURED_CUSTOM_FIELD
		defined( 'SITE_FEATURED_CUSTOM_FIELD' ) or define( 'SITE_FEATURED_CUSTOM_FIELD', '_featured' );

		if ( ! $this->mrss_is_enabled() ) {
			return;
		}

		// Set number
		add_filter( 'option_posts_per_rss', array( $this, 'option_posts_per_rss' ), 15, 1 );

		if ( $this->show_only_featured() ) {
			add_action( 'pre_get_posts', array( $this, 'filter_featured' ) );
		}

		if ( $this->show_only_to_push() ) {
			add_action( 'pre_get_posts', array( $this, 'filter_to_push' ) );
		}

		if ( $this->show_only_to_twitter() ) {
			add_action( 'pre_get_posts', array( $this, 'filter_to_twitter' ) );
		}

		if ( $this->show_only_to_facebook() ) {
			add_action( 'pre_get_posts', array( $this, 'filter_to_facebook' ) );
		}

		if ( $this->show_only_to_linkedin() ) {
			add_action( 'pre_get_posts', array( $this, 'filter_to_linkedin' ) );
		}

		add_filter( 'option_rss_use_excerpt', array( $this, 'option_rss_use_excerpt' ), 15, 1 );

		add_action( 'template_redirect', array( $this, 'template_redirect' ) );

	}

	/**
	 * Show only featured posts
	 *
	 * @param $query
	 */
	public function filter_featured( $query ) {
		if ( ! is_admin() && $query->is_main_query() && $query->is_feed() ) {
			$query->set( 'meta_key', SITE_FEATURED_CUSTOM_FIELD );
			$query->set( 'meta_value', '1' );
			return;
		}
	}

	/**
	 * Show only posts to push
	 *
	 * @param $query
	 */
	public function filter_to_push( $query ) {
		if ( ! is_admin() && $query->is_main_query() && $query->is_feed() ) {
			$args = array(
				'relation' => 'AND',
				'tbm_pushed_date' => array(
					'key' => 'tbm_pushed_date',
				),
				'tbm_pushed' => array(
					'key' => 'tbm_pushed',
					'value' => '1',
				),
			);
			$query->set( 'meta_query', $args );
			$query->set( 'orderby', 'tbm_pushed_date' );
			return;
		}
	}

	/**
	 * Show only posts to notify on twitter
	 *
	 * @param $query
	 */
	public function filter_to_twitter( $query ) {
		if ( ! is_admin() && $query->is_main_query() && $query->is_feed() ) {
			$args = array(
				'relation' => 'AND',
				'tbm_twittered_date' => array(
					'key' => 'tbm_twittered_date',
				),
				'tbm_twittered' => array(
					'key' => 'tbm_twittered',
					'value' => '1',
				),
			);
			$query->set( 'meta_query', $args );
			$query->set( 'orderby', 'tbm_twittered_date' );
			return;
		}
	}

	/**
	 * Show only posts to post on Facebook
	 *
	 * @param $query
	 */
	public function filter_to_facebook( $query ) {
		if ( ! is_admin() && $query->is_main_query() && $query->is_feed() ) {
			$args = array(
				'relation' => 'AND',
				'tbm_facebooked_date' => array(
					'key' => 'tbm_facebooked_date',
				),
				'tbm_facebooked' => array(
					'key' => 'tbm_facebooked',
					'value' => '1',
				),
			);
			$query->set( 'meta_query', $args );
			$query->set( 'orderby', 'tbm_facebooked_date' );
			return;
		}
	}

	/**
	 * Show only posts to post on Linkedin
	 *
	 * @param $query
	 */
	public function filter_to_linkedin( $query ) {
		if ( ! is_admin() && $query->is_main_query() && $query->is_feed() ) {
			$args = array(
				'relation' => 'AND',
				'tbm_linkedined_date' => array(
					'key' => 'tbm_linkedined_date',
				),
				'tbm_linkedined' => array(
					'key' => 'tbm_linkedined',
					'value' => '1',
				),
			);
			$query->set( 'meta_query', $args );
			$query->set( 'orderby', 'tbm_linkedined_date' );
			return;
		}
	}

	/**
	 * Load these filters after pre post has run.
	 */
	public function template_redirect() {

		if ( ! is_feed() ) {
			return;
		}

		$this->set_image_size();
		//  no large enough image sizes, lets quit
		if ( count( $this->get_image_sizes() ) == 0 ) {
			return;
		}

		add_action( 'rss2_ns', array( $this, 'mrss_ns' ) );
		add_action( 'rss2_item', array( $this, 'mrss_item' ), 10, 0 );

		//Add an additional filter to handle images in the post content
		add_filter( 'img_caption_shortcode', array( $this, 'flipboard_caption' ), 10, 3 );
		add_filter( 'the_content', array( $this, 'cleanup_feed_of_tags' ), 5 );

		//we run this after auto embeds so we can remove scripts like twitter
		add_filter( 'the_content', array( $this, 'remove_script_style_tags' ), 11 );

		// Send no-cache header
		add_filter( 'wp_headers', function ( $headers ) {
			$headers['Cache-Control'] = 'no-cache, no-store, must-revalidate';
		} );

	}

	/**
	 * Returns whether this plugin should be enabled.
	 * This function is filterable - flipboard_rss_feed_enabled
	 *
	 * @return boolean true | false
	 * @since    1.0.0
	 */
	public function mrss_is_enabled() {
		return apply_filters( 'flipboard_rss_feed_enabled', ( ( isset( $_GET['mrss'] ) && $_GET['mrss'] == '1' ) || ( isset( $_GET['push'] ) && $_GET['push'] == '1' ) ) );
	}

	/**
	 * Returns whether should show only featured posts
	 * This function is filterable - flipboard_rss_feed_enabled
	 *
	 * @return boolean true | false
	 * @since    1.0.0
	 */
	public function show_only_featured() {
		return apply_filters( 'flipboard_show_only_featured', ( ( isset( $_GET['featured'] ) && $_GET['featured'] == '1' ) ) );
	}

	/**
	 * Returns whether should show only posts with push notification enabled
	 * This function is filterable - flipboard_show_only_to_push
	 *
	 * @return boolean true | false
	 * @since    1.0.0
	 */
	public function show_only_to_push() {
		return apply_filters( 'flipboard_show_only_to_push', ( ( isset( $_GET['push'] ) && $_GET['push'] === '1' ) ) );
	}

	/**
	 * Returns whether should show only posts with twitter notification enabled
	 * This function is filterable - flipboard_show_only_to_twitter
	 *
	 * @return boolean true | false
	 * @since    1.0.0
	 */
	public function show_only_to_twitter() {
		return apply_filters( 'flipboard_show_only_to_twitter', ( ( isset( $_GET['twitter'] ) && $_GET['twitter'] === '1' ) ) );
	}

	/**
	 * Returns whether should show only posts with facebook notification enabled
	 * This function is filterable - flipboard_show_only_to_facebook
	 *
	 * @return boolean true | false
	 * @since    1.0.0
	 */
	public function show_only_to_facebook() {
		return apply_filters( 'flipboard_show_only_to_facebook', ( ( isset( $_GET['facebook'] ) && $_GET['facebook'] === '1' ) ) );
	}

	/**
	 * Returns whether should show only posts with linkedin notification enabled
	 * This function is filterable - flipboard_show_only_to_linkedin
	 *
	 * @return boolean true | false
	 * @since    1.0.0
	 */
	public function show_only_to_linkedin() {
		return apply_filters( 'flipboard_show_only_to_linkedin', ( ( isset( $_GET['linkedin'] ) && $_GET['linkedin'] === '1' ) ) );
	}

	/**
	 * Return the images sizes array.
	 *
	 * @return    image sizes variable.
	 * @since    1.0.0
	 *
	 */
	public function get_image_sizes() {
		return apply_filters( 'flipboard_rss_feed_image_sizes', $this->image_sizes );
	}

	/**
	 * Return an instance of this class.
	 *
	 * @return    object    A single instance of this class.
	 * @since     1.0.0
	 *
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Set the image sizes. Only get image sizes for image size that have width more than 400px
	 *
	 * @author   Jonathan Harris
	 * @since    1.0.0
	 */
	protected function set_image_size() {
		global $_wp_additional_image_sizes;

		$image_width = apply_filters( 'flipboard_rss_feed_image_width', 400 );

		foreach ( get_intermediate_image_sizes() as $s ) {
			$size = 0;
			if ( in_array( $s, array( 'thumbnail', 'medium', 'large' ) ) ) {
				$size = get_option( $s . '_size_w' );
			} else {
				if ( isset( $_wp_additional_image_sizes ) && isset( $_wp_additional_image_sizes[ $s ] ) ) {
					$size = $_wp_additional_image_sizes[ $s ]['width'];
				}
			}

			if ( $size >= $image_width ) {
				$this->image_sizes[] = $s;
			}
		}


	}

	/**
	 * Force show full content in RSS feed.
	 *
	 * @author   Jonathan Harris
	 * @since    1.0.0
	 */
	public function option_rss_use_excerpt( $value ) {
		return 0;
	}

	/**
	 * Force show 30 items in the feed.
	 *
	 * @author   Jonathan Harris
	 * @since    1.0.0
	 */
	public function option_posts_per_rss( $value ) {
		return apply_filters( 'flipboard_rss_feed_per_rss', 10 );
	}

	/**
	 * Gets the image caption
	 *
	 * @author   Jonathan Harris
	 * @since    1.0.0
	 */
	public function flipboard_figure( $attachment_id ) {

		if ( empty( $attachment_id ) || ! wp_attachment_is_image( $attachment_id ) ) {
			return '';
		}

		$attachment  = get_post( $attachment_id );
		$description = get_post_field( 'post_content', $attachment );
		$caption     = get_post_field( 'post_excerpt', $attachment );
		$alt         = get_post_meta( $attachment_id, '_wp_attachment_image_alt', true );

		if ( $description ) {
			$fig_caption = $description;
		} else if ( $caption ) {
			$fig_caption = $caption;
		} else if ( $alt ) {
			$fig_caption = $alt;
		} else {
			$fig_caption = '';
		}

		return $fig_caption;
	}

	/**
	 * Add extra fields to header of RSS to make RSS feed valid
	 *
	 * @author   Jonathan Harris
	 * @since    1.0.0
	 */
	public function mrss_ns() {
		echo 'xmlns:media="http://search.yahoo.com/mrss/"
        xmlns:georss="http://www.georss.org/georss"';
	}

	/**
	 * Add mrss and georss fields to rss item
	 *
	 * @author   Jonathan Harris
	 * @since    1.0.0
	 */
	public function mrss_item() {

		// GEO values are be set by core
		$geo_latitude  = get_post_meta( get_the_ID(), 'geo_latitude', true );
		$geo_longitude = get_post_meta( get_the_ID(), 'geo_longitude', true );

		// If geo latitude / geo_longitude are set, show georss
		if ( $geo_latitude && $geo_longitude ) {
			echo "<georss:point>$geo_latitude $geo_longitude</georss:point>";
		}

		$post_thumbnail_id = apply_filters( 'flipboard_post_thumbnail_id', get_post_thumbnail_id() );

		$post_thumbnail_alt = htmlspecialchars( trim( strip_tags( $this->flipboard_figure( $post_thumbnail_id ) ) ) );
		$format             = '<media:content type="%1$s" medium="image" width="%2$s" height="%3$s"  url="%4$s"><media:description type="plain">%5$s</media:description></media:content>';
		$format_enclosure   = '<enclosure url="%1$s" type="%2$s" length="0" />';
		$image_enclosure    = '';

		$media   = "";
		$counter = 0;

		if ( ! empty( $post_thumbnail_id ) && wp_attachment_is_image( $post_thumbnail_id ) ) {
			$media = '';

			$mimeType = get_post_mime_type( $post_thumbnail_id );

			/*
			 * Set enclosure only in push notification feed
			 */
			if ( $this->show_only_to_push() ) {
				$image_enclosure = sprintf( $format_enclosure, tbm_wp_get_attachment_image_url($post_thumbnail_id,array(1080,540)),$mimeType );
			}



			$existing_images = array();
			foreach ( $this->get_image_sizes() as $size ) {
				$image_attributes = wp_get_attachment_image_src( $post_thumbnail_id, $size );

				// Don't show the same image more than once
				if ( ! in_array( $image_attributes[0], $existing_images ) ) {
					$media    .= apply_filters( 'flipboard_media_single_item', sprintf( $format, $mimeType, $image_attributes[1], $image_attributes[2], $image_attributes[0], $post_thumbnail_alt ), $post_thumbnail_id );
					$counter ++;
				}
				$existing_images[] = $image_attributes[0];
			}
		}
		$media_display = apply_filters( 'flipboard_media_element', $media );

		if ( $media_display !== $media ) {
			$counter ++;
		}

		switch ( $counter ) {
			case 0:
				$output = '';
				break;
			case 1:
				$output = $media_display;
				break;
			default:
				$output = "<media:group>" . $media_display . "</media:group>";
				break;
		}
		$output .= $image_enclosure;

		echo $output;

	}

	/**
	 * Use WPs code to convert captions into HTML 5 markup in case the theme doesn't support it already.
	 *
	 * @author   Simon McWhinnie
	 * @since    1.0.7
	 */
	function flipboard_caption( $output, $attr, $content ) {
		$atts = shortcode_atts( array(
			'id'      => '',
			'align'   => 'alignnone',
			'width'   => '',
			'caption' => '',
			'class'   => '',
		), $attr, 'caption' );

		$atts['width'] = (int) $atts['width'];
		if ( $atts['width'] < 1 || empty( $atts['caption'] ) ) {
			return $content;
		}

		if ( ! empty( $atts['id'] ) ) {
			$atts['id'] = 'id="' . esc_attr( $atts['id'] ) . '" ';
		}

		$class = trim( 'wp-caption ' . $atts['align'] . ' ' . $atts['class'] );

		return '<figure ' . $atts['id'] . 'style="width: ' . (int) $atts['width'] . 'px;" class="' . esc_attr( $class ) . '">'
		       . do_shortcode( $content ) . '<figcaption class="wp-caption-text">' . $atts['caption'] . '</figcaption></figure>';

	}

	/**
	 * Clean up feed, removing empty html tags and script/style tags
	 *
	 * @author Simon McWhinnie
	 * @since  1.0.7
	 */
	function cleanup_feed_of_tags( $string ) {
		// Return if string not given or empty
		if ( ! is_string( $string ) || trim( $string ) == '' ) {
			return $string;
		}

		// Recursive empty HTML tags
		$string = preg_replace(
			'/<(\w+)\b(?:\s+[\w\-.:]+(?:\s*=\s*(?:"[^"]*"|"[^"]*"|[\w\-.:]+))?)*\s*\/?>\s*<\/\1\s*>/',
			'',
			$string
		);

		return $string;
	}

	/**
	 * Remove script and style tags
	 *
	 * @author Simon McWhinnie
	 * @since  1.0.7
	 */
	function remove_script_style_tags( $string ) {
		// Return if string not given or empty
		if ( ! is_string( $string ) || trim( $string ) == '' ) {
			return $string;
		}

		//remove all <script> and <style> tags
		$string = preg_replace(
			'/<(style|script).*>.*<\/(style|script)>/',
			'',
			$string
		);

		return $string;
	}

}

// Credits: https://it.wordpress.org/plugins/enable-svg/
class TBM_SVG_Uploader {

	var $mimes = array(
		'svg'  => 'image/svg+xml',
		'svgz' => 'image/svg+xml',
	);
	var $svg_strings = array(
		'<svg',
		'"http://www.w3.org/2000/svg"',
	);

	function __construct() {
		add_filter( 'upload_mimes', array( @$this, 'upload_mimes' ), 10, 1 );
		add_filter( 'wp_check_filetype_and_ext', array( @$this, 'wp_check_filetype_and_ext' ), 10, 3 );
		add_filter( 'wp_get_attachment_metadata', array( @$this, 'wp_get_attachment_metadata' ), 10, 2 );
	}

	// Add SVG MIME types to allowed uploads
	function upload_mimes( $mimes ) {
		return $mimes + $this->mimes;
	}

	// Modify WP 4.7 upload validation to handle SVG
	function wp_check_filetype_and_ext( $wp_check_filetype_and_ext, $file, $filename ) {
		extract( $wp_check_filetype_and_ext );

		// Get passed file's extension
		$file_ext = pathinfo( $filename, PATHINFO_EXTENSION );

		// Check if the default WP validator returned false but the file appears to be an SVG
		if (
			$type == false &&
			$ext == false &&
			in_array( $file_ext, array_keys( $this->mimes ) ) &&
			file_exists( $file )
		) {

			// Read file contents and confirm valid XML
			$file_contents = file_get_contents( $file );
			$valid_xml     = simplexml_load_string( $file_contents );

			// Check file contents for required SVG strings
			$valid_svg = ( $valid_xml != false );
			if ( $valid_svg ) {
				foreach ( $this->svg_strings as $svg_string ) {
					if ( strpos( $file_contents, $svg_string ) === false ) {
						$valid_svg = false;
						break;
					}
				}
			}

			// Valid SVG
			if ( $valid_svg ) {

				// Set type and extension for SVG
				$ext  = $file_ext;
				$type = $this->mimes[ $ext ];

				// Set proper filename
				// Modified from wp-includes/functions.php lines 2307-2314
				$filename_parts = explode( '.', $filename );
				array_pop( $filename_parts );
				$filename_parts[] = $ext;
				$proper_filename  = implode( '.', $filename_parts );
			} // Invalid SVG
			else {
				$ext = $type = $proper_filename = false;
			}

			// Update return values
			$wp_check_filetype_and_ext = compact( 'ext', 'type', 'proper_filename' );
		}

		return $wp_check_filetype_and_ext;
	}

	// Add minimal amount of file meta data to get Media Library to display SVGs in grid
	function wp_get_attachment_metadata( $data, $post_id ) {
		if ( ! $data ) {
			$data = array(
				'sizes' => array(
					'large' => array(
						'file' => pathinfo( wp_get_attachment_url( $post_id ), PATHINFO_BASENAME )
					)
				)
			);
		}

		return $data;
	}
}

/**
 * WP Async Request
 *
 * @package WP-Background-Processing
 */
if ( ! class_exists( 'WP_Async_Request' ) ) {

	/**
	 * Abstract WP_Async_Request class.
	 *
	 * @abstract
	 */
	abstract class WP_Async_Request {

		/**
		 * Prefix
		 *
		 * (default value: 'wp')
		 *
		 * @var string
		 * @access protected
		 */
		protected $prefix = 'wp';

		/**
		 * Action
		 *
		 * (default value: 'async_request')
		 *
		 * @var string
		 * @access protected
		 */
		protected $action = 'async_request';

		/**
		 * Identifier
		 *
		 * @var mixed
		 * @access protected
		 */
		protected $identifier;

		/**
		 * Data
		 *
		 * (default value: array())
		 *
		 * @var array
		 * @access protected
		 */
		protected $data = array();

		/**
		 * Initiate new async request
		 */
		public function __construct() {
			$this->identifier = $this->prefix . '_' . $this->action;

			add_action( 'wp_ajax_' . $this->identifier, array( $this, 'maybe_handle' ) );
			add_action( 'wp_ajax_nopriv_' . $this->identifier, array( $this, 'maybe_handle' ) );
		}

		/**
		 * Set data used during the request
		 *
		 * @param array $data Data.
		 *
		 * @return $this
		 */
		public function data( $data ) {
			$this->data = $data;

			return $this;
		}

		/**
		 * Dispatch the async request
		 *
		 * @return array|WP_Error
		 */
		public function dispatch() {
			$url  = add_query_arg( $this->get_query_args(), $this->get_query_url() );
			$args = $this->get_post_args();

			return wp_remote_post( esc_url_raw( $url ), $args );
		}

		/**
		 * Get query args
		 *
		 * @return array
		 */
		protected function get_query_args() {
			if ( property_exists( $this, 'query_args' ) ) {
				return $this->query_args;
			}

			return array(
				'action' => $this->identifier,
				'nonce'  => wp_create_nonce( $this->identifier ),
			);
		}

		/**
		 * Get query URL
		 *
		 * @return string
		 */
		protected function get_query_url() {
			if ( property_exists( $this, 'query_url' ) ) {
				return $this->query_url;
			}

			return admin_url( 'admin-ajax.php' );
		}

		/**
		 * Get post args
		 *
		 * @return array
		 */
		protected function get_post_args() {
			if ( property_exists( $this, 'post_args' ) ) {
				return $this->post_args;
			}

			return array(
				'timeout'   => 0.01,
				'blocking'  => false,
				'body'      => $this->data,
				'cookies'   => $_COOKIE,
				'sslverify' => apply_filters( 'https_local_ssl_verify', false ),
			);
		}

		/**
		 * Maybe handle
		 *
		 * Check for correct nonce and pass to handler.
		 */
		public function maybe_handle() {
			// Don't lock up other requests while processing
			session_write_close();

			check_ajax_referer( $this->identifier, 'nonce' );

			$this->handle();

			wp_die();
		}

		/**
		 * Handle
		 *
		 * Override this method to perform any actions required
		 * during the async request.
		 */
		abstract protected function handle();

	}
}

/**
 * WP Background Process
 * Credits: https://github.com/deliciousbrains/wp-background-processing/
 */
if ( ! class_exists( 'WP_Background_Process' ) ) {

	/**
	 * Abstract WP_Background_Process class.
	 *
	 * @abstract
	 * @extends WP_Async_Request
	 */
	abstract class WP_Background_Process extends WP_Async_Request {

		/**
		 * Action
		 *
		 * (default value: 'background_process')
		 *
		 * @var string
		 * @access protected
		 */
		protected $action = 'background_process';

		/**
		 * Start time of current process.
		 *
		 * (default value: 0)
		 *
		 * @var int
		 * @access protected
		 */
		protected $start_time = 0;

		/**
		 * Cron_hook_identifier
		 *
		 * @var mixed
		 * @access protected
		 */
		protected $cron_hook_identifier;

		/**
		 * Cron_interval_identifier
		 *
		 * @var mixed
		 * @access protected
		 */
		protected $cron_interval_identifier;

		/**
		 * Initiate new background process
		 */
		public function __construct() {
			parent::__construct();

			$this->cron_hook_identifier     = $this->identifier . '_cron';
			$this->cron_interval_identifier = $this->identifier . '_cron_interval';

			add_action( $this->cron_hook_identifier, array( $this, 'handle_cron_healthcheck' ) );
			add_filter( 'cron_schedules', array( $this, 'schedule_cron_healthcheck' ) );
		}

		/**
		 * Dispatch
		 *
		 * @access public
		 * @return void
		 */
		public function dispatch() {
			// Schedule the cron healthcheck.
			$this->schedule_event();

			// Perform remote post.
			return parent::dispatch();
		}

		/**
		 * Push to queue
		 *
		 * @param mixed $data Data.
		 *
		 * @return $this
		 */
		public function push_to_queue( $data ) {
			$this->data[] = $data;

			return $this;
		}

		/**
		 * Save queue
		 *
		 * @return $this
		 */
		public function save() {
			$key = $this->generate_key();

			if ( ! empty( $this->data ) ) {
				update_site_option( $key, $this->data );
			}

			return $this;
		}

		/**
		 * Update queue
		 *
		 * @param string $key Key.
		 * @param array $data Data.
		 *
		 * @return $this
		 */
		public function update( $key, $data ) {
			if ( ! empty( $data ) ) {
				update_site_option( $key, $data );
			}

			return $this;
		}

		/**
		 * Delete queue
		 *
		 * @param string $key Key.
		 *
		 * @return $this
		 */
		public function delete( $key ) {
			delete_site_option( $key );

			return $this;
		}

		/**
		 * Generate key
		 *
		 * Generates a unique key based on microtime. Queue items are
		 * given a unique key so that they can be merged upon save.
		 *
		 * @param int $length Length.
		 *
		 * @return string
		 */
		protected function generate_key( $length = 64 ) {
			$unique  = md5( microtime() . rand() );
			$prepend = $this->identifier . '_batch_';

			return substr( $prepend . $unique, 0, $length );
		}

		/**
		 * Maybe process queue
		 *
		 * Checks whether data exists within the queue and that
		 * the process is not already running.
		 */
		public function maybe_handle() {
			// Don't lock up other requests while processing
			session_write_close();

			if ( $this->is_process_running() ) {
				// Background process already running.
				wp_die();
			}

			if ( $this->is_queue_empty() ) {
				// No data to process.
				wp_die();
			}

			check_ajax_referer( $this->identifier, 'nonce' );

			$this->handle();

			wp_die();
		}

		/**
		 * Is queue empty
		 *
		 * @return bool
		 */
		protected function is_queue_empty() {
			global $wpdb;

			$table  = $wpdb->options;
			$column = 'option_name';

			if ( is_multisite() ) {
				$table  = $wpdb->sitemeta;
				$column = 'meta_key';
			}

			$key = $wpdb->esc_like( $this->identifier . '_batch_' ) . '%';

			$count = $wpdb->get_var( $wpdb->prepare( "
			SELECT COUNT(*)
			FROM {$table}
			WHERE {$column} LIKE %s
		", $key ) );

			return ( $count > 0 ) ? false : true;
		}

		/**
		 * Is process running
		 *
		 * Check whether the current process is already running
		 * in a background process.
		 */
		protected function is_process_running() {
			if ( get_site_transient( $this->identifier . '_process_lock' ) ) {
				// Process already running.
				return true;
			}

			return false;
		}

		/**
		 * Lock process
		 *
		 * Lock the process so that multiple instances can't run simultaneously.
		 * Override if applicable, but the duration should be greater than that
		 * defined in the time_exceeded() method.
		 */
		protected function lock_process() {
			$this->start_time = time(); // Set start time of current process.

			$lock_duration = ( property_exists( $this, 'queue_lock_time' ) ) ? $this->queue_lock_time : 60; // 1 minute
			$lock_duration = apply_filters( $this->identifier . '_queue_lock_time', $lock_duration );

			set_site_transient( $this->identifier . '_process_lock', microtime(), $lock_duration );
		}

		/**
		 * Unlock process
		 *
		 * Unlock the process so that other instances can spawn.
		 *
		 * @return $this
		 */
		protected function unlock_process() {
			delete_site_transient( $this->identifier . '_process_lock' );

			return $this;
		}

		/**
		 * Get batch
		 *
		 * @return stdClass Return the first batch from the queue
		 */
		protected function get_batch() {
			global $wpdb;

			$table        = $wpdb->options;
			$column       = 'option_name';
			$key_column   = 'option_id';
			$value_column = 'option_value';

			if ( is_multisite() ) {
				$table        = $wpdb->sitemeta;
				$column       = 'meta_key';
				$key_column   = 'meta_id';
				$value_column = 'meta_value';
			}

			$key = $wpdb->esc_like( $this->identifier . '_batch_' ) . '%';

			$query = $wpdb->get_row( $wpdb->prepare( "
			SELECT *
			FROM {$table}
			WHERE {$column} LIKE %s
			ORDER BY {$key_column} ASC
			LIMIT 1
		", $key ) );

			$batch       = new stdClass();
			$batch->key  = $query->$column;
			$batch->data = maybe_unserialize( $query->$value_column );

			return $batch;
		}

		/**
		 * Handle
		 *
		 * Pass each queue item to the task handler, while remaining
		 * within server memory and time limit constraints.
		 */
		protected function handle() {
			$this->lock_process();

			do {
				$batch = $this->get_batch();

				foreach ( $batch->data as $key => $value ) {
					$task = $this->task( $value );

					if ( false !== $task ) {
						$batch->data[ $key ] = $task;
					} else {
						unset( $batch->data[ $key ] );
					}

					if ( $this->time_exceeded() || $this->memory_exceeded() ) {
						// Batch limits reached.
						break;
					}
				}

				// Update or delete current batch.
				if ( ! empty( $batch->data ) ) {
					$this->update( $batch->key, $batch->data );
				} else {
					$this->delete( $batch->key );
				}
			} while ( ! $this->time_exceeded() && ! $this->memory_exceeded() && ! $this->is_queue_empty() );

			$this->unlock_process();

			// Start next batch or complete process.
			if ( ! $this->is_queue_empty() ) {
				$this->dispatch();
			} else {
				$this->complete();
			}

			wp_die();
		}

		/**
		 * Memory exceeded
		 *
		 * Ensures the batch process never exceeds 90%
		 * of the maximum WordPress memory.
		 *
		 * @return bool
		 */
		protected function memory_exceeded() {
			$memory_limit   = $this->get_memory_limit() * 0.9; // 90% of max memory
			$current_memory = memory_get_usage( true );
			$return         = false;

			if ( $current_memory >= $memory_limit ) {
				$return = true;
			}

			return apply_filters( $this->identifier . '_memory_exceeded', $return );
		}

		/**
		 * Get memory limit
		 *
		 * @return int
		 */
		protected function get_memory_limit() {
			if ( function_exists( 'ini_get' ) ) {
				$memory_limit = ini_get( 'memory_limit' );
			} else {
				// Sensible default.
				$memory_limit = '128M';
			}

			if ( ! $memory_limit || - 1 === intval( $memory_limit ) ) {
				// Unlimited, set to 32GB.
				$memory_limit = '32000M';
			}

			return intval( $memory_limit ) * 1024 * 1024;
		}

		/**
		 * Time exceeded.
		 *
		 * Ensures the batch never exceeds a sensible time limit.
		 * A timeout limit of 30s is common on shared hosting.
		 *
		 * @return bool
		 */
		protected function time_exceeded() {
			$finish = $this->start_time + apply_filters( $this->identifier . '_default_time_limit', 20 ); // 20 seconds
			$return = false;

			if ( time() >= $finish ) {
				$return = true;
			}

			return apply_filters( $this->identifier . '_time_exceeded', $return );
		}

		/**
		 * Complete.
		 *
		 * Override if applicable, but ensure that the below actions are
		 * performed, or, call parent::complete().
		 */
		protected function complete() {
			// Unschedule the cron healthcheck.
			$this->clear_scheduled_event();
		}

		/**
		 * Schedule cron healthcheck
		 *
		 * @access public
		 *
		 * @param mixed $schedules Schedules.
		 *
		 * @return mixed
		 */
		public function schedule_cron_healthcheck( $schedules ) {
			$interval = apply_filters( $this->identifier . '_cron_interval', 5 );

			if ( property_exists( $this, 'cron_interval' ) ) {
				$interval = apply_filters( $this->identifier . '_cron_interval', $this->cron_interval );
			}

			// Adds every 5 minutes to the existing schedules.
			$schedules[ $this->identifier . '_cron_interval' ] = array(
				'interval' => MINUTE_IN_SECONDS * $interval,
				'display'  => sprintf( __( 'Every %d Minutes' ), $interval ),
			);

			return $schedules;
		}

		/**
		 * Handle cron healthcheck
		 *
		 * Restart the background process if not already running
		 * and data exists in the queue.
		 */
		public function handle_cron_healthcheck() {
			if ( $this->is_process_running() ) {
				// Background process already running.
				exit;
			}

			if ( $this->is_queue_empty() ) {
				// No data to process.
				$this->clear_scheduled_event();
				exit;
			}

			$this->handle();

			exit;
		}

		/**
		 * Schedule event
		 */
		protected function schedule_event() {
			if ( ! wp_next_scheduled( $this->cron_hook_identifier ) ) {
				wp_schedule_event( time(), $this->cron_interval_identifier, $this->cron_hook_identifier );
			}
		}

		/**
		 * Clear scheduled event
		 */
		protected function clear_scheduled_event() {
			$timestamp = wp_next_scheduled( $this->cron_hook_identifier );

			if ( $timestamp ) {
				wp_unschedule_event( $timestamp, $this->cron_hook_identifier );
			}
		}

		/**
		 * Cancel Process
		 *
		 * Stop processing queue items, clear cronjob and delete batch.
		 *
		 */
		public function cancel_process() {
			if ( ! $this->is_queue_empty() ) {
				$batch = $this->get_batch();

				$this->delete( $batch->key );

				wp_clear_scheduled_hook( $this->cron_hook_identifier );
			}

		}

		/**
		 * Task
		 *
		 * Override this method to perform any actions required on each
		 * queue item. Return the modified item for further processing
		 * in the next pass through. Or, return false to remove the
		 * item from the queue.
		 *
		 * @param mixed $item Queue item to iterate over.
		 *
		 * @return mixed
		 */
		abstract protected function task( $item );

	}
}
