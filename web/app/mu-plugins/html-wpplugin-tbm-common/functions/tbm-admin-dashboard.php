<?php
/**
 * Disable all panels in dashboard
 */
add_action( 'wp_dashboard_setup', function () {
	remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_browser_nag', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_secondary', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_activity', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_site_health', 'dashboard', 'normal' );
	remove_action( 'welcome_panel', 'wp_welcome_panel' );
	remove_meta_box( 'bbp-dashboard-right-now', 'dashboard', 'normal' );
	remove_meta_box( 'wn_dashboard_widget', 'dashboard', 'normal' );
} );

/**
 * Enable custom panels
 */
add_action( 'wp_dashboard_setup', function () {
	wp_add_dashboard_widget( 'wn_recent_trends', 'Temi importanti', 'wn_recent_trends' );
	if ( current_user_can( 'edit_posts' ) ) {
		wp_add_dashboard_widget( 'wn_recent_posts_author', 'I tuoi ultimi 15 articoli', 'wn_recent_posts_author' );
	}
	if ( current_user_can( 'edit_others_posts' ) ) {
		wp_add_dashboard_widget( 'wn_recent_posts_administrator', 'Articoli recenti: ultimi 15 programmati o in attesa di revisione; ultimi 50 pubblicati', 'wn_recent_posts_administrator' );
	}
} );

/**
 * Set Dashboard to one single column
 */
add_filter( 'screen_layout_columns', function ( $columns ) {
	$columns['dashboard'] = 1;

	return $columns;
} );

/**
 * Set Dashboard to one single column in user profile settings
 */
add_filter( 'get_user_option_screen_layout_dashboard', function () {
	return 1;
} );

################################
# Helpers and common functions #
################################

/**
 * Check the sanity
 *
 * @param int $post The $post object or the post id
 * @param int $clean To embed or not in badge tag from Bootstrap
 *
 * @return string
 */
function wn_check_sanity( $post = 0, $clean = 0 ) {
	$post = get_post( $post );

	if ( empty( $post ) ) {
		return '';
	}

	$out    = array();
	$return = '';

	if ( ! has_post_thumbnail( $post ) ) {
		if ( $clean == 0 ) {
			$out[] = '<span class="badge badge-important" title="Immagine">Img</span>';
		} else {
			$out[] = 'img ';
		}
	}

	$mandatory_taxes = get_field( 'tbm_dashboard_mandatory_taxonomies', 'options' ) ?: array();
	$suggested_taxes = get_field( 'tbm_dashboard_suggested_taxonomies', 'options' ) ?: array();

	// Loop mandatory taxes
	if ( $mandatory_taxes ) {
		foreach ( $mandatory_taxes as $mytax ) {
			if ( taxonomy_exists( $mytax ) ) {
				if ( ! has_term( '', $mytax, $post ) ) {
					$taxname  = get_taxonomy( $mytax )->name;
					$taxlabel = get_taxonomy( $mytax )->label;

					$badge = 'badge-important';

					if ( $clean == 0 ) {
						$out[] = sprintf( "<span class='badge %s' title='%s' style='margin-left:2px;'>%s</span>", $badge, ucfirst( $taxname ), ucfirst( substr( $taxlabel, 0, 1 ) ) );
					} else {
						$out[] = $taxname . ' ';
					}
				}
			}
		}
	}

	// Loop suggested taxes
	if ( $suggested_taxes ) {
		foreach ( $suggested_taxes as $mytax ) {
			if ( taxonomy_exists( $mytax ) ) {
				if ( ! has_term( '', $mytax, $post ) ) {
					$taxname  = get_taxonomy( $mytax )->name;
					$taxlabel = get_taxonomy( $mytax )->label;

					$badge = 'badge-warning';

					if ( $clean == 0 ) {
						$out[] = sprintf( "<span class='badge %s' title='%s' style='margin-left:2px;'>%s</span>", $badge, ucfirst( $taxname ), ucfirst( substr( $taxlabel, 0, 1 ) ) );
					} else {
						$out[] = $taxname . ' ';
					}
				}
			}
		}
	}

	if ( $out ) {
		$return = join( '', $out );
	}

	return $return;
}

/**
 * Build the table for admins
 */
function wn_recent_posts_administrator() {
	build_post_table( 'administrator' );
}

/**
 * Build the table for authors
 */
function wn_recent_posts_author() {
	build_post_table( 'author' );
}

/**
 * Build the table
 */
function build_post_table( $user = 'author' ) {

	// Create the nonce
	$nonce = wp_create_nonce( 'change_post_status' );

	// Set the post types
	$enabled_post_types = array_merge( get_post_types( array(
		'public'   => true,
		'_builtin' => false
	) ), array( 'post' ) );

	/**
	 * Filter the post types enabled for admin
	 */
	$post_types_admin = apply_filters( 'tbm_dashboard_post_types_admin', $enabled_post_types );

	/**
	 * Filter the post types enabled for authors
	 */
	$post_types_authors = apply_filters( 'tbm_dashboard_post_types_authors', $enabled_post_types );

	// Set the query
	$query = array(
		'numberposts' => 50,
		'post_type'   => $post_types_admin,
	);

	// Set the future query
	$future = array(
		'numberposts' => 15,
		'orderby'     => 'date',
		'order'       => 'ASC',
		'post_type'   => $post_types_admin,
		'post_status' => array(
			'future'
		)
	);

	if ( $user === 'author' ) {
		$query['author']      = get_current_user_id();
		$query['post_type']   = $post_types_authors;
		$query['numberposts'] = 15;
	}

	/** @var array $mandatory_taxes The mandatory taxes */
	$mandatory_taxes = get_field( 'tbm_dashboard_mandatory_taxonomies', 'options' ) ?: array();

	/** @var array $suggested_taxes The suggested taxes */
	$suggested_taxes = get_field( 'tbm_dashboard_suggested_taxonomies', 'options' ) ?: array();

	$mandatory_out = array();
	$suggested_out = array();

	// Build mandatory name
	if ( $mandatory_taxes ) {
		foreach ( $mandatory_taxes as $mytax ) {
			if ( taxonomy_exists( $mytax ) ) {
				$taxname         = get_taxonomy( $mytax )->name;
				$taxlabel        = get_taxonomy( $mytax )->label;
				$badge           = 'badge-important badge-' . $user;
				$mandatory_out[] = sprintf( '<span class="badge %s" title="%s" data-filter="%s">%s</span>', $badge, $taxname, $taxname, $taxlabel );
			}
		}
	}

	// Build suggested name
	if ( $suggested_taxes ) {
		foreach ( $suggested_taxes as $mytax ) {
			if ( taxonomy_exists( $mytax ) ) {
				$taxname         = get_taxonomy( $mytax )->name;
				$taxlabel        = get_taxonomy( $mytax )->label;
				$badge           = 'badge-warning badge-' . $user;
				$suggested_out[] = sprintf( '<span class="badge %s" title="%s" data-filter="%s">%s</span>', $badge, $taxname, $taxname, $taxlabel );
			}
		}
	}

	/** Enqueue styles and scripts */
	wp_enqueue_script( 'tbm-mixitup', HTML_TBM_COMMON_PLUGIN_URL . '/dist/js/jquery_mixitup.min.js', array( 'jquery' ), HTML_TBM_COMMON_PLUGIN_VERSION, false );
	wp_enqueue_script( 'tbm-dashboard', HTML_TBM_COMMON_PLUGIN_URL . '/dist/js/tbm-dashboard.min.js', array( 'tbm-mixitup' ), HTML_TBM_COMMON_PLUGIN_VERSION, false );
	wp_enqueue_style( 'tbm-fontawesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', array(), '4.3.0' );
	wp_enqueue_style( 'tbm-dashboard', HTML_TBM_COMMON_PLUGIN_URL . '/dist/css/tbm-dashboard.min.css', array(), HTML_TBM_COMMON_PLUGIN_VERSION );
	?>
    <table class="widefat" cellspacing="0">
        <thead>
        <tr>
            <th colspan="7" class="element-selector">Mostra solo articoli in cui manca
                <span class="badge badge-important badge-<?php echo $user; ?>"
                      title="Immagine" data-filter="img">Img</span>
				<?php echo join( '', $mandatory_out ); ?>
				<?php echo join( '', $suggested_out ); ?>
                &nbsp;•&nbsp;Mostra solo articoli
                <span class="badge publish badge-<?php echo $user; ?>" title="publish"
                      data-filter="publish">Pubblicati</span>
                <span class="badge pending badge-<?php echo $user; ?>" title="pending" data-filter="pending">Da approvare</span>
                <span class="badge future badge-<?php echo $user; ?>" title="future"
                      data-filter="future">Programmati</span>
                &nbsp;•&nbsp;<span class="badge badge-info badge-<?php echo $user; ?>" title="Mostra tutti gli articoli"
                                   data-filter="all">Mostra tutti</span>
                <br/><br/>
                Clic su <i class="fa fa-star fa-lg fa-tbm-off" style="color:#B8860B"></i> per impostare primo piano,
                clic su <i class="fa fa-bell fa-lg fa-tbm-off" style="color:#B8860B"></i> per <a
                        href="/feed?mrss=1&push=1" target="_blank">inviare push</a>, clic su <i
                        class="fa fa-twitter fa-lg fa-tbm-off" style="color:#B8860B"></i> <a
                        href="/feed?mrss=1&twitter=1" target="_blank">per twittare</a>, clic su <i
                        class="fa fa-facebook fa-lg fa-tbm-off" style="color:#B8860B"></i> <a
                        href="/feed?mrss=1&facebook=1" target="_blank">per postare su FB</a>, clic su <i
                        class="fa fa-linkedin fa-lg fa-tbm-off" style="color:#B8860B"></i> <a
                        href="/feed?mrss=1&linkedin=1" target="_blank">per postare su Linkedin</a>.
                Saranno postati solo articoli pubblicati. L'ordine di "posting" dipende dal momento di aggiunta alla
                coda e non dalla data di pubblicazione.
            </th>
        </tr>
        <tr>
            <th>Ora</th>
            <th>Trigger</th>
            <th>Titolo</th>
            <th>Tipologia</th>
            <th>Stato</th>
            <th>Autore</th>
            <th>Sanity</th>
        </tr>


        </thead>
        <tfoot>
        <tr>
            <th>Ora</th>
            <th>Trigger</th>
            <th>Titolo</th>
            <th>Tipologia</th>
            <th>Stato</th>
            <th>Autore</th>
            <th>Sanity</th>
        </tr>
        </tfoot>
        <tbody id="element-filter-<?php echo $user; ?>">
		<?php
		$my_published_posts = get_posts( $query );
		$my_future_posts    = get_posts( $future );

		$myposts = array_merge( array_reverse( $my_future_posts ), $my_published_posts );

		foreach ( $myposts as $post ) : setup_postdata( $post );
			$style = array();
			$icons = array();

			// Look in site options the active triggers
			$trigger_array = get_field( 'tbm_dashboard_trigger', 'options' ) ?: array();

			// If featured trigger is active
			if ( in_array( 'featured', $trigger_array ) ) {
				if ( get_field( '_featured', $post->ID ) ) {
					$icons[] = sprintf( '<a href="#" data-id="%d" data-featured="true" class="tbmSetter" title="Clic per rimuovere dal primo piano"><i class="fa fa-star fa-lg fa-tbm-on" ></i></a>', $post->ID );
				} else {
					$icons[] = sprintf( '<a href="#" data-id="%d" data-featured="false" class="tbmSetter" title="Clic per mettere in primo piano"><i class="fa fa-star fa-lg fa-tbm-off"></i></a>', $post->ID );
				}
			}

			// If Push trigger is active
			if ( in_array( 'push', $trigger_array ) ) {
				if ( get_post_meta( $post->ID, 'tbm_pushed', true ) ) {
					$icons[] = sprintf( '<a href="#" data-id="%d" data-push="true" class="tbmSetter" title="Clic per NON pushare"><i class="fa fa-bell fa-lg fa-tbm-on" ></i></a>', $post->ID );
				} else {
					$icons[] = sprintf( '<a href="#" data-id="%d" data-push="false" class="tbmSetter" title="Clic per pushare"><i class="fa fa-bell fa-lg fa-tbm-off"></i></a>', $post->ID );
				}
			}

			// If Twitter trigger is active
			if ( in_array( 'twitter', $trigger_array ) ) {
				if ( get_post_meta( $post->ID, 'tbm_twittered', true ) ) {
					$icons[] = sprintf( '<a href="#" data-id="%d" data-twitter="true" class="tbmSetter" title="Clic per NON twittare"><i class="fa fa-twitter fa-lg fa-tbm-on" ></i></a>', $post->ID );
				} else {
					$icons[] = sprintf( '<a href="#" data-id="%d" data-twitter="false" class="tbmSetter" title="Clic per twittare"><i class="fa fa-twitter fa-lg fa-tbm-off"></i></a>', $post->ID );
				}
			}

			// If Facebook trigger is active
			if ( in_array( 'facebook', $trigger_array ) ) {
				if ( get_post_meta( $post->ID, 'tbm_facebooked', true ) ) {
					$icons[] = sprintf( '<a href="#" data-id="%d" data-facebook="true" class="tbmSetter" title="Clic per NON postare"><i class="fa fa-facebook fa-lg fa-tbm-on" ></i></a>', $post->ID );
				} else {
					$icons[] = sprintf( '<a href="#" data-id="%d" data-facebook="false" class="tbmSetter" title="Clic per postare"><i class="fa fa-facebook fa-lg fa-tbm-off"></i></a>', $post->ID );
				}
			}

			// If Linkedin trigger is active
			if ( in_array( 'linkedin', $trigger_array ) ) {
				if ( get_post_meta( $post->ID, 'tbm_linkedined', true ) ) {
					$icons[] = sprintf( '<a href="#" data-id="%d" data-linkedin="true" class="tbmSetter" title="Clic per NON postare"><i class="fa fa-linkedin fa-lg fa-tbm-on" ></i></a>', $post->ID );
				} else {
					$icons[] = sprintf( '<a href="#" data-id="%d" data-linkedin="false" class="tbmSetter" title="Clic per postare"><i class="fa fa-linkedin fa-lg fa-tbm-off"></i></a>', $post->ID );
				}
			}


			if ( $post->post_status != 'publish' ) {
				$style[] = 'font-style:italic';
			}

			$url = get_permalink( $post );

			?>
            <tr class="mix <?php echo wn_check_sanity( $post->ID, 1 ); ?> <?php echo get_post_status( $post->ID ); ?>"
                style="<?php echo implode( ";", $style ); ?>">
                <td style="width: 13%">
                    <a href="<?php echo $url; ?>" title='Apri il post'
                       target="_blank"><?php echo get_the_date( 'j M, H:i', $post ); ?></a>
                </td>
                <td>
					<?php
					echo join( '&nbsp;|&nbsp;', $icons );
					?>
                </td>
                <td><?php if ( current_user_can( 'edit_post', $post->ID ) ) { ?>
                    <a href='<?php echo get_edit_post_link( $post->ID ); ?>'
                       title='Modifica il post'><?php } ?><?php echo get_the_title( $post ); ?><?php if ( current_user_can( 'edit_post', $post->ID ) ) { ?></a><?php } ?>
                </td>
                <td><?php echo ucfirst( $post->post_type ); ?></td>
                <td><?php
					switch ( $post->post_status ) {
						case 'private':
							_e( 'Privately Published' );
							break;
						case 'publish':
							_e( 'Published' );
							break;
						case 'future':
							_e( 'Scheduled' );
							break;
						case 'pending':
							_e( 'Pending Review' );
							break;
						case 'draft':
						case 'auto-draft':
							_e( 'Draft' );
							break;
					}
					?></td>
                <td>
                    <a href="<?php echo get_edit_user_link( $post->post_author ); ?>"><?php echo get_the_author(); ?></a>
                </td>
                <td><?php echo wn_check_sanity( $post ); ?></td>
            </tr>
		<?php
		endforeach;
		wp_reset_postdata();
		?>
        </tbody>
    </table>
	<?php
	echo "<script>var tbmDashNonce='{$nonce}';</script>";
}

/**
 * Get the trends to sanity check
 *
 * @return bool
 */
function wn_recent_trends() {
	$trend_taxonomy = get_field( 'tbm_dashboard_topic_taxonomy', 'options' ) ?: array();

	if ( ! $trend_taxonomy || ! taxonomy_exists( $trend_taxonomy ) ) {
		echo '<p>Non è stata selezionata la tassonomia che categorizza i temi importanti.</p>';

		return true;
	}

	$name = get_taxonomy( $trend_taxonomy )->label;

	$flussi = get_terms( $trend_taxonomy, array(
		'orderby'    => 'id',
		'order'      => 'DESC',
		'hide_empty' => false,
		'fields'     => 'names'
	) );

	if ( $flussi ) {
		echo '<p style="font-size: 1.1em">La tassonomia <em>' . $name . '</em> è importante per categorizzare gli articoli. Per semplificare la loro attribuzione, pubblichiamo qui gli ultimi topic della tassonomia <em>' . $name . '</em> creati.</p>';
		echo '<p style="font-size: 1.1em;line-height: 2em"><strong>Ultimi 10</strong>: <span class="first">' . join( '</span> <span class="first">', array_slice( $flussi, 0, 10 ) ) . '</span> <span class="more" onclick="jQuery(\'span.second\').toggle()">+</span> <span class="second">' . join( '</span> <span class="second">', array_slice( $flussi, 11 ) ) . '</span></p>';
	} else {
		echo '<p>La tassonomia ' . $name . ' non contiene alcun post.</p>';
	}
}

/**
 * Handle Ajax calls
 */
add_action( 'wp_ajax_change_post_status', function () {
	$gump = new GUMP();

	if ( $_SERVER['REQUEST_METHOD'] !== 'POST' ) {
		gump_send_error_response( 'Invalid request' );
	}

	$_POST = $gump->sanitize( $_POST );

	$gump->validation_rules( array(
		'_wpnonce' => 'required',
		'id'       => 'required|integer',
		'date'     => 'required|integer',
		'status'   => 'required|contains_list,featuredDown;featuredUp;pushDown;pushUp;twitterDown;twitterUp;facebookDown;facebookUp;linkedinDown;linkedinUp'
	) );

	$gump->filter_rules( array(
		'_wpnonce' => 'trim|sanitize_string',
		'id'       => 'trim|sanitize_numbers',
		'date'     => 'trim|sanitize_numbers',
		'status'   => 'trim|sanitize_string'
	) );

	$validated_data = $gump->run( $_POST );

	if ( $validated_data === false ) {
		tbm_gump_send_error_response( $gump );
	}

	$id   = $validated_data['id'];
	$date = $validated_data['date'];

	if ( $validated_data['status'] === 'featuredDown' ) {
		$result = delete_field( '_featured', $id );
	}

	if ( $validated_data['status'] === 'featuredUp' ) {
		$result = update_field( '_featured', '1', $id );
	}

	if ( $validated_data['status'] === 'pushDown' ) {
		$result = delete_field( 'tbm_pushed', $id ) && delete_post_meta( $id, 'tbm_pushed_date' );
	}

	if ( $validated_data['status'] === 'pushUp' ) {
		$result = update_field( 'tbm_pushed', '1', $id ) && update_post_meta( $id, 'tbm_pushed_date', $date );
	}

	if ( $validated_data['status'] === 'twitterDown' ) {
		$result = delete_field( 'tbm_twittered', $id ) && delete_post_meta( $id, 'tbm_twittered_date' );
	}

	if ( $validated_data['status'] === 'twitterUp' ) {
		$result = update_field( 'tbm_twittered', '1', $id ) && update_post_meta( $id, 'tbm_twittered_date', $date );
	}

	if ( $validated_data['status'] === 'facebookDown' ) {
		$result = delete_field( 'tbm_facebooked', $id ) && delete_post_meta( $id, 'tbm_facebooked_date' );
	}

	if ( $validated_data['status'] === 'facebookUp' ) {
		$result = update_field( 'tbm_facebooked', '1', $id ) && update_post_meta( $id, 'tbm_facebooked_date', $date );
	}

	if ( $validated_data['status'] === 'linkedinDown' ) {
		$result = delete_field( 'tbm_linkedined', $id ) && delete_post_meta( $id, 'tbm_linkedined_date' );
	}

	if ( $validated_data['status'] === 'linkedinUp' ) {
		$result = update_field( 'tbm_linkedined', '1', $id ) && update_post_meta( $id, 'tbm_linkedined_date', $date );
	}

	if ( $result ) {
		tbm_gump_send_success_response( $id );
	} else {
		tbm_gump_send_error_response( $id );
	}

	exit;

} );


