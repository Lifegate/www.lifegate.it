<?php
/**
 * Created by PhpStorm.
 * User: Francesco
 * Date: 04/10/2018
 * Time: 12:45
 */

/**
 * Remove MP Payment plugin metabox
 */
add_filter( 'mppp_hide_editorial_metabox', '__return_true' );

/**
 * Add editorial post type to Article Json+LD Yoast Schema
 */
add_action( 'after_setup_theme', function () {
	add_filter( 'wpseo_schema_article_post_types', function ( $post_types ) {

		defined( 'HTML_QA_QUESTION_POST_TYPE' ) ? $post_types[] = HTML_QA_QUESTION_POST_TYPE : null;
		defined( 'HTML_VIDEO_POST_TYPE' ) ? $post_types[] = HTML_VIDEO_POST_TYPE : null;
		defined( 'HTML_CARD_POST_TYPE' ) ? $post_types[] = HTML_CARD_POST_TYPE : null;
		defined( 'HTML_EVENT_POST_TYPE' ) ? $post_types[] = HTML_EVENT_POST_TYPE : null;
		defined( 'HTML_GALLERY_POST_TYPE' ) ? $post_types[] = HTML_GALLERY_POST_TYPE : null;
		defined( 'HTML_LESSON_POST_TYPE' ) ? $post_types[] = HTML_LESSON_POST_TYPE : null;
		defined( 'HTML_GUIDE_POST_TYPE' ) ? $post_types[] = HTML_GUIDE_POST_TYPE : null;
		$post_types[] = "longform";
		return $post_types;
	} );
} );

/**
 * Disable upload bmp, jpe images
 */
add_filter( 'upload_mimes', function ( array $mimes ) {
	unset( $mimes['bmp'] );
	unset( $mimes['jpg|jpeg|jpe'] );
	$mimes['jpg|jpeg'] = 'image/jpeg';

	return $mimes;
} );

/**
 * Filter the title in Google Producer feed
 */
add_filter( 'the_title', function ( $title, $id = null ) {

	if ( is_feed( 'gp' ) ) {
		return $title . ' | ' . get_bloginfo( 'name' );
	}


	return $title;
}, 10, 2 );

/**
 * Remove querystring from paginate links function (used in gallery pagination)
 */
add_filter( 'paginate_links', function ( $link ) {

	return strtok( $link, '?' );
} );

/**
 * Remove unusued box from all sites
 */
add_action( 'add_meta_boxes', function () {
	$post_types = get_post_type();
	remove_meta_box( 'gdrts-metabox', $post_types, 'normal' ); // GD Star Rating
}, 11 );

/**
 * Getting rid of archive “label”
 */
add_filter( 'get_the_archive_title', function ( $title ) {

	$page = '';

	if ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_tag() ) {
		$title = single_tag_title( '', false );
	} elseif ( is_author() ) {
		$title = '<span class="vcard">' . get_the_author() . '</span>';
	} elseif ( is_post_type_archive() ) {
		$title = post_type_archive_title( '', false );
	} elseif ( is_tax() ) {
		$title = single_term_title( '', false );
	}

	if ( is_paged() ) {
		$page = sprintf( '<span>' . __( 'pagina', 'lifegate' ) . ' %d</span>', get_query_var( 'paged' ) );
	}

	return $title . ' ' . $page;
} );

/**
 * Add excerpt to page
 */
add_post_type_support( 'page', 'excerpt' );

/**
 * Remove tag if has a small numeber of posts
 *
 * @param $terms Array of terms
 * @param $post_id ID of the post
 * @param $taxonomy Taxonomy slug
 *
 * @return mixed
 */
add_filter( 'get_the_terms', function ( $terms, $post_id, $taxonomy ) {

	if ( is_admin() ) {
		return $terms;
	}

	if ( $taxonomy !== 'post_tag' ) {
		return $terms;
	}

	if ( $terms ) {
		foreach ( $terms as $key => $term ) {
			if ( $term->count < 7 ) {
				unset( $terms[ $key ] );
			}
		}
	}

	return $terms;

}, 10, 3 );

/**
 * Set the Public Post Preview plugin expiration to 7 days
 */
add_filter( 'ppp_nonce_life', function () {
	return 60 * 60 * 24 * 7; // 7 days
} );

/**
 * Format custom pagination layout with wp_pagenavi plugin
 *
 * @param string $html HTML code of pagination
 */
add_filter( 'wp_pagenavi', function ( $html ) {
	$out = '';
	$out = preg_replace( "/<span class='current'>([0-9]+)<\/span>/", '<li class="selected-item">$1</li>', $html );
	$out = str_replace( "<a", "<li><a", $out );
	$out = str_replace( "</a>", "</a></li>", $out );
	$out = str_replace( "<span", "<li><span", $out );
	$out = str_replace( "</span>", "</span></li>", $out );
	$out = str_replace( "<div class='wp-pagenavi'>", "", $out );
	$out = str_replace( "</div>", "", $out );

	return '<ol>' . $out . '</ol>';
}, 10, 2 );

/**
 * Search SQL filter for matching against post title only.
 *
 * @link    http://wordpress.stackexchange.com/a/11826/1685
 *
 * @param string $search
 * @param WP_Query $wp_query
 */
add_filter( 'posts_search', function ( $search, $wp_query ) {

	if ( empty( $search ) ) {
		return $search;
	}

	if ( is_admin() ) {
		return $search;
	}
	if ( ! empty( $search ) && ! empty( $wp_query->query_vars['search_terms'] ) ) {
		global $wpdb;

		$q = $wp_query->query_vars;
		$n = ! empty( $q['exact'] ) ? '' : '%';

		$search = array();

		foreach ( ( array ) $q['search_terms'] as $term ) {
			$search[] = $wpdb->prepare( "$wpdb->posts.post_title LIKE %s", $n . $wpdb->esc_like( $term ) . $n );
		}

		if ( ! is_user_logged_in() ) {
			$search[] = "$wpdb->posts.post_password = ''";
		}

		$search = ' AND ' . implode( ' AND ', $search );
	}

	return $search;
}, 10, 2 );

/**
 * Modifico la paginazione del plugin FacetWP
 *
 * @param $output
 * @param $params
 *
 * @return string
 */
add_filter( 'facetwp_pager_html', function ( $output, $params ) {
	$output      = '<div class="pagination"><div>';
	$page        = (int) $params['page'];
	$total_pages = (int) $params['total_pages'];
	$a           = 0;
	// Se la pagina attuale è maggiore di uno
	if ( 1 < $page ) {
		$output .= '<div><a class="facetwp-page pagination__nav nav-arrow nav-arrow__prev"  data-page="' . ( $page - 1 ) . '"></a><ol>';
	} else {
		$output .= '<div class="pagination__first-page"><a class="facetwp-page pagination__nav nav-arrow nav-arrow__prev"  data-page="' . ( $page - 1 ) . '"></a><ol>';
	}

	// Eseguo il loop sulle pagine precedenti la pagina attiva
	for ( $i = 2; $i > 0; $i -- ) {
		if ( 0 < ( $page - $i ) ) {
			$link = $page - $i;
			//Todo Cambiare il nome del post type hardcoded con il define del plugin
			$post_type_link = get_post_type_archive_link( 'esperto' ) . '/page/' . $link;
			$output         .= '<li class="loop1"><a href="' . $post_type_link . '" class="facetwp-page" data-page="' . $link . '">' . $link . '</a></li>';
		}
		$a ++;
	}

	// Pubblico la pagina attiva
	$output .= '<li><a class="facetwp-page active selected-item" data-page="' . $page . '">' . $page . '</a></li>';

	// Eseguo il loop sulle pagine successive la pagina attiva
	for ( $i = 1; $i <= 2; $i ++ ) {
		if ( $total_pages >= ( $page + $i ) ) {
			$link           = $page + $i;
			$post_type_link = get_post_type_archive_link( 'esperto' ) . '/page/' . $link;
			$output         .= '<li class="loop2"><a href="' . $post_type_link . '" class="facetwp-page" data-page="' . $link . '">' . $link . '</a></li>';
		}
	}

	// Se le pagine totali son di più della pagina attuale
	if ( $page < $total_pages ) {
		$output .= '<a class="pagination__nav nav-arrow nav-arrow__next facetwp-page" data-page="' . ( $page + 1 ) . '"></a>';
	}

	$output .= '</div></div>';

	return $output;
}, 10, 2 );

/**
 * Insert code for Teads after a paragraph of single post content
 *
 * @param string $content Post content
 *
 * @return string Amended content
 */
add_filter( 'the_content', function ( $content ) {
	global $post;
	$disabled          = false;
	$ad_code           = tbm_get_the_banner( 'TEADS', '', '', false, false );
	$enabled_post_type = get_field( 'tbm_common_teads_shortcode', 'option' ) ?: array();

	// Se non è un post abilitato, esco
	if ( ! get_field( 'tbm_common_teads_shortcode_enabled', 'option' ) || ! in_array( get_post_type(), $enabled_post_type ) ) {
		return $content;
	}

	// Se è un feed esco
	if ( is_feed() ) {
		return $content;
	}

	// Se è un AMP esco
	if ( tbm_is_amp() ) {
		return $content;
	}

	/**
	 * Filters whether to insert the code
	 *
	 * @param bool $disabled Whether the teads code should be disabled. Default false.
	 */

	$disabled = apply_filters( 'tbm_disable_teads', $disabled );

	if ( $disabled ) {
		return $content;
	}


	/**
	 * Check if Teads shortcode is present AND position is not forced
	 */
	if ( strpos( $content, 'ghteads' ) && ! get_field( 'tbm_common_teads_force', 'option' ) ) {
		return $content;
	}

	// Controllo il punto di visualizzazione
	$position_tag       = get_field( 'tbm_common_teads_shortcode_tag', 'option' );
	$position_paragraph = (int) get_field( 'tbm_common_teads_shortcode_paragraph', 'option' );

	// Se non c'è posizione, esco
	if ( empty( $position_tag ) && empty( $position_paragraph ) ) {
		return $content;
	}

	if ( $ad_code ) {
		if ( $position_tag ) {
			$tag = tbm_insert_before_tag( $ad_code, $position_tag, 1, $content );
			if ( $tag ) {
				return $tag;
			}
		}

		if ( $position_paragraph ) {
			$paragraph = tbm_insert_after_paragraph( $ad_code, $position_paragraph, $content );

			if ( $paragraph ) {
				return $paragraph;
			}

		}
	}

	return $content;

}, 10 );

/**
 * Insert code for video ads after a paragraph of single post content
 *
 * @param string $content Post content
 *
 * @return string Amended content
 */
add_filter( 'the_content', function ( $content ) {
	global $post;
	$ad_code  = '';
	$enabled  = true;
	$disabled = false;

	$enabled_post_type = get_field( 'tbm_common_video_shortcode', 'option' ) ?: array();

	/**
	 * Filters whether to insert the video
	 *
	 * Returning false to this hook is the recommended way to disable video in content.
	 *
	 * @param bool $enabled Whether the video should be inserted. Default true.
	 */

	$enabled = apply_filters( 'tbm_common_insert_video', $enabled );

	if ( ! $enabled ) {
		return $content;
	}

	/**
	 * Filters whether to insert the code
	 *
	 * @param bool $disabled Whether the video should be disabled. Default false.
	 */

	$disabled = apply_filters( 'tbm_disable_cnvid', $disabled );

	if ( $disabled ) {
		return $content;
	}

	if ( ! is_single() ) {
		return $content;
	}

	// Se non è un post abilitato
	if ( ! get_field( 'tbm_common_video_shortcode_enabled', 'option' ) || ! in_array( get_post_type(), $enabled_post_type ) ) {
		return $content;
	}

	// Se non esiste il post type video
	if ( ! in_array( 'video', get_post_types( array( 'public' => true ) ) ) ) {
		return $content;
	}

	// Se ho disabilitato la visualizzazione del video, esco.
	if ( get_field( 'video_single_disabled', $post->ID ) ) {
		return $content;
	}

	// Se ha già un video da shortcode, esco.
	if ( strpos( $content, 'ghvideo' ) ) {
		return $content;
	}

	/**
	 * Check if Random video shortcode is present AND position is not forced
	 */
	if ( strpos( $content, 'ghrandvideo' ) && ! get_field( 'tbm_common_video_force', 'option' ) ) {
		return $content;
	}

	// Controllo il punto di visualizzazione
	$position_tag       = get_field( 'tbm_common_video_shortcode_tag', 'option' );
	$position_paragraph = (int) get_field( 'tbm_common_video_shortcode_paragraph', 'option' );

	// Se non c'è posizione, esco
	if ( empty( $position_tag ) && empty( $position_paragraph ) ) {
		return $content;
	}

	// Prendo il video correlato.
	$related = tbm_get_custom_related_posts( $post, 1, null, 'post_tag', 'video' );

	if ( ! $related[0] ) {
		return $content;
	}

	$post_id = $related[0]->ID;

	if ( get_field( 'tbm_videoid', $post_id ) && isset( get_field( 'tbm_videoid', $post_id )['url'] ) && filter_var( get_field( 'tbm_videoid', $post_id )['url'], FILTER_VALIDATE_URL ) ) {
		$ad_code = '<video src="' . get_field( 'tbm_videoid', $post_id )['url'] . '" controls controlsList="nodownload"></video>';
	}

	if ( function_exists( 'html_video_generate_video_player' ) ) {
		$ad_code = html_video_generate_video_player( $post_id, '', false ) ?: $ad_code;
	}

	$ad_code = '<div class="video-post video-post--inside">' . $ad_code . '</div>';

	if ( $ad_code ) {
		if ( $position_tag ) {
			$tag = tbm_insert_before_tag( $ad_code, $position_tag, 1, $content );
			if ( $tag ) {
				return $tag;
			}
		}

		if ( $position_paragraph ) {
			$paragraph = tbm_insert_after_paragraph( $ad_code, $position_paragraph, $content );
			if ( $paragraph ) {
				return $paragraph;
			}
		}
	}

	return $content;
}, 10 );

/**
 * Insert code for sharethrough after a paragraph of single post content
 *
 * @param string $content Post content
 *
 * @return string Amended content
 */
add_filter( 'the_content', function ( $content ) {
	global $post;
	$disabled          = false;
	$ad_code           = tbm_get_the_banner( 'NATIVE', '', '', false, false );
	$enabled_post_type = get_field( 'tbm_common_native_shortcode', 'option' ) ?: array();

	// Se non è un post abilitato
	if ( ! get_field( 'tbm_common_native_shortcode_enabled', 'option' ) || ! in_array( get_post_type(), $enabled_post_type ) ) {
		return $content;
	}

	// Se è un feed esco
	if ( is_feed() ) {
		return $content;
	}

	// Se è un AMP esco
	if ( tbm_is_amp() ) {
		return $content;
	}

	/**
	 * Filters whether to insert the code
	 *
	 * @param bool $disabled Whether the native code should be disabled. Default false.
	 */

	$disabled = apply_filters( 'tbm_disable_native', $disabled );

	if ( $disabled ) {
		return $content;
	}

	/**
	 * Check if Native shortcode is present AND position is not forced
	 */
	if ( strpos( $content, 'ghsharethrough' ) && ! get_field( 'tbm_common_native_force', 'option' ) ) {
		return $content;
	}

	// Controllo il punto di visualizzazione
	$position_tag       = get_field( 'tbm_common_native_shortcode_tag', 'option' );
	$position_paragraph = (int) get_field( 'tbm_common_native_shortcode_paragraph', 'option' );

	// Se non c'è posizione, esco
	if ( empty( $position_tag ) && empty( $position_paragraph ) ) {
		return $content;
	}

	if ( $ad_code ) {
		if ( $position_tag ) {
			$tag = tbm_insert_before_tag( $ad_code, $position_tag, 1, $content );
			if ( $tag ) {
				return $tag;
			}
		}

		if ( $position_paragraph ) {
			$paragraph = tbm_insert_after_paragraph( $ad_code, $position_paragraph, $content );
			if ( $paragraph ) {
				return $paragraph;
			}
		}
	}

	return $content;

}, 10 );

/**
 * Insert editorial fields at the bottom of the content
 *
 * @param string $content Post content
 *
 * @return string Amended content
 */
add_filter( 'the_content', function ( $content ) {
	global $post;

	if ( ! $post ) {
		return null;
	}

	$editorial_field = get_field( 'post_tipologia', $post->ID );

	if ( empty( $editorial_field ) ) {
		return $content;
	}

	if ( $editorial_field === 'tbm_ed_opinion' ) {
		$text = __("Le opinioni espresse dall'autore non necessariamente riflettono quelle di LifeGate.", "lifegate");
	}

	if ( $editorial_field === 'tbm_ed_sponsored' ) {
		$text = __('Articolo sponsorizzato', "lifegate");
	}

	if ( empty( $text ) ) {
		return $content;
	}

	$return = sprintf( '<div class="single__hero-footer__box"><p class="cardpartner"><span>%s</span></p></div>', $text );

	return $content . $return;

}, 20 );

/**
 * Change logo url in login page
 */
add_filter( 'login_headerurl', function () {
	return get_bloginfo( 'url' );
} );

/**
 * Filters the youtube embed
 *
 * @return string
 */
add_filter( 'embed_oembed_html', function ( $cached_html, $url, $attr, $post_id ) {

	if ( strpos( $cached_html, 'youtube' ) !== false ) {
		$cached_html = '<div class="embed-responsive embed-responsive-16by9">' . $cached_html . '</div>';
	} else if ( strpos( $cached_html, 'twitter' ) !== false ) {
		// aggiungo il lazyload per twitter
		$cached_html = str_replace( '<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>', ' ', $cached_html);
		$cached_html = str_replace( '<blockquote class="twitter-tweet"', '<blockquote class="twitter-tweet lazyload"  data-script="//platform.twitter.com/widgets.js"', $cached_html);
		$cached_html = '<div class="embed-responsive embed-responsive-4by3">' . $cached_html . '</div>';

	} else {
		$cached_html = '<div class="embed-responsive embed-responsive-4by3">' . $cached_html . '</div>';
	}

	return $cached_html;
}, 99, 4 );

/**
 * Lazyload the iframe
 *
 * @return string
 */
add_filter( 'embed_oembed_html', function ( $cached_html, $url, $attr, $post_id ) {

	if ( tbm_is_amp() ) {
		return $cached_html;
	}

	if ( is_admin() ) {
		return $cached_html;
	}

	if ( ! get_field( 'tbm_common_lazyiframe_enabled', 'option' ) ) {
		return $cached_html;
	}


	$dom = new DOMDocument();
	@$dom->loadHTML( '<?xml encoding="utf-8" ?>' . $cached_html );
	$iframe = $dom->getElementsByTagName( 'iframe' )->item( 0 );

	if ( ! $iframe ) {
		return $cached_html;
	}

	/**
	 * Save the original code
	 */
	$iframe_original = $dom->saveHtml( $iframe );

	/**
	 * Elaborate the iframe
	 */
	$classes = $iframe->getAttribute( 'class' );
	$src     = $iframe->getAttribute( 'src' );
	$classes .= " lazyload";
	$iframe->setAttribute( 'class', trim( $classes ) );
	$iframe->setAttribute( 'data-src', $src );
	$iframe->removeAttribute( 'src' );

	/**
	 * Save the final code
	 */
	$iframe_final = $dom->saveHtml( $iframe );

	/**
	 * Replace the original code with the final code
	 */
	$cached_html = str_replace( $iframe_original, $iframe_final, $cached_html );

	return $cached_html;
}, 10, 4 );

