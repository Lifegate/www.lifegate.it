<?php

/**
 * Creo un nuovo pulsante in TinyMCE per caricare la popup
 *
 * Documentazione su http://www.garyc40.com/2010/03/how-to-make-shortcodes-user-friendly/
 * Documentazione su http://codex.wordpress.org/TinyMCE_Custom_Buttons
 */
add_action( 'admin_init', function () {
	if ( current_user_can( 'edit_posts' ) && get_field( 'tbm_common_embedder_enabled', 'option' ) ) {
		add_filter( 'mce_buttons', 'gh_shortcode_filter_mce_button' );
		add_filter( 'mce_external_plugins', 'gh_shortcode_filter_mce_plugin' );
	}
} );


// Inserisce un separatore
function gh_shortcode_filter_mce_button( $buttons ) {
	array_push( $buttons, '|', 'gh_shortcode', 'tbm_native_button', 'tbm_teads_button', 'tbm_ghvideo_button' );

	return $buttons;
}

// Inserisce i pulsanti
function gh_shortcode_filter_mce_plugin( $plugins ) {
	// this plugin file will work the magic of our button
	$plugins['gh_shortcode'] = HTML_TBM_COMMON_PLUGIN_URL . 'dist/js/html_it_shortcode_embed.min.js?ver=' . HTML_TBM_COMMON_PLUGIN_VERSION;
	$plugins['tbm_ghvideo']  = HTML_TBM_COMMON_PLUGIN_URL . 'dist/js/ghvideo_editor_plugin.min.js?ver=' . HTML_TBM_COMMON_PLUGIN_VERSION;
	$plugins['tbm_native']   = HTML_TBM_COMMON_PLUGIN_URL . 'dist/js/native_editor_plugin.min.js?ver=' . HTML_TBM_COMMON_PLUGIN_VERSION;
	$plugins['tbm_teads']    = HTML_TBM_COMMON_PLUGIN_URL . 'dist/js/teads_editor_plugin.min.js?ver=' . HTML_TBM_COMMON_PLUGIN_VERSION;

	return $plugins;
}

/**
 * Registra gli script per gli shortcode
 */
add_action( 'wp_enqueue_scripts', function () {
	wp_register_script( 'sky-script', '//adx.4strokemedia.com/www/delivery/asyncjs.php', array(), '1.0.0', true );
} );