<?php
/**
 * Created by PhpStorm.
 * User: francesco
 * Date: 23/04/2018
 * Time: 22:22
 */


function tbm_acf_sponsor_select() {
	$out = array();

	$terms = get_terms( array(
		'taxonomy'   => 'sponsor',
		'hide_empty' => false,
	) );

	if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
		foreach ( $terms as $term ) {
			$out[ $term->term_id ] = $term->name;
		}
	}


	return $out;

}

function tbm_acf_post_type_select() {
	$out = array();

	$choices = get_post_types( array( 'public' => true ) );

	if ( is_array( $choices ) ) {
		foreach ( $choices as $choice ) {
			$out[ $choice ] = get_post_type_object( $choice )->labels->singular_name;
		}
	}

	return $out;

}

function tbm_acf_menu_select() {
	$out = array();

	$choices = wp_get_nav_menus();

	if ( is_array( $choices ) ) {
		foreach ( $choices as $choice ) {
			$out[ $choice->menu_id ] = $choice->name;

		}
	}

	return $out;

}

function tbm_acf_taxonomy_select() {
	$out = array();

	$choices = get_taxonomies( array( 'public' => true ) );

	if ( is_array( $choices ) ) {
		foreach ( $choices as $choice ) {
			$out[ $choice ] = get_taxonomy( $choice )->labels->singular_name;
		}
	}

	return $out;

}

// Imposto la Google API Key
add_action( 'acf/init', function () {
	acf_update_setting( 'google_api_key', GMAPS_API );
} );

/**
 * Mostra il post type nella lista del relation field
 *
 * @param $title
 * @param $post
 * @param $field
 * @param $post_id
 *
 * @return string
 */
function tbm_relationship_result( $title, $post, $field, $post_id ) {
	$title .= ' (' . $post->post_type . ')';

	return $title;
}

add_filter( 'acf/fields/relationship/result', 'tbm_relationship_result', 10, 4 );

/**
 * Ordino il relational e il post_object per data decrescente
 *
 * @param $args
 * @param $field
 * @param $post_id
 *
 * @return mixed
 */
function tbm_relationship_query( $args ) {

	$args['orderby']     = "date";
	$args['order']       = "DESC";
	$args['post_status'] = "publish";

	return $args;
}

add_filter( 'acf/fields/relationship/query', 'tbm_relationship_query', 10, 3 );
add_filter( 'acf/fields/post_object/query', 'tbm_relationship_query', 10, 3 );

