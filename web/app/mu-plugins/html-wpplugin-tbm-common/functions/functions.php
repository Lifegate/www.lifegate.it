<?php
/**
 * Created by PhpStorm.
 * User: francesco
 * Date: 07/06/2018
 * Time: 09:30
 */

/**
 * TBM Functions
 */
require HTML_TBM_COMMON_PLUGIN_DIR . 'functions/tbm-functions.php';

/**
 * TBM AMP
 */
require HTML_TBM_COMMON_PLUGIN_DIR . 'functions/tbm-amp.php';

/**
 * ACF: Setup
 */
require HTML_TBM_COMMON_PLUGIN_DIR . 'functions/tbm-acf-setup.php';

/**
 * ACF: Opzioni
 */
require HTML_TBM_COMMON_PLUGIN_DIR . 'functions/tbm-acf-options.php';

/**
 * ACF: Campi
 */
require HTML_TBM_COMMON_PLUGIN_DIR . 'functions/tbm-acf-fields.php';

/**
 * ACF: Classi
 */
require HTML_TBM_COMMON_PLUGIN_DIR . 'functions/tbm-classes.php';

/**
 * Sanitizer
 */
require HTML_TBM_COMMON_PLUGIN_DIR . 'functions/tbm-sanitizer.php';

/**
 * JS and CSS enqueue
 */
require HTML_TBM_COMMON_PLUGIN_DIR . 'functions/tbm-setup-theme.php';

/**
 * Filters
 */
require HTML_TBM_COMMON_PLUGIN_DIR . 'functions/tbm-filters.php';

/**
 * Filters
 */
require HTML_TBM_COMMON_PLUGIN_DIR . 'functions/tbm-cpt.php';

/**
 * Handle post publishing constraints
 */
require HTML_TBM_COMMON_PLUGIN_DIR . 'functions/tbm-publishing-constraints.php';

/**
 * Feed
 **/
require HTML_TBM_COMMON_PLUGIN_DIR . 'functions/tbm-feed.php';

/**
 * Enqueue files
 **/
require HTML_TBM_COMMON_PLUGIN_DIR . 'functions/tbm-enqueue.php';

/**
 * Ajax functions
 **/
require HTML_TBM_COMMON_PLUGIN_DIR . 'functions/tbm-ajax.php';

/**
 * Actions
 **/
require HTML_TBM_COMMON_PLUGIN_DIR . 'functions/tbm-actions.php';

/**
 * Tinymce modifications
 **/
require HTML_TBM_COMMON_PLUGIN_DIR . 'functions/tbm-tinymce.php';

/**
 * Shortcode
 **/
require HTML_TBM_COMMON_PLUGIN_DIR . 'functions/tbm-shortcode.php';

/**
 * SEO
 **/
require HTML_TBM_COMMON_PLUGIN_DIR . 'functions/tbm-seo.php';

/**
 * Customize WP Dashboard
 **/
require HTML_TBM_COMMON_PLUGIN_DIR . 'functions/tbm-admin-dashboard.php';
