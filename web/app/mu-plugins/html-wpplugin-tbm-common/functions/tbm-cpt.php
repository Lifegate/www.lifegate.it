<?php
/**
 * Created by PhpStorm.
 * User: Francesco
 * Date: 29/10/2018
 * Time: 15:21
 */

// Register Sponsor Custom Taxonomy
function custom_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Sponsor e Partner', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Sponsor e Partner', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Sponsor e Partner', 'text_domain' ),
		'all_items'                  => __( 'All Items', 'text_domain' ),
		'parent_item'                => __( 'Parent Item', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
		'new_item_name'              => __( 'New Item Name', 'text_domain' ),
		'add_new_item'               => __( 'Add New Item', 'text_domain' ),
		'edit_item'                  => __( 'Edit Item', 'text_domain' ),
		'update_item'                => __( 'Update Item', 'text_domain' ),
		'view_item'                  => __( 'View Item', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Items', 'text_domain' ),
		'search_items'               => __( 'Search Items', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
		'no_terms'                   => __( 'No items', 'text_domain' ),
		'items_list'                 => __( 'Items list', 'text_domain' ),
		'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
	);
	$args   = array(
		'labels'             => $labels,
		'hierarchical'       => false,
		'public'             => false,
		'show_ui'            => true,
		'show_admin_column'  => true,
		'show_in_nav_menus'  => false,
		'show_tagcloud'      => false,
		'show_in_rest'       => false,
		'show_in_quick_edit' => false,
		'meta_box_cb'        => false
	);


	/**
	 * Filters the post type where enable the sponsor taxonomy.
	 *
	 * @param array Array of post types
	 *
	 */
	$tbm_sponsor_post_types = apply_filters( 'tbm_sponsor_post_types', array( 'post' ) );

	register_taxonomy( 'sponsor', $tbm_sponsor_post_types, $args );

}

add_action( 'init', 'custom_taxonomy', 0 );