<?php
/**
 * Created by PhpStorm.
 * User: francesco
 * Date: 23/04/2018
 * Time: 22:12
 */

/**
 * Rimuove un po' di schifezze dall'header
 */
function theme_init() {
	remove_action( 'wp_head', 'feed_links_extra', 3 );
	remove_action( 'wp_head', 'index_rel_link' );
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	remove_action( 'wp_head', 'rsd_link' );
	remove_action( 'wp_head', 'wlwmanifest_link' );
	remove_action( 'wp_head', 'wp_generator' );
	remove_action( 'wp_head', 'rel_canonical' );
}

add_action( 'init', 'theme_init' );


/**
 * Funzioni soil
 */

add_theme_support( 'soil-clean-up' );
add_theme_support( 'soil-disable-trackbacks' );


/**
 * Remove invalid rel attribute values in the categorylist
 */


add_filter( 'the_category', function ( $thelist ) {
	return str_replace( 'rel="category tag"', 'rel="tag"', $thelist );
} );


/**
 * Add page slug to body class
 *
 * @param $classes Classes defined in body
 *
 * @return array
 */
function tbm_add_slug_to_body_class( $classes ) {
	global $post;
	if ( is_home() ) {
		$key = array_search( 'blog', $classes, true );
		if ( $key > - 1 ) {
			unset( $classes[ $key ] );
		}
	} elseif ( is_page() ) {
		$classes[] = sanitize_html_class( $post->post_name );
	} elseif ( is_singular() ) {
		$classes[] = sanitize_html_class( $post->post_name );
	}

	return $classes;
}

add_filter( 'body_class', 'tbm_add_slug_to_body_class' );

/**
 * pmi functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package pmi
 */


add_action( 'after_setup_theme', function () {
	load_theme_textdomain( 'tbm', get_template_directory() . '/languages' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );

	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	add_post_type_support( 'page', 'excerpt' );
	add_theme_support( 'customize-selective-refresh-widgets' );

	// Remove comment feed
	add_filter( 'feed_links_show_comments_feed', '__return_false' );


} );

/**
 * Add Site logo to login page
 */
add_action( 'login_enqueue_scripts', function () {
	$logo = get_field( 'tbm_site_amp_logo', 'option' ) ? tbm_wp_get_attachment_image_url( get_field( 'tbm_site_amp_logo', 'option' ), array(220,0) ) : tbm_assets_url( 'images/login-logo.png', true );
	?>

    <style type="text/css">
        #login h1 {
            margin-bottom: 24px;
            width: 100%;
            left: 0;
        }
        #login h1 a, .login h1 a {
            background-image: url('<?php echo $logo; ?>');
            background-size: 100% !important;
            background-position: center top;
            background-repeat: no-repeat;
            -webkit-background-size: 256px;
            color: #999;
            margin: 0 auto;
            padding: 0;
            width: auto;
            text-indent: -9999px;
            outline: none;
            overflow: hidden;
            display: block;
            -webkit-transition: none;
            transition: none;
        }
    </style>
<?php } );


/**
 * Change logo link name in login page
 */
function tbm_custom_login_name_url() {
	return get_bloginfo( 'name' );
}

if ( version_compare( $GLOBALS['wp_version'], '5.2', '<' ) ) {
	add_filter( 'login_headertitle', 'tbm_custom_login_name_url' );
} else {
	add_filter( 'login_headertext', 'tbm_custom_login_name_url' );
}


/**
 * Disabilita gravatar
 */

add_filter( 'user_profile_picture_description', function ( $desc ) {
	return "";
} );


/**
 * Remove HTML filter from $term description
 */

$filters = array( 'pre_term_description' );

foreach ( $filters as $filter ) {
	remove_filter( $filter, 'wp_filter_kses' );
}

foreach ( array( 'term_description' ) as $filter ) {
	remove_filter( $filter, 'wp_kses_data' );
}


/**
 * Replace custom CTA in newsletter form
 */
add_filter( 'newsletter_html_parser_template', function ( $html_newsletter ) {
	if ( is_admin() ) {
		return $html_newsletter;
	}
	$nl_cta = get_field( 'cta_newsletter' );
	if ( ! empty( $nl_cta ) ) {
		$html_newsletter = str_replace( '[TITLE]', $nl_cta, $html_newsletter );
	}

	return $html_newsletter;
}, 10, 3 );


/**
 * Disabilito la querystring di post type dalle breadcrumb
 * Cfr https://mtekk.us/archives/guides/how-to-remove-post_type-from-breadcrumb-navxt-urls/
 */

add_filter( 'bcn_add_post_type_arg', function ( $add_query_arg, $type, $taxonomy ) {
	return false;
}, 10, 3 );
