<?php
/**
 * Created by PhpStorm.
 * User: francesco
 * Date: 02/02/2019
 * Time: 13:04
 */


/**
 * Enqueue scripts in the WordPress admin
 *
 * @param int $hook Hook suffix for the current admin page.
 */
add_action( 'admin_enqueue_scripts', function ( $hook ) {

	// Enqueue custom js functions
	if ( $hook === 'post-new.php' || $hook === 'post.php' ) {
		wp_enqueue_script( 'tbm_common_admin', HTML_TBM_COMMON_PLUGIN_URL . 'dist/js/admin_common_footer.min.js', array( 'jquery' ), HTML_TBM_COMMON_PLUGIN_VERSION, true );
		wp_localize_script( 'tbm_common_admin', 'tbmCommon', array(
			'url'     => HTML_TBM_COMMON_PLUGIN_URL,
			'version' => HTML_TBM_COMMON_PLUGIN_VERSION
		) );
	}

} );
