<?php
//Gridonic
use Gridonic\JsonResponse\ErrorJsonResponse;
use Gridonic\JsonResponse\SuccessJsonResponse;

/**
 * Deprecated functions
 */
function tmb_plural_handle( $count = 1, $singular = 'nome', $plural = 'nomi' ) {
	return tbm_plural_handle( $count, $singular, $plural );
}

function tmb_get_card_cover( $post = null ) {
	tbm_get_card_cover( $post );
}

function tbm_get_sponsor_term( $term_id, $taxonomy ) {
	tbm_get_formatted_sponsor_term( $term_id, $taxonomy );
}

function tbm_send_rocket_notifications( $message = array(), $level = 'warning', $interval = 10, $channel = 'alert' ) {
	return false;
}

/**
 * Get the post type in new or edit admin screen
 *
 * @return false|string|WP_Screen|null
 */
function tbm_get_post_type_in_admin() {
	global $post, $parent_file, $typenow, $current_screen, $pagenow;

	$post_type = null;

	if ( $post && ( property_exists( $post, 'post_type' ) || method_exists( $post, 'post_type' ) ) ) {
		$post_type = $post->post_type;
	}

	if ( empty( $post_type ) && ! empty( $current_screen ) && ( property_exists( $current_screen, 'post_type' ) || method_exists( $current_screen, 'post_type' ) ) && ! empty( $current_screen->post_type ) ) {
		$post_type = $current_screen->post_type;
	}

	if ( empty( $post_type ) && ! empty( $typenow ) ) {
		$post_type = $typenow;
	}

	if ( empty( $post_type ) && function_exists( 'get_current_screen' ) ) {
		$post_type = get_current_screen();
	}

	if ( empty( $post_type ) && isset( $_REQUEST['post'] ) && ! empty( $_REQUEST['post'] ) && function_exists( 'get_post_type' ) && $get_post_type = get_post_type( (int) $_REQUEST['post'] ) ) {
		$post_type = $get_post_type;
	}

	if ( empty( $post_type ) && isset( $_REQUEST['post_type'] ) && ! empty( $_REQUEST['post_type'] ) ) {
		$post_type = sanitize_key( $_REQUEST['post_type'] );
	}

	if ( empty( $post_type ) && 'edit.php' == $pagenow ) {
		$post_type = 'post';
	}

	if ( empty( $post_type ) && 'post-new.php' == $pagenow ) {
		$post_type = 'post';
	}

	return $post_type;
}

/**
 * Get the critical css
 * /dist/css/custom.min.css?ver=
 */
/**
 * Returns the HTML code for critical css path inclusion.
 *
 * @param array $critical_css_path The path, relative to te theme, where we can find the critical css. Example '/dist/css/critical/homepage--critical.css'
 * @param array $custom_css_paths The path, relative to the theme, where we can find the main css. Example '/dist/css/custom.min.css'
 * @param string $ver The version to append. If
 *
 * @return string
 */
function tbm_critical_css( $critical_css_paths = array(), $custom_css_paths = array(), $ver = '000' ) {

	$critical_css = array();
	$custom_css   = array();

	if ( ! $critical_css_paths || ! $custom_css_paths ) {
		return '';
	}

	/**
	 * Cast to array
	 */
	if ( ! is_array( $critical_css_paths ) ) {
		$critical_css_paths = (array) $critical_css_paths;
	}

	if ( ! is_array( $custom_css_paths ) ) {
		$custom_css_paths = (array) $custom_css_paths;
	}

	// Get the theme object
	$tbm_theme = wp_get_theme();

	if ( $tbm_theme->exists() && $ver === '000' ) {
		$ver = $tbm_theme->get( 'Version' ) ?: '000';
	}

	foreach ( $critical_css_paths as $critical_css_path ) {
		$critical_css[] = file_get_contents( get_theme_file_path() . $critical_css_path );
	}

	foreach ( $custom_css_paths as $custom_css_path ) {
		$css          = get_theme_file_uri() . $custom_css_path . '?ver=' . $ver;
		$custom_css[] = sprintf( '<link rel="stylesheet" href="%s" media="print" onload="this.media=\'all\'">', $css );
	}

	$deferred_css        = implode( '', $custom_css );
	$critical_css_source = implode( '', $critical_css );

	return sprintf( '<style>%s</style>%s', $critical_css_source, $deferred_css );
}

/**
 * Fire the code to include in head (before wp_head() )
 *
 * @since 4.2.0
 */
function tbm_head() {

	// If not active ACF
	if ( ! function_exists( 'get_field' ) ) {
		return false;
	}

	\Tbm_Plugins\TBM_tms::print_active_zones();

	if ( is_tax() ) {
		if ( get_field( 'adv_disabled', get_queried_object() ) ) {
			\Tbm_Plugins\TBM_tms::disable_banner();
		}
	} else {
		if ( get_field( 'adv_disabled' ) ) {
			\Tbm_Plugins\TBM_tms::disable_banner();
		}
	}

	if ( get_field( 'viewmax_disabled' ) ) {
		\Tbm_Plugins\TBM_tms::disable_viewmax();
	}

	\Tbm_Plugins\TBM_tms::include_dataLayer();


	// Embed Atatus
	if ( get_field( 'tbm_common_enable_atatus', 'option' ) && ! empty( get_field( 'tbm_common_code_atatus', 'option' ) ) ) {
		\Tbm_Plugins\TBM_tms::atatus_tag( get_field( 'tbm_common_code_atatus', 'option' ) );
	}

	// Embed GTM
	if ( get_field( 'tbm_common_enable_gtm', 'option' ) && ! empty( get_field( 'tbm_common_code_gtm', 'option' ) ) ) {
		\Tbm_Plugins\TBM_tms::gtm_script_tag( get_field( 'tbm_common_code_gtm', 'option' ) );
	}

	// Embed Tealium
	if ( get_field( 'tbm_common_enable_tealium', 'option' ) && ! empty( get_field( 'tbm_common_code_tealium', 'option' ) ) ) {
		\Tbm_Plugins\TBM_tms::tealium_tag( get_field( 'tbm_common_code_tealium', 'option' ) );
	}

}

/**
 * Return 404 template and set status header 404 using as a filter function
 * Example: add_filter( 'template_include', 'tbm_404_template_filter' );
 *
 */
function tbm_404_template_filter() {

	status_header( 404 );
	global $wp_query;
	$wp_query->set_404();
	if ( function_exists( '\App\locate_template' ) ) {
		return \App\locate_template( '404' );
	} else {
		return locate_template( '404' );
	}


	exit;
}

/**
 * Include 404 template and set status header (default 404) (use IN a template filter function)
 *
 * Example:
 * <code>
 * add_filter( 'template_include', function ( $template ) {
 *     if ( is_single( 774083 ) ) {
 *         tbm_set_404(); //default header status is 404
 *     }
 *
 *     return $template;
 * }, 99 );
 * </code>
 *
 * @param int $header_status The header status to set
 */
function tbm_set_404( $header_status = 404 ) {
	status_header( $header_status );
	global $wp_query;
	$wp_query->set_404();
	if ( function_exists( '\App\locate_template' ) ) {
		include( \App\locate_template( '404' ) );
	} else {
		include( locate_template( '404.php' ) );
	}

	exit;
}

/**
 * Remove content from Rest response (to avoid easy stealing)
 */
add_filter( 'rest_prepare_post', function ( $data, $post, $request ) {

	// If not active ACF
	if ( ! function_exists( 'get_field' ) ) {
		return $data;
	}

	$params = $request->get_params();

	// Check full querystring
	if ( isset( $params['full'] ) && $params['full'] == true ) {
		return $data;
	}

	// Check the theme option
	if ( empty( get_field( 'tbm_common_rest_hide_content', 'option' ) ) ) {
		return $data;
	}

	$_data = $data->data;

	// Check if is not a direct request
	if ( ! isset( $params['id'] ) ) {
		unset( $_data['content'] );
	}

	$data->data = $_data;

	return $data;
}, 10, 3 );

/**
 * Returns post type name (label field)
 *
 * @param string $posttype The name of post type
 */
function tbm_get_post_type_label( $posttype ) {
	$label = '';
	$obj   = get_post_type_object( $posttype );
	if ( $obj ) {
		$label = $obj->labels->singular_name;
	}

	return $label;
}

/**
 * Convert search results from /?s=query to /cerca/query/
 */
add_action( 'template_redirect', function () {
	global $wp_rewrite;
	if ( ! isset( $wp_rewrite ) || ! is_object( $wp_rewrite ) || ! $wp_rewrite->get_search_permastruct() ) {
		return;
	}
	$search_base = $wp_rewrite->search_base;
	if ( is_search() && ! is_admin() && strpos( $_SERVER['REQUEST_URI'], "/{$search_base}/" ) === false && strpos( $_SERVER['REQUEST_URI'], ' & ' ) === false ) {
		wp_redirect( get_search_link() );
		exit();
	}
} );

/**
 * Check if a tag is speciale
 *
 * @return mixed
 */
function tbm_is_speciale() {
	if ( function_exists( 'get_field' ) ) {
		$term = get_queried_object();

		return get_field( 'speciale', $term );
	}
}

/**
 * Get the URL of an image attachment, rewrited with Thumbor (if available and activated).
 *
 * @param int $attachment_id Image attachment ID.
 * @param array $size Optional. Image size to retrieve. Accepts array of height and width dimensions. Default 50,50.
 * @param bool $metadata Optional. If true, returns the image metadata and not the image itself. Default false.
 * @param bool $strip_dimensions Optional. If true, remove the image dimensions (p.es. 800x200) from filename. Default false.
 *
 * @return string|false Attachment URL rewrited with Thumbor, regular attachment URL or false if no image is available.
 */
function tbm_wp_get_attachment_image_url(
		$attachment_id = 0, $size = array(
		50,
		50
), $metadata = false, $strip_dimensions = false
) {

	if ( empty( $attachment_id ) ) {
		return '';
	}

	if ( function_exists( 'tbm_get_thumbor_img' ) ) {
		remove_filter( 'wp_get_attachment_image_src', 'tbm_fix_thumbnail_img', 1, 2 );
		remove_filter( 'wp_calculate_image_srcset', 'tbm_fix_srcset_img', 10, 3 );
		$src = wp_get_attachment_image_src( $attachment_id,'full' );

		if ( ! isset( $src['0'] ) ) {
			return '';
		}
		$width  = isset( $size[0] ) ? $size[0] : 0;
		$height = isset( $size[1] ) ? $size[1] : 0;
		add_filter( 'wp_get_attachment_image_src', 'tbm_fix_thumbnail_img', 1, 2 );
		add_filter( 'wp_calculate_image_srcset', 'tbm_fix_srcset_img', 10, 3 );

		return tbm_get_thumbor_img( $src['0'], $width, $height, $metadata, $strip_dimensions );
	}

	$image = wp_get_attachment_image_src( $attachment_id, $size, false );

	return isset( $image['0'] ) ? $image['0'] : false;

}

/**
 * Return the post thumbnail URL, rewrited with Thumbor (if available and activated).
 *
 * @param int|WP_Post $post Optional. Post ID or WP_Post object.  Default is global `$post`.
 * @param array $size Optional. Image size to retrieve. Accepts array of height and width dimensions. Defaults 50,50.
 * @param bool $metadata Optional. Returns thumbor metadata
 * @param int $default Optional. Default thumb ID to print if the thumbnail url is not found
 * @param bool $strip_dimensions Optional. If true, remove the image dimensions (p.es. 800x200) from filename. Default false.
 *
 * @return string|false Post thumbnail URL rewrited with Thumbor, regular post thumbnail URL or false if no URL is available.
 */
function tbm_get_the_post_thumbnail_url(
		$post = null, $size = array(
		50,
		50
), $metadata = false, $default = '', $strip_dimensions = false
) {

	$post = get_post( $post );
	if ( empty( $post ) ) {
		return '';
	}

	$default = empty( $default ) && defined( 'DEFAULT_IMAGE_ID' ) ? DEFAULT_IMAGE_ID : $default;

	$post_thumbnail_id = has_post_thumbnail( $post->ID ) ? get_post_thumbnail_id( $post->ID ) : false;

	if ( ! $post_thumbnail_id && ! $default ) {
		return false;
	}

	if ( ! $post_thumbnail_id && $default ) {
		$post_thumbnail_id = $default;
	}


	if ( is_array( $size ) && function_exists( 'tbm_get_thumbor_img' ) ) {
		// Remove filter added by Thumbor HTML.it plugin
		remove_filter( 'wp_get_attachment_image_src', 'tbm_fix_thumbnail_img', 1, 2 );
		remove_filter( 'wp_calculate_image_srcset', 'tbm_fix_srcset_img', 10, 3 );

		$width  = $size[0];
		$height = $size[1];
		$src    = wp_get_attachment_image_url( $post_thumbnail_id, 'full' );
		add_filter( 'wp_get_attachment_image_src', 'tbm_fix_thumbnail_img', 1, 2 );
		add_filter( 'wp_calculate_image_srcset', 'tbm_fix_srcset_img', 10, 3 );

		return tbm_get_thumbor_img( $src, $width, $height, $metadata, $strip_dimensions );

	}

	return wp_get_attachment_image_url( $post_thumbnail_id, $size );
}

/**
 * Returns the second level domain from ad url (eg. wallstreetitalia from https://www.wallstreetitalia.com/pil-giuseppe-conte-spread/)
 *
 * @param $url string A valid url
 *
 * @return mixed|string
 */
function tbm_get_domain( $url = '' ) {
	if ( ! isset( $_SERVER['HTTP_HOST'] ) ) {
		return;
	}

	if ( ! filter_var( $url, FILTER_VALIDATE_URL ) ) {
		$url = 'http://' . $_SERVER['HTTP_HOST'];
	}

	$array    = array_slice( explode( '.', parse_url( $url )['host'] ), - 2, 1 );
	$hostname = reset( $array );

	return $hostname;
}

/**
 * Outputs a banner content
 *
 * @param string $name The banner zone name
 * @param string $_before An optional text to prepend to banner content
 * @param string $_after An optional text to append to banner content
 * @param bool $display True to echo result, false to return
 * @param bool $disable_div True to disable the div surrounding the code, false to nothing
 *
 * @return string
 */
function tbm_get_the_banner( $name = '', $_before = '', $_after = '', $display = true, $disable_div = false ) {

	if ( is_single() ) {
		$disable_banner = get_field( 'adv_disabled', get_the_ID() );
	} elseif ( is_tax() ) {
		$disable_banner = get_field( 'adv_disabled', get_queried_object() );
	} else {
		$disable_banner = false;
	}

	$banner = '';
	// If is a  post, page or post type preview, output nothing
	if ( is_preview() ) {
		$banner = '';
	} elseif ( class_exists( 'TBM_Advertising' ) && $disable_banner && ( $name == 'TRIBOOADV' || $name == 'HEAD' ) ) {
		$banner = TBM_Advertising::output( $name, '', '', false, $disable_div );
	} elseif ( class_exists( 'TBM_Advertising' ) && $disable_banner ) {
		$banner = '';
	} elseif ( class_exists( 'TBM_Advertising' ) ) {
		$banner = TBM_Advertising::output( $name, $_before, $_after, false, $disable_div );
	} elseif ( class_exists( 'MP_Advertising' ) && $disable_banner && ( $name == 'TRIBOOADV' || $name == 'HEAD' ) ) {
		$banner = MP_Advertising::output( $name, '', '', false, $disable_div );
	} elseif ( class_exists( 'MP_Advertising' ) && $disable_banner ) {
		$banner = '';
	} elseif ( class_exists( 'MP_Advertising' ) ) {
		$banner = MP_Advertising::output( $name, $_before, $_after, false, $disable_div );
	} elseif ( $name && class_exists( 'HTMLIT_Advertising' ) ) {
		$banner = HTMLIT_Advertising::output( $name, $_before, $_after, false, $disable_div );
	} elseif ( $name && class_exists( 'HTML_Advertising' ) ) {
		$banner = HTML_Advertising::output( $name, $_before, $_after, false, $disable_div );
	}
	if ( $display ) {
		echo $banner;
	} else {
		return $banner;
	}
}

/**
 * Get first paragraph from a WordPress post
 *
 * @param int|WP_Post $post Optional. Post ID or post object. Default is the global `$post`.
 * @param int $numbers Numbers of paragraph to return
 *
 * @return string
 */
function tbm_get_paragraph( $post, $numbers = 4 ) {
	$post = get_post( $post );

	if ( empty( $post ) ) {
		return '';
	}

	$out = array();
	$i   = 0;

	$content = wpautop( get_the_content( '', '', $post ) );
	$content = strip_shortcodes( $content );
	$content = str_replace( '<p><!-- wp:paragraph --></p>', '', $content );
	$content = preg_replace( '#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content );
	$content = preg_replace( '~\s?<p>(\s|&nbsp;)+</p>\s?~', '', $content );

	$dom = new \DOMDocument;
	@$dom->loadHTML( '<?xml encoding="utf-8" ?>' . $content );
	foreach ( $dom->getElementsByTagName( 'p' ) as $tag ) {

		if ( $numbers <= $i ) {
			return '<p>' . implode( '</p>' . PHP_EOL . '<p>', $out ) . '</p>';
		}

		$out[] = $tag->nodeValue;
		$i ++;
	}

}

/**
 * Insert something after a specific <p> paragraph in WP content.
 *
 * @param string $insertion Code to insert: HTML markup, ad script code etc.
 * @param int $paragraph_id After which paragraph should the insertion be added. Starts at 1.
 * @param string $content WP pr pther content
 * @param bool $display If true, echoes the results, otherwise returns it
 *
 * @return string               Likely HTML markup.
 */
function tbm_insert_after_paragraph( $insertion, $paragraph_id, $content, $display = 0 ) {

	/**
	 * new DOM instance
	 */
	$dom = new \DOMDocument;
	@$dom->loadHTML( '<?xml encoding="utf-8" ?>' . $content );

	/**
	 *
	 */
	if ( empty( $paragraph_id ) ) {
		return false;
	}

	$item = $paragraph_id;

	/**
	 * Check if we have <p>
	 */
	$nodes = $dom->getElementsByTagName( 'p' )->item( $item );

	if ( empty( $nodes ) ) {
		return false;
	}

	/**
	 * Create new fragment
	 */
	$template = $dom->createDocumentFragment();
	$template->appendXML( $insertion );

	/**
	 * InsertBefore
	 */
	$nodes->parentNode->insertBefore( $template, $nodes );

	/**
	 * Strip body
	 */
	$body_elements = $dom->getElementsByTagName( 'body' );
	$body          = $body_elements->item( 0 );

	/**
	 * Extract HTML
	 */
	$output = $dom->saveHTML( $body );

	/**
	 * Remove <body> and </body> and white spaces
	 */
	$output = substr( $output, 6, - 7 );
	$output = trim( $output );

	if ( $display ) {
		echo $output;
	} else {
		return $output;
	}

}

/**
 * Insert something before a specific tag in content.
 *
 * @param string $insertion Code to insert: HTML markup, ad script code etc.
 * @param string $tag An HTML tag to search in content; without < and >
 * @param int $position_id Before which occurence of tag should the insertion be added. Starts at 1.
 * @param string $content WP o other content
 * @param int|bool $display Whether to echo or return the results. Accepts 0|false for return,
 *                            1|true for echo. Default 0|false.
 * @param string $min Minimun numbers of tags being present in $content (if less, no new content is inserted).
 *
 * @return string               Likely HTML markup.
 */
function tbm_insert_before_tag( $insertion, $tag, $position_id, $content, $display = 0, $min = 1 ) {

	// The real min is + 1
	if ( $min > 0 ) {
		$min = $min + 1;
	}

	if ( strpos( $content, $tag ) === false ) {
		return false;
	}


	$closing_tag     = ' </' . $tag . ' > ';
	$open_tag        = '<' . $tag . ' > ';
	$open_tag_needle = '<' . $tag;

	// Rimuovo paragrafi vuoti
	$content = preg_replace( '#' . $open_tag . '([ \n\r\t]+|&nbsp;)?' . $closing_tag . '#', '', $content );

	$tags = explode( $closing_tag, $content );

	if ( count( $tags ) < $min ) {
		return $content;
	}

	foreach ( $tags as $index => $item ) {
		if ( trim( $item ) && strpos( $item, $open_tag_needle ) ) {
			$tags[ $index ] .= $closing_tag;
		}

		if ( $position_id == $index + 1 ) {
			$tags[ $index ] = str_replace( $open_tag_needle, $insertion . $open_tag_needle, $tags[ $index ] );
		}
	}

	$code = implode( '', $tags );

	if ( $display ) {
		echo $code;
	} else {
		return $code;
	}
}

/**
 * Returns url, name and image of a sponsor
 *
 * @param int $sponsor_term_id The id of the term
 *
 * @return array|string
 */
function tbm_get_sponsor( $sponsor_term_id ) {

	$term = get_term( $sponsor_term_id, 'sponsor' );

	if ( empty( $term ) || is_wp_error( $term ) ) {
		return '';
	}

	$id = $term->taxonomy . '_' . $term->term_id;

	$sponsor_title       = $term->name;
	$sponsor_description = $term->description;
	$sponsor_name        = get_field( 'sponsor_name', $id );
	$sponsor_img_id      = get_field( 'sponsor_img', $id );
	$sponsor_url         = get_field( 'sponsor_landing', $id );
	$sponsor_type        = get_field( 'sponsor_type', $id ) ?: 'tbm_sponsor_sponsor';

	/**
	 * Get the image link
	 */
	if ( $sponsor_img_id ) {
		$sponsor_img_url = wp_get_attachment_url( $sponsor_img_id );
	}

	/**
	 * Return the array
	 */
	return array(
			'description' => $sponsor_description,
			'title'       => $sponsor_title,
			'name'        => $sponsor_name,
			'image'       => $sponsor_img_url,
			'url'         => $sponsor_url,
			'imageId'     => $sponsor_img_id,
			'type'        => $sponsor_type,
	);

}

/**
 * Print the html code to show sponsor
 *
 * @param $sponsor WP Term object relative to the sponsor term
 *
 * @return string
 */
function tbm_sponsor( $sponsor ) {
	if ( ! is_single() ) {
		$sponsor = array_shift( $sponsor );
	}
	$post_id = $sponsor->taxonomy . '_' . $sponsor->term_id;
	// Get the term meta from ACF
	$output         = array();
	$output['name'] = get_field( 'sponsor_name', $post_id ) ? get_field( 'sponsor_name', $post_id ) : "";
	$sponsor_img_id = get_field( 'sponsor_img', $post_id );
	$style          = '<style>.mycompany-id:after,.mycompany-id:before,.mycompany-id>div a:after,.mycompany-id>div a:before{content:\'\';display:table}.mycompany-id:after,.mycompany-id>div a:after{clear:both}.mycompany-id{margin:0 0 8px;display:inline-block}.mycompany-id>div{float:left}.mycompany-id>div p{line-height:30px;font-style:italic}.mycompany-id>div span{color:#343434;font-size:16px;font-weight:700;line-height:30px;padding:0;float:left}.mycompany-id>div a span{color:#FF9000;text-decoration:underline}.mycompany-id>div img{display:blocK;width:auto;max-height:30px;margin:0 8px 0 8px;float:left}</style>';
	if ( $sponsor_img_id ) {
		$output['img'] = wp_get_attachment_url( $sponsor_img_id );
	}
	$output['url'] = get_field( 'sponsor_landing', $post_id );

	if ( isset( $output['img'] ) ) {
		$return_sponsor = isset( $output['url'] ) && ! empty( $output['url'] ) ? "<img src='{$output['img']}' /><span>{$output['name']}</span></a>" : "<img src='{$output['img']}' /><span>{$output['name']}</span>";
	} else {
		$return_sponsor = isset( $output['url'] ) && ! empty( $output['url'] ) ? "<a target='_blank' rel='nofollow' href='{$output['url']}'><span>{$output['name']}</span></a>" : "<span>{$output['name']}</span>";
	}

	$return = sprintf( '<div><p class="abstract">' . __( 'A cura di', 'lifegate' ) . '&nbsp;</p></div><div>%s</div>',
			$return_sponsor
	);
	if ( $return ) {
		return $style . '<aside class="mycompany-id">' . $return . '</aside>';
	}
}

function tbm_get_formatted_sponsor_term( $term_id, $taxonomy ) {

	$post_id = $taxonomy . '_' . $term_id;

	$sponsors = get_field( 'speciale_sponsor', $post_id );
	if ( empty( $sponsors ) ) {
		return false;
	}

	echo tbm_sponsor( $sponsors );
}

/**
 * Get the speciale (one or more) related to the post
 *
 * @param int|WP_Post $post Optional. Post ID or post object. Default is the global `$post`.
 *
 * @return string
 */
function tbm_get_speciale_terms( $post = null ) {
	$post = get_post( $post );
	if ( empty( $post ) ) {
		return '';
	}

	$args  = array(
			'meta_query' => array(
					array(
							'key'   => 'speciale',
							'value' => true,
					),
			)
	);
	$terms = wp_get_object_terms( $post->ID, 'post_tag', $args );
	if ( $terms ) : ?>
		<div class="inspecial">
			<div>
				<span>Tratto dallo speciale: </span>
				<ul>
					<?php
					foreach ( $terms as $term ) :
						echo '<li class="inspecial__item"><a href="' . get_term_link( $term, 'post_tag' ) . '">' . $term->name . '</a></li>';
					endforeach;
					?>
				</ul>
			</div>
		</div>
	<?php
	endif;
}

/**
 * Returns or echoes the code of the related sponsor
 *
 * @param int|WP_Post $post Optional. Post ID or post object. Default is the global `$post`.
 * @param bool $echo True to echo the results, false to returns
 *
 * @return bool|string
 */
function tbm_get_sponsor_post( $post = null, $echo = true ) {
	$post = get_post( $post );
	if ( empty( $post ) ) {
		return '';
	}

	$sponsor_term = get_field( 'post_sponsor', $post->ID );

	if ( empty( $sponsor_term ) ) {
		return false;
	}

	if ( ! is_a( $sponsor_term, 'WP_Term' ) ) {
		$sponsor_term = get_term( $sponsor_term );
	}

	if ( $echo ) {
		echo tbm_sponsor( $sponsor_term );
	} else {
		return tbm_sponsor( $sponsor_term );
	}

}

/**
 * Get the cover (the first page) of the card post type
 *
 * @param $post
 *
 * @return string
 */
function tbm_get_card_cover( $post = null ) {
	$post = get_post( $post );
	if ( empty( $post ) ) {
		return '';
	}

	if ( ! has_post_thumbnail( $post ) ) {
		return '';
	}

	$sponsor = tbm_get_sponsor_post( $post );

	$out = $sponsor;
	$out .= '<picture>
                <!--[if IE 9]>
                <video style="display: none;"><![endif]-->
                <source srcset="' . tbm_get_the_post_thumbnail_url( $post->ID, array( 320, 172 ) ) . '"
                        media="(max-width: 736px)"/>
                <!--[if IE 9]></video><![endif]-->
                <img class="lazyload" data-srcset="' . tbm_get_the_post_thumbnail_url( $post->ID, array( 662, 357 ) ) . '"
                     alt="' . tbm_get_the_post_thumbnail_alt() . '"/>
            </picture>
			<div class="article-description">
	            <i>' . get_the_excerpt() . '</i>
                <div class="card-author_block__author">
                    <div>
                        <p>di ' . tbm_get_the_author() . '</p>
                    </div>
                </div>
	        </div>';

	return $out;
}

/**
 * Restituisce una parola al plurale o singolare in base ad un numero: tmb_plural_handle($number,'nome','nomi');
 *
 * @param int $count Il numero su cui basare la gestione dei plurali
 * @param string $singular Il nome al singolare
 * @param string $plural Il nome al plurale
 *
 * @return string
 */
function tbm_plural_handle( $count = 1, $singular = 'nome', $plural = 'nomi' ) {
	return abs( $count ) >= 2 ? $plural : $singular;
}

function tbm_get_post_source( $post, $before = '', $after = '' ) {
	$post = get_post( $post );
	if ( empty( $post ) ) {
		return '';
	}
	$name = get_field( '_fonte_name' );
	$url  = get_field( '_fonte_url' );

	if ( $name && $url )  :
		$source = sprintf(
				'<a href="%s" class="text-link text-link--special" target="_blank" rel="nofollow"><span class="underline">%s</span><i class="ico-external"></i></a>',
				$url,
				$name );

		return ( $before . $source . $after );
	endif;

	return false;

}

function tbm_get_post_via( $post, $before = '', $after = '' ) {
	$post = get_post( $post );
	if ( empty( $post ) ) {
		return '';
	}
	$name = get_field( '_via_name' );
	$url  = get_field( '_via_url' );

	if ( $name && $url )  :
		$source = sprintf(
				'<a href="%s" class="text-link text-link--special" target="_blank" rel="nofollow"><span class="underline">%s</span><i class="ico-external"></i></a>',
				$url,
				$name );

		return ( $before . $source . $after );
	endif;

	return false;

}

function tbm_get_thumb_credit( $thumbid = 0, $before = '', $after = '' ) {
	if ( empty( $thumbid ) ) {
		return '';
	}

	if ( get_post_type( $thumbid ) !== 'attachment' ) {
		return '';
	}

	$credit_source   = esc_attr( get_post_meta( $thumbid, '_wp_attachment_source_name', true ) );
	$credit_link     = esc_url( get_post_meta( $thumbid, '_wp_attachment_source_url', true ) );
	$source_dofollow = esc_attr( get_post_meta( $thumbid, '_wp_attachment_source_dofollow', true ) );

	if ( ! empty( $credit_source ) ) {
		if ( empty( $credit_link ) ) {
			$credits = $credit_source;
		} else {
			if ( ( $source_dofollow == true ) || ( $source_dofollow == 1 ) ) {
				$credits = '<a href="' . $credit_link . '" target="_blank">' . $credit_source . '</a>';
			} else {
				$credits = '<a href="' . $credit_link . '" rel="nofollow" target="_blank">' . $credit_source . '</a>';
			}
		}

		return ( $before . $credits . $after );
	}

	return false;

}

function tbm_has_title( $id = 0 ) {
	$post = get_post( $id );

	return ( ! empty( $post->post_title ) );
}

function tbm_get_logo( $img_class = '', $a_class = '' ) {
	$before = '<a class="' . $a_class . '" href="' . get_home_url() . '">';
	$after  = '</a>';

	return sprintf( '%s<img class="' . $img_class . '" src="' . pmi_get_assets_url( 'images/logo-pmi.png' ) . '"/>%s', $before, $after );
}

/**
 * Return the path the $path to theme path uri (use without slash: tbm_assets_url('images/logo.png'); ). The value is echoed
 *
 * @param string $path Il percorso del file al di sotto dell'uri. Required
 * @param bool $return If true, returns the value. If false, echo the value. Default is false
 *
 * @return string
 */
function tbm_assets_url( $path = '', $return = false ) {
	if ( ! $path ) {
		return '';
	}
	$path_base = "%s/assets/%s";
	if ( $return ) {
		return sprintf( $path_base, get_template_directory_uri(), $path );
	}
	echo sprintf( $path_base, get_template_directory_uri(), $path );
}

/**
 * Visualizza il nome dell'autore linkato alla sua pagina
 *
 * @param int|WP_Post $post Optional. Post ID or post object. Default is the global `$post`.
 *
 * @return string
 */
function tbm_get_the_author( $post = null ) {
	$post = get_post( $post );
	if ( empty( $post ) ) {
		return '';
	}
	$author_id = get_post_field( 'post_author', $post->ID );
	$text      = '<span class="author vcard"><a class="textual-link text-link author__name" href="%1$s">%2$s</a></span>';
	$args      = array(
			esc_url( get_author_posts_url( $author_id ) ),
			esc_html( get_the_author_meta( 'display_name', $author_id ) )
	);

	return vsprintf( $text, $args );
}

/**
 * Visualizza la data di pubblicazione del post
 *
 * @param int|WP_Post $post Optional. Post ID or post object. Default is the global `$post`.
 *
 * @return string
 */
function tbm_get_pub_date( $post = null ) {
	$post = get_post( $post );
	if ( empty( $post ) ) {
		return '';
	}

	$lang = function_exists( 'pll_current_language' ) ? pll_current_language() : 'it';

	$text = '<time class="entry-date published time--' . $lang . '" datetime="%1$s">%2$s</time>';
	$args = array( esc_attr( get_the_date( 'c' ) ), esc_html( get_the_date() ) );

	return vsprintf( $text, $args );
}

/**
 * Visualizza la data di aggiornamento del post
 *
 * @param int|WP_Post $post Optional. Post ID or post object. Default is the global `$post`.
 *
 * @return string
 */
function tbm_get_update_date( $post = null ) {
	$post = get_post( $post );
	if ( empty( $post ) ) {
		return '';
	}
	$text = '<time class="entry-date updated" datetime="%1$s">%2$s</time>';
	$args = array( esc_attr( get_the_modified_date( 'c' ) ), esc_html( get_the_modified_date() ) );

	return vsprintf( $text, $args );
}

/**
 * Restituisce il testo ALT dell'immagine
 *
 * @param int|WP_Post $post Optional. Post ID or WP_Post object. Default is global `$post`.
 *
 * @return string|false Post thumbnail ALT text or false if no ALT text is available.
 *
 */
function tbm_get_the_post_thumbnail_alt( $post = null ) {
	$post_thumbnail_id = get_post_thumbnail_id( $post );
	if ( ! $post_thumbnail_id ) {
		return false;
	}

	$alt_text = get_post_meta( $post_thumbnail_id, '_wp_attachment_image_alt', true );

	if ( $alt_text ) {
		return esc_html( $alt_text );
	}

	return false;
}

/**
 * Formatta elegantemente i messaggi di var_export
 *
 * @param string|array|object $data I dati da mostrare
 * @param bool $die Se interrompere l'esecuzione o no
 */
function tbm_log( $data = null, $die = true ) {
	$style = 'font-family: "Courier New", Courier, monospace;z-index: 999999;background-color: white;position: absolute;top: 0;left: 0;width: 99%;padding: 10px 0 10px 10px;border: 2px solid lightsalmon;';
	echo '<script>document.onkeydown=function(a){a=a||window.event;if("key"in a?"Escape"==a.key||"Esc"==a.key:27==a.keyCode)document.getElementById(\'tbm_log\').style.display="none"};</script>';
	echo "<div id='tbm_log' style='{$style}'><h3 style='margin-top: 10px;font-family: arial'>Log</h3>\n";
	highlight_string( "<?php\n\$data =\n" . var_export( $data, true ) . ";\n?>" );
	echo "</div>\n";
	if ( $die ) {
		die( 'finito' );
	}
}

/**
 * Un helper per la creazione di query via WP_Query. Gli argomenti sono gli stessi di WP_Query
 *
 * @param string/array $category_in
 * @param string/array $tag_in
 * @param string/array $post_not_in
 * @param string/array $tax_query
 * @param string/array $limit
 * @param string/array $post_types
 * @param bool $date
 * @param string $publish
 * @param bool $category_and
 * @param bool $tag_and
 *
 * @return bool|WP_Query
 */
function tbm_helper_build_wpquery( $category_in = false, $tag_in = false, $post_not_in = false, $tax_query = false, $limit = false, $post_types = array( 'post' ), $date = false, $publish = 'publish', $category_and = false, $tag_and = false, $orderby = 'date' ) {
	$args = array();

	if ( ! empty( $category_in ) ) {
		$args['category__in'] = $category_in;
	}

	if ( ! empty( $tag_in ) ) {
		$args['tag__in'] = $tag_in;
	}

	if ( ! empty( $post_not_in ) ) {
		if ( ! empty( $GLOBALS['ids_to_exclude'] ) ) {
			$post_not_in = array_merge( $post_not_in, $GLOBALS['ids_to_exclude'] );
		}
		$args['post__not_in'] = $post_not_in;
	}

	if ( ! empty( $tax_query ) ) {
		$args['tax_query'] = $tax_query;
	}

	if ( ! empty( $limit ) ) {
		$args['posts_per_page'] = $limit;
	}

	if ( ! empty( $post_types ) ) {
		$args['post_type'] = $post_types;
	}

	if ( ! empty( $publish ) && $publish != false ) {
		$args['post_status'] = $publish;
	}

	if ( ! empty( $date ) && $date != false ) {
		$args['date_query'] = $date;
	}

	if ( ! empty( $category_and ) ) {
		$args['category__and'] = $category_and;
	}

	if ( ! empty( $tag_and ) ) {
		$args['tag__and'] = $tag_and;
	}

	$args['orderby'] = $orderby;


	$results = get_posts( $args );

	if ( ! empty( $results ) ) {
		return $results;
	}

	return false;
}

/**
 * La funzione restituisce i contenuti correlati ad uno specifico post. Restituisce array, oggetto o un array semplificata
 *
 * I correlati a fondo post e i correlati nella sidebar dovrebbero seguire questa logica:
 *
 * - Prima, se disponibile, gli articoli correlati a mano con il plugin MRP_get_related_posts
 * - Poi, se non si raggiunge il limite, gli articoli che fanno parte dello stesso trend del post visualizzato (ordinati per data)
 * - Poi, se non si raggiunge il limite, gli articoli che hanno gli stessi identici tag del post visualizzato ( tag__and non tag__in ) (ordinati per data)
 * - Poi, se non si raggiunge il limite, gli articoli che hanno uno fra i tag del post visualizzato ( tag__in ) e la stessa categoria del post visualizzato ( category__and )(ordinati per data)
 * - Poi, se non si raggiunge il limite, gli articoli che hanno le stesse categorie del post visualizzato (category__and non category__in) (ordinati per data)
 * - Poi, se non si raggiunge il limite, gli articoli che hanno una fra le categorie del post visualizzato (category__in) (ordinati per data)
 *
 *
 * @param int|WP_Post $post_id Il post cui correlare i contenuti. Optional. Default is the global `$post`.
 * @param int $limit Il numero massimo di post. Optional. Default 4
 * @param array $ids_to_exclude Id dei post da escludere. Optional.
 * @param string $special La tassonomia di correlazione principale. Optional. Default `trend`.
 * @param string|array $post_type Il post type su cui eseguire da query. Optional. Default `post`.
 * @param bool $simple Se true, restituisce solo ID, titolo, url e post_type e non l'oggetto completo del $post; se 'object' restituisce l'oggetto; se false (default) restituisce l'array da get_posts
 *
 * @return array|bool|mixed|object
 */
function tbm_get_custom_related_posts( $post = null, $limit = 4, $ids_to_exclude = array(), $special = SITE_SPECIAL_TAXONOMY, $post_type = 'post', $simple = false ) {
	$post = get_post( $post );
	if ( empty( $post ) ) {
		return '';
	}

	/**
	 * Create transient key
	 */
	$transient_key = 'tbm_rel_' . hash( 'crc32', serialize( $post->ID . $limit . implode( ',', (array) $ids_to_exclude ) . $special . implode( ',', (array) $post_type ) . $simple ) );

	/**
	 * If transient, output the query
	 */
	if ( false !== ( $result = get_transient( $transient_key ) ) ) {
		return $result;
	}

	// Convert, if not, $post_type in array
	if ( ! is_array( $post_type ) ) {
		$post_type = array( $post_type );
	}

	// Cycle $post_type array to check if the post type exists in theme
	foreach ( $post_type as $key => $pt ) {
		if ( ! post_type_exists( $pt ) ) {
			unset( $post_type[ $key ] );
		}
	}

	$related_posts = array();
	$post_id       = $post->ID;

	// Get the cateories of the post
	$category_array = wp_get_post_categories( $post_id );

	// Get the tags of the post
	$tag_array = array();
	$tags      = wp_get_post_tags( $post_id );
	foreach ( $tags as $tag ) {
		$tag_array[] = $tag->term_id;
	}

	// Get the special taxonomy
	$storie = wp_get_post_terms( $post_id, $special );

	$storie_array = array();
	if ( $storie && ! is_wp_error( $storie ) ) {
		foreach ( $storie as $storia ) {
			$storie_array[] = $storia->slug;
		}
	}

	// Query for special taxonomy
	$tax_query = '';
	if ( sizeof( $storie_array ) > 0 ) {
		$tax_query = array(
				array(
						'taxonomy' => $special,
						'field'    => 'slug',
						'terms'    => $storie_array
				)
		);
	}

	if ( empty( $ids_to_exclude ) ) {
		if ( ! empty( $GLOBALS['ids_to_exclude'] ) ) {
			$ids_to_exclude = $GLOBALS['ids_to_exclude'];
		}
	}

	if ( is_array( $ids_to_exclude ) ) {
		$ids_to_exclude = array_merge( array( $post_id ), $ids_to_exclude );
	} else {
		$ids_to_exclude = array( $post_id );
	}

	/*
	 * canale -> preso automaticamente tramite $wpdb->prefix che sceglie solamente il canale in cui siamo
	 * 1) stessa storia, stessi tag, stesse cat, canale (Ultimi 30 giorni)
	 * 2) stessa storia, stessi tag, canale (Ultimi 30 giorni)
	 * 3) stessa storia, canale (Ultimi 30 giorni)
	 * 4) alcuni tag, alcune categorie, canale (Ultimi 30 giorni)
	 * 5) alcuni tag, canale (Ultimi 30 giorni)
	 * 6) alcune categorie, canale (Ultimi 30 giorni)
	 * 7) canale (Ultimi 30 giorni)
	 */

	$a           = 1;
	$cycle_limit = $limit;
	if ( function_exists( "MRP_get_related_posts" ) ) {
		$relateds = MRP_get_related_posts( $post_id );
		if ( $relateds ) {
			$i = 1;
			foreach ( $relateds as $k => $v ) {
				if ( $i <= $limit ) {
					$ids_to_exclude = array_merge( array( $k ), $ids_to_exclude );
					array_push( $related_posts, $k );
				}
				$i ++;
			}
		}
	}

	while ( ( count( $related_posts ) < $cycle_limit ) && ( $a < 7 ) ) {
		switch ( $a ) {
			case 1:
				if ( $tax_query ) {
					$tmp_posts = tbm_helper_build_wpquery( false, false, $ids_to_exclude, $tax_query, $limit, $post_type );
				} else {
					$tmp_posts = array();
				}

				break;
			case 2:
				if ( $tag_array ) {
					$tmp_posts = tbm_helper_build_wpquery( false, false, $ids_to_exclude, false, $limit, $post_type, false, 'publish', false, $tag_array );
				} else {
					$tmp_posts = array();
				}
				break;
			case 3:
				if ( $category_array ) {
					$tmp_posts = tbm_helper_build_wpquery( false, false, $ids_to_exclude, false, $limit, $post_type, false, 'publish', $category_array, false );
				} else {
					$tmp_posts = array();
				}
				break;
			case 4:
				if ( $category_array ) {
					$tmp_posts = tbm_helper_build_wpquery( $category_array, false, $ids_to_exclude, false, $limit, $post_type, false, 'publish', false, false );
				} else {
					$tmp_posts = array();
				}

				break;
			case 5:
				if ( $tag_array ) {
					$tmp_posts = tbm_helper_build_wpquery( false, $tag_array, $ids_to_exclude, false, $limit, $post_type );
				} else {
					$tmp_posts = array();
				}
				break;
			default:
				$tmp_posts = tbm_helper_build_wpquery( false, false, $ids_to_exclude, false, $limit, $post_type, '', 'publish', '', '', 'rand' );
				break;
		}


		if ( ! empty( $tmp_posts ) ) {
			foreach ( $tmp_posts as $key => $tmp ) : setup_postdata( $post );
				$GLOBALS['ids_to_exclude'][] = $tmp->ID;
				$tmp_posts[ $key ]->case     = $a;
			endforeach;
			wp_reset_postdata();

			$related_posts = array_merge( $related_posts, $tmp_posts );
			$limit         -= count( $tmp_posts );

		}
		$a ++;
	}


	if ( ! empty( $related_posts ) ) :
		if ( 'object' === $simple ) :
			$ids                  = wp_list_pluck( $related_posts, 'ID' );
			$return_related_posts = new WP_Query( array( 'post__in' => $ids, 'post_type' => 'any' ) );
			wp_reset_postdata();

			$out = $return_related_posts;
		elseif ( true === $simple ) :
			$return_related_posts = array();
			$i                    = 0;
			foreach ( $related_posts as $related_post ) {
				$return_related_posts[ $i ]['ID']         = $related_post->ID;
				$return_related_posts[ $i ]['post_title'] = $related_post->post_title;
				$return_related_posts[ $i ]['permalink']  = get_permalink( $related_post->ID );
				$return_related_posts[ $i ]['post_type']  = $related_post->post_type;
				$return_related_posts[ $i ]['case']       = $related_post->case;
				$i ++;
			}

			$out = json_decode( json_encode( $return_related_posts ) );
		else :
			$out = $related_posts;
		endif;

		set_transient( $transient_key, $out, 24 * HOUR_IN_SECONDS );

		return $out;

	endif;

	return false;

}

/**
 * Check if two post have some differences in slug, date, title, excerpt or content
 *
 * @param array $post_new First post: array which correspond to a WP_Post object
 * @param array $post_old Second post: array which correspond to a WP_Post object
 *
 * @return bool|array|WP_Error
 */
function tbm_posts_differences( $post_new = '', $post_old = '' ) {

	$out = array();

	if ( ! $post_new || ! $post_old ) {
		return new WP_Error( 'broke', 'Errore nella elaborazione delle differenze' );
	}

	if ( $post_new['post_name'] !== $post_old['post_name'] ) {
		$out[] = 'name';
	}

	if ( $post_new['post_date'] !== $post_old['post_date'] ) {
		$out[] = 'date';
	}

	if ( wp_unslash( $post_new['post_title'] ) !== wp_unslash( $post_old['post_title'] ) ) {
		$out[] = 'title';
	}

	if ( wp_unslash( $post_new['post_excerpt'] ) !== wp_unslash( $post_old['post_excerpt'] ) ) {
		$out[] = 'excerpt';
	}

	if ( wp_unslash( $post_new['post_content'] ) !== wp_unslash( $post_old['post_content'] ) ) {
		$out[] = 'content';
	}

	if ( ! empty( $out ) ) {
		return $out;
	}

	return false;

}

/**
 * Retrieves a page given its path.
 *
 * @param string $page_name Page post-name: will search exactly this post-name (or slug)
 * @param string|array $post_type Optional. Post type or array of post types. Default 'post'.
 *
 * @return int|null Integer on success, or null on failure.
 *
 */
function tbm_get_postid_from_post_name( $page_name, $post_type = 'post' ) {
	global $wpdb;

	$last_changed = wp_cache_get_last_changed( 'posts' );

	$hash      = md5( $page_name . serialize( $post_type ) );
	$cache_key = "get_id_from_name:$hash:$last_changed";
	$cached    = wp_cache_get( $cache_key, 'posts' );
	if ( false !== $cached ) {
		// Special case: '0' is a bad `$page_path`.
		if ( '0' === $cached || 0 === $cached ) {
			return;
		} else {
			return get_post( $cached );
		}
	}

	$page_name           = rawurlencode( urldecode( $page_name ) );
	$page_path_in_string = "'" . $page_name . "'";

	if ( is_array( $post_type ) ) {
		$post_types = $post_type;
	} else {
		$post_types = array( $post_type );
	}

	$post_types          = esc_sql( $post_types );
	$post_type_in_string = "'" . implode( "','", $post_types ) . "'";
	$sql                 = "
		SELECT ID
		FROM $wpdb->posts
		WHERE post_name = ($page_path_in_string)
		AND post_type IN ($post_type_in_string)
	";

	$pages = $wpdb->get_results( $sql );

	if ( ! $pages ) {
		return;
	}

	$foundid = $pages[0]->ID;

	// We cache misses as well as hits.
	wp_cache_set( $cache_key, $foundid, 'posts' );

	if ( $foundid ) {
		return $foundid;
	}
}

/**
 * La funzione restituisce i contenuti correlati ad uno specifico post basandosi su una specifica tassonomia. Restituisce array, oggetto o un array semplificata
 *
 * @param int|WP_Post $post_id Il post cui correlare i contenuti. Optional. Default is the global `$post`.
 * @param int $limit Il numero massimo di post. Optional. Default 4
 * @param array $ids_to_exclude Id dei post da escludere. Optional.
 * @param string $special La tassonomia di correlazione. Optional. Default constant SITE_SPECIAL_TAXONOMY
 * @param string|array $post_type Il post type su cui eseguire da query. Optional. Default `post`.
 * @param bool $strict se true (default) la query viene eseguita con operator AND (più stringente) altrimenti con operatore IN
 * @param bool $simple Se true, restituisce solo ID, titolo, url e post_type e non l'oggetto completo del $post; se 'object' restituisce l'oggetto; se false (default) restituisce l'array da get_posts
 *
 * @return array|bool|mixed|object
 */
function tbm_get_strict_related_posts( $post = null, $limit = 4, $ids_to_exclude = array(), $taxonomy = SITE_SPECIAL_TAXONOMY, $post_type = 'post', $strict = true, $simple = false ) {

	$GLOBALS['ids_to_exclude'] = array();

	$post = get_post( $post );
	if ( empty( $post ) ) {
		return '';
	}

	// Convert, if not, $post_type in array
	if ( ! is_array( $post_type ) ) {
		$post_type = array( $post_type );
	}

	// Cycle $post_type array to check if the post type exists in theme
	foreach ( $post_type as $pt ) {
		if ( ! post_type_exists( $pt ) ) {
			return '';
		}
	}

	$related_posts = array();
	$post_id       = $post->ID;

	// Get the special taxonomy
	$storie = wp_get_post_terms( $post_id, $taxonomy );

	/**
	 * Is strict comparison, so if aren't terms we exit and return null
	 */
	if ( ! $storie ) {
		return null;
	}

	$storie_array = wp_list_pluck( $storie, 'term_id' );

	// Query for special taxonomy
	$tax_query = [];

	$tax_query['taxonomy']         = $taxonomy;
	$tax_query['terms']            = $storie_array;
	$tax_query['include_children'] = false;

	/*
	 * Tax query strict
	 */
	$tax_query['operator'] = 'AND';
	$tax_query_and         = array( $tax_query );

	/*
	 * Tax query less strict
	 */
	$tax_query['operator'] = 'IN';
	$tax_query_or          = array( $tax_query );

	if ( empty( $ids_to_exclude ) ) {
		if ( ! empty( $GLOBALS['ids_to_exclude'] ) ) {
			$ids_to_exclude = $GLOBALS['ids_to_exclude'];
		}
	}

	if ( is_array( $ids_to_exclude ) ) {
		$ids_to_exclude = array_merge( array( $post_id ), $ids_to_exclude );
	} else {
		$ids_to_exclude = array( $post_id );
	}

	$a           = 1;
	$cycle_limit = $limit;

	/**
	 * If strict, execute only first loop with $tax_query_and
	 */
	$loop_limit = $strict ? 2 : 3;


	while ( ( count( $related_posts ) < $cycle_limit ) && ( $a < $loop_limit ) ) {
		switch ( $a ) {
			case 1:
				$tmp_posts = tbm_helper_build_wpquery( false, false, $ids_to_exclude, $tax_query_and, $limit, $post_type );
				break;
			case 2:
				$tmp_posts = tbm_helper_build_wpquery( false, false, $ids_to_exclude, $tax_query_or, $limit, $post_type );
				break;
		}

		if ( ! empty( $tmp_posts ) ) {

			foreach ( $tmp_posts as $key => $tmp ) :
				$GLOBALS['ids_to_exclude'][] = $tmp->ID;
				$tmp_posts[ $key ]->case     = $a;
			endforeach;

			$related_posts = array_merge( $related_posts, $tmp_posts );
			$limit         -= count( $tmp_posts );

		}
		$a ++;
	}

	if ( ! empty( $related_posts ) ) :
		if ( 'object' === $simple ) :
			$ids                  = wp_list_pluck( $related_posts, 'ID' );
			$return_related_posts = new WP_Query( array( 'post__in' => $ids, 'post_type' => 'any' ) );
			wp_reset_postdata();

			return $return_related_posts;
		elseif ( true === $simple ) :
			$return_related_posts = array();
			$i                    = 0;
			foreach ( $related_posts as $related_post ) {
				$return_related_posts[ $i ]['ID']         = $related_post->ID;
				$return_related_posts[ $i ]['post_title'] = $related_post->post_title;
				$return_related_posts[ $i ]['permalink']  = get_permalink( $related_post->ID );
				$return_related_posts[ $i ]['post_type']  = $related_post->post_type;
				$return_related_posts[ $i ]['case']       = $related_post->case;
				$i ++;
			}

			return json_decode( json_encode( $return_related_posts ) );
		else :
			return $related_posts;
		endif;
	endif;

	return false;
}

/**
 * Get the enable posttype in shortcode
 *
 * @param $ghOptions
 *
 * @return array
 */
function get_enabled_post_types( $ghOptions ) {
	$pattern                  = get_shortcode_regex();
	$post_types               = get_post_types( array( 'public' => true ), 'object' );
	$disabled_post_types      = array();
	$disabled_post_types_info = array();
	$enabled_post_types       = array();

	foreach ( $post_types as $post_type ) {
		$optionsArray = explode( ",", $ghOptions );
		if ( in_array( 'sc[' . $post_type->name . ']', $optionsArray ) ) {
			if ( preg_match( '/' . $pattern . '/', '[ghshort' . $post_type->name . ']' ) ) {
				$enabled_post_types[] = '<label class="btn btn-sm btn-primary"><input type="checkbox" autocomplete="off" id="' . $post_type->name . '" value="' . $post_type->name . '"> ' . $post_type->label . '</label>';
			} else {
				$enabled_post_types[]       = '<label class="btn btn-sm btn-primary disabled" data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom"><input type="checkbox" autocomplete="off" id="' . $post_type->name . '" value="' . $post_type->name . '"> ' . $post_type->label . '</label>';
				$disabled_post_types[]      = '<mark>' . $post_type->label . '</mark> (<code>[ghshort' . $post_type->name . ']</code>)';
				$disabled_post_types_info[] = $post_type->label . ' =&gt; <code>[ghshort' . $post_type->name . ' id=1 title="xxx"]</code>';
			}
		}
	}

	return array(
			'enabled_post_types'       => $enabled_post_types,
			'disabled_post_types'      => $disabled_post_types,
			'disabled_post_types_info' => $disabled_post_types_info
	);
}

/**
 * Search in templates directory the available layouts
 *
 * @return array
 */
function get_enabled_layouts() {
	$enabled_themes = array();

	$themes = preg_grep( "/^(\.|\.\.)$|^_(.*)|generic/", scandir( HTML_TBM_COMMON_SHORTCODE_TEMPLATE_PATH ), PREG_GREP_INVERT );

	foreach ( $themes as $file ) {
		$file_label_blade = str_replace( array( '.blade.php' ), '', $file );
		$file_label       = str_replace( array( '.php' ), '', $file_label_blade );
		$file_name        = ucfirst( str_replace( array( '.blade.php', '.php', '_' ), array(
				'',
				'',
				' '
		), $file ) );

		$enabled_themes[] = '<label class="btn btn-sm btn-default"><input type="radio" autocomplete="off" name="layout" id="' . $file_label . '" value="' . $file_label . '"> ' . $file_name . '</label>';
	}

	return $enabled_themes;
}

/**
 * Send json error response with a message ($text)
 *
 * @param object|string $text Could be a Gump object or a string
 * @param bool $simple If true, send directly the text, otherwise encapsulate it in a json error message
 */
function tbm_gump_send_error_response( $body = array(), $message = 'message', $title = 'error', $headers = array() ) {

	if ( method_exists( $body, 'get_errors_array' ) ) {
		$message = $body->get_errors_array();
	} else {
		$message = $body;
	}

	$response = new ErrorJsonResponse( array(), $message, $title, 400, $headers );

	$callback = isset( $_GET['callback'] ) ? preg_replace( '/[^a-z0-9$_]/si', '', $_GET['callback'] ) : false;
	if ( $callback ) {
		$response->setCallback( $callback );
	}
	$response->sendHeaders();
	$response->sendContent();

	exit;
}

/**
 * Send json success response with a message ($text) and a title ($title)
 *
 * @param array $body The data to embed in response
 * @param string $message The message of the response
 * @param string $title The title of the response
 * @param array $header Headers to set for the response. Default is 'Cache_control: none'
 */
function tbm_gump_send_success_response( $body = array(), $message = 'message', $title = 'response', $headers = array() ) {

	$response = new SuccessJsonResponse( $body, $message, $title, '200', $headers );

	$callback = isset( $_GET['callback'] ) ? preg_replace( '/[^a-z0-9$_]/si', '', $_GET['callback'] ) : false;
	if ( $callback ) {
		$response->setCallback( $callback );
	}
	$response->sendHeaders();
	$response->sendContent();

	exit;
}

/**
 * Get page id searched by template page name (ie 'views/page-chisiamo.blade.php') for Sage based template. The results are limited to 10
 *
 * @param string $template_name (Required) The name of the template page name
 *
 * @return bool|int[]|WP_Post[]
 */
function tbm_get_page_by_template_name( $template_name = '' ) {

	if ( empty( $template_name ) ) {
		return false;
	}

	$args  = [
			'post_type'      => 'page',
			'fields'         => 'ids',
			'posts_per_page' => 10,
			'meta_key'       => '_wp_page_template',
			'meta_value'     => $template_name
	];
	$pages = get_posts( $args );
	if ( $pages ) {
		return $pages;
	}

	return false;
}
