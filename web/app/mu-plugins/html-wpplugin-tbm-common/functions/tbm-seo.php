<?php

/**
 * Remove Yoast rel next from homepage
 */
add_filter( 'wpseo_next_rel_link', function () {
	if ( is_home() || is_front_page() ) {
		return false;
	}
} );

/**
 * Remove Yoast rel prev from homepage
 */
add_filter( 'wpseo_prev_rel_link', function () {
	if ( is_home() || is_front_page() ) {
		return false;
	}
} );

/**
 * Make no index the paginated galleries
 */
add_filter( 'wpseo_robots', function ( $robotsstr ) {

	if ( ! is_singular( array( 'gallery', 'galleria', 'gallerie' ) ) ) {
		return $robotsstr;
	}

	if ( get_query_var( 'page' ) > 1 ) {
		return "noindex, follow";
	}

	return $robotsstr;
}, 99, 1 );

/**
 * Rewrite cerca
 */
add_filter( 'wpseo_json_ld_search_url', function ( $url ) {
	return str_replace( '/?s=', '/search/', $url );
} );

