<?php
/**
 * Created by PhpStorm.
 * User: francesco
 * Date: 16/02/2019
 * Time: 18:50
 */

add_action( 'init', function () {
	if ( wp_doing_ajax() ) {
		if ( isset( $_GET['action'] ) && 'tbm_policy' === $_GET['action'] ) {
			header( 'Content-Security-Policy: default-src \'none\'; script-src \'unsafe-inline\'; connect-src \'self\'; img-src \'self\'; style-src \'unsafe-inline\';frame-ancestors \'self\' *.borse.it *.finanzaonline.com *.finanza.com *.blogo.it' );
		}
	}
} );

/**
 * Shortcodes
 */
add_action( 'wp_ajax_gh_shortcode_search_posts', 'tbm_shortcode_search_posts' );
add_action( 'wp_ajax_gh_shortcode_save_options', 'tbm_shortcode_save_options' );
add_action( 'wp_ajax_gh_shortcode_tinymce', 'tbm_shortcode_tinymce' );
/**
 * Chiama la pop-up TinyMCE via admin-ajax
 *
 */
function tbm_shortcode_tinymce() {

	// check for rights
	if ( ! current_user_can( 'edit_posts' ) ) {
		die( __( "Vietato" ) );
	}

	include_once( HTML_TBM_COMMON_PLUGIN_DIR . 'components/tinymce/dialog.php' );

	die();
}

/**
 * Funzione per salvare le opzioni della popup
 */
function tbm_shortcode_save_options() {
	//Verifiche varie: nonce
	$nonce = isset( $_POST["mynonce"] ) ? sanitize_key( $_POST["mynonce"] ) : '';
	if ( empty( $nonce ) ) {
		return;
	}

	//Verifiche varie: nonce
	if ( ! wp_verify_nonce( $nonce, "gh_short_save_options" ) ) {
		return;
	}

	//Verifiche varie: data
	$gh_shortcode_options = $_POST['postType'];
	if ( empty( $gh_shortcode_options ) ) {
		return;
	}


	//wp_options: aggiungo, aggiorno o cancello le Informazioni
	$old_gh_shortcode_options = get_option( 'gh_shortcode_options' );
	if ( $gh_shortcode_options && ! $old_gh_shortcode_options ) {
		add_option( 'gh_shortcode_options', $gh_shortcode_options, '', 'no' );
	} elseif ( $gh_shortcode_options && $old_gh_shortcode_options != $gh_shortcode_options ) {
		update_option( 'gh_shortcode_options', $gh_shortcode_options );
	} elseif ( ! $gh_shortcode_options && $old_gh_shortcode_options ) {
		delete_option( 'gh_shortcode_options' );
	}

	exit( 'ok' );
}

/**
 * Funzione per cercare i post
 */
function tbm_shortcode_search_posts() {
	//Verifiche varie: nonce
	$nonce = isset( $_GET["mynonce"] ) ? sanitize_key( $_GET["mynonce"] ) : '';
	if ( empty( $nonce ) ) {
		return;
	}

	//Verifiche varie: nonce
	if ( ! wp_verify_nonce( $nonce, "gh_short_search_posts" ) ) {
		return;
	}

	header( 'content-type: text/javascript;' );

	$return_arr = array();

	if ( isset( $_GET['q'] ) && strlen( $_GET['q'] ) > 0 ) {

		global $wpdb;

		$s         = esc_sql( $_GET['q'] );
		$post_type = esc_sql( $_GET['mrp_post_type'] );
		$res       = intval( $_GET['mrp_res'] );
		$limit     = intval( $_GET['limit'] );
		$draft     = isset( $_GET['draft'] ) && intval( $_GET['draft'] ) === 1 ? "NOT IN ('inherit', 'auto-draft', 'trash')" : "NOT IN ('inherit', 'auto-draft', 'trash', 'draft')";
		$get_id    = null;

		// Formatto $s
		if ( $s == '123' ) {
			$where = "WHERE post_title = 0";
		} else {
			$regexp = "%" . $s . "%";
			$where  = "WHERE post_title LIKE '$regexp'";
		}

		// Formatto $post_type
		if ( $post_type == 'id' ) {
			$where     = "WHERE ID LIKE $s";
			$get_id    = 1;
			$post_type = "";
		} elseif ( is_array( $post_type ) ) {
			$post_explode = "'" . implode( "','", $post_type ) . "'";
			//$ids = join(',',$post_type);
			$post_type = "AND post_type IN ($post_explode)";
		} else {
			$post_type = "AND post_type = '$post_type'";
		}

		//Compongo la query
		$query   = "SELECT ID, post_title, post_type, post_status, guid, post_date FROM $wpdb->posts $where $post_type ";
		$query   .= " AND post_parent = 0 AND post_status " . $draft;
		$query   .= " ORDER BY post_date DESC LIMIT $limit";
		$results = $wpdb->get_results( $query );

		if ( $results ) {

			foreach ( $results as $result ) {
				$row_array['id']     = $result->ID;
				$row_array['title']  = $result->post_title;
				$row_array['type']   = $result->post_type;
				$row_array['status'] = $result->post_status == 'draft' ? 'bozza' : $result->post_status;
				$row_array['data']   = date( 'd/m/Y', strtotime( $result->post_date ) );

				if ( $res == 1 ) {
					$row_array['link'] = $result->guid;
				} elseif ( $res == 2 ) {
					$row_array['link'] = get_permalink( $result->ID );
				} elseif ( $res == 3 ) {
					$row_array['link'] = $result->ID;
				}
				array_push( $return_arr, $row_array );
			}
		} else {
			$row_array['id']     = 0;
			$row_array['title']  = utf8_encode( 'Nessun risultato' );
			$row_array['type']   = utf8_encode( ' ' );
			$row_array['status'] = utf8_encode( ' ' );
			$row_array['data']   = utf8_encode( ' ' );
			array_push( $return_arr, $row_array );
		}
		if ( $get_id ) {
			$json = json_encode( $row_array );
		} else {
			$json = json_encode( $return_arr );
		}
		echo isset( $_GET['callback'] ) ? "{$_GET['callback']}($json)" : $json;
	}
	exit();
}
