<?php
/**
 * Created by PhpStorm.
 * User: francesco
 * Date: 07/02/2018
 * Time: 20:15
 */

/**
 * Editorial shortcodes
 */
add_shortcode( "ghshortpost", "tbm_shortcodes" );
add_shortcode( "ghshortvideo", "tbm_shortcodes" );
add_shortcode( "ghshortcard", "tbm_shortcodes" );
add_shortcode( "ghshortesperto", "tbm_shortcodes" );
add_shortcode( "ghshortevent", "tbm_shortcodes" );
add_shortcode( "ghshortlongform", "tbm_shortcodes" );
add_shortcode( "ghvideo", "tbm_shortcodes" );
add_shortcode( "ghgallery", "tbm_shortcodes" );
add_shortcode( "nggallery", "tbm_shortcodes" );
add_shortcode( "ghproduct", "tbm_shortcodes_products" );
add_shortcode( "ghproductlink", "tbm_shortcodes_lazyproducts" );
add_shortcode( "ghproductsummary", "tbm_shortcodes_products_summary" );

/**
 * Adv shortcodes
 */
add_shortcode( "ghsharethrough", "tbm_simple_shortcodes" );
add_shortcode( "ghrandvideo", "tbm_simple_shortcodes" );
add_shortcode( "ghteads", "tbm_simple_shortcodes" );

/**
 * Shortcode Products
 *
 * @return string
 */
function tbm_shortcodes_products( $atts ) {

	extract( shortcode_atts( array(
		"id"     => "",
		"title"  => "",
		"layout" => "",
		"badge"  => "",
		"cta"    => "",
	), $atts ) );

	if ( ! isset( $atts['id'] ) ) {
		return '';
	}

	$post_id = get_the_ID();

	$shortcode_id = $atts['id'];

	$post = get_post( $shortcode_id );

	if ( ! $post ) {
		if ( is_user_logged_in() && current_user_can( 'edit_posts' ) ) {
			return '<div style="padding:10px;background-color: #e85959;color:white">La scheda non esiste. <small style="font-size: 70%"><span Questo="" testo="" è="$è$" visibile="" solo="" per="" editor=""></span></small></div>';
		}

		return '';
	}

	// If the product is not published
	if ( 'publish' !== get_post_status( $post ) ) {
		if ( is_user_logged_in() && current_user_can( 'edit_posts' ) ) {
			return '<div style="padding:10px;background-color: #e85959;color:white">La scheda non è pubblicata. <small style="font-size: 70%">Questo testo è visibile solo per editor</small></div>';
		}

		return '';
	}

	// Get Tag
	$tag = get_post_meta( $post_id, 'tbm_ghproduct', true );
	if ( ! $tag ) {
		if ( is_user_logged_in() && current_user_can( 'edit_posts' ) ) {
			return '<div style="padding:10px;background-color: #e85959;color:white">All\'articolo non è associato alcun tracking ID. Controlla se ci sono tracking ID disponibili o avvisa i dev. <small style="font-size: 70%">Questo testo è visibile solo per editor</small></div>';
		}

		return '';
	}

	// Get Pro
	if ( function_exists( 'get_field' ) ) {
		$pro = get_field( 'tbm_product_pro', $post->ID );
	}

	// Get Cons
	if ( function_exists( 'get_field' ) ) {
		$cons = get_field( 'tbm_product_cons', $post->ID );
	}

	// Get URL
	if ( function_exists( 'get_field' ) ) {
		$url = get_field( 'tbm_amazon_link', $post->ID );
	}

	// Get Claim
	if ( function_exists( 'get_field' ) ) {
		$claim = get_field( 'tbm_product_claim', $post->ID );
	}

	// Get Cta
	if ( function_exists( 'get_field' ) ) {
		$cta = get_field( 'tbm_product_cta', $post->ID );
	}

	// Get Badge
	if ( function_exists( 'get_field' ) ) {
		$badge = get_field( 'tbm_product_badge', $post->ID );
	}

	// Get Amazon price
	if ( function_exists( 'get_field' ) ) {
		$amz_price = get_field( 'tbm_amazon_price', $post->ID );
	}

	// Get Discounted price
	if ( function_exists( 'get_field' ) ) {
		$discounted_price = get_field( 'tbm_amazon_discounted_price', $post->ID );
	}

	// Check if Amazon prime is available
	if ( function_exists( 'get_field' ) ) {
		$amz_prime = get_field( 'tbm_amazon_prime', $post->ID );
	}

	// Get thumbnail
	if ( has_post_thumbnail( $post->ID ) ) {
		$image = function_exists( 'tbm_get_the_post_thumbnail_url' ) ? tbm_get_the_post_thumbnail_url( $post->ID, array(
			480,
			0
		) ) : get_the_post_thumbnail_url( $post->ID, array( 320, 320 ) );
	} else {
		$image = '';
	}

	// Enequeue script
	wp_enqueue_script( 'tbm-products' );

	// Define class
	$sc = new stdClass();

	// Custom fields
	$sc->pro        = $pro ? wp_list_pluck( $pro, 'tbm_product_single_pro' ) : '';
	$sc->cons       = $cons ? wp_list_pluck( $cons, 'tbm_product_single_cons' ) : '';
	$sc->amz_price  = $amz_price ?: 0;
	$sc->discounted = $discounted_price ?: 0;
	$sc->price      = $discounted_price && $discounted_price > 0 ? $discounted_price : $amz_price;
	$sc->saving     = (float) $sc->price < (float) $sc->amz_price && ( (float) $sc->amz_price - (float) $sc->price ) > 0.99 ? $sc->amz_price - $sc->price : 0;
	$sc->cta        = $cta ?: '';
	$sc->aid        = $tag ?: '';
	$sc->prime      = $amz_prime ? true : false;
	$sc->url        = $url ? str_replace( get_field( 'tbm_product_amazon_associate_tag', 'options' ), $tag, $url ) : '';
	$sc->badge      = $badge ?: '';
	$sc->badge      = ! empty( $atts['badge'] ) ? $atts['badge'] : $sc->badge;
	$sc->claim      = $claim ?: '';
	$sc->claim      = ! empty( $atts['claim'] ) ? $atts['claim'] : $sc->claim;
	$sc->excerpt    = $post->post_excerpt ? strip_tags( $post->post_excerpt, '<em><strong><i><b>' ) : '';
	$sc->content    = get_the_content( '', '', $post->ID ) ?: '';


	// Check file with or withouth the underscore. A template preceeded with underscore is a template that is not publsihed as option to the user but is active
	$sc->title       = ! empty( $atts['title'] ) ? $atts['title'] : get_the_title( $post );
	$sc->image       = $image;
	$sc->image_alt   = function_exists( 'tbm_get_the_post_thumbnail_alt' ) ? tbm_get_the_post_thumbnail_alt( $post ) : '';
	$sc->image_id    = get_post_thumbnail_id( $post );
	$sc->permalink   = get_permalink( $post );
	$sc->id          = $post->ID;
	$layout_template = ! empty( $atts['layout'] ) ? 'post_' . $atts['layout'] : 'post_product_slim';

	ob_start();

	// If is a Sage Template (based on Blade) we need a different path structure
	if ( function_exists( 'App\template' ) && is_file( HTML_TBM_COMMON_SHORTCODE_TEMPLATE_PATH . 'post_product.blade.php' ) ) {
		$data['sc'] = $sc;
		echo App\template( str_replace( get_template_directory() . '/views/', '', HTML_TBM_COMMON_SHORTCODE_TEMPLATE_PATH ) . 'post_product', $data );
	} else if ( is_file( HTML_TBM_COMMON_SHORTCODE_TEMPLATE_PATH . $layout_template . '.php' ) ) {
		include( HTML_TBM_COMMON_SHORTCODE_TEMPLATE_PATH . $layout_template . '.php' );
	} else {
		include( HTML_TBM_COMMON_PLUGIN_DIR . 'templates/embedder/' . $layout_template . '.php' );
	}
	$output_string = ob_get_contents();

	// End
	ob_end_clean();

	// Reset post data
	wp_reset_postdata();

	return $output_string;
}

/**
 * Shortcode Lazy Products
 *
 * @return string
 */
function tbm_shortcodes_lazyproducts( $atts ) {

	extract( shortcode_atts( array(
		"asin"    => "",
		"cta"     => "",
		"product" => ""
	), $atts ) );

	if ( empty( $atts['asin'] ) ) {
		return '';
	}

	$post_id = get_the_ID();

	// Get Tag
	$tag = get_post_meta( $post_id, 'tbm_ghproduct', true );
	if ( ! $tag ) {
		if ( is_user_logged_in() && current_user_can( 'edit_posts' ) ) {
			return '<div style="padding:10px;background-color: #e85959;color:white">All\'articolo non è associato alcun tracking ID. Controlla se ci sono tracking ID disponibili o avvisa i dev. <small style="font-size: 70%">Questo testo è visibile solo per editor</small></div>';
		}

		return '';
	}


	$url = 'https://www.amazon.it/dp/' . $atts['asin'] . '/?tag=' . $tag;
	$cta = $atts['cta'] ?: 'Compra su Amazon';

	return sprintf( '<a href="%s" target="_blank" rel="noopener nofollow sponsored">%s</a>', $url, $cta );
}

/**
 * Shortcode Products Summary
 *
 * @return string
 */
function tbm_shortcodes_products_summary() {
	global $post;
	$out = array();

	// If not present
	if ( ! preg_match_all( '/' . get_shortcode_regex() . '/s', $post->post_content, $shorts ) ) {
		return '';
	}

	foreach ( $shorts[0] as $key => $short ) {
		if ( preg_match_all( '/\[ghproduct id="?([0-9]+)"? title="([^"]+)"/i', $short, $atts ) ) {

			// Get the post_id
			$post_id = $atts[1][0];

			if ( 'publish' !== get_post_status( $post_id ) ) {
				continue;
			}

			$title = get_the_title( $post_id );
			$slug  = sanitize_title( $title );

			$out[] = sprintf( '<li><a href="#%s">%s</a></li>', $slug, $title );
		}
	}


	if ( $out ) {
		return '<div class="product-list"><ul>' . implode( '', $out ) . '</ul></div>';
	}

	return '';
}

/**
 * Ritorna gli shortcode del plugin
 *
 * @param array $atts Attributi dello shortcode
 *
 * @return string
 */
function tbm_shortcodes( $atts ) {

	extract( shortcode_atts( array(
		"source"  => "",
		"id"      => "",
		"title"   => "",
		"layout"  => "",
		"videoid" => "",
		"zoneid"  => ""
	), $atts ) );


	if ( ! isset( $atts['id'] ) && ! isset( $atts['url'] ) ) {
		return '';
	}

	$post_id = $atts['id'];

	if ( ! isset( $atts['id'] ) && isset( $atts['url'] ) ) {
		$post_id = url_to_postid( $atts['url'] );
	}

	/**
	 * Sky source
	 */
	if ( isset( $atts['source'] ) && $atts['source'] == 'sky' ) {

		// Sky shortcode is: [ghvideo source="sky" videoid="469596" zoneid="8468" id="6609ba8ff88606794e22ff360400bcea"]
		// Rewrited to: <ins data-revive-videoid="469596" data-revive-zoneid="8468" data-revive-id="6609ba8ff88606794e22ff360400bcea"></ins>
		// <script async src="//adx.4strokemedia.com/www/delivery/asyncjs.php"></script>
		if ( ! isset( $atts['videoid'] ) || ! isset( $atts['zoneid'] ) ) {
			return '';
		}

		wp_enqueue_script( 'sky-script' );

		return sprintf( '<ins data-revive-videoid="%s" data-revive-zoneid="%s" data-revive-id="%s"></ins>', $atts['videoid'], $atts['zoneid'], $atts['id'] );

	}

	if ( isset( $atts['source'] ) && $atts['source'] == 'ngg' ) {
		$post = get_posts(
			array(
				'post_type'      => 'any',
				'posts_per_page' => 1,
				'meta_key'       => 'nggallery_id',
				'meta_value'     => $atts['id']
			)
		);

		if ( $post ) {
			$post_id = $post[0]->ID;
		}

	}

	$post = get_post( $post_id );

	if ( ! $post ) {
		if ( is_user_logged_in() && current_user_can( 'edit_posts' ) ) {
			return '<div style="padding:10px;background-color: #e85959;color:white">L\'elemento previsto in questo punto non esiste. <br><small style="font-size: 70%">Questo testo è visibile solo per editor.</small></div>';
		}

		return '';
	}

	// If the product is not published
	if ( 'publish' !== get_post_status( $post ) ) {
		if ( is_user_logged_in() && current_user_can( 'edit_posts' ) ) {
			return '<div style="padding:10px;background-color: #e85959;color:white">L\'elemento  previsto in questo punto non è pubblicato. <br><small style="font-size: 70%">Questo testo è visibile solo per editor.</small></div>';
		}

		return '';
	}

	// Check gallery
	if ( function_exists( 'get_field' ) ) {
		$gallery = get_field( 'tbm_gallery', $post->ID );
	}

	// Check image
	if ( has_post_thumbnail( $post->ID ) ) {
		$image = function_exists( 'tbm_get_the_post_thumbnail_url' ) ? tbm_get_the_post_thumbnail_url( $post->ID, array(
			150,
			100
		) ) : get_the_post_thumbnail_url( $post->ID, array( 150, 100 ) );
	} else {
		$image = '';
	}

	$sc          = new stdClass();
	$sc->gallery = isset( $gallery ) ? $gallery : array();
	$sc->source  = isset( $atts['source'] ) ? $atts['source'] : "internal";
	$sc->layout  = isset( $atts['layout'] ) ? $atts['layout'] : "generic";

	// Check file with or withouth the underscore. A template preceeded with underscore is a template that is not publsihed as option to the user but is active
	$sc->layout    = file_exists( HTML_TBM_COMMON_SHORTCODE_TEMPLATE_PATH . '_' . $sc->layout . '.php' ) ? '_' . $sc->layout : $sc->layout;
	$sc->layout    = file_exists( HTML_TBM_COMMON_SHORTCODE_TEMPLATE_PATH . '_' . $sc->layout . '.blade.php' ) ? '_' . $sc->layout : $sc->layout;
	$sc->title     = isset( $atts['title'] ) ? $atts['title'] : get_the_title( $post );
	$sc->image     = $image;
	$sc->image_alt = function_exists( 'tbm_get_the_post_thumbnail_alt' ) ? tbm_get_the_post_thumbnail_alt( $post ) : '';
	$sc->image_id  = get_post_thumbnail_id( $post );
	$sc->date      = get_the_date( '', $post );
	$sc->permalink = get_permalink( $post );
	$sc->id        = $post->ID;

	ob_start();

	// If is a Sage Template (based on Blade) we need a different path structure
	if ( function_exists( 'App\template' ) && is_file( HTML_TBM_COMMON_SHORTCODE_TEMPLATE_PATH . $sc->layout . '.blade.php' ) ) {
		$data['sc'] = $sc;
		echo App\template( str_replace( get_template_directory() . '/views/', '', HTML_TBM_COMMON_SHORTCODE_TEMPLATE_PATH ) . $sc->layout, $data );
	} else if ( is_file( HTML_TBM_COMMON_SHORTCODE_TEMPLATE_PATH . $sc->layout . '.php' ) ) {
		include( HTML_TBM_COMMON_SHORTCODE_TEMPLATE_PATH . $sc->layout . '.php' );
	} else {
		echo '';
	}

	$output_string = ob_get_contents();
	ob_end_clean();
	wp_reset_postdata();

	return $output_string;
}

/**
 * Simple shortcodes
 *
 * @param string $atts
 * @param string $content
 * @param string $tag
 *
 * @return bool|false|string
 */
function tbm_simple_shortcodes( $atts = '', $content = '', $tag = '' ) {

	if ( ! $tag ) {
		return false;
	}

	/**
	 * Check if Random video position is forced. If true, disable shortcode
	 */
	if ( 'ghrandvideo' === $tag && get_field( 'tbm_common_video_force', 'option' ) ) {
		return false;
	}

	/**
	 * Check if video is disabled
	 */
	if ( get_field( 'video_single_disabled' ) ) {
		return false;
	}

	/**
	 * Check if Teads position is forced. If true, disable shortcode
	 */
	if ( 'ghteads' === $tag && get_field( 'tbm_common_teads_force', 'option' ) ) {
		return false;
	}

	/**
	 * Check if Native position is forced. If true, disable shortcode
	 */
	if ( 'ghsharethrough' === $tag && get_field( 'tbm_common_native_force', 'option' ) ) {
		return false;
	}

	if ( file_exists( HTML_TBM_COMMON_SHORTCODE_TEMPLATE_PATH . '_' . $tag . '.php' ) ) {
		$template = '_' . $tag;
	} else if ( file_exists( HTML_TBM_COMMON_SHORTCODE_TEMPLATE_PATH . '_' . $tag . '.blade.php' ) ) {
		$template = '_' . $tag;
	} else if ( file_exists( HTML_TBM_COMMON_SHORTCODE_TEMPLATE_PATH . '_' . $tag . '.php' ) ) {
		$template = $tag;
	} else if ( file_exists( HTML_TBM_COMMON_PLUGIN_DIR . 'templates/' . $tag . '.php' ) ) {
		$template = $tag;
	} else if ( file_exists( HTML_TBM_COMMON_PLUGIN_DIR . 'templates/' . '_' . $tag . '.php' ) ) {
		$template = '_' . $tag;
	} else {
		return '';
	}

	$sc         = new stdClass();
	$sc->layout = $template;

	ob_start();

	// If is a Sage Template (based on Blade) we need a different path structure
	if ( function_exists( 'App\template' ) && is_file( HTML_TBM_COMMON_SHORTCODE_TEMPLATE_PATH . $sc->layout . '.blade.php' ) ) {
		$data['sc'] = $sc;
		echo App\template( str_replace( get_template_directory() . '/views/', '', HTML_TBM_COMMON_SHORTCODE_TEMPLATE_PATH ) . $sc->layout, $data );
	} else if ( is_file( HTML_TBM_COMMON_SHORTCODE_TEMPLATE_PATH . $sc->layout . '.php' ) ) {
		include( HTML_TBM_COMMON_SHORTCODE_TEMPLATE_PATH . $sc->layout . '.php' );
	} else {
		echo '';
	}

	$output_string = ob_get_contents();
	ob_end_clean();
	wp_reset_postdata();

	return $output_string;
}



