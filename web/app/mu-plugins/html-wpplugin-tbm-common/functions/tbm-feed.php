<?php
/**
 * Created by TribooMedia.
 * User: Natale Mazza
 * Date: 02/02/2019
 * Time: 12:45
 */


add_action( 'init', function () {
	add_feed( 'flipboard', function () {
		load_template( HTML_TBM_COMMON_PLUGIN_DIR . '/templates/tbm-template-feed-flipboard.php' );
	} );
	add_feed( 'gp', function () {
		load_template( HTML_TBM_COMMON_PLUGIN_DIR . '/templates/tbm-template-feed-flipboard.php' );
	} );
} );


add_action( 'init', function () {
	$categories     = get_categories();
	$category_array = wp_list_pluck( $categories, 'slug' );

	add_rewrite_rule( '^(' . implode( '|', $category_array ) . ')/(?:feed/)?(gp|flipboard)/?$', 'index.php?category_name=$matches[1]&feed=$matches[2]', 'top' );
}
);
