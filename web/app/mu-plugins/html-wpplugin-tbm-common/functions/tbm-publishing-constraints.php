<?php
/**
 * Created by PhpStorm.
 * User: Francesco
 * Date: 26/10/2018
 * Time: 17:47
 */

/**
 * Check the element costraints
 */
add_filter( 'wp_insert_post_data', 'tbm_check_post', '99', 2 );
add_action( 'admin_head-post.php', 'tbm_show_post_notice' );

/**
 * Check if all costraints are complied
 *
 * @param $data An array of slashed post data
 * @param $postarr An array of sanitized, but otherwise unmodified post data
 *
 * @return mixed
 */
function tbm_check_post( $data, $postarr ) {

	$enabled   = false;
	$err_msg   = array();
	$status    = $data['post_status'];
	$post_type = $data['post_type'];

	/**
	 * Check the filter
	 */
	$enabled = apply_filters( 'tbm_check_constraints', $enabled );

	if ( $enabled !== true ) {
		return $data;
	}

	if ( $post_type !== 'post' ) {
		return $data;
	}

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return $data;
	}

	if ( $status !== 'publish' && $status !== 'future' ) {
		return $data;
	}

	// Recupero le categorie associate e filtro i termini vuoti
	$terms = isset( $postarr['post_category'] ) ? array_filter( $postarr['post_category'] ) : array();

	if ( ( count( $terms ) < 1 ) ) {
		$err_msg[] = "Deve essere selezionata almeno una categoria";
	}

	if ( $err_msg ) {
		$data['post_status']    = 'draft';
		$postarr['post_status'] = 'draft';
		tbm_enable_error_message( '<ul style="list-style: inherit;padding: 0 20px;"><li>' . implode( '</li><li>', $err_msg ) . '</li></ul>' );
	}

	return $data;
}

/**
 * Compose and show the notice
 */
function tbm_show_post_notice() {
	$display_error_message = get_option( 'tbm_common_display_error_msg' );
	$err_msg               = get_option( 'tbm_common_error_msg' );
	$msg                   = __( "Sono stati rilevati i seguenti problemi", 'lifegate' ) . ":";
	$class                 = 'notice notice-error is-dismissible';
	$dismiss_button        = '<button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';

	if ( $display_error_message == 1 && ! empty( $err_msg ) ) {
		add_action( 'admin_notices', function () use ( $err_msg, $msg, $dismiss_button, $class ) {
			echo '<div class="' . $class . '"><p>' . $msg . '</p>' . $err_msg . $dismiss_button;
		} );
		tbm_disable_error_message();
		add_filter( 'redirect_post_location', 'tbm_modify_redirect_location' );
	}
}

function tbm_enable_error_message( $msg = '' ) {
	// Check if the error message is already set and if the same of the new
	$existing_msg = get_option( 'tbm_common_error_msg' );

	if ( $existing_msg !== $msg ) {
		$text = $msg . $existing_msg;
	} else {
		$text = $msg;
	}

	update_option( 'tbm_common_error_msg', $text );
	update_option( 'tbm_common_display_error_msg', 1 ); // turn on the message
}

function tbm_disable_error_message() {
	update_option( 'tbm_common_error_msg', '' );
	update_option( 'tbm_common_display_error_msg', 0 ); // turn off the message
}

/**
 * Replace the default published message
 *
 * @param $location The redirect url
 *
 * @return mixed
 */
function tbm_modify_redirect_location( $location ) {
	return str_replace( 'message=6', 'message=1', $location );
}


/**
 * Add jQuery function for post counting
 */
add_action( 'admin_print_footer_scripts', function () {
	global $ccp_setting;
	?>
	<style type="text/css">
		.ccp_excerpt_counter {
			float: right;
		}

		,
		.cce_count {
			font-weight: bold;
		}
	</style>
	<?php
} );
