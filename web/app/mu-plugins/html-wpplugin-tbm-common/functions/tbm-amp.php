<?php
/**
 * Whether this is an AMP endpoint.
 *
 * @see https://github.com/Automattic/amp-wp/blob/e4472bfa5c304b6c1b968e533819e3fa96579ad4/includes/amp-helper-functions.php#L248
 * @return bool
 */
function tbm_is_amp() {
	return function_exists( 'is_amp_endpoint' ) && is_amp_endpoint();
}

/**
 * Whether the AMP plugin is enabled
 *
 * @return bool
 */
function tbm_is_amp_plugin_enabled() {
	return function_exists( 'is_amp_endpoint' );
}

/**
 * Sanitize AMP Gestione Banner code and embed script and style in AMP way
 */
add_filter( 'amp_post_template_data', function ( $data ) {

	// Get the banner code
	$teads      = tbm_get_the_banner( 'AMP_TEADS', '', '', false, false );
	$top        = tbm_get_the_banner( 'AMP_BOX_INSIDE_TOP', '', '', false, false );
	$strip      = tbm_get_the_banner( 'AMP_STRIP', '', '', false, false );
	$sticky     = tbm_get_the_banner( 'AMP_STICKY', '', '', false, false );
	$footer     = tbm_get_the_banner( 'AMP_POST_FOOTER', '', '', false, false );
	$sanitizers = amp_get_content_sanitizers();

	// Analyze the DOM and sanitize
	$dom     = AMP_DOM_Utils::get_dom_from_content( $teads . $top . $strip . $sticky . $footer );
	$results = AMP_Content_Sanitizer::sanitize_document( $dom, $sanitizers, array() );

	// Embed the data
	$data['amp_component_scripts'] = array_merge( $data['amp_component_scripts'], $results['scripts'] );
	$data['post_amp_stylesheets']  = array_merge( $data['post_amp_stylesheets'], $results['stylesheets'] );
	$data['post_amp_styles']       = array_merge( $data['post_amp_styles'], $results['styles'] );

	return $data;
} );

/**
 * Change amp post permalink
 */
add_action( 'init', function () {
	/**
	 * Change AMP permalink
	 */
	add_filter( 'amp_pre_get_permalink', function ( $permalink, $post_id ) {
		return add_query_arg( 'amp', '', get_permalink( $post_id ) );
	}, 9, 2 );
} );

/**
 * Add googleanalytics custom data / datalayer
 */
add_filter( 'amp_post_template_analytics', function ( $analytics, $post ) {

	$site = tbm_get_domain();

	$cd = array(
			'lifegate' => '',
	);

	if ( ! is_array( $analytics ) ) {
		$analytics = array();
	}

	if ( is_multisite() ) {
		global $blog_id;
		$current_blog_details = get_blog_details( array(
				'blog_id' => $blog_id
		) );
		$siteName             = $current_blog_details->blogname;
	} else {
		$siteName = get_bloginfo( 'name', 'display' );
	}

	// LOAD DATA
	$extraParams = array(
			'siteName'          => $siteName,
			'visitorLoginState' => is_user_logged_in() ? 'logged-in' : 'logged-out',
			'contentType'       => 'single',
			'thumbnail'         => '',
			'pagePostID'        => $post->ID,
			'pagePostType'      => get_post_type(),
			'pagePostWordCount' => str_word_count( strip_tags( get_post_field( 'post_content', $post->ID ) ) ),
			'pagePostAuthor'    => get_the_author_meta( 'display_name', get_post_field( 'post_author', $post->ID, 'raw' ) ),
			'pagePostTitle'     => get_post_field( 'post_title', $post->ID, 'raw' ),
			'pagePostDate'      => get_the_date( '/Y/m/d/W/H/', $post->ID ),
			'pagePostDateYear'  => get_the_date( "Y", $post->ID ),
			'pagePostDateMonth' => get_the_date( "m", $post->ID ),
			'pagePostDateDay'   => get_the_date( "d", $post->ID ),
			'pagePostDateHour'  => get_the_date( "H", $post->ID ),
			'refreshRate'       => '',
			'themeVer'          => wp_get_theme()->get( 'Version' ),
			'wpVer'             => get_bloginfo( 'version' ),
		//'atatusId' => '87afa1dd833f47b99c330ad064dc8f5d'
	);

	// thumbnail
	if ( has_post_thumbnail( $post->ID ) ) {
		$thumb_array              = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
		$thumb_url                = $thumb_array[0];
		$extraParams['thumbnail'] = $thumb_url;
	}

	// taxonomy
	$taxonomies = get_taxonomies( '', 'names' );
	foreach ( $taxonomies as $taxonomy ) {
		$terms = get_the_terms( $post->ID, $taxonomy );
		if ( ! empty( $terms ) ) {
			$val = 0;
			foreach ( $terms as $term ) {
				$taxonomy                              = ucfirst( $taxonomy );
				$extraParams["page{$taxonomy}_{$val}"] = $term->name;
				$val ++;
			}
			unset( $val );
		}
	}

	// refreshRate
	$refreshValue               = tbm_get_the_banner( 'REFRESH', '', '', false );
	$extraParams['refreshRate'] = ( is_numeric( $refreshValue ) && ! empty( $refreshValue ) ) ? ( $refreshValue * 1000 ) : 86400000;


	$analytics['tbm-analytics-it'] = array(
			'type'        => 'gtag',
			'attributes'  => array(),
			'config_data' => array(
					'vars'           => array(
							"gtag_id" => 'UA-16610377-1',
							"linkers" => [ 'enabled' => 'true' ],
							"config"  => [
									'UA-16610377-1' => [ "groups" => "default" ]
							]
					),
					"triggers"       => [
							"button"      => [
									"selector" => ".cta--readmore",
									"on"       => "click",
									"vars"     => [
											"event_name"     => "Cta",
											"event_category" => "Click"
									]
							],
							"scrollPings" => [
									"on"         => "scroll",
									"scrollSpec" => [
											"verticalBoundaries" => [ 25, 50, 75, 100 ]
									],
									"request"    => "event",
									"vars"       => [
											"event_name"      => "Posizione",
											"method"          => "Scrolled",
											"event_category"  => "Scroll",
											"non_interaction" => true,
											"event_label"     => '${verticalScrollBoundary}%'
									],
							],
					],
					"extraUrlParams" => [ $cd[ $site ] => "true" ]
			),
	);
	if ( function_exists( 'pll_current_language' ) && pll_current_language() === 'en' ) {
		$analytics['tbm-analytics-com'] = array(
				'type'        => 'gtag',
				'attributes'  => array(),
				'config_data' => array(
						'vars'           => array(
								"gtag_id" => 'UA-16610377-11',
								"linkers" => [ 'enabled' => 'true' ],
								"config"  => [
										'UA-16610377-11' => [ "groups" => "default" ]
								]
						),
						"triggers"       => [
								"button"      => [
										"selector" => ".cta--readmore",
										"on"       => "click",
										"vars"     => [
												"event_name"     => "Cta",
												"event_category" => "Click"
										]
								],
								"scrollPings" => [
										"on"         => "scroll",
										"scrollSpec" => [
												"verticalBoundaries" => [ 25, 50, 75, 100 ]
										],
										"request"    => "event",
										"vars"       => [
												"event_name"      => "Posizione",
												"method"          => "Scrolled",
												"event_category"  => "Scroll",
												"non_interaction" => true,
												"event_label"     => '${verticalScrollBoundary}%'
										],
								],
						],
						"extraUrlParams" => [ $cd[ $site ] => "true" ]
				),
		);
	}

	$shinystat_id = get_field( 'tbm_shinystat_id', 'options' );

	if ( $shinystat_id ) {
		$analytics['tbm-shinystat'] = array(
				'type'        => 'shinystat',
				'attributes'  => array(),
				'config_data' => array(
						'vars'     => array(
								'account' => $shinystat_id,
						),
						'requests' => array(
								'pageview' => "\${base}?PAG=\${canonicalUrl}&\${commpar}\${pagepar}",
						),
				),
		);
	}


	return $analytics;

}, 9, 2 );

/**
 * Define AMP template for single post
 */
add_filter( 'amp_post_template_file', function ( $template, $template_type, $post ) {

	if ( file_exists( get_theme_file_path() . '/amp/single-amp.php' ) ) {
		return get_theme_file_path() . '/amp/single-amp.php';
	}

	if ( file_exists( HTML_TBM_COMMON_PLUGIN_TEMPLATE_PATH . 'single-amp.php' ) ) {
		return HTML_TBM_COMMON_PLUGIN_TEMPLATE_PATH . 'single-amp.php';
	}

	return $template;
}, 10, 3 );

/**
 *  Embed custom style
 */
add_action( 'amp_post_template_css', function () {
	$site     = tbm_get_domain();
	$template = '';

	if ( empty( $site ) ) {
		return '';
	}

	// All rogiostrap based template
	if ( file_exists( get_theme_file_path() . '/dist/css/amp-single-post--' . $site . '.min.css' ) ) {
		$template = file_get_contents( get_theme_file_path() . '/dist/css/amp-single-post--' . $site . '.min.css' );

		// Other sites
	} else if ( file_exists( get_theme_file_path() . '/assets/css/amp-single-post--' . $site . '.min.css' ) ) {
		$template = file_get_contents( get_theme_file_path() . '/assets/css/amp-single-post--' . $site . '.min.css' );
	}

	echo $template;

} );

/**
 *  Add meta description
 */
add_action( 'amp_post_template_head', function ( $amp_template ) {
	$metadescription = function_exists( 'get_seo_meta_value' ) ? get_seo_meta_value( 'metadesc', $amp_template->get( 'post' )->ID ) : '';
	$description     = ! empty( $metadescription ) ? $metadescription : ( ! empty( $amp_template->get( 'post' )->post_excerpt ) ? $amp_template->get( 'post' )->post_excerpt : $amp_template->get( 'post' )->post_title );
	?>
	<meta name="description" content="<?php echo esc_attr( wp_strip_all_tags( $description ) ); ?>">
	<?php
} );

/**
 * Add logo to structured data (ld+json)
 */
add_filter( 'amp_site_icon_url', function ( $data ) {
	$logo = tbm_wp_get_attachment_image_url( get_field( 'tbm_site_amp_logo', 'options' ), array( 1000, 0 ) );
	if ( $logo ) {
		return $logo;
	}
} );

/**
 * Fix validation warning for amp-ad component
 */
add_filter( 'amp_post_template_data', function ( $data ) {

	$data['amp_component_scripts'] = array_merge(
			$data['amp_component_scripts'],
			array(
					'amp-ad' => 'https://cdn.ampproject.org/v0/amp-ad-0.1.js',
			)
	);

	return $data;
} );

/**
 * Add placeholder image to JSON-LD data (otherwise structured data test fails)
 */
add_filter( 'amp_post_template_metadata', function ( $metadata ) {
	/**
	 * Get site name
	 */
	$site = tbm_get_domain();

	/**
	 * Change type to Article
	 */
	$metadata['@type'] = 'Article';


	/**
	 * Fix logo, cfr.
	 */
	$logo = $metadata['publisher']['logo'];

	if ( ! is_array( $logo ) && filter_var( $logo, FILTER_VALIDATE_URL ) ) {
		$metadata['publisher']['logo'] = array( '@type' => 'ImageObject', 'url' => $logo );
	}

	/**
	 * If image is present, return metadata
	 */
	if ( isset( $metadata['image'] ) ) {
		return $metadata;
	}

	/**
	 * If image is not present, set the generic images and return metadata
	 */
	if ( empty( $site ) ) {
		return $metadata;
	}

	/**
	 * Check if is Sage based template
	 */
	if ( function_exists( 'App\locate_template' ) ) {
		$image_url = get_template_directory() . '/../dist/images/' . $site . '_amp_generic_thumb.png';
	} else {
		$image_url = get_template_directory() . '/dist/images/' . $site . '_amp_generic_thumb.png';
	}

	$metadata['image'] = [
			'@type'  => 'ImageObject',
			'url'    => $image_url,
			'width'  => 1920,
			'height' => 1080
	];


	return $metadata;

} );

/**
 *  Add sticky-ad code at content bottom
 */
add_filter( 'the_content', function ( $content ) {
	/**
	 * Act only in AMP content
	 */
	if ( ! tbm_is_amp() ) {
		return $content;
	}

	ob_start();
	include_once( HTML_TBM_COMMON_PLUGIN_DIR . 'templates/partials/amp/ad-sticky.php' );
	$sticky_ad = ob_get_contents();
	ob_end_clean();

	if ( ! empty( $sticky_ad ) ) {
		$content = $content . $sticky_ad;
	}

	return $content;
}, 11 );

/**
 *  Add CTA readmore and banner after 1st paragraph
 */
add_filter( 'the_content', function ( $content ) {
	/**
	 * Act only in AMP content
	 */
	if ( ! tbm_is_amp() ) {
		return $content;
	}

	ob_start();
	include_once( HTML_TBM_COMMON_PLUGIN_DIR . 'templates/partials/amp/readmore.php' );
	$readmore_code = ob_get_contents();
	ob_end_clean();

	if ( ! empty( $readmore_code ) ) {
		$newcontent = tbm_insert_after_paragraph( $readmore_code, 1, $content );
		if ( ! empty( $newcontent ) ) {
			return $newcontent;
		}
	}

	return $content;
}, 11 );

/**
 * Add teads banner in amp post, after 1st paragraph - after READMORE button
 */
add_filter( 'the_content', function ( $content ) {
	/**
	 * Act only in AMP content
	 */
	if ( ! tbm_is_amp() ) {
		return $content;
	}

	$enabled_post_type = get_field( 'tbm_common_teads_shortcode', 'option' ) ?: array();

	// If TEADS is disabled, esc
	if ( ! get_field( 'tbm_common_teads_shortcode_enabled', 'option' ) || ! in_array( get_post_type(), $enabled_post_type ) ) {
		return $content;
	}

	// Se ha già un ID da shortcode, esco
	if ( strpos( $content, 'ghteads' ) && ! get_field( 'tbm_common_teads_force', 'option' ) ) {
		return $content;
	}

	ob_start();
	include_once( HTML_TBM_COMMON_PLUGIN_DIR . 'templates/partials/amp/ad-teads.php' );
	$ad_code = ob_get_contents();
	ob_end_clean();

	if ( ! empty( $ad_code ) ) {
		$newcontent = tbm_insert_after_paragraph( $ad_code, 3, $content );

		if ( ! empty( $newcontent ) ) {
			return $newcontent;
		}
	}

	return $content;
}, 10 );

/**
 * Disabled Yoast JSON-LD Schema
 */
add_filter( 'wpseo_json_ld_output', function ( $this_data ) {
	/**
	 * Act only in AMP content
	 */
	if ( ! tbm_is_amp() ) {
		return $this_data;
	}

	return false;

} );

/**
 * Register AMP global navigation menu
 */
add_action( 'after_setup_theme', function () {
	if ( ! tbm_is_amp_plugin_enabled() ) {
		return;
	}
	register_nav_menus( [
			'amp_hamburger_navigation' => __( 'AMP Hamburger', 'sage' ),
			'amp_slider_navigation'    => __( 'AMP Slider', 'sage' ),
	] );
} );



