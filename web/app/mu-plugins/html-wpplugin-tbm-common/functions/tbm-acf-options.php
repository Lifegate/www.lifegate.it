<?php
/**
 * Created by PhpStorm.
 * User: francesco
 * Date: 27/07/2018
 * Time: 15:23
 */

/**
 *
 * Opzioni generali
 */
if ( function_exists( 'acf_add_options_page' ) ) {
	acf_add_options_page( array(
		'page_title' => 'TBM Config',
		'menu_slug'  => 'tbm_config',
		'icon_url'   => 'dashicons-list-view',
		'capability' => 'activate_plugins'
	) );

	acf_add_options_sub_page( array(
		'page_title'  => 'Configurazione TBM Common',
		'menu_title'  => '[Common] Configurazione',
		'menu_slug'   => 'common_options',
		'parent_slug' => 'tbm_config',
		'capability'  => 'activate_plugins'
	) );

}

function load_field_groups() {

	if ( ! function_exists( 'acf_add_local_field_group' ) ) {
		return '';
	}

	acf_add_local_field_group( array(
		'key'                   => 'group_5b5b21dc1bfa8',
		'title'                 => 'Tbm Common',
		'fields'                => array(
			/** Dati di base TAB */
			array(
				'key'               => 'field_5c601a072uy74',
				'label'             => 'Dati di base',
				'name'              => '',
				'type'              => 'tab',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'placement'         => 'top',
				'endpoint'          => 0,
			),
			array(
				'key'           => 'field_5d67e3c2f2df2',
				'label'         => 'Logo',
				'name'          => 'tbm_site_amp_logo',
				'type'          => 'image',
				'instructions'  => 'Indicare l\'immagine da usare come logo nel nelle pagine AMP e in altri punti del sito',
				'return_format' => 'id',
				'preview_size'  => 'medium',
				'library'       => 'all',
				'wrapper'       => array(
					'width' => '50',
					'class' => '',
					'id'    => '',
				),
			),
			array(
				'key'           => 'field_5d67e3c2f3rft5',
				'label'         => 'Immagine di default',
				'name'          => 'tbm_site_default_image',
				'type'          => 'image',
				'instructions'  => 'Indicare l\'immagine di default da utilizzare quando non è presente la featured image o quando la featured image è inferiore a 980 pixel',
				'return_format' => 'id',
				'preview_size'  => 'medium',
				'library'       => 'all',
				'min_width'         => 980,
				'min_height'        => 550,
				'wrapper'       => array(
					'width' => '50',
					'class' => '',
					'id'    => '',
				),
			),
			array(
				'key'          => 'field_3eb219b7f1y75',
				'label'        => 'Facebook App ID',
				'name'         => 'tbm_fb_app_id',
				'type'         => 'text',
				'instructions' => '',
				'wrapper'      => array(
					'width' => '33',
					'class' => '',
					'id'    => '',
				),
			),
			array(
				'key'          => 'field_34ijmb7f1y75',
				'label'        => 'Google Analytics ID (es: UA-272662-3)',
				'name'         => 'tbm_ga_id',
				'type'         => 'text',
				'instructions' => '',
				'wrapper'      => array(
					'width' => '34',
					'class' => '',
					'id'    => '',
				),
			),
			array(
				'key'          => 'field_34ijmb74fgtd6',
				'label'        => 'Shinystat ID (es: TRO-greenstyleit)',
				'name'         => 'tbm_shinystat_id',
				'type'         => 'text',
				'instructions' => '',
				'wrapper'      => array(
					'width' => '33',
					'class' => '',
					'id'    => '',
				),
			),
			array(
				'key'          => 'field_5df5e4b0fb2ab',
				'label'        => 'Tassonomie obbligatorie e suggerite',
				'name'         => '',
				'type'         => 'message',
				'instructions' => 'Tassonomie obbligatorie e suggerite',
				'wrapper'      => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'message'      => 'Seleziona le tassonomie obbligatorie e suggerite. Le selezioni verranno usate nella dashboard per segnalare visualmente l\'assenza della tassonomia e permettere di correggere il problema.',
				'new_lines'    => 'wpautop',
				'esc_html'     => 0,
			),
			array(
				'key'               => 'field_5df5e5f290d0b',
				'label'             => 'Tassonomie obbligatorie',
				'name'              => 'tbm_dashboard_mandatory_taxonomies',
				'type'              => 'select',
				'instructions'      => 'Seleziona le tassonomie che devono essere obbligatoriamente aggiunte ad ogni post',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '33',
					'class' => '',
					'id'    => '',
				),
				'choices'           => tbm_acf_taxonomy_select(),
				'default_value'     => array(),
				'allow_null'        => 1,
				'multiple'          => 1,
				'ui'                => 1,
				'return_format'     => 'value',
				'ajax'              => 0,
				'placeholder'       => '',
			),
			array(
				'key'               => 'field_5df5e6a090d0c',
				'label'             => 'Tassonomie consigliate',
				'name'              => 'tbm_dashboard_suggested_taxonomies',
				'type'              => 'select',
				'instructions'      => 'Seleziona le tassonomie che è bene siano aggiunte ad ogni post, ma non sono obbligatorie',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '34',
					'class' => '',
					'id'    => '',
				),
				'choices'           => tbm_acf_taxonomy_select(),
				'default_value'     => array(),
				'allow_null'        => 1,
				'multiple'          => 1,
				'ui'                => 1,
				'return_format'     => 'value',
				'ajax'              => 0,
				'placeholder'       => '',
			),
			array(
				'key'               => 'field_5df5e6a873td5',
				'label'             => 'Tassonomia speciale',
				'name'              => 'tbm_dashboard_topic_taxonomy',
				'type'              => 'select',
				'instructions'      => 'Seleziona la tassonomia usata per gli speciali/storie/trend o per i topic più di attualità',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '33',
					'class' => '',
					'id'    => '',
				),
				'choices'           => tbm_acf_taxonomy_select(),
				'default_value'     => array(),
				'allow_null'        => 1,
				'multiple'          => 0,
				'ui'                => 1,
				'return_format'     => 'value',
				'ajax'              => 0,
				'placeholder'       => '',
			),
			array(
				'key'          => 'field_5df5e4b0fbdab',
				'label'        => 'Fast trigger',
				'name'         => '',
				'type'         => 'message',
				'instructions' => 'Fast trigger',
				'message'      => 'Seleziona quali trigger mostrare nella dashoboard. I trigger sono pulsanti per la selezione veloce di contenuti speciali (p.es. contenuti di primo piano)',
				'new_lines'    => 'wpautop',
				'esc_html'     => 0,
			),
			array(
				'key'               => 'field_5e17351afa15f',
				'label'             => 'Seleziona i trigger',
				'name'              => 'tbm_dashboard_trigger',
				'type'              => 'select',
				'instructions'      => 'Seleziona i fast trigger',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'choices'           => array(
					'featured' => 'Primo Piano',
					'push'     => 'Push notifications',
					'twitter'  => 'Twitter tweets',
					'facebook' => 'Facebook posts',
					'linkedin' => 'Linkedin posts',
				),
				'default_value'     => array(),
				'allow_null'        => 1,
				'multiple'          => 1,
				'ui'                => 1,
				'ajax'              => 0,
				'return_format'     => 'value',
				'placeholder'       => '',
			),

			/** Funzioni generali TAB */
			array(
				'key'               => 'field_5c685a072uy74',
				'label'             => 'Funzioni generali',
				'name'              => '',
				'type'              => 'tab',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'placement'         => 'top',
				'endpoint'          => 0,
			),
			array(
				'key'               => 'field_5b5b3712398uy',
				'label'             => 'Nascondi il contenuto nelle API Rest',
				'name'              => 'tbm_common_rest_hide_content',
				'type'              => 'true_false',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '25',
					'class' => '',
					'id'    => '',
				),
				'message'           => '',
				'default_value'     => 1,
				'ui'                => 1,
				'ui_on_text'        => '',
				'ui_off_text'       => '',
			),
			array(
				'key'               => 'field_5b5b372e23hsyt',
				'label'             => 'Abilita il pulsante di embedder',
				'name'              => 'tbm_common_embedder_enabled',
				'type'              => 'true_false',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '25',
					'class' => '',
					'id'    => '',
				),
				'message'           => '',
				'default_value'     => 0,
				'ui'                => 1,
				'ui_on_text'        => '',
				'ui_off_text'       => '',
			),
			array(
				'key'               => 'field_5b5b37lazyimg',
				'label'             => 'Abilita le immagini Lazy nei contenuti',
				'name'              => 'tbm_common_lazyimgs_enabled',
				'type'              => 'true_false',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '25',
					'class' => '',
					'id'    => '',
				),
				'message'           => '',
				'default_value'     => 0,
				'ui'                => 1,
				'ui_on_text'        => '',
				'ui_off_text'       => '',
			),
			array(
				'key'               => 'field_5b5b37lazyframe',
				'label'             => 'Abilita gli oembed Lazy nei contenuti',
				'name'              => 'tbm_common_lazyiframe_enabled',
				'type'              => 'true_false',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '25',
					'class' => '',
					'id'    => '',
				),
				'message'           => '',
				'default_value'     => 0,
				'ui'                => 1,
				'ui_on_text'        => '',
				'ui_off_text'       => '',
			),
			array(
				'key'               => 'field_5b5b3717776e2',
				'label'             => 'Includi campi editoriali',
				'name'              => 'tbm_common_editorial_field_enabled',
				'type'              => 'true_false',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'message'           => '',
				'default_value'     => 0,
				'ui'                => 1,
				'ui_on_text'        => '',
				'ui_off_text'       => '',
			),
			array(
				'key'               => 'field_5b5b23a091852',
				'label'             => 'Dove includere campi editoriali?',
				'name'              => 'tbm_common_editorial_field',
				'type'              => 'select',
				'instructions'      => 'Seleziona i post type in cui includere, nel backend, i campi editoriali',
				'required'          => 0,
				'conditional_logic' => array(
					array(
						array(
							'field'    => 'field_5b5b3717776e2',
							'operator' => '==',
							'value'    => '1',
						),
					),
				),
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'choices'           => tbm_acf_post_type_select(),
				'default_value'     => array(),
				'allow_null'        => 1,
				'multiple'          => 1,
				'ui'                => 1,
				'ajax'              => 0,
				'return_format'     => 'value',
				'placeholder'       => '',
			),


			array(
				'key'               => 'field_5bb2156ff0i86',
				'label'             => 'Feed Flipboard',
				'name'              => 'tbm_common_flipboard_post_type_shortcode',
				'type'              => 'select',
				'instructions'      => 'Seleziona i post type per che vuoi includere nei feed flipboard e Google Producer. Default <code>post</code>',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '50',
					'class' => '',
					'id'    => '',
				),
				'choices'           => tbm_acf_post_type_select(),
				'default_value'     => array(),
				'allow_null'        => 1,
				'multiple'          => 1,
				'ui'                => 1,
				'ajax'              => 0,
				'return_format'     => 'value',
				'placeholder'       => '',
			),
			array(
				'key'               => 'field_5b5b21de35497',
				'label'             => 'Post type per sitemap news?',
				'name'              => 'tbm_common_news_sitemap_post_type',
				'type'              => 'select',
				'instructions'      => 'Seleziona i post type che vuoi includere nella sitemap News',
				'required'          => 0,
				'conditional_logic' => array(),
				'wrapper'           => array(
					'width' => '50',
					'class' => '',
					'id'    => '',
				),
				'choices'           => tbm_acf_post_type_select(),
				'default_value'     => array(),
				'allow_null'        => 1,
				'multiple'          => 0,
				'ui'                => 1,
				'ajax'              => 0,
				'return_format'     => 'value',
				'placeholder'       => '',
			),


			/** Funzioni pubblicitarie TAB */
			array(
				'key'               => 'field_5c685a072lb7t',
				'label'             => 'Funzioni pubblicitarie',
				'name'              => '',
				'type'              => 'tab',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'placement'         => 'top',
				'endpoint'          => 0,
			),
			array(
				'key'               => 'field_5b5b36d8776e1',
				'label'             => 'Includi video ADV',
				'name'              => 'tbm_common_video_shortcode_enabled',
				'type'              => 'true_false',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'message'           => '',
				'default_value'     => 0,
				'ui'                => 1,
				'ui_on_text'        => '',
				'ui_off_text'       => '',
			),
			array(
				'key'               => 'field_5b5b21de57730',
				'label'             => 'Dove includere il video ADV?',
				'name'              => 'tbm_common_video_shortcode',
				'type'              => 'select',
				'instructions'      => 'Seleziona i post type in cui vuoi includere automaticamente lo shortcode video',
				'required'          => 0,
				'conditional_logic' => array(
					array(
						array(
							'field'    => 'field_5b5b36d8776e1',
							'operator' => '==',
							'value'    => '1',
						),
					),
				),
				'wrapper'           => array(
					'width' => '40',
					'class' => '',
					'id'    => '',
				),
				'choices'           => tbm_acf_post_type_select(),
				'default_value'     => array(),
				'allow_null'        => 1,
				'multiple'          => 1,
				'ui'                => 1,
				'ajax'              => 0,
				'return_format'     => 'value',
				'placeholder'       => '',
			),
			array(
				'key'               => 'tc_5bb217eff7sjdgh',
				'label'             => 'Forzo posizionamento?',
				'name'              => 'tbm_common_video_force',
				'type'              => 'true_false',
				'instructions'      => 'Se abilitato, viene ignorato il posizionamento manuale del video random',
				'required'          => 0,
				'conditional_logic' => array(
					array(
						array(
							'field'    => 'field_5b5b36d8776e1',
							'operator' => '==',
							'value'    => '1',
						),
					),
				),
				'wrapper'           => array(
					'width' => '20',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => 0,
				'ui'                => 1,
			),
			array(
				'key'               => 'field_5bb219b7f0d7c',
				'label'             => 'Dopo quale tag includere il video?',
				'name'              => 'tbm_common_video_shortcode_tag',
				'type'              => 'text',
				'instructions'      => 'Indicare il tag prima del quale includere il video (p.es. h2). Ha priorità rispetto al numero di paragrafi.',
				'required'          => 0,
				'conditional_logic' => array(
					array(
						array(
							'field'    => 'field_5b5b36d8776e1',
							'operator' => '==',
							'value'    => '1',
						),
					),
				),
				'wrapper'           => array(
					'width' => '20',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
			),
			array(
				'key'               => 'field_5bb219ecf0d7d',
				'label'             => 'Dopo quanti paragrafi includere il video?',
				'name'              => 'tbm_common_video_shortcode_paragraph',
				'type'              => 'number',
				'instructions'      => 'Indicare il numero di paragrafi dopo i quali includere il video (è secondario rispetto al tag del campo precedente)',
				'required'          => 0,
				'conditional_logic' => array(
					array(
						array(
							'field'    => 'field_5b5b36d8776e1',
							'operator' => '==',
							'value'    => '1',
						),
					),
				),
				'wrapper'           => array(
					'width' => '20',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => 2,
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'min'               => 0,
				'max'               => 10,
				'step'              => '',
			),
			array(
				'key'               => 'field_5bb21556f0d78',
				'label'             => 'Includi Native',
				'name'              => 'tbm_common_native_shortcode_enabled',
				'type'              => 'true_false',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'message'           => '',
				'default_value'     => 0,
				'ui'                => 1,
				'ui_on_text'        => '',
				'ui_off_text'       => '',
			),
			array(
				'key'               => 'field_5bb2156ff0d79',
				'label'             => 'Dove includere il native?',
				'name'              => 'tbm_common_native_shortcode',
				'type'              => 'select',
				'instructions'      => 'Seleziona i post type in cui vuoi includere automaticamente lo shortcode per far comparire il native',
				'required'          => 0,
				'conditional_logic' => array(
					array(
						array(
							'field'    => 'field_5bb21556f0d78',
							'operator' => '==',
							'value'    => '1',
						),
					),
				),
				'wrapper'           => array(
					'width' => '40',
					'class' => '',
					'id'    => '',
				),
				'choices'           => tbm_acf_post_type_select(),
				'default_value'     => array(),
				'allow_null'        => 1,
				'multiple'          => 1,
				'ui'                => 1,
				'ajax'              => 0,
				'return_format'     => 'value',
				'placeholder'       => '',
			),
			array(
				'key'               => 'tc_5bb217eff78idk6',
				'label'             => 'Forzo posizionamento?',
				'name'              => 'tbm_common_native_force',
				'type'              => 'true_false',
				'instructions'      => 'Se abilitato, viene ignorato il posizionamento manuale',
				'required'          => 0,
				'conditional_logic' => array(
					array(
						array(
							'field'    => 'field_5bb21556f0d78',
							'operator' => '==',
							'value'    => '1',
						),
					),
				),
				'wrapper'           => array(
					'width' => '20',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => 0,
				'ui'                => 1,
			),
			array(
				'key'               => 'field_5bb217eff0d7b',
				'label'             => 'Dopo quale tag includere il native?',
				'name'              => 'tbm_common_native_shortcode_tag',
				'type'              => 'text',
				'instructions'      => 'Indicare il tag prima del quale includere il native (p.es. h2). Ha priorità rispetto al numero di paragrafi.',
				'required'          => 0,
				'conditional_logic' => array(
					array(
						array(
							'field'    => 'field_5bb21556f0d78',
							'operator' => '==',
							'value'    => '1',
						),
					),
				),
				'wrapper'           => array(
					'width' => '20',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
			),
			array(
				'key'               => 'field_5bb215d7f0d7a',
				'label'             => 'Dopo quanti paragrafi includere il native?',
				'name'              => 'tbm_common_native_shortcode_paragraph',
				'type'              => 'number',
				'instructions'      => 'Indicare il numero di paragrafi dopo i quali includere il native (è secondario rispetto al tag del campo precedente)',
				'required'          => 0,
				'conditional_logic' => array(
					array(
						array(
							'field'    => 'field_5bb21556f0d78',
							'operator' => '==',
							'value'    => '1',
						),
					),
				),
				'wrapper'           => array(
					'width' => '20',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => 2,
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'min'               => 0,
				'max'               => 10,
				'step'              => '',
			),
			array(
				'key'               => 'field_5bb21556f04rt',
				'label'             => 'Includi Teads',
				'name'              => 'tbm_common_teads_shortcode_enabled',
				'type'              => 'true_false',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'message'           => '',
				'default_value'     => 0,
				'ui'                => 1,
				'ui_on_text'        => '',
				'ui_off_text'       => '',
			),
			array(
				'key'               => 'field_5bb2156ff0i85',
				'label'             => 'Dove includere il Teads?',
				'name'              => 'tbm_common_teads_shortcode',
				'type'              => 'select',
				'instructions'      => 'Seleziona i post type in cui vuoi includere automaticamente lo shortcode per far comparire il teads',
				'required'          => 0,
				'conditional_logic' => array(
					array(
						array(
							'field'    => 'field_5bb21556f04rt',
							'operator' => '==',
							'value'    => '1',
						),
					),
				),
				'wrapper'           => array(
					'width' => '40',
					'class' => '',
					'id'    => '',
				),
				'choices'           => tbm_acf_post_type_select(),
				'default_value'     => array(),
				'allow_null'        => 1,
				'multiple'          => 1,
				'ui'                => 1,
				'ajax'              => 0,
				'return_format'     => 'value',
				'placeholder'       => '',
			),
			array(
				'key'               => 'tc_5bb217eff7yha',
				'label'             => 'Forzo posizionamento?',
				'name'              => 'tbm_common_teads_force',
				'type'              => 'true_false',
				'instructions'      => 'Se abilitato, viene ignorato il posizionamento manuale',
				'required'          => 0,
				'conditional_logic' => array(
					array(
						array(
							'field'    => 'field_5bb21556f04rt',
							'operator' => '==',
							'value'    => '1',
						),
					),
				),
				'wrapper'           => array(
					'width' => '20',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => 0,
				'ui'                => 1,
			),
			array(
				'key'               => 'field_5bb217eff8u62',
				'label'             => 'Dopo quale tag includere Teads?',
				'name'              => 'tbm_common_teads_shortcode_tag',
				'type'              => 'text',
				'instructions'      => 'Indicare il tag prima del quale includere il teads (p.es. h2). Ha priorità rispetto al numero di paragrafi.',
				'required'          => 0,
				'conditional_logic' => array(
					array(
						array(
							'field'    => 'field_5bb21556f04rt',
							'operator' => '==',
							'value'    => '1',
						),
					),
				),
				'wrapper'           => array(
					'width' => '20',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
			),
			array(
				'key'               => 'field_5bb215d7f6tg1',
				'label'             => 'Dopo quanti paragrafi includere il teads?',
				'name'              => 'tbm_common_teads_shortcode_paragraph',
				'type'              => 'number',
				'instructions'      => 'Indicare il numero di paragrafi dopo i quali includere il teads (è secondario rispetto al tag del campo precedente)',
				'required'          => 0,
				'conditional_logic' => array(
					array(
						array(
							'field'    => 'field_5bb21556f04rt',
							'operator' => '==',
							'value'    => '1',
						),
					),
				),
				'wrapper'           => array(
					'width' => '20',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => 2,
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'min'               => 0,
				'max'               => 10,
				'step'              => '',
			),

			/** Funzioni TMS TAB */
			array(
				'key'               => 'field_5c685a072tr38',
				'label'             => 'Funzioni TMS',
				'name'              => '',
				'type'              => 'tab',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'placement'         => 'top',
				'endpoint'          => 0,
			),
			array(
				'key'               => 'field_5c7f7c6390d59',
				'label'             => 'Information',
				'name'              => '',
				'type'              => 'message',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '',
					'class' => '',
					'id'    => '',
				),
				'message'           => 'Add <code>tbm_wphead()</code> between <code>&lt;head&gt; [...] &lt;/head&gt;</code> tag to enable all this inclusion. The function will add, in this particular order, the <em>disable_banner</em> code, the <em>datalayer</em>, the Atatus code, the GTM code and, finally, the Tealium (TMS) code.',
				'new_lines'         => 'wpautop',
				'esc_html'          => 0,
			),
			array(
				'key'               => 'field_5c7e9c5c82888',
				'label'             => 'Abilita Tealium',
				'name'              => 'tbm_common_enable_tealium',
				'type'              => 'true_false',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '100',
					'class' => '',
					'id'    => '',
				),
				'message'           => '',
				'default_value'     => 0,
				'ui'                => 1,
				'ui_on_text'        => '',
				'ui_off_text'       => '',
			),
			array(
				'key'               => 'field_5c7e9c7f82889',
				'label'             => 'Codice Tealium',
				'name'              => 'tbm_common_code_tealium',
				'type'              => 'text',
				'instructions'      => 'Esempio: own-html.it',
				'required'          => 0,
				'conditional_logic' => array(
					array(
						array(
							'field'    => 'field_5c7e9c5c82888',
							'operator' => '==',
							'value'    => '1',
						),
					),
				),
				'wrapper'           => array(
					'width' => '100',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
			),
			array(
				'key'               => 'field_5c7e9cb98288a',
				'label'             => 'Abilita GTM',
				'name'              => 'tbm_common_enable_gtm',
				'type'              => 'true_false',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '100',
					'class' => '',
					'id'    => '',
				),
				'message'           => '',
				'default_value'     => 0,
				'ui'                => 1,
				'ui_on_text'        => '',
				'ui_off_text'       => '',
			),
			array(
				'key'               => 'field_5c7e9cc68288b',
				'label'             => 'Codice GTM',
				'name'              => 'tbm_common_code_gtm',
				'type'              => 'text',
				'instructions'      => 'Esempio: 5QPXTVW',
				'required'          => 0,
				'conditional_logic' => array(
					array(
						array(
							'field'    => 'field_5c7e9cb98288a',
							'operator' => '==',
							'value'    => '1',
						),
					),
				),
				'wrapper'           => array(
					'width' => '100',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
			),
			array(
				'key'               => 'field_5c7e9cf68288d',
				'label'             => 'Abilita Atatus',
				'name'              => 'tbm_common_enable_atatus',
				'type'              => 'true_false',
				'instructions'      => '',
				'required'          => 0,
				'conditional_logic' => 0,
				'wrapper'           => array(
					'width' => '100',
					'class' => '',
					'id'    => '',
				),
				'message'           => '',
				'default_value'     => 0,
				'ui'                => 1,
				'ui_on_text'        => '',
				'ui_off_text'       => '',
			),
			array(
				'key'               => 'field_5c7e9d028288e',
				'label'             => 'Codice Atatus',
				'name'              => 'tbm_common_code_atatus',
				'type'              => 'text',
				'instructions'      => 'Esempio: 2ad206f396a84ae3934185ebb4063bb3',
				'required'          => 0,
				'conditional_logic' => array(
					array(
						array(
							'field'    => 'field_5c7e9cf68288d',
							'operator' => '==',
							'value'    => '1',
						),
					),
				),
				'wrapper'           => array(
					'width' => '100',
					'class' => '',
					'id'    => '',
				),
				'default_value'     => '',
				'placeholder'       => '',
				'prepend'           => '',
				'append'            => '',
				'maxlength'         => '',
			),
		),
		'location'              => array(
			array(
				array(
					'param'    => 'options_page',
					'operator' => '==',
					'value'    => 'common_options',
				),
			),
		),
		'menu_order'            => 0,
		'position'              => 'normal',
		'style'                 => 'default',
		'label_placement'       => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen'        => '',
		'active'                => 1,
		'description'           => '',
		'recaptcha'             => 0,
	) );

}

add_action( 'admin_init', 'load_field_groups' );
