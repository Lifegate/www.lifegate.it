<?php

/**
 * Check timestamp from transient and published all missed posts
 */
add_action( 'init', function () {
	global $wpdb;

	//Set default check interval - every 15 min.
	if ( false === defined( 'MISSED_SCHEDULE_CHECK_INTERVAL' ) ) {
		define( 'MISSED_SCHEDULE_CHECK_INTERVAL', 15 * MINUTE_IN_SECONDS );
	}

	$last_scheduled_missed_time = get_transient( 'tbm_missed_schedule_time' );

	$time = current_time( 'timestamp', 0 );

	if ( false !== $last_scheduled_missed_time && absint( $last_scheduled_missed_time ) > ( $time - MISSED_SCHEDULE_CHECK_INTERVAL ) ) {
		return;
	}

	set_transient( 'tbm_missed_schedule_time', $time, MISSED_SCHEDULE_CHECK_INTERVAL );

	$tbm_query = "SELECT ID FROM {$wpdb->posts} WHERE ( ( post_date > 0 && post_date <= %s ) ) AND post_status = 'future' LIMIT 0,%d";

	$sql = $wpdb->prepare( $tbm_query, current_time( 'mysql', 0 ), 10 );

	$scheduled_post_ids = $wpdb->get_col( $sql );

	if ( ! count( $scheduled_post_ids ) ) {
		return;
	}

	foreach ( $scheduled_post_ids as $scheduled_post_id ) {
		if ( ! $scheduled_post_id ) {
			continue;
		}

		wp_publish_post( $scheduled_post_id );
	}
}, 0 );

/**
 * Preload
 */
if ( class_exists( 'Tbm_Plugins\WP_Async_Request' ) ) {
	class TBM_Preload extends Tbm_Plugins\WP_Async_Request {

		/**
		 * Handle the $_POST data
		 */
		protected function handle() {
			$url = $_POST['preloadurl'];

			if ( $url ) {
				wp_remote_get( $url, array(
					'timeout'    => 1,
					'blocking'   => false,
					'user-agent' => 'TBM Preloader',
					'cookies'    => '',
					'sslverify'  => apply_filters( 'https_local_ssl_verify', false ),
				) );
			}
		}

	}
}
$preload = new TBM_Preload();

/**
 * Action to launch gallery preload
 */
add_action( 'tbm_preload_url', function ( $url ) {
	if ( ! class_exists( 'TBM_Preload' ) ) {
		return false;
	}

	/**
	 * If we can't check the user agent, exit
	 */
	if ( empty( $_SERVER['HTTP_USER_AGENT'] ) ) {
		return false;
	}

	/**
	 * If is a self reqesut, exit
	 */
	if ( strpos( $_SERVER['HTTP_USER_AGENT'], 'Preloader' ) !== false ) {
		return false;
	}

	/**
	 * Instantiate class
	 */
	$preload = new TBM_Preload();

	/**
	 * Dispatch the request
	 */
	$preload->data( array( 'preloadurl' => $url ) );
	$preload->dispatch();
}, 10, 1 );

/**
 * Init all classes and other functions
 */
add_action( 'plugins_loaded', function () {
	/**
	 * Enable News Sitemap if in option
	 */
	if ( get_field( 'tbm_common_news_sitemap_post_type', 'option' ) ) {
		$wpseo_news_xml = new \Tbm_Plugins\TBM_News_Sitemap();
	}

	/**
	 * Enable Flipboard RSS
	 */
	$TBM_mediarss = new \Tbm_Plugins\Flipboard_RSS_Feed();

	/**
	 * Enable SVG Uploader
	 */
	$R34SVG = new \Tbm_Plugins\TBM_SVG_Uploader;

	/**
	 * Enable Policy
	 */
	$TbmPolicy = new \Tbm_Plugins\TBM_Privacy_Policy;

} );

/**
 * Set the correct term in sponsor taxonomy when a post_sponsor ACF fields is saved
 */
add_action( 'acf/save_post', function ( $post_id ) {
	if ( empty( $_POST['acf'] ) ) {
		return;
	}

	$values = $_POST['acf'];

	if ( ! isset( $values['field_5a79fc8e9831c'] ) ) {
		return;
	}

	if ( empty( $values['field_5a79fc8e9831c'] ) && has_term( '', 'sponsor' ) ) {
		wp_delete_object_term_relationships( $post_id, 'sponsor' );
	}

	wp_set_object_terms( $post_id, (int) $values['field_5a79fc8e9831c'], 'sponsor' );

}, 5 );

/**
 * Set the date custom field when a post is selected for push or twitter
 */
add_action( 'acf/save_post', function ( $post_id ) {
	if ( empty( $_POST['acf'] ) ) {
		return;
	}

	$values = $_POST['acf'];


	// Get the old values
	$push     = get_field( 'tbm_pushed' );  // field_5a5a02927cxd4
	$twitter  = get_field( 'tbm_twittered' ); // field_5a5a02927dmxn8
	$facebook = get_field( 'tbm_facebooked' ); // field_5a5a02927tr65g
	$linkedin = get_field( 'tbm_linkedined' ); // field_5a5a02927dx653s

	// Don't act if ACF values are not present
	if ( ! isset( $values['field_5a5a02927cxd4'] ) && ! isset( $values['field_5a5a02927dmxn8'] ) && ! isset( $values['field_5a5a02927tr65g'] ) ) {
		return;
	}

	// Delete date if setted push date
	if ( empty( $values['field_5a5a02927cxd4'] ) && get_post_meta( $post_id, 'tbm_pushed_date' ) ) {
		delete_post_meta( $post_id, 'tbm_pushed_date' );
	}

	// Delete date if not setted twitter date
	if ( empty( $values['field_5a5a02927dmxn8'] ) && get_post_meta( $post_id, 'tbm_twittered_date' ) ) {
		delete_post_meta( $post_id, 'tbm_twittered_date' );
	}

	// Delete date if not setted facebook date
	if ( empty( $values['field_5a5a02927tr65g'] ) && get_post_meta( $post_id, 'tbm_facebooked_date' ) ) {
		delete_post_meta( $post_id, 'tbm_facebooked_date' );
	}

	// Delete date if not setted linkedin date
	if ( empty( $values['field_5a5a02927dx653s'] ) && get_post_meta( $post_id, 'tbm_linkedined_date' ) ) {
		delete_post_meta( $post_id, 'tbm_linkedined_date' );
	}

	// Pushes only if old value was !== 1
	if ( empty( $push ) && $values['field_5a5a02927cxd4'] === '1' ) {
		update_post_meta( $post_id, 'tbm_pushed_date', time() );
	}

	// Twitters only if old value was !== 1
	if ( empty( $twitter ) && $values['field_5a5a02927dmxn8'] === '1' ) {
		update_post_meta( $post_id, 'tbm_twittered_date', time() );
	}

	// linkedins only if old value was !== 1
	if ( empty( $facebook ) && $values['field_5a5a02927tr65g'] === '1' ) {
		update_post_meta( $post_id, 'tbm_facebooked_date', time() );
	}

	// Linkedins only if old value was !== 1
	if ( empty( $linkedin ) && $values['field_5a5a02927dx653s'] === '1' ) {
		update_post_meta( $post_id, 'tbm_linkedined_date', time() );
	}
}, 5 );

/**
 * Add Adblocker code to footer
 */
/*
add_action( 'wp_footer', function () {
	$js = file_get_contents( HTML_TBM_COMMON_PLUGIN_DIR . '/dist/js/tbm-adblocker.min.js', true );
	if ( empty( $js ) ) {
		echo '';
	} else {
		echo '<script>' . $js . '</script>';
	}
} );
*/
/**
 * Add GTM code to header
 *
 */
add_action( 'admin_head', function () {
	global $current_user, $wp_query;
	/**
	 * Set Datalayer
	 */
	$dataLayer              = array();
	$dataLayer["userEmail"] = ( ! empty( $current_user->data->user_email ) ? $current_user->data->user_email : '' );
	$dataLayer["userType"]  = ( ! empty( $current_user->roles[0] ) ? $current_user->roles[0] : '' );
	$dl = isset( $dataLayer ) ? "var wpLayer = [" . json_encode( $dataLayer ) . "];" : '';



	/**
	 * SET Gtm loader
	 */
	$js = "(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','wpLayer','GTM-N7RZSXQ');";

	/**
	 * Print script
	 */
	echo sprintf( '<script>%s%s</script>', $dl, $js );
} );

/**
 * Disable image linking
 */
add_action( 'admin_init', function () {
	$image_set = get_option( 'image_default_link_type' );

	if ( $image_set !== 'none' ) {
		update_option( 'image_default_link_type', 'none' );
	}
}, 10 );
