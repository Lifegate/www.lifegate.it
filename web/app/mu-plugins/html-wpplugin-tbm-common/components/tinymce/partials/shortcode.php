<?php
/**
 * Created by PhpStorm.
 * User: francesco
 * Date: 07/02/2018
 * Time: 19:54
 */
?>
<div role="tabpanel" class="tab-pane fade active in" id="cerca">
    <p>Seleziona un tipo di contenuto nel campo <em>Cerca in</em> e un layout di visualizzazione. Digita almeno tre
        lettere per iniziae la ricerca. Digita <strong>123</strong> per l'elenco delle ultime pubblicazioni.</p>
    <div class="form-group">
        <select id="ghshortcode" class="form-control"
                data-ajax--url="<?php echo admin_url( 'admin-ajax.php' ); ?>"
                data-ajax--cache="false">
        </select>

    </div>

    <div class="form-inline form-group">
        <input type="text" class="form-control" id="ghshortcode_title"
               placeholder="titolo (modificabile)">
    </div>
    <div class="form-inline form-group">
        <label class="control-label">Cerca in:&nbsp;&nbsp;</label>
		<?php
		if ( $post_check ) {
			$enabled_post_types       = $post_check['enabled_post_types'];
			$disabled_post_types      = $post_check['disabled_post_types'];
			$disabled_post_types_info = $post_check['disabled_post_types_info'];
		}

		if ( $enabled_post_types ) {
			echo '<div id="posttype" class="btn-group" data-toggle="buttons">';
			echo implode( '', $enabled_post_types );
			echo '</div>';
		} else {
			echo '<mark>Nessun tipo di contenuto selezionato. Scegline uno dalla scheda Opzioni.</mark>';
		}
		?>

    </div>
    <div class="form-inline form-group">
        <label class="control-label">Layout:&nbsp;</label>
		<?php
		if ( $layouts_check ) {
			echo '<div id="posttype" class="btn-group" data-toggle="buttons">';
			echo implode( '', $layouts_check );
			echo '</div>';
		} else {
			echo '<mark>Nessun layout disponibile. Aggiungine uno nel tema.</mark>';
		}
		?>
    </div>
    <div id="draft" class="checkbox">
        <label>
            <input type="checkbox" value="1" name="draft"> Includi bozze
        </label>
    </div>
    <div class="form-group">
        <input type="submit" id="ghshortcode-submit-close" class="btn btn-default"
               value="Inserisci e chiudi" name="submit"/>
        <input type="submit" id="ghshortcode-submit" class="btn btn-default" value="Inserisci"
               name="submit"/>
        <input type="submit" id="ghshortcode-close" class="btn btn-link" value="Annulla"
               name="submit"/>
    </div>
    <hr style="margin:5px 0 10px 0">
    <div class="row">
        <div class="col-md-12 message">
			<?php if ( ! empty( $disabled_post_types ) ) : ?>
                <div class="alert alert-info alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    Per i seguenti contenuti non esiste un modello di visualizzazione e dunque sono stati
                    disabilitati: <?php echo implode( ', ', $disabled_post_types ) ?>. Chiedi ad uno sviluppatore di
                    crearli oppure disabilita il tipo di contenuto dalla scheda
                    <mark>Opzioni</mark>
                    .
                </div>
			<?php endif; ?>
        </div>
    </div>
</div>

