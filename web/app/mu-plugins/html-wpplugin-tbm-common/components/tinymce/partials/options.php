<?php
/**
 * Created by PhpStorm.
 * User: francesco
 * Date: 07/02/2018
 * Time: 19:53
 */
$ghOptions     = get_option( 'gh_shortcode_options', '' );
$ghSearchNonce = wp_create_nonce( 'gh_short_search_posts' );
if ( current_user_can( 'moderate_comments' ) ) {
	$ghSaveNonce = wp_create_nonce( 'gh_short_save_options' );
} else {
	$ghSaveNonce = '';
}
require_once( HTML_TBM_COMMON_PLUGIN_DIR . 'functions/functions.php' );
$post_types = get_post_types( array( 'public' => true ), 'objects' );
?>

<div role="tabpanel" class="tab-pane fade" id="opzioni">
    <form id="ghshortcode-options">

        <p>Seleziona i tipi di contenuto che vorresti abilitare per l'inclusione. Ogni tipo di
            contenuto dovrebbe avere un modello di visualizzazione.</p>
        <div class="form-group">
			<?php
			foreach ( $post_types as $post_type ) {
				echo '<label class="checkbox-inline"><input type="checkbox" id="sc' . $post_type->name . '" name="sc' . $post_type->name . '" value="sc[' . $post_type->name . ']"> ' . $post_type->label . '</label>';
			}
			?>
        </div>
    </form>
    <div class="form-group">
        <button id="ghshortcode-options-save" class="btn btn-default ladda-button"
                data-style="expand-right" data-spinner-color="#444"><span class="ladda-label">Salva</span>
        </button>
        <input type="submit" id="ghshortcode-options-close" class="btn btn-link" value="Annulla"
               name="submit"/>
    </div>

</div>
<div role="tabpanel" class="tab-pane fade" id="info">
    <p>Il modulo serve per includere, sotto forma di shortcode, all'interno di un post un
        riferimento ad un altro contenuto presente nel sito. I contenuti da abilitare per
        l'inclusione possono essere scelti dalla scheda
        <mark>Opzioni</mark>
        . Per poter valorizzare il contenuto serve integrare un template in grado di traformare
        lo shortcode in un elemento visuale. Qui sotto vengono indicati, se presenti, gli
        shortcode mancanti per i tipi di post abilitati.
    </p>
    <p><?php echo implode( ', ', $disabled_post_types_info ); ?></p>
</div>
