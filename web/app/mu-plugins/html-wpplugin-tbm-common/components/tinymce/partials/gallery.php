<?php
/**
 * Created by PhpStorm.
 * User: francesco
 * Date: 07/02/2018
 * Time: 19:54
 */
?>
<div role="tabpanel" class="tab-pane fade" id="galleria">
    <p>Seleziona una galleria da includere nel post. Digita <strong>123</strong> per l'elenco delle ultime pubblicazioni.
    </p>
    <div class="form-group">
        <select id="ghgalleria" class="form-control"
                data-ajax--url="<?php echo admin_url( 'admin-ajax.php' ); ?>"
                data-ajax--cache="false"
        style="width: 100%">
        </select>

    </div>

    <div class="form-inline form-group">
        <input type="text" class="form-control" id="ghgalleria_title" placeholder="Titolo (modificabile)">
    </div>

    <!--div class="form-inline form-group">
        <label class="control-label">Layout:&nbsp;&nbsp;</label>

        <div id="ghgalleria_button" class="btn-group" data-toggle="buttons">
            <label class="btn btn-sm btn-info"><input type="radio" autocomplete="off" name="gallery" id="gallery_slider" value="gallery_slider">Slider</label>
            <label class="btn btn-sm btn-info"><input type="radio" autocomplete="off" name="gallery" id="gallery_link" value="gallery_link">Link</label>
        </div>
    </div-->
    <div class="form-group">
        <input type="submit" id="ghgalleria-submit-close" class="btn btn-default"
               value="Inserisci e chiudi" name="submit"/>
        <input type="submit" id="ghgalleria-submit" class="btn btn-default" value="Inserisci"
               name="submit"/>
        <input type="submit" id="ghgalleria-close" class="btn btn-link" value="Annulla"
               name="submit"/>
    </div>
</div>
