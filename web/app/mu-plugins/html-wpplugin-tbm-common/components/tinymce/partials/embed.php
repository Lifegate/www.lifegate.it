<?php
/**
 * Created by PhpStorm.
 * User: francesco
 * Date: 07/02/2018
 * Time: 19:52
 */
?>

<div role="tabpanel" class="tab-pane fade" id="embed">
    <form id="ghembed-services" class="form-horizontal">
        <p>Aggiungi il codice necessario alla pubblicazione dell'embed e invia.</p>
        <div class="form-group">
            <label for="embed_pinterest" class="col-sm-2 control-label">Pinterest</label>
            <div class="col-sm-10">
                <input type="text" class="embed_service form-control" id="embed_pinterest"
                       placeholder="L'id del pin da embeddare">
            </div>
        </div>
    </form>
    <div class="form-group">
        <input type="submit" id="ghembed-submit-close" class="btn btn-default"
               value="Inserisci e chiudi" name="submit"/>
        <input type="submit" id="ghembed-submit" class="btn btn-default" value="Inserisci"
               name="submit"/>
        <input type="submit" id="ghembed-close" class="btn btn-link" value="Annulla"
               name="submit"/>
    </div>
</div>
