<?php
/**
 * Enable panel only if we haves tracking_id avilable
 */
$enabled = get_option( 'tbm_products_tracking_id', '' );
if ( $enabled ) :
	?>
    <div role="tabpanel" class="tab-pane fade" id="product">
        <p>Digita almeno tre lettere per iniziae la ricerca. Digita <strong>123</strong> per l'elenco delle ultime
            pubblicazioni.</p>
        <div class="form-group">
            <select id="ghproduct" class="form-control"
                    data-ajax--url="<?php echo admin_url( 'admin-ajax.php' ); ?>"
                    data-ajax--cache="false"
                    style="width: 100%">
            </select>
        </div>
        <div id="draft" class="checkbox">
            <label>
                <input type="checkbox" value="1" name="draft"> Cerca anche nelle bozze
            </label>
        </div>
        <div class="form-inline form-group">
            <input type="text" class="form-control" id="ghproduct_title"
                   placeholder="Titolo (sovrascrive originale)" style="width: 100%">
        </div>
        <div class="form-inline form-group">
            <input type="text" class="form-control" id="ghproduct_badge"
                   placeholder="Badge (sovrascrive originale)" style="width: 100%">
        </div>
        <div class="form-inline form-group">
            <input type="text" class="form-control" id="ghproduct_claim"
                   placeholder="Claim (sovrascrive originale)" style="width: 100%">
        </div>
        <div class="form-inline form-group">
            <label class="control-label">Layout:&nbsp;</label>
            <div id="posttype" class="btn-group" data-toggle="buttons">
                <label class="btn btn-sm btn-primary active">
                    <input type="radio" autocomplete="off" name="product_layout" id="product_slim" value="product_slim" checked>Slim</label>
                <label class="btn btn-sm btn-primary">
                    <input type="radio" autocomplete="off" name="product_layout" id="product_full" value="product_full">Full</label>
            </div>
        </div>

        <div class="form-group">
            <input type="submit" id="ghproduct-submit-close" class="btn btn-default"
                   value="Inserisci e chiudi" name="submit"/>
            <input type="submit" id="ghproduct-submit" class="btn btn-default" value="Inserisci"
                   name="submit"/>&nbsp;
            <input type="submit" id="ghproduct-summary-submit" class="btn btn-default" value="Inserisci sommario"
                   name="submit"/>&nbsp;
            <input type="submit" id="ghproduct-close" class="btn btn-link" value="Annulla"
                   name="submit"/>
        </div>
    </div>
    <hr style="margin:5px 0 10px 0">
    <div class="row">
        <div class="col-md-12 message">
        </div>
    </div>
<?php
else :
	?>
    <div role="tabpanel" class="tab-pane fade" id="product">
        <div class="notice notice-error notice-large">
            <p>Al momento non sono disponibili Tracking ID, necessari per il tracciamento degli articoli. Aggiungi dei
                Tracking ID e riprova.</p>
        </div>
    </div>
<?php
endif;
