<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( "Can't load this file directly" );
}
@header( 'Content-Type: ' . get_option( 'html_type' ) . '; charset=' . get_option( 'blog_charset' ) );
$ghOptions     = get_option( 'gh_shortcode_options', '' );
$ghSearchNonce = wp_create_nonce( 'gh_short_search_posts' );
if ( current_user_can( 'moderate_comments' ) ) {
	$ghSaveNonce = wp_create_nonce( 'gh_short_save_options' );
} else {
	$ghSaveNonce = '';
}

require_once( HTML_TBM_COMMON_PLUGIN_DIR . 'functions/functions.php' );

// Show product tab only if products are visible
$products_pt = defined( 'HTML_PRODUCTS_POST_TYPE' ) && post_type_exists( HTML_PRODUCTS_POST_TYPE ) && wp_count_posts( HTML_PRODUCTS_POST_TYPE ) > 0;
$video_pt    = defined( 'HTML_VIDEO_POST_TYPE' ) && post_type_exists( HTML_VIDEO_POST_TYPE ) && wp_count_posts( HTML_VIDEO_POST_TYPE ) > 0;
$gallery_pt  = defined( 'HTML_GALLERY_POST_TYPE' ) && post_type_exists( HTML_GALLERY_POST_TYPE ) && wp_count_posts( HTML_GALLERY_POST_TYPE ) > 0;

?>
<!doctype html>
<html lang="it">
<head>
    <meta charset="utf-8"/>
    <title>Inserisci Gallerie e Video</title>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootswatch/3.3.7/paper/bootstrap.min.css"/>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/ladda-bootstrap/0.9.4/ladda-themeless.min.css"/>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/i18n/it.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/ladda-bootstrap/0.9.4/spin.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/ladda-bootstrap/0.9.4/ladda.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/ladda-bootstrap/0.9.4/ladda.jquery.min.js"></script>

    <style type="text/css">
        .nav-tabs > li.active > a,
        .nav-tabs > li.active > a:focus {
            border: none;
            -webkit-box-shadow: inset 0 -2px 0 #2196f3;
            box-shadow: inset 0 -2px 0 #2196f3;
            color: #2196f3;
        }

        .nav-tabs > li.active > a:hover,
        .nav-tabs > li.active > a:focus:hover {
            border: none;
            color: #2196f3;
        }

        .btn.disabled, .btn[disabled], fieldset[disabled] .btn {
            -webkit-box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.3);
            box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.3);
        }

        .tab-content > .tab-pane {
            padding: 15px 5px 0 5px;
        }
    </style>
</head>
<body style="padding-top: 10px">
<script>
    // Recuper un argomento dall'iniziatore
    //var website = top.tinymce.activeEditor.windowManager.getParams().website;
</script>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div role="tabpanel">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#cerca" aria-controls="cerca" role="tab" data-toggle="tab">
                            <span class="glyphicon glyphicon-search" aria-hidden="true"></span>&nbsp;&nbsp;Correlato
                        </a>
                    </li>
					<?php if ( $gallery_pt ) : ?>
                        <li role="presentation">
                            <a href="#galleria" aria-controls="galleria" role="tab" data-toggle="tab">
                                <span class="glyphicon glyphicon-picture" aria-hidden="true"></span>&nbsp;&nbsp;Gallery
                            </a>
                        </li>
					<?php endif; ?>
					<?php if ( $video_pt ) : ?>
                        <li role="presentation">
                            <a href="#player" aria-controls="player" role="tab" data-toggle="tab">
                                <span class="glyphicon glyphicon-facetime-video" aria-hidden="true"></span>&nbsp;&nbsp;Player
                                Video
                            </a>
                        </li>
					<?php endif; ?>
					<?php if ( $products_pt ) : ?>
                        <li role="presentation">
                            <a href="#product" aria-controls="product" role="tab" data-toggle="tab">
                                <span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>&nbsp;&nbsp;Prodotto
                            </a>
                        </li>
					<?php endif; ?>
					<?php if ( current_user_can( 'moderate_comments' ) ) : ?>
                        <li role="presentation">
                            <a href="#opzioni" aria-controls="opzioni" role="tab" data-toggle="tab">
                                <span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>&nbsp;&nbsp;Opzioni
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#info" aria-controls="info" role="tab" data-toggle="tab">
                                <span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span>&nbsp;&nbsp;Info
                            </a>
                        </li>
					<?php endif; ?>
                </ul>
                <div class="tab-content">
					<?php
					$post_check    = get_enabled_post_types( $ghOptions );
					$layouts_check = get_enabled_layouts();
					include_once( 'partials/shortcode.php' );
					if ( $gallery_pt ) {
						include_once( 'partials/gallery.php' );
					}
					if ( $video_pt ) {
						include_once( 'partials/video.php' );
					}
					if ( $products_pt ) {
						include_once( 'partials/product.php' );
					}
					if ( current_user_can( 'moderate_comments' ) ) :
						include_once( 'partials/options.php' );
					endif;
					?>
                </div>
            </div>
        </div>
    </div>

</div>


<script>
    var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>', myOptions = '<?php echo $ghOptions; ?>',
        searchNonce = '<?php echo $ghSearchNonce; ?>', saveNonce = '<?php echo $ghSaveNonce; ?>';
</script>
<script src="<?php echo plugins_url( 'dialog.js', __FILE__ ); ?>?ver=<?php echo date( 'U' ); ?>"></script>

</body>
</html>