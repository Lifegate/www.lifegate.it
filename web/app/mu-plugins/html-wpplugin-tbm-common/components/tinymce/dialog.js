/*! HTML.it Shortcode Embed - v0.1.0
 * http://www.gruppohtml.it
 * Copyright (c) 2015; * Licensed GPLv2+ */
/*jshint browser: true */
(
    function (window, undefined) {
        'use strict';

        function reload() {
            setTimeout(function () {
                location.reload();
            }, 1000);
        }

        var posttype = '',
            service = '',
            ghl = $('#ghshortcode-options-save').ladda();

        jQuery(document).ready(function ($) {

            $(function () {
                $("input[type=submit]")
                    .button()
                    .click(function (event) {
                        event.preventDefault();
                    });
            });

            $("#ghshortcode").select2({
                ajax: {
                    dataType: 'jsonp',
                    delay: 250,
                    data: function (params) {
                        return {
                            action: "gh_shortcode_search_posts",
                            mynonce: searchNonce,
                            limit: 30,
                            mrp_post_type: fsc,
                            mrp_res: 3,
                            q: params.term,
                            page: params.page,
                            draft: draft
                        };
                    },
                    processResults: function (data, page) {
                        return {
                            results: data
                        };
                    }
                },
                minimumInputLength: 3,
                templateResult: formatRepo,
                templateSelection: formatRepoSelection,
                escapeMarkup: function (markup) {
                    return markup;
                }
            });

            $("#ghgalleria").select2({
                ajax: {
                    dataType: 'jsonp',
                    delay: 250,
                    data: function (params) {
                        return {
                            action: "gh_shortcode_search_posts",
                            mynonce: searchNonce,
                            limit: 30,
                            mrp_post_type: ['galleria', 'gallery'],
                            mrp_res: 3,
                            q: params.term,
                            page: params.page,
                            draft: draft
                        };
                    },
                    processResults: function (data, page) {
                        return {
                            results: data
                        };
                    }
                },
                minimumInputLength: 3,
                templateResult: formatRepo,
                templateSelection: formatRepoSelection,
                escapeMarkup: function (markup) {
                    return markup;
                }
            });

            $("#ghvideo").select2({
                ajax: {
                    dataType: 'jsonp',
                    delay: 250,
                    data: function (params) {
                        return {
                            action: "gh_shortcode_search_posts",
                            mynonce: searchNonce,
                            limit: 30,
                            mrp_post_type: 'video',
                            mrp_res: 3,
                            q: params.term,
                            page: params.page,
                            draft: draft
                        };
                    },
                    processResults: function (data, page) {
                        return {
                            results: data
                        };
                    }
                },
                minimumInputLength: 3,
                templateResult: formatRepo,
                templateSelection: formatRepoSelection,
                escapeMarkup: function (markup) {
                    return markup;
                }
            });

            $("#ghproduct").select2({
                ajax: {
                    dataType: 'jsonp',
                    delay: 250,
                    data: function (params) {
                        return {
                            action: "gh_shortcode_search_posts",
                            mynonce: searchNonce,
                            limit: 30,
                            mrp_post_type: 'product',
                            mrp_res: 3,
                            q: params.term,
                            page: params.page,
                            draft: draft
                        };
                    },
                    processResults: function (data, page) {
                        return {
                            results: data
                        };
                    }
                },
                minimumInputLength: 3,
                templateResult: formatRepo,
                templateSelection: formatRepoSelection,
                escapeMarkup: function (markup) {
                    return markup;
                }
            });
        });

        function getShortcode(postType) {
            var value = $("#ghshortcode").val();
            var title = $("#ghshortcode_title").val();
            var layout = $("input[name=layout]:checked").val();
            if (layout) {
                var shortcode = '[ghshort' + postType + ' id=' + value + ' title="' + title + '" layout="' + layout + '"]';
            } else {
                $('.message').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Attenzione</strong> Devi selezionare almeno un layout di visualizzazione</div>');
                return false;
            }

            return shortcode;
        }

        function getProduct(postType) {
            var value = $("#ghproduct").val() ? ' id="' + $("#ghproduct").val() + '" ' : '',
                title = $("#ghproduct_title").val() ? ' title="' + $("#ghproduct_title").val() + '" ' : '',
                badge = $("#ghproduct_badge").val() ? ' badge="' + $("#ghproduct_badge").val() + '" ' : '',
                claim = $("#ghproduct_claim").val() ? ' claim="' + $("#ghproduct_claim").val() + '" ' : '',
                layout = $("input[name=product_layout]:checked").val() ? ' layout="' + $("input[name=product_layout]:checked").val() + '" ' : '';

            // Build shortcode
            if (layout) {
                var shortcode = '[ghproduct ' + value + badge + claim + title + layout + ']';
            } else {
                $('.message').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Attenzione</strong> Devi selezionare almeno un layout di visualizzazione</div>');
                return false;
            }

            // Return shortcode
            return shortcode;
        }

        function getVideo() {
            var value = $("#ghvideo").val();
            var title = $("#ghvideo_title").val();
            var layout = $("input[name=mp4]:checked").val();
            if (layout) {
                console.log('layout');
                var shortcode = '[ghvideo id=' + value + ' title="' + title + '" layout="' + layout + '"]';
            } else {
                console.log('no layout');
                $('.message').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Attenzione</strong> Devi selezionare almeno un layout di visualizzazione</div>');
                return false;
            }

            return shortcode;
        }

        function getGallery() {
            var value = $("#ghgalleria").val();
            var title = $("#ghgalleria_title").val();
            // Todo: add more layout
            //var layout = $( "input[name=gallery]:checked" ).val();

            // Force layout to the only available
            var layout = '_gallery_slider';
            if (layout) {
                var shortcode = '[ghgallery id=' + value + ' title="' + title + '" layout="' + layout + '"]';
            } else {
                console.log('no layout');
                $('.message').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Attenzione</strong> Devi selezionare almeno un layout di visualizzazione</div>');
                return false;
            }

            return shortcode;
        }

        function getService(input) {
            console.log(service);
            if (input.val()) {
                service = input.attr('id').replace('embed_', '');
            }
        }

        function getEmbed() {
            var output = [];
            $("#ghembed-services input").each(function (index) {
                if ($(this).val()) {
                    var service = $(this).attr('id').replace('embed_', '');
                    var value = $(this).val();
                    var shortcode = '[ghembed_' + service + ' id=' + value + ' size="large"]';
                    output.push(shortcode);
                }
            });
            return output.join(' ');
        }

        function formatRepo(repo) {
            console.log(repo);
            if (repo.loading) {
                return repo.text;
            }
            var markup = '<div class="clearfix">' +
                '<div clas="col-xs-12">' +
                '<div class="clearfix">';
            markup += '<div class="col-xs-6">' + repo.title + '</div>' +
                '<div class="col-xs-2">' + repo.type + '</div>' +
                '<div class="col-xs-2">' + repo.data + '</div>';
            if (repo.status === 'bozza') {
                markup += '<div class="col-xs-2 text-danger">' + repo.status + '</div>';
            } else {
                markup += '<div class="col-xs-2 text-success">' + repo.status + '</div>';
            }
            markup += '</div></div></div>';
            return markup;
        }

        function formatRepoSelection(repo) {
            posttype = repo.type;
            return repo.title;
        }

        /**
         * Gestione dei pulsanti degli shortcode
         */
        $('#ghshortcode-submit-close').click(function () {
            if ($("#ghshortcode").val()) {
                var shortcode = getShortcode(posttype);
                // inserts the shortcode into the active editor
                top.tinymce.activeEditor.execCommand('mceInsertContent', 0, shortcode);
                // closes Thickbox
                top.tinymce.activeEditor.windowManager.close();
            } else {
                $('.message').html('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Attenzione</strong> Devi selezionare almeno un contenuto</div>');
            }
        });
        $('#ghshortcode-submit').click(function () {
            if ($("#ghshortcode").val()) {
                var shortcode = getShortcode(posttype);
                // inserts the shortcode into the active editor
                top.tinymce.activeEditor.execCommand('mceInsertContent', 0, shortcode);
            } else {
                $('.message').html('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Attenzione</strong> Devi selezionare almeno un contenuto</div>');
            }
        });
        $('#ghshortcode-close').click(function () {
            top.tinymce.activeEditor.windowManager.close();
        });
        $('#ghshortcode-options-close').click(function () {
            top.tinymce.activeEditor.windowManager.close();
        });

        /**
         * Gestione del pannello PRODOTTO
         */
        $('#ghproduct-submit-close').click(function () {
            if ($("#ghproduct").val()) {
                var shortcode = getProduct(posttype);
                // inserts the shortcode into the active editor
                top.tinymce.activeEditor.execCommand('mceInsertContent', 0, shortcode);
                // closes Thickbox
                top.tinymce.activeEditor.windowManager.close();
            } else {
                $('.message').html('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Attenzione</strong> Devi selezionare almeno un contenuto</div>');
            }
        });
        $('#ghproduct-submit').click(function () {
            if ($("#ghproduct").val()) {
                var shortcode = getProduct(posttype);
                // inserts the shortcode into the active editor
                top.tinymce.activeEditor.execCommand('mceInsertContent', 0, shortcode);
            } else {
                $('.message').html('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Attenzione</strong> Devi selezionare almeno un contenuto</div>');
            }
        });
        $('#ghproduct-summary-submit').click(function () {
            var shortcode = '[ghproductsummary]';
            top.tinymce.activeEditor.execCommand('mceInsertContent', 0, shortcode);
        });
        $('#ghproduct-close').click(function () {
            top.tinymce.activeEditor.windowManager.close();
        });
        $('#ghproduct-options-close').click(function () {
            top.tinymce.activeEditor.windowManager.close();
        });

        /**
         * Gestione dei pulsanti degli embed
         */
        $('#ghembed-submit-close').click(function () {

//Service è una variabile globale che viene riempita al focus del testo
            var shortcode = getEmbed(service);
            if (shortcode) {
                // inserts the shortcode into the active editor
                top.tinymce.activeEditor.execCommand('mceInsertContent', 0, shortcode);
                // closes Thickbox
                top.tinymce.activeEditor.windowManager.close();
            } else {
                $('.message').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Attenzione</strong> Devi aggiungere almeno un codice</div>');
            }
        });
        $('#ghembed-submit').click(function () {
            if (service) {
                var shortcode = getEmbed(service);
                // inserts the shortcode into the active editor
                top.tinymce.activeEditor.execCommand('mceInsertContent', 0, shortcode);
            } else {
                $('.message').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Attenzione</strong> Devi aggiungere almeno un codice</div>');
            }
        });
        $('#ghembed-close').click(function () {
            top.tinymce.activeEditor.windowManager.close();
        });
        $('#ghembed-options-close').click(function () {
            top.tinymce.activeEditor.windowManager.close();
        });

        /**
         * Gestione dei pulsanti dei video
         */
        $('#ghvideo-submit-close').click(function () {
            if ($("#ghvideo").val()) {
                var shortcode = getVideo();
                // inserts the shortcode into the active editor
                top.tinymce.activeEditor.execCommand('mceInsertContent', 0, shortcode);
                // closes Thickbox
                top.tinymce.activeEditor.windowManager.close();
            } else {
                $('.message').html('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Attenzione</strong> Devi selezionare almeno un contenuto</div>');
            }
        });
        $('#ghvideo-submit').click(function () {
            if ($("#ghvideo").val()) {
                var shortcode = getVideo();
                // inserts the shortcode into the active editor
                top.tinymce.activeEditor.execCommand('mceInsertContent', 0, shortcode);
            } else {
                $('.message').html('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Attenzione</strong> Devi selezionare almeno un contenuto</div>');
            }
        });
        $('#ghvideo-close').click(function () {
            top.tinymce.activeEditor.windowManager.close();
        });
        $('#ghvideo-options-close').click(function () {
            top.tinymce.activeEditor.windowManager.close();
        });

        /**
         * Gestione dei pulsanti della gallery
         */
        $('#ghgalleria-submit-close').click(function () {
            if ($("#ghgalleria").val()) {
                var shortcode = getGallery();
                // inserts the shortcode into the active editor
                top.tinymce.activeEditor.execCommand('mceInsertContent', 0, shortcode);
                // closes Thickbox
                top.tinymce.activeEditor.windowManager.close();
            } else {
                $('.message').html('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Attenzione</strong> Devi selezionare almeno un contenuto</div>');
            }
        });
        $('#ghgalleria-submit').click(function () {
            if ($("#ghgalleria").val()) {
                var shortcode = getGallery();
                // inserts the shortcode into the active editor
                top.tinymce.activeEditor.execCommand('mceInsertContent', 0, shortcode);
            } else {
                $('.message').html('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Attenzione</strong> Devi selezionare almeno un contenuto</div>');
            }
        });
        $('#ghgalleria-close').click(function () {
            top.tinymce.activeEditor.windowManager.close();
        });
        $('#ghgalleria-options-close').click(function () {
            top.tinymce.activeEditor.windowManager.close();
        });

        /**
         * Gestione del pulsante di salvataggio
         */
        $('#ghshortcode-options-save').click(function () {
            ghl.ladda('start');
            var el = $(this);
            var postTypeArray = [];
            $("#ghshortcode-options input:checked").each(function () {
                postTypeArray.push($(this).val());
            });
            postTypeArray = postTypeArray.toString();
            var optionsData = {
                action: 'gh_shortcode_save_options',
                postType: postTypeArray,
                mynonce: saveNonce
            };
            jQuery.post(ajaxurl, optionsData, function (response) {
                console.log(response);
                if (response.trim() !== 'ok') {
                    $('.message').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Errore!</strong> Abbiamo avuto un problema. Se si dovesse ripetere avvisa la NASA</div>');
                } else {
                    $('.message').html('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Ok!</strong> Dati salvati correttamente</div>');
                    reload();
                }
            }).fail(function () {
                $('.message').html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Errore!</strong> Abbiamo avuto un problema. Se si dovesse ripetere avvisa la NASA</div>');
            }).always(function () {
                ghl.ladda('stop');
            });

        });

        // Gestione post type
        var fsc = $('#posttype input[type=checkbox]:checked').map(function () {
            return this.value;
        }).get();

        $('#posttype input[type=checkbox]').change(function () {
            fsc = $('#posttype input[type=checkbox]:checked').map(function () {
                return this.value;
            }).get();
            $('#ghshortcode').prop("disabled", false);
        });


        // Gestione Bozze
        var draft = $('#draft input[type=checkbox]:checked').map(function () {
            return this.value;
        }).get();

        $('#draft input[type=checkbox]').change(function () {
            draft = $('#draft input[type=checkbox]:checked').map(function () {
                return this.value;
            }).get();
        });


        $('#ghshortcode').on("select2:open", function () {
            if (fsc.length < 1) {
                $(this).prop("disabled", true);
                $('.message').html('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Devi selezionare almeno un contenuto altrimenti non otterrai risultati</div>');
            }
        });

        $('#ghproduct').on("select2:select", function () {
            var title = $('#ghproduct :selected').attr('title');
            $('#ghproduct_title').val(title);
        });

        $('#ghshortcode').on("select2:select", function () {
            var title = $('#ghshortcode :selected').attr('title');
            $('#ghshortcode_title').val(title);
        });

        $('#ghvideo').on("select2:select", function () {
            var title = $('#ghvideo :selected').attr('title');
            $('#ghvideo_title').val(title);
        });

        $('#ghgalleria').on("select2:select", function () {
            var title = $('#ghgalleria :selected').attr('title');
            $('#ghgalleria_title').val(title);
        });
    }
)(this);