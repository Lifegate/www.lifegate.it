<?php
/*
Plugin Name: Html.it Sage Controller
Description: Load the Sober controller at init to enable Sage based theme
Version: 1.1.0
*/

/**
 * TODO: Remove temporary workaround to start soberwp/controller
 *
 * @see https://github.com/soberwp/controller/issues/48
 */
if ( function_exists( 'Sober\Controller\loader' ) ) {
	add_action( 'init', 'Sober\Controller\loader' );
}
if ( function_exists( 'Sober\Controller\debugger' ) ) {
	add_action( 'init', 'Sober\Controller\debugger' );
}