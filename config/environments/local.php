<?php
/**
 * Configuration overrides for WP_ENV === 'development'
 */

use Roots\WPConfig\Config;

Config::define( 'SAVEQUERIES', true );
Config::define( 'WP_DEBUG', true );
if ( isset( $_GET['debug'] ) && $_GET['debug'] === '1' ) {
	$whoops = new \Whoops\Run;
	$whoops->pushHandler( new \Whoops\Handler\PrettyPageHandler );
	$whoops->register();
}
Config::define( 'WP_DEBUG_DISPLAY', true );
Config::define( 'WP_DISABLE_FATAL_ERROR_HANDLER', true );
Config::define( 'SCRIPT_DEBUG', true );

ini_set( 'display_errors', '1' );

// Enable plugin and theme updates and installation from the admin
Config::define( 'DISALLOW_FILE_MODS', false );
