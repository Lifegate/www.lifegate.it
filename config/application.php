<?php
/**
 * Your base production configuration goes in this file. Environment-specific
 * overrides go in their respective config/environments/{{WP_ENV}}.php file.
 *
 * A good default policy is to deviate from the production config as little as
 * possible. Try to define as much of your configuration in this file as you
 * can.
 */

use Roots\WPConfig\Config;

/** @var string Directory containing all of the site's files */
$root_dir = dirname( __DIR__ );

/** @var string Document Root */
$webroot_dir = $root_dir . '/web';

/**
 * Expose global env() function from oscarotero/env
 */
Env::init();

/**
 * Use Dotenv to set required environment variables and load .env file in root
 */
$dotenv = Dotenv\Dotenv::create( $root_dir );
if ( file_exists( $root_dir . '/.env' ) ) {
	$dotenv->load();
	$dotenv->required( [ 'WP_HOME', 'WP_SITEURL' ] );
	if ( ! env( 'DATABASE_URL' ) ) {
		$dotenv->required( [ 'DB_NAME', 'DB_USER', 'DB_PASSWORD' ] );
	}
}

/**
 * Set up our global environment constant and load its config first
 * Default: production
 */
define( 'WP_ENV', env( 'WP_ENV' ) ?: 'production' );

/**
 * URLs
 */
Config::define( 'WP_HOME', env( 'WP_HOME' ) );
Config::define( 'WP_SITEURL', env( 'WP_SITEURL' ) );

/**
 * Custom post type
 */
Config::define( 'HTML_GALLERY_POST_TYPE', env( 'HTML_GALLERY_POST_TYPE' ) ?: 'gallery' );
Config::define( 'HTML_GALLERY_POST_TYPE_SLUG', env( 'HTML_GALLERY_POST_TYPE_SLUG' ) ?: 'galleria' );

/**
 * Custom Content Directory
 */
Config::define( 'CONTENT_DIR', '/app' );
Config::define( 'WP_CONTENT_DIR', $webroot_dir . Config::get( 'CONTENT_DIR' ) );
Config::define( 'WP_CONTENT_URL', Config::get( 'WP_HOME' ) . Config::get( 'CONTENT_DIR' ) );
Config::define( 'BEDROCK_DIR', $webroot_dir );
Config::define( 'ROOT_DIR', $root_dir );
Config::define( 'COOKIE_DOMAIN', $_SERVER['HTTP_HOST'] );

/**
 * DB settings
 */
Config::define( 'DB_NAME', env( 'DB_NAME' ) );
Config::define( 'DB_USER', env( 'DB_USER' ) );
Config::define( 'DB_PASSWORD', env( 'DB_PASSWORD' ) );
Config::define( 'DB_HOST', env( 'DB_HOST' ) ?: 'localhost' );
Config::define( 'DB_CHARSET', 'utf8mb4' );
Config::define( 'DB_COLLATE', '' );
$table_prefix = env( 'DB_PREFIX' ) ?: 'wp_';

if ( env( 'DATABASE_URL' ) ) {
	$dsn = (object) parse_url( env( 'DATABASE_URL' ) );

	Config::define( 'DB_NAME', substr( $dsn->path, 1 ) );
	Config::define( 'DB_USER', $dsn->user );
	Config::define( 'DB_PASSWORD', isset( $dsn->pass ) ? $dsn->pass : null );
	Config::define( 'DB_HOST', isset( $dsn->port ) ? "{$dsn->host}:{$dsn->port}" : $dsn->host );
}

/**
 * Authentication Unique Keys and Salts
 */
Config::define( 'AUTH_KEY', env( 'AUTH_KEY' ) );
Config::define( 'SECURE_AUTH_KEY', env( 'SECURE_AUTH_KEY' ) );
Config::define( 'LOGGED_IN_KEY', env( 'LOGGED_IN_KEY' ) );
Config::define( 'NONCE_KEY', env( 'NONCE_KEY' ) );
Config::define( 'AUTH_SALT', env( 'AUTH_SALT' ) );
Config::define( 'SECURE_AUTH_SALT', env( 'SECURE_AUTH_SALT' ) );
Config::define( 'LOGGED_IN_SALT', env( 'LOGGED_IN_SALT' ) );
Config::define( 'NONCE_SALT', env( 'NONCE_SALT' ) );

Config::define( 'PLL_COOKIE', env( 'PLL_COOKIE' ) ?: false );

/**
 * Custom Settings
 */
Config::define( 'AUTOMATIC_UPDATER_DISABLED', true );
Config::define( 'DISABLE_WP_CRON', env( 'DISABLE_WP_CRON' ) ?: false );
Config::define( 'WP_CACHE', env( 'WP_CACHE' ) ?: false );
// Disable the plugin and theme file editor in the admin
Config::define( 'DISALLOW_FILE_EDIT', true );
// Disable plugin and theme updates and installation from the admin
Config::define( 'DISALLOW_FILE_MODS', true );
// Set the taxonomy for the most valuable content
Config::define( 'SITE_SPECIAL_TAXONOMY', env( 'SITE_SPECIAL_TAXONOMY' ) ?: 'trend' );
Config::define( 'TBM_SITE_NAME', env( 'TBM_SITE_NAME' ) ?: '' );
Config::define( 'WP_POST_REVISIONS', env( 'WP_POST_REVISIONS' ) ?: 3 );
Config::define( 'GMAPS_API', env( 'GMAPS_API' ) ?: '' );

Config::define( 'OEMBED_PLUS_FACEBOOK_APP_ID', env( 'OEMBED_PLUS_FACEBOOK_APP_ID' ) ?: '' );
Config::define( 'OEMBED_PLUS_FACEBOOK_APP_ID', env( 'OEMBED_PLUS_FACEBOOK_APP_ID' ) ?: '' );
Config::define( 'OEMBED_PLUS_FACEBOOK_SECRET', env( 'OEMBED_PLUS_FACEBOOK_SECRET' ) ?: '' );

/**
 * Defining Lifegae post type slug
 */
Config::define( 'HTML_EVENT_POST_TYPE_SLUG', 'evento' );
Config::define( 'HTML_GALLERY_POST_TYPE_SLUG', 'galleria' );

/**
 * Debugging Settings
 */
Config::define( 'WP_DEBUG_DISPLAY', false );
Config::define( 'SCRIPT_DEBUG', false );
ini_set( 'display_errors', '0' );
if ( isset( $_GET['eDebug'] ) && $_GET['eDebug'] == 'whoops' ) {
	Config::define( 'WP_DEBUG', true );
	$whoops = new \Whoops\Run;
	$whoops->pushHandler( new \Whoops\Handler\PrettyPageHandler );
	$whoops->register();
} else if ( isset( $_GET['eDebug'] ) ) {
	Config::define( 'WP_DEBUG', true );
} else {
	Config::define( 'WP_DEBUG', env( 'WP_DEBUG' ) ?: false );
}

/**
 * Allow WordPress to detect HTTPS when used behind a reverse proxy or a load balancer
 * See https://codex.wordpress.org/Function_Reference/is_ssl#Notes
 */
if ( isset( $_SERVER['HTTP_X_FORWARDED_PROTO'] ) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ) {
	$_SERVER['HTTPS'] = 'on';
}

$env_config = __DIR__ . '/environments/' . WP_ENV . '.php';

if ( file_exists( $env_config ) ) {
	require_once $env_config;
}

Config::apply();

/**
 * Bootstrap WordPress
 */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', $webroot_dir . '/wp/' );
}
